/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Model;

import com.bean.ImageRatioBean;
import com.bean.QuestionBean;
import com.db.DbConnection;
import com.db.operations.ImageRatioOperation;
import com.db.operations.QuestionOperation;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Aniket
 */
public class temp2 {

    private Connection conn;
    private PreparedStatement ps;
    private ResultSet rs;
    
    public temp2() {
        conn = new DbConnection().getConnection();
    }
    
    
    public void setRatio() {
        ArrayList<QuestionBean> totalQuesList = new QuestionOperation().getQuestuionsList();
        ArrayList<ImageRatioBean> imageRatioList = new ImageRatioOperation().getImageRatioList();
        ArrayList<String> imageRatioNameList = new ArrayList<String>();
        ArrayList<ImageRatioBean> updateRatioList = new ArrayList<ImageRatioBean>();
        for(ImageRatioBean ratioBean : imageRatioList) 
            imageRatioNameList.add(ratioBean.getImageName().trim());
        
        ImageRatioBean imageRatioBean = null;
        for(QuestionBean questionBean : totalQuesList) {
            if(questionBean.isIsQuestionAsImage()) {
                if(!imageRatioNameList.contains(questionBean.getQuestionImagePath().trim())) {
                    imageRatioBean = new ImageRatioBean();
                    imageRatioBean.setImageName(questionBean.getQuestionImagePath().trim());
                    imageRatioBean.setViewDimention(1.0);
                    updateRatioList.add(imageRatioBean);
                }
            }
            if(questionBean.isIsOptionAsImage()) {
                if(!imageRatioNameList.contains(questionBean.getOptionImagePath().trim())) {
                    imageRatioBean = new ImageRatioBean();
                    imageRatioBean.setImageName(questionBean.getOptionImagePath().trim());
                    imageRatioBean.setViewDimention(1.0);
                    updateRatioList.add(imageRatioBean);
                }
            }
            if(questionBean.isIsHintAsImage()) {
                if(!imageRatioNameList.contains(questionBean.getHintImagePath().trim())) {
                    imageRatioBean = new ImageRatioBean();
                    imageRatioBean.setImageName(questionBean.getHintImagePath().trim());
                    imageRatioBean.setViewDimention(1.0);
                    updateRatioList.add(imageRatioBean);
                }
            }
        }
        
        updateRatioList.addAll(imageRatioList);
        imageRatioNameList.clear();
        System.out.println("Size:"+updateRatioList.size());
        for(ImageRatioBean bean : updateRatioList) 
            imageRatioNameList.add(bean.getImageName());
        
        for(ImageRatioBean ratioBean : imageRatioList) 
            imageRatioNameList.add(ratioBean.getImageName().trim());
        
        imageRatioList.clear();
        int index = 0;
        for(QuestionBean questionBean : totalQuesList) {
            if(questionBean.isIsQuestionAsImage()) {
                index = imageRatioNameList.indexOf(questionBean.getQuestionImagePath().trim());
                imageRatioList.add(updateRatioList.get(index));
            }
            if(questionBean.isIsOptionAsImage()) {
                index = imageRatioNameList.indexOf(questionBean.getOptionImagePath().trim());
                imageRatioList.add(updateRatioList.get(index));
            }
            if(questionBean.isIsHintAsImage()) {
                index = imageRatioNameList.indexOf(questionBean.getHintImagePath().trim());
                imageRatioList.add(updateRatioList.get(index));
            }
        }
        
        System.out.println("After Sorting Size:"+imageRatioList.size());
//        for(ImageRatioBean bean : imageRatioList) {
//            System.out.println(bean.getImageName().trim()+"  "+bean.getViewDimention());
//        }
        
        if(addMasterRatio(imageRatioList))
            JOptionPane.showMessageDialog(null, "Image Ratio Updated SuccessFully.");
        else
            JOptionPane.showMessageDialog(null, "Error in Image Ratio Updation.");
    }
    
    
    private boolean addMasterRatio(ArrayList<ImageRatioBean> updateRatioList) {
        boolean returnValue = false;
        try {
            String query = "DELETE FROM IMAGERATIO";
            ps = conn.prepareStatement(query);
            ps.executeUpdate();
            query = "INSERT INTO IMAGERATIO VALUES(?,?)";
            for(ImageRatioBean imageRatioBean : updateRatioList) {
                ps = conn.prepareStatement(query);
                ps.setString(1, imageRatioBean.getImageName().trim());
                ps.setDouble(2, imageRatioBean.getViewDimention());
                ps.executeUpdate();
            }
            returnValue = true;
        } catch (Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        }
        return returnValue;
    }
    
    public static void main(String[] args) {
        new temp2().setRatio();
    }
}
