/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Model;

import com.bean.SMSHistoryBean;
import com.db.operations.SMSHistoryOperation;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.PostMethod;
import ui.SenderIdSMS;

/**
 *
 * @author Aniket
 */
public class SmsOperation {

    boolean connection, Status;
    SMSHistoryBean smsHistoryBean = new SMSHistoryBean();

//    this url for route sms
    public String sendMessages(String messageText, String destination) {
        String returnString = "";
        try {
            URL url = new URL("http://103.16.101.52:8080/bulksms/bulksms?");
            HttpURLConnection hc = (HttpURLConnection) url.openConnection();
            hc.setRequestMethod("POST");
            hc.setDoInput(true);
            hc.setDoOutput(true);
            hc.setUseCaches(false);
            DataOutputStream dout = new DataOutputStream(hc.getOutputStream());
            dout.writeBytes("username=" + URLEncoder.encode("crun-cstpune", "UTF-8")
                    + "&password=" + URLEncoder.encode("12312312", "UTF-8")
                    + "&dlr=" + URLEncoder.encode("1", "UTF-8")
                    + "&type=" + URLEncoder.encode("0", "UTF-8")
                    + "&destination=" + URLEncoder.encode(destination, "UTF-8")
                    + "&source=" + URLEncoder.encode("CTPUNE", "UTF-8")
                    + "&message=" + URLEncoder.encode(messageText, "UTF-8"));
            dout.flush();
            dout.close();

            BufferedReader br = new BufferedReader(new InputStreamReader(hc.getInputStream()));
            String s;
            while ((s = br.readLine()) != null) {
                returnString += s;
            }
            br.close();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Check Internet Connection.");
            ex.printStackTrace();
        }
        return returnString;
    }

//    for Tata Services
    public String sendMessages1(String UserName, String Password, String SenderId, String messageText, String destination) {

        String returnString = "";
        try {
            URL url = new URL("http://173.45.76.227/send.aspx?");
            HttpURLConnection hc = (HttpURLConnection) url.openConnection();
            hc.setRequestMethod("POST");
            hc.setDoInput(true);
            hc.setDoOutput(true);
            hc.setUseCaches(false);
            DataOutputStream dout = new DataOutputStream(hc.getOutputStream());
            dout.writeBytes("username=" + URLEncoder.encode(UserName, "UTF-8")
                    + "&password=" + URLEncoder.encode(Password, "UTF-8")
                    //                    + "&dlr=" + URLEncoder.encode("1", "UTF-8")      
                    //                    + "&type=" + URLEncoder.encode("0", "UTF-8")                                       
                    + "&source=" + URLEncoder.encode(SenderId, "UTF-8")
                    + "&destination=" + URLEncoder.encode(destination, "UTF-8")
                    + "&message=" + URLEncoder.encode(messageText, "UTF-8")
                    + "&route=" + URLEncoder.encode("trans1", "UTF-8")
            );
            dout.flush();
            dout.close();

            BufferedReader br = new BufferedReader(new InputStreamReader(hc.getInputStream()));
            String s;
            while ((s = br.readLine()) != null) {
                returnString += s;
            }
            br.close();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Check Internet Connection.");
            ex.printStackTrace();
        }
        return returnString;
    }

    //    for Tata Services
    private static String SMS_SERVICE_URL = "http://173.45.76.227/send.aspx";

    public void sendSMS(final String StudentName, final int testid, final String testname, final int RollNo, final String username, final String password, final String messageText, final String senderid, final String MobileNo) {

//        Thread t = new Thread(new Runnable() {
//
//            public void run() {

                try {
                    HttpClient client = new HttpClient();
                    PostMethod method = new PostMethod(SMS_SERVICE_URL);

                    method.addParameter("username", username);
                    method.addParameter("pass", password);
                    method.addParameter("senderid", senderid);
                    method.addParameter("numbers", MobileNo);
                    method.addParameter("message", messageText);
                    method.addParameter("route", "trans1");
                    method.addParameter("action", "send");
                    client.executeMethod(method);
                                                      
                    String smsResponse = method.getResponseBodyAsString() + ",";
                    System.out.println(smsResponse);
                    
                    String message = "";
                    String[] responseArray = smsResponse.split(",");

                    for (String response : responseArray) {
                        String[] tempArray = response.split("\\|");

//                        tempArray = tempArray[1].split(":");
//                        System.out.println("tempArray"+tempArray);
//                        String mobileNumber = tempArray[0].substring(2, tempArray[0].length());
                        String mobileNumber = MobileNo;
                        if (response.startsWith("1")) {

                            try {
                                message += mobileNumber + " Message Send Successfully...!!!\n";
                                int sid = new SMSHistoryOperation().getNewSId();
                                smsHistoryBean.setSid(sid);
                                smsHistoryBean.setRollNo(RollNo);
                                smsHistoryBean.setStudentName(StudentName);
                                smsHistoryBean.setMobileNo(MobileNo);
                                smsHistoryBean.setMessageText(messageText);
                                smsHistoryBean.setTestId(testid);
                                smsHistoryBean.setTestName(testname);
                                smsHistoryBean.setDeliveryReport(true);

                                if (new SMSHistoryOperation().InsertSMSHistory(smsHistoryBean)) {
                                    System.out.println("Successfully Inserted");
                                } else {
                                    System.out.println("Not Successfully Inserted");
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
//                                Logger.getLogger(SmsOperation.class.getName()).log(Level.SEVERE, null, ex);
                            }

                        } else if (response.startsWith("2")) {
                            message += mobileNumber + " Invalid Destination...!!!\n";
                            int sid = new SMSHistoryOperation().getNewSId();
                            smsHistoryBean.setSid(sid);
                            smsHistoryBean.setRollNo(RollNo);
                            smsHistoryBean.setStudentName(StudentName);
                            smsHistoryBean.setMobileNo(MobileNo);
                            smsHistoryBean.setMessageText(messageText);
                            smsHistoryBean.setTestId(testid);
                            smsHistoryBean.setTestName(testname);
                            smsHistoryBean.setDeliveryReport(false);
                            try {

                                if (new SMSHistoryOperation().InsertSMSHistory(smsHistoryBean)) {
                                    System.out.println("Successfully Inserted");
                                } else {
                                    System.out.println("Not Successfully Inserted");
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                Logger.getLogger(SmsOperation.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        } else if (response.startsWith("3")) {
                            message += mobileNumber + " Insufficient Credit...!!!\n";
                            int sid = new SMSHistoryOperation().getNewSId();
                            smsHistoryBean.setSid(sid);
                            smsHistoryBean.setRollNo(RollNo);
                            smsHistoryBean.setStudentName(StudentName);
                            smsHistoryBean.setMobileNo(MobileNo);
                            smsHistoryBean.setMessageText(messageText);
                            smsHistoryBean.setTestId(testid);
                            smsHistoryBean.setTestName(testname);
                            smsHistoryBean.setDeliveryReport(false);
                            try {

                                if (new SMSHistoryOperation().InsertSMSHistory(smsHistoryBean)) {
                                    System.out.println("Successfully Inserted");
                                } else {
                                    System.out.println("Not Successfully Inserted");
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                Logger.getLogger(SmsOperation.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        } else if (response.startsWith("4")) {
                            message += mobileNumber + " Err...!!!\n";
                            int sid = new SMSHistoryOperation().getNewSId();
                            smsHistoryBean.setSid(sid);
                            smsHistoryBean.setRollNo(RollNo);
                            smsHistoryBean.setStudentName(StudentName);
                            smsHistoryBean.setMobileNo(MobileNo);
                            smsHistoryBean.setMessageText(messageText);
                            smsHistoryBean.setTestId(testid);
                            smsHistoryBean.setTestName(testname);
                            smsHistoryBean.setDeliveryReport(false);
                            try {

                                if (new SMSHistoryOperation().InsertSMSHistory(smsHistoryBean)) {
                                    System.out.println("Successfully Inserted");
                                } else {
                                    System.out.println("Not Successfully Inserted");
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                Logger.getLogger(SmsOperation.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        } else if (response.startsWith("5")) {
                            message += mobileNumber + " Invalid SenderId...!!!\n";
                            int sid = new SMSHistoryOperation().getNewSId();
                            smsHistoryBean.setSid(sid);
                            smsHistoryBean.setRollNo(RollNo);
                            smsHistoryBean.setStudentName(StudentName);
                            smsHistoryBean.setMobileNo(MobileNo);
                            smsHistoryBean.setMessageText(messageText);
                            smsHistoryBean.setTestId(testid);
                            smsHistoryBean.setTestName(testname);
                            smsHistoryBean.setDeliveryReport(false);
                            try {

                                if (new SMSHistoryOperation().InsertSMSHistory(smsHistoryBean)) {
                                    System.out.println("Successfully Inserted");
                                } else {
                                    System.out.println("Not Successfully Inserted");
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                Logger.getLogger(SmsOperation.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        } else if (response.startsWith("6")) {
                            message += mobileNumber + " Invalid Route ...!!!\n";
                            int sid = new SMSHistoryOperation().getNewSId();
                            smsHistoryBean.setSid(sid);
                            smsHistoryBean.setRollNo(RollNo);
                            smsHistoryBean.setStudentName(StudentName);
                            smsHistoryBean.setMobileNo(MobileNo);
                            smsHistoryBean.setMessageText(messageText);
                            smsHistoryBean.setTestId(testid);
                            smsHistoryBean.setTestName(testname);
                            smsHistoryBean.setDeliveryReport(false);
                            try {

                                if (new SMSHistoryOperation().InsertSMSHistory(smsHistoryBean)) {
                                    System.out.println("Successfully Inserted");
                                } else {
                                    System.out.println("Not Successfully Inserted");
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                Logger.getLogger(SmsOperation.class.getName()).log(Level.SEVERE, null, ex);
                            }

                        } else if (response.startsWith("7")) {
                            message += mobileNumber + " Submission Error...!!!\n";
                            int sid = new SMSHistoryOperation().getNewSId();
                            smsHistoryBean.setSid(sid);
                            smsHistoryBean.setRollNo(RollNo);
                            smsHistoryBean.setStudentName(StudentName);
                            smsHistoryBean.setMobileNo(MobileNo);
                            smsHistoryBean.setMessageText(messageText);
                            smsHistoryBean.setTestId(testid);
                            smsHistoryBean.setTestName(testname);
                            smsHistoryBean.setDeliveryReport(false);
                            try {

                                if (new SMSHistoryOperation().InsertSMSHistory(smsHistoryBean)) {
                                    System.out.println("Successfully Inserted");
                                } else {
                                    System.out.println("Not Successfully Inserted");
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                Logger.getLogger(SmsOperation.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        } else {
                            message += mobileNumber + " Message Sending Error...!!!\n";
                            int sid = new SMSHistoryOperation().getNewSId();
                            smsHistoryBean.setSid(sid);
                            smsHistoryBean.setRollNo(RollNo);
                            smsHistoryBean.setStudentName(StudentName);
                            smsHistoryBean.setMobileNo(MobileNo);
                            smsHistoryBean.setMessageText(messageText);
                            smsHistoryBean.setTestId(testid);
                            smsHistoryBean.setTestName(testname);
                            smsHistoryBean.setDeliveryReport(false);
                            try {

                                if (new SMSHistoryOperation().InsertSMSHistory(smsHistoryBean)) {
                                    System.out.println("Successfully Inserted");
                                } else {
                                    System.out.println("Not Successfully Inserted");
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();

                            }
                        }
                    }
                    JOptionPane.showMessageDialog(null, message);
                } catch (HttpException e) {
                    JOptionPane.showMessageDialog(null, "Check Internet Connection.");
                    System.out.println("Error in sending SMS." + e.toString());
                } catch (IOException e) {
                    JOptionPane.showMessageDialog(null, "Check Internet Connection.");
                    System.out.println("Error in sending SMS." + e.toString());
                }
            }

//        });
//        t.start();
    }
   


