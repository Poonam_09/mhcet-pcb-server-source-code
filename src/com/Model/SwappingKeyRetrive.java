/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Model;

/**
 *
 * @author Aniket
 */
public class SwappingKeyRetrive {
    public String swapPinKey(String key,String mobileNumber) {
        String returnValue = "";
        int sumOfDigit = getDigitTotal(mobileNumber.trim());
        switch(sumOfDigit) {
            case 1 : String[] listOne = {"9","12","2","17","7","26","22","18","29","24","6","14","20","1","11",
                                         "4","23","28","8","25","15","13","19","3","27","5","0","10","21","16"};
                     returnValue = swapPinChars(key, listOne);
                     break;
                     
            case 2 : String[] listTwo = {"23","8","19","2","13","15","27","5","21","10","25","1","0","28","22",
                                         "3","14","17","20","9","29","16","7","4","11","6","26","18","24","12"};
                     returnValue = swapPinChars(key, listTwo);
                     break;
                     
            case 3 : String[] listThree = {"28","21","3","10","25","14","8","12","0","2","16","19","11","5","22",
                                           "26","20","4","9","15","13","7","24","18","1","27","6","29","17","23"};
                     returnValue = swapPinChars(key, listThree);
                     break;
                     
            case 4 : String[] listFour = {"19","5","18","6","14","23","12","26","9","0","29","25","16","3","10",
                                          "24","4","20","1","28","15","13","22","11","17","7","8","2","27","21"};
                     returnValue = swapPinChars(key, listFour);
                     break;
                     
            case 5 : String[] listFive = {"2","6","29","11","18","21","26","0","23","12","14","22","10","19","7",
                                          "16","28","13","4","1","9","25","27","15","17","20","5","8","24","3"};
                     returnValue = swapPinChars(key, listFive);
                     break;
            
            case 6 : String[] listSix = {"15","27","7","19","14","4","24","11","23","8","1","28","20","26","3",
                                         "21","12","0","25","22","17","10","16","29","13","6","2","5","9","18"};
                     returnValue = swapPinChars(key, listSix);
                     break;
                    
            case 7 : String[] listSeven = {"14","9","4","27","16","23","13","21","17","1","28","2","20","24","5",
                                           "11","29","19","26","22","12","7","10","15","8","25","6","18","0","3"};
                     returnValue = swapPinChars(key, listSeven);
                     break;
                     
            case 8 : String[] listEight = {"26","17","14","9","4","7","11","2","24","8","28","16","22","1","13",
                                           "19","6","10","15","27","12","0","3","25","18","21","5","23","29","20"};
                     returnValue = swapPinChars(key, listEight);
                     break;
                     
            case 9 : String[] listNine = {"22","21","0","16","12","26","1","17","6","19","15","3","7","28","20",
                                          "14","10","23","27","2","5","11","25","9","29","13","8","4","18","24"};
                     returnValue = swapPinChars(key, listNine);
                     break;
                     
            default: returnValue = key;
                     break;
        }
        
        return returnValue;
    }
    
    public String swapCdKey(String key,String mobileNumber) {
        String returnValue = "";
        int sumOfDigit = getDigitTotal(mobileNumber.trim());
        switch(sumOfDigit) {
            case 1 : String[] listOne = {"4","6","1","8","14","15","11","9",
                                         "13","2","7","10","0","5","3","12"};
                     returnValue = swapCdChars(key, listOne);
                     break;
                     
            case 2 : String[] listTwo = {"12","4","9","1","8","6","14","3",
                                         "5","13","0","10","15","11","2","7"};
                     returnValue = swapCdChars(key, listTwo);
                     break;
                     
            case 3 : String[] listThree = {"15","11","2","5","7","13","4","6",
                                           "1","8","9","0","3","12","14","10"};
                     returnValue = swapCdChars(key, listThree);
                     break;
                     
            case 4 : String[] listFour = {"11","3","10","4","12","8","7","14",
                                          "0","15","13","5","1","6","9","2"};
                     returnValue = swapCdChars(key, listFour);
                     break;
                     
            case 5 : String[] listFive = {"1","2","15","5","11","9","14","0",
                                          "6","7","12","13","10","3","8","4"};
                     returnValue = swapCdChars(key, listFive);
                     break;
            
            case 6 : String[] listSix = {"8","14","3","9","2","7","12","5",
                                         "4","0","15","11","13","1","10","6"};
                     returnValue = swapCdChars(key, listSix);
                     break;
                    
            case 7 : String[] listSeven = {"7","4","2","13","11","8","6","10",
                                           "0","14","1","9","12","3","5","15"};
                     returnValue = swapCdChars(key, listSeven);
                     break;
                     
            case 8 : String[] listEight = {"14","11","9","6","4","2","7","1",
                                           "5","15","10","13","0","8","12","3"};
                     returnValue = swapCdChars(key, listEight);
                     break;
                     
            case 9 : String[] listNine = {"13","12","0","8","14","5","1","9",
                                          "10","7","2","3","15","11","6","4"};
                     returnValue = swapCdChars(key, listNine);
                     break;
                     
            default: returnValue = key;
                     break;
        }
        
        return returnValue;
    }

    private String swapPinChars(String key,String[] list) {
        String returnValue = "";
        char[] tempArray = new char[key.length()];
        int j = 0;
        for(int i=0;i<key.length();i++) {
            if(i == 6 || i == 13 || i == 20 || i == 27) {
                tempArray[i] = '-';
            } else {
                int index = Integer.parseInt(list[j++]);

                if(index >= 6 && index <= 11)
                    index += 1;
                else if(index >= 12 && index <= 17)
                    index += 2;
                else if(index >= 18 && index <= 23)
                    index += 3;
                else if(index >= 24)
                    index += 4;
                
                tempArray[i] = key.charAt(index);
            }
        }
        returnValue = String.valueOf(tempArray);
        return returnValue;
    }
    
    private String swapCdChars(String key,String[] list) {
        String returnValue = "";
        char[] tempArray = new char[key.length()];
        int j = 0;
        for(int i=0;i<key.length();i++) {
//            if(i == 6 || i == 13 || i == 20) {
            if(i == 4 || i == 9 || i == 14) {
                tempArray[i] = '-';
            } else {
                int index = Integer.parseInt(list[j++]);
                
                if(index >= 4 && index <= 7)
                    index += 1;
                else if(index >= 8 && index <= 11)
                    index += 2;
                else if(index >= 12)
                    index += 3;
                
//                if(index >= 6 && index <= 11)
//                    index += 1;
//                else if(index >= 12 && index <= 17)
//                    index += 2;
//                else if(index >= 18)
//                    index += 3;
                
                tempArray[i] = key.charAt(index);
            }
        }
        returnValue = String.valueOf(tempArray);
        return returnValue;
    }
    
    private int getDigitTotal(String str) {
        char[] chArray = str.toCharArray();
        int sum = 0;
        for (char ch : chArray) {
            sum += Character.getNumericValue(ch);
        }
        while (true) {
            if (sum < 10) {
                break;
            } else {
                sum = getDigitCal(sum);
            }
        }
        return sum;
    }
    
    private int getDigitCal(int no) {
        int sum = 0;
        while (no != 0) {
            sum += no % 10;
            no /= 10;
        }
        return sum;
    }
    
    public static void main(String[] args) {
        System.out.println(new SwappingKeyRetrive().swapPinKey("QGKZJJ-WKZUQT-Y8MYQW-2GRU1U-18R8JM","9970707070"));
        System.out.println(new SwappingKeyRetrive().swapCdKey("H1HP-DQXZ-562X-5LND","9970707070"));
    }
}