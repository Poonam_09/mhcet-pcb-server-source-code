/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Model;

import com.LatexProcessing.pdf.QuestionsProcessingChapterWise;
import com.LatexProcessing.pdf.QuestionsProcessingGroupWise;
import com.LatexProcessing.pdf.QuestionsProcessingGroupWiseYearWise;
import com.LatexProcessing.pdf.QuestionsProcessingSubjectWise;
import com.LatexProcessing.pdf.QuestionsProcessingUnitWise;
import com.Pdf.Generation.PdfFinalPanel;
import com.bean.ChapterBean;
import com.bean.HeaderFooterTextBean;
import com.bean.PdfFirstFormatBean;
import com.bean.PdfMainStringBean;
import com.bean.PdfPageSetupBean;
import com.bean.PrintPdfFormatFirstBean;
import com.bean.QuestionBean;
import com.bean.SubjectBean;
import com.db.operations.SubjectOperation;
import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class PrintFormatFirstPdf {
    private PdfPageSetupBean pdfPageSetupBean;
    
    public void setPrintPdf(PdfPageSetupBean pdfPageSetupBean,PdfFirstFormatBean pdfFirstFormatBean,ArrayList<SubjectBean> selectedSubjectList,ArrayList<ChapterBean> chapterList,ArrayList<QuestionBean> selectedQuestionList,String printingPaperType,int totalMarks,boolean correctAnswerSheet,boolean solutionSheet,boolean solutionSheetTwoColumn,boolean teachersCopy,String instituteName,PdfFinalPanel pdfFinalPanel,String fileLocation) {
        PdfMainStringBean pdfMainStringBean = null;
        this.pdfPageSetupBean = pdfPageSetupBean;
        PrintPdfFormatFirstBean printPdfFormatFirstBean = new PrintPdfFormatFirstBean();
        printPdfFormatFirstBean.setPageType(getPageType());
        printPdfFormatFirstBean.setPageBorder(getPageBorder());
        printPdfFormatFirstBean.setHeaderFooterOnFirstPage(getHeaderFooterOnFirstPage());
        printPdfFormatFirstBean.setWaterMark(getWaterMarkString(instituteName));
        printPdfFormatFirstBean.setClgDepartment(getInstituteDepartmentHeading(pdfFirstFormatBean, instituteName));
        printPdfFormatFirstBean.setFourFields(getFourFields(pdfFirstFormatBean, selectedSubjectList, selectedQuestionList, printingPaperType));
        printPdfFormatFirstBean.setRollDivisionTime(getRollDivisionTime(pdfFirstFormatBean));
        printPdfFormatFirstBean.setPaperName(getPaperName(pdfFirstFormatBean));
        if(printingPaperType.equalsIgnoreCase("GroupWise") ) {
            if(pdfPageSetupBean.isOptimizeLineSpace())
            {
                pdfMainStringBean = new QuestionsProcessingGroupWise().getOptimizeMainString1(selectedQuestionList, selectedSubjectList,chapterList,totalMarks, printingPaperType, pdfPageSetupBean, correctAnswerSheet, solutionSheet, solutionSheetTwoColumn);
                printPdfFormatFirstBean.setMainString(pdfMainStringBean.getMainString()); 
            }
            else
            {
            pdfMainStringBean = new QuestionsProcessingGroupWise().getMainString(selectedQuestionList, selectedSubjectList,chapterList,totalMarks, printingPaperType, pdfPageSetupBean, correctAnswerSheet, solutionSheet, solutionSheetTwoColumn);
            printPdfFormatFirstBean.setMainString(pdfMainStringBean.getMainString());
            }
        }else if( printingPaperType.equalsIgnoreCase("YearWise")) {
            if(pdfPageSetupBean.isOptimizeLineSpace())
            {
                pdfMainStringBean = new QuestionsProcessingGroupWiseYearWise().getOptimizeMainString(selectedQuestionList, selectedSubjectList,chapterList,totalMarks, printingPaperType, pdfPageSetupBean, correctAnswerSheet, solutionSheet, solutionSheetTwoColumn);
                printPdfFormatFirstBean.setMainString(pdfMainStringBean.getMainString()); 
            }
            else
            {
            pdfMainStringBean = new QuestionsProcessingGroupWiseYearWise().getMainString(selectedQuestionList, selectedSubjectList,chapterList,totalMarks, printingPaperType, pdfPageSetupBean, correctAnswerSheet, solutionSheet, solutionSheetTwoColumn);
            printPdfFormatFirstBean.setMainString(pdfMainStringBean.getMainString());
            }
        
        } else if(printingPaperType.equalsIgnoreCase("ChapterWise")) {
            if(pdfPageSetupBean.isOptimizeLineSpace())
            {
                pdfMainStringBean = new QuestionsProcessingChapterWise().getOptimizeMainString(selectedQuestionList, selectedSubjectList,chapterList,totalMarks, printingPaperType, pdfPageSetupBean, correctAnswerSheet, solutionSheet, solutionSheetTwoColumn);
                printPdfFormatFirstBean.setMainString(pdfMainStringBean.getMainString()); 
            }
            else
            {
            pdfMainStringBean = new QuestionsProcessingChapterWise().getMainString(selectedQuestionList, selectedSubjectList,chapterList,totalMarks, printingPaperType, pdfPageSetupBean, correctAnswerSheet, solutionSheet, solutionSheetTwoColumn);
            printPdfFormatFirstBean.setMainString(pdfMainStringBean.getMainString());
            }
        } else if(printingPaperType.equalsIgnoreCase("UnitWise")) {
            if(pdfPageSetupBean.isOptimizeLineSpace())
            {
                pdfMainStringBean = new QuestionsProcessingUnitWise().getOptimizeMainString(selectedQuestionList, selectedSubjectList,chapterList,totalMarks, printingPaperType, pdfPageSetupBean, correctAnswerSheet, solutionSheet, solutionSheetTwoColumn);
                printPdfFormatFirstBean.setMainString(pdfMainStringBean.getMainString());
            }
            else
            {
            pdfMainStringBean = new QuestionsProcessingUnitWise().getMainString(selectedQuestionList, selectedSubjectList,chapterList,totalMarks, printingPaperType, pdfPageSetupBean, correctAnswerSheet, solutionSheet, solutionSheetTwoColumn);
            printPdfFormatFirstBean.setMainString(pdfMainStringBean.getMainString());
            }
        } else if(printingPaperType.equalsIgnoreCase("SubjectWise")) {
            if(pdfPageSetupBean.isOptimizeLineSpace())
            {
            pdfMainStringBean = new QuestionsProcessingSubjectWise().getOptimizeMainString(selectedQuestionList, selectedSubjectList,chapterList,totalMarks, printingPaperType, pdfPageSetupBean, correctAnswerSheet, solutionSheet, solutionSheetTwoColumn);
            printPdfFormatFirstBean.setMainString(pdfMainStringBean.getMainString());
            }
            else
            {
            pdfMainStringBean = new QuestionsProcessingSubjectWise().getMainString(selectedQuestionList, selectedSubjectList,chapterList,totalMarks, printingPaperType, pdfPageSetupBean, correctAnswerSheet, solutionSheet, solutionSheetTwoColumn);
            printPdfFormatFirstBean.setMainString(pdfMainStringBean.getMainString());
            }
        }
        printPdfFormatFirstBean.setHeaderLeft(getHeaderFooterTextValue(pdfPageSetupBean.getHeaderLeftBean(),pdfFirstFormatBean,instituteName,selectedSubjectList,printingPaperType));
        printPdfFormatFirstBean.setHeaderCenter(getHeaderFooterTextValue(pdfPageSetupBean.getHeaderCenterBean(),pdfFirstFormatBean,instituteName,selectedSubjectList,printingPaperType));
        printPdfFormatFirstBean.setHeaderRight(getHeaderFooterTextValue(pdfPageSetupBean.getHeaderRightBean(),pdfFirstFormatBean,instituteName,selectedSubjectList,printingPaperType));
        printPdfFormatFirstBean.setFooterLeft(getHeaderFooterTextValue(pdfPageSetupBean.getFooterLeftBean(),pdfFirstFormatBean,instituteName,selectedSubjectList,printingPaperType));
        printPdfFormatFirstBean.setFooterCenter(getHeaderFooterTextValue(pdfPageSetupBean.getFooterCenterBean(),pdfFirstFormatBean,instituteName,selectedSubjectList,printingPaperType));
        printPdfFormatFirstBean.setFooterRight(getHeaderFooterTextValue(pdfPageSetupBean.getFooterRightBean(),pdfFirstFormatBean,instituteName,selectedSubjectList,printingPaperType));
        new ExportProcess().createFormatFirstPdf(printPdfFormatFirstBean, fileLocation, pdfFinalPanel);
        if(teachersCopy)
            new PrintTeacherCopy().setPrintPdf(pdfMainStringBean.getTeacherMainString(), instituteName, pdfFirstFormatBean.getSelectedTime(), totalMarks, getTeacherHeaderText(printingPaperType,selectedSubjectList,chapterList), fileLocation);
    }

    //Proccessing Methods
    private String getPageType() {
        return "\\documentclass[14pt," + pdfPageSetupBean.getPaperType().trim() + "]{article}";
    }
    
    private String getPageBorder() {
        String returnValue = "";
        if(pdfPageSetupBean.isPageBorder()) {
            returnValue += "\\usepackage{pgf}\n" +
                            "\\usepackage{pgfpages}\n" +
                            "\n" +
                            "\\pgfpagesdeclarelayout{boxed}\n" +
                            "{\n" +
                            "	\\edef\\pgfpageoptionborder{0pt}\n" +
                            "}\n" +
                            "{\n" +
                            "	\\pgfpagesphysicalpageoptions\n" +
                            "	{%\n" +
                            "		logical pages=1,%\n" +
                            "	}\n" +
                            "	\\pgfpageslogicalpageoptions{1}\n" +
                            "	{\n" +
                            "		border code=\\pgfsetlinewidth{"+pdfPageSetupBean.getBorderWidth()+"pt}\\pgfstroke,%\n" +
                            "		border shrink=\\pgfpageoptionborder,%\n" +
                            "		resized width=.90\\pgfphysicalwidth,%\n" +
                            "		resized height=.95\\pgfphysicalheight,%\n" +
                            "		center=\\pgfpoint{.5\\pgfphysicalwidth}{.5\\pgfphysicalheight}%\n" +
                            "	}%\n" +
                            "}\n" +
                            "\n" +
                            "\\pgfpagesuselayout{boxed}\n" +
                            "";
        } else {
            returnValue += " ";
        }
        return returnValue;
    }
    
    private String getHeaderFooterOnFirstPage() {
        String returnValue = "";
        if(pdfPageSetupBean.isHeaderFooterOnFirstPage()) {
            returnValue += " ";
        } else {
            returnValue += "\\thispagestyle{empty}";
        }
        return returnValue;
    }
    
    private String getWaterMarkString(String instituteName) {
        String returnValue = "";
        int waterMarkPosition = pdfPageSetupBean.getWaterMarkInfo();
        int waterMarkScale = pdfPageSetupBean.getWaterMarkScale();
        int waterMarkAngle = pdfPageSetupBean.getWaterMarkAngle();
        int waterMarkTextGrayScale = pdfPageSetupBean.getWaterMarkTextGrayScale();
        String waterMarkLogoPath = pdfPageSetupBean.getWaterMarkLogoPath();
//        waterMarkScale = waterMarkScale + 1;
        waterMarkTextGrayScale = waterMarkTextGrayScale + 1;
        String waterMarkTextGrayScales = "0." + waterMarkTextGrayScale;
        if (waterMarkPosition == 1 || waterMarkPosition == 2) {
            waterMarkLogoPath = waterMarkLogoPath.replace("\\", "/");
            if (waterMarkPosition == 1) {
                returnValue = "\\SetWatermarkText{"+ instituteName +"}\\SetWatermarkScale{"+ (waterMarkScale+1) +"}\\SetWatermarkAngle{"+ waterMarkAngle +"}\\SetWatermarkFontSize{0.5cm}\\SetWatermarkColor[gray]{"+ waterMarkTextGrayScales +"}";
            } else if (waterMarkPosition == 2) {
                returnValue = "\\SetWatermarkText{\\includegraphics{"+ waterMarkLogoPath +"}}\\SetWatermarkScale{"+ waterMarkScale +"}\\SetWatermarkAngle{"+ waterMarkAngle +"}\\SetWatermarkFontSize{0.5cm}";
            }
        } else {
            returnValue = "\\SetWatermarkText{ }\\SetWatermarkScale{"+ waterMarkScale +"}\\SetWatermarkAngle{"+ waterMarkAngle +"}\\SetWatermarkFontSize{1cm}\\SetWatermarkColor[rgb]{160,160,160}";
        }
        return returnValue;
    }
    
    private String getInstituteDepartmentHeading(PdfFirstFormatBean firstPageSetupBean,String instituteName) {
        int logoPosition = pdfPageSetupBean.getLogoPosition(); 
        String returnValue = "";
        String departmentName = firstPageSetupBean.getDepartmentName();
        if (pdfPageSetupBean.isSetLogo()) {
            String logoPath = pdfPageSetupBean.getLogoPath();
            String logoScaleCode = "[width=2.1cm,height=2.1cm]";
            logoPath = logoPath.replace("\\", "/");
            if(pdfPageSetupBean.isSetImageActualsize()) {
                logoScaleCode=" ";
                if(logoPosition == 3) {
                    returnValue = "\\begin{center}\\begin{tabular}{  >{\\centering}p{17.3cm} }\n" +
//                  "{\\huge{\\textbf{" + instituteName + "}}}\\tabularnewline		\n" +
                        "{\\includegraphics"+logoScaleCode+"{"+logoPath+"}} "  +
                        "\\end{tabular}\\end{center}";
                } else {
                  returnValue = "\\begin{center}\\begin{tabular}{  >{\\centering}p{17.3cm} }\n" +
                  "{\\includegraphics"+logoScaleCode+"{"+logoPath+"}}  " +
//                           "\\tabularnewline		\n" +
//                " \\vspace*{0.01in} {\\huge{\\textbf{" + instituteName + "}}} 	"+
                        "\\end{tabular}\\end{center}";
                }
            } else {
                if(firstPageSetupBean.isShowDepartmentName()) {
                    if(logoPosition == 0) {
                        returnValue = "\\begin{center}\\begin{tabular}{>{\\raggedright}p{1.9cm}  >{\\centering}p{15.4cm} }\\multirow{2}{*}[\\normalbaselineskip]{\\includegraphics"+logoScaleCode+"{"+logoPath+"}}&{\\huge{\\textbf{" + instituteName + "}}} \\tabularnewline				&                  \\fbox{\\LARGE\\textbf{" + departmentName + "}}\\end{tabular}\\end{center}";
                    } else if(logoPosition == 1) {
                        returnValue = "\\begin{center}\\begin{tabular}{ >{\\centering}p{15.4cm} >{\\raggedleft}p{1.9cm} }{\\huge{\\textbf{" + instituteName + "}}} & \\multirow{2}{*}[\\normalbaselineskip]{\\includegraphics"+logoScaleCode+"{"+logoPath+"}} \\tabularnewline		\\fbox{\\LARGE\\textbf{" + departmentName + "}}	&      \\end{tabular}\\end{center}";
                    } else if(logoPosition == 2) {
                        returnValue = "\\begin{center}\\begin{tabular}{  >{\\centering}p{17.3cm} }\n" +
                            "{\\includegraphics"+logoScaleCode+"{"+logoPath+"}}  \\tabularnewline		\n"
                            + " \\vspace*{0.01in} {\\huge{\\textbf{" + instituteName + "}}} \\tabularnewline	\\fbox{\\LARGE\\textbf{" + 
                            departmentName + "}}\\end{tabular}\\end{center}";
                    } else if(logoPosition == 3) {
                        returnValue = "\\begin{center}\\begin{tabular}{  >{\\centering}p{17.3cm} }\n" +
                            "{\\huge{\\textbf{" + instituteName + "}}}\\tabularnewline		\n"
                            + "{\\includegraphics"+logoScaleCode+"{"+logoPath+"}} \\tabularnewline	\\fbox{\\LARGE\\textbf{" +
                            departmentName + "}}\\end{tabular}\\end{center}";
                    }
                } else {
                    if(logoPosition == 0) {
                        returnValue = "\\begin{center}\\begin{tabular}{>{\\raggedright}p{1.9cm}  >{\\centering}p{15.4cm} }\\multirow{2}{*}[\\normalbaselineskip]{\\includegraphics"+logoScaleCode+"{"+logoPath+"}}&{\\huge{\\textbf{" + instituteName + "}}} \\tabularnewline				&                  \\end{tabular}\\end{center}";
                    } else if(logoPosition == 1) {
                        returnValue = "\\begin{center}\\begin{tabular}{ >{\\centering}p{15.4cm} >{\\raggedleft}p{1.9cm} }{\\huge{\\textbf{" + instituteName + "}}} & \\multirow{2}{*}[\\normalbaselineskip]{\\includegraphics"+logoScaleCode+"{"+logoPath+"}} \\tabularnewline		&      \\end{tabular}\\end{center}";
                    } else if(logoPosition == 2) {
                        returnValue = "\\begin{center}\\begin{tabular}{  >{\\centering}p{17.3cm} }\n" +
                            "{\\includegraphics"+logoScaleCode+"{"+logoPath+"}}  \\tabularnewline		\n"
                            + " \\vspace*{0.01in} {\\huge{\\textbf{" + instituteName + "}}} \\end{tabular}\\end{center}";
                    } else if(logoPosition == 3) {
                        returnValue = "\\begin{center}\\begin{tabular}{  >{\\centering}p{17.3cm} }\n" +
                            "{\\huge{\\textbf{" + instituteName + "}}}\\tabularnewline		\n"
                            + "{\\includegraphics"+logoScaleCode+"{"+logoPath+"}} \\end{tabular}\\end{center}";
                    }
                }
            }
        } else {
            if(firstPageSetupBean.isShowDepartmentName()) 
                returnValue = "\\begin{center}{\\Huge{\\textbf{" + instituteName + "}}}\\\\\\vspace*{0.08in}\\fbox{\\LARGE\\textbf{" + departmentName + "}}\\end{center}";
            else
                returnValue = "\\begin{center}{\\Huge{\\textbf{" + instituteName + "}}}\\end{center}";
        }
        return returnValue;
    }
    
    private String getFourFields(PdfFirstFormatBean pdfFirstFormatBean,ArrayList<SubjectBean> selectedSubjectList,ArrayList<QuestionBean> selectedQuestionList,String printingPaperType) {
        String returnString = "";
        if(printingPaperType.equalsIgnoreCase("YearWise") || printingPaperType.equalsIgnoreCase("GroupWise")) {
            
            ArrayList<SubjectBean> totalSubjectList = new SubjectOperation().getSubjectsList();

            int firstSubjectQueCount = 0;
            int secondSubjectQueCount = 0;
            int thirdSubjectQueCount = 0;
            int index = 0;

            for(SubjectBean bean : selectedSubjectList) {
                if(index == 0)
                    firstSubjectQueCount = getQuestionCount(bean.getSubjectId(), selectedQuestionList);
                else if(index == 1)
                    secondSubjectQueCount = getQuestionCount(bean.getSubjectId(), selectedQuestionList);
                else if(index == 2)
                    thirdSubjectQueCount = getQuestionCount(bean.getSubjectId(), selectedQuestionList);
                index ++;
            }
            returnString = "{\n" +
                            "\\setlength{\\extrarowheight}{5.5pt}\n" +
                            "\\begin{center}\n" +
                            "\\Large \\bf\n" +
                            "\n" +
                            "    \\begin{tabular}{ |>{\\centering} p{3.4cm}| >{\\centering}p{4.2cm} |>{\\centering} p{4.2cm} | >{\\centering}p{4.4cm}|}\n" +
                            "    \\hline\n" +
                            "    Subject &  "+totalSubjectList.get(0).getSubjectName()+" & "+totalSubjectList.get(1).getSubjectName()+" &   "+totalSubjectList.get(2).getSubjectName()+" \\tabularnewline \\hline\n" +
                            "    Questions & "+firstSubjectQueCount+" & "+secondSubjectQueCount+" & "+thirdSubjectQueCount+"  \\tabularnewline \\hline\n" +
                            "    Marks & "+(firstSubjectQueCount * pdfFirstFormatBean.getMarksPerQueSubjectOne())+" & "+(secondSubjectQueCount * pdfFirstFormatBean.getMarksPerQueSubjectTwo())+" & "+(thirdSubjectQueCount * pdfFirstFormatBean.getMarksPerQueSubjectThree())+"  \\tabularnewline \\hline\n" +
                            "    \\end{tabular}\n" +
                            "\\end{center}\n" +
                            "}";
        } else {
            String totalMarks = "";
            if(pdfFirstFormatBean.isShowTotal()) {
                if(pdfFirstFormatBean.getMarksPerQueSubjectOne() != 0 )
                    totalMarks = ""+(selectedQuestionList.size() * pdfFirstFormatBean.getMarksPerQueSubjectOne());
                else if(pdfFirstFormatBean.getMarksPerQueSubjectTwo() != 0 )
                    totalMarks = ""+(selectedQuestionList.size() * pdfFirstFormatBean.getMarksPerQueSubjectTwo());
                else
                    totalMarks = ""+(selectedQuestionList.size() * pdfFirstFormatBean.getMarksPerQueSubjectThree());
            }
            
            /*if(pdfFirstFormatBean.isShowSubjectName() && pdfFirstFormatBean.isShowTotalQuestions() && pdfFirstFormatBean.isShowTotal())
//                returnString = "{\\centerline{\\Large \\bf "+pdfFirstFormatBean.getSubjectName()+"}}\\ \\\\{\\hspace*{0.15in}\\raggedright {\\Large \\bf Total Questions:"+selectedQuestionList.size()+"}\\hfill\\raggedleft {\\Large \\bf Total Marks:"+totalMarks+"} \\hspace*{0.32in}\\vspace*{0.05in}}";
                returnString = "{\\hspace*{0.15in}\\raggedright {\\Large \\bf Total Questions : "+selectedQuestionList.size()+"} \\hfill  \\Large \\bf "+pdfFirstFormatBean.getSubjectName()+" \\hfill\\raggedleft {\\Large \\bf Total Marks : "+totalMarks+"} \\hspace*{0.32in}\\vspace*{0.05in}}";
            else if(pdfFirstFormatBean.isShowSubjectName() && pdfFirstFormatBean.isShowTotalQuestions() && !pdfFirstFormatBean.isShowTotal())
//                returnString = "{\\centerline{\\Large \\bf "+pdfFirstFormatBean.getSubjectName()+"}}\\ \\\\{\\hspace*{0.15in}\\raggedright {\\Large \\bf Total Questions:"+selectedQuestionList.size()+"}}";
                returnString = "{\\hspace*{0.15in}\\raggedright {\\Large \\bf "+pdfFirstFormatBean.getSubjectName()+"}\\hfill\\raggedleft {\\Large \\bf Total Questions:"+selectedQuestionList.size()+"} \\hspace*{0.32in}\\vspace*{0.05in}}";
            else if(pdfFirstFormatBean.isShowSubjectName() && !pdfFirstFormatBean.isShowTotalQuestions() && pdfFirstFormatBean.isShowTotal())
//                returnString = "{\\centerline{\\Large \\bf "+pdfFirstFormatBean.getSubjectName()+"}}\\ \\\\{\\hfill \\raggedleft {\\Large \\bf Total Marks:"+totalMarks+" }\\hspace*{0.32in}\\vspace*{0.05in}}";
                returnString = "{\\hspace*{0.15in}\\raggedright {\\Large \\bf "+pdfFirstFormatBean.getSubjectName()+"}\\hfill\\raggedleft {\\Large \\bf Total Marks:"+totalMarks+"} \\hspace*{0.32in}\\vspace*{0.05in}}";
            else if(pdfFirstFormatBean.isShowSubjectName() && !pdfFirstFormatBean.isShowTotalQuestions() && !pdfFirstFormatBean.isShowTotal())
                returnString = "{\\centerline{\\Large \\bf "+pdfFirstFormatBean.getSubjectName()+"}}";
            else if(!pdfFirstFormatBean.isShowSubjectName() && pdfFirstFormatBean.isShowTotalQuestions() && pdfFirstFormatBean.isShowTotal())
                returnString = "{\\hspace*{0.15in}\\raggedright {\\Large \\bf Total Questions:"+selectedQuestionList.size()+"}\\hfill\\raggedleft {\\Large \\bf Total Marks:"+totalMarks+"} \\hspace*{0.32in}\\vspace*{0.05in}}";
            else if(!pdfFirstFormatBean.isShowSubjectName() && pdfFirstFormatBean.isShowTotalQuestions() && !pdfFirstFormatBean.isShowTotal())
//                returnString = "{\\hspace*{0.15in}\\raggedright {\\Large \\bf Total Questions:"+selectedQuestionList.size()+"}}";
                returnString = "{\\centerline{\\Large \\bf "+selectedQuestionList.size()+"}}";
            else if(!pdfFirstFormatBean.isShowSubjectName() && !pdfFirstFormatBean.isShowTotalQuestions() && pdfFirstFormatBean.isShowTotal())
//                returnString = "{\\hfill\\raggedleft {\\Large \\bf Total Marks:"+totalMarks+" }\\hspace*{0.32in}\\vspace*{0.05in}}";
                returnString = "{\\centerline{\\Large \\bf Total Marks:"+totalMarks+"}}";*/
            
            if(pdfFirstFormatBean.isShowSubjectName() && pdfFirstFormatBean.isShowTotalQuestions() && pdfFirstFormatBean.isShowTotal())
                returnString = "{\\centerline{\\Large \\bf "+pdfFirstFormatBean.getSubjectName()+"}}\\ \\\\{\\hspace*{0.15in}\\raggedright {\\Large \\bf Total Questions : "+selectedQuestionList.size()+"}\\hfill\\raggedleft {\\Large \\bf Total Marks : "+totalMarks+"} \\hspace*{0.32in}\\vspace*{0.05in}}";
//                returnString = "{\\hspace*{0.15in}\\raggedright {\\Large \\bf Total Questions : "+selectedQuestionList.size()+"} \\hfill  \\Large \\bf "+pdfFirstFormatBean.getSubjectName()+" \\hfill\\raggedleft {\\Large \\bf Total Marks : "+totalMarks+"} \\hspace*{0.32in}\\vspace*{0.05in}}";
            else if(pdfFirstFormatBean.isShowSubjectName() && pdfFirstFormatBean.isShowTotalQuestions() && !pdfFirstFormatBean.isShowTotal())
                returnString = "{\\centerline{\\Large \\bf }}\\ \\\\{\\hspace*{0.15in}\\raggedright {\\Large \\bf "+pdfFirstFormatBean.getSubjectName()+"}\\hfill\\raggedleft {\\Large \\bf Total Questions : "+selectedQuestionList.size()+"} \\hspace*{0.32in}\\vspace*{0.05in}}";
//                returnString = "{\\centerline{\\Large \\bf "+pdfFirstFormatBean.getSubjectName()+"}}\\ \\\\{\\hspace*{0.15in}\\raggedright {\\Large \\bf Total Questions:"+selectedQuestionList.size()+"}}";
            else if(pdfFirstFormatBean.isShowSubjectName() && !pdfFirstFormatBean.isShowTotalQuestions() && pdfFirstFormatBean.isShowTotal())
                returnString = "{\\centerline{\\Large \\bf }}\\ \\\\{\\hspace*{0.15in}\\raggedright {\\Large \\bf "+pdfFirstFormatBean.getSubjectName()+"}\\hfill\\raggedleft {\\Large \\bf Total Marks : "+totalMarks+"} \\hspace*{0.32in}\\vspace*{0.05in}}";
//                returnString = "{\\hspace*{0.15in}\\raggedright {\\Large \\bf "+pdfFirstFormatBean.getSubjectName()+"}\\hfill\\raggedleft {\\Large \\bf Total Marks:"+totalMarks+"} \\hspace*{0.32in}\\vspace*{0.05in}}";
            else if(pdfFirstFormatBean.isShowSubjectName() && !pdfFirstFormatBean.isShowTotalQuestions() && !pdfFirstFormatBean.isShowTotal())
                returnString = "{\\centerline{\\Large \\bf "+pdfFirstFormatBean.getSubjectName()+"}}";
            else if(!pdfFirstFormatBean.isShowSubjectName() && pdfFirstFormatBean.isShowTotalQuestions() && pdfFirstFormatBean.isShowTotal())
                returnString = "{\\centerline{\\Large \\bf }}\\ \\\\{\\hspace*{0.15in}\\raggedright {\\Large \\bf Total Questions : "+selectedQuestionList.size()+"}\\hfill\\raggedleft {\\Large \\bf Total Marks : "+totalMarks+"} \\hspace*{0.32in}\\vspace*{0.05in}}";
//                returnString = "{\\hspace*{0.15in}\\raggedright {\\Large \\bf Total Questions:"+selectedQuestionList.size()+"}\\hfill\\raggedleft {\\Large \\bf Total Marks:"+totalMarks+"} \\hspace*{0.32in}\\vspace*{0.05in}}";
            else if(!pdfFirstFormatBean.isShowSubjectName() && pdfFirstFormatBean.isShowTotalQuestions() && !pdfFirstFormatBean.isShowTotal())
//                returnString = "{\\hspace*{0.15in}\\raggedright {\\Large \\bf Total Questions:"+selectedQuestionList.size()+"}}";
                returnString = "{\\centerline{\\Large \\bf Total Questions : "+selectedQuestionList.size()+"}}";
            else if(!pdfFirstFormatBean.isShowSubjectName() && !pdfFirstFormatBean.isShowTotalQuestions() && pdfFirstFormatBean.isShowTotal())
//                returnString = "{\\hfill\\raggedleft {\\Large \\bf Total Marks:"+totalMarks+" }\\hspace*{0.32in}\\vspace*{0.05in}}";
                returnString = "{\\centerline{\\Large \\bf Total Marks : "+totalMarks+"}}";
        }
        
        return returnString;
    }
    
    public String getRollDivisionTime(PdfFirstFormatBean firstPageSetupBean) {
        String returnString = "";
        
        if(firstPageSetupBean.isShowDivision() && firstPageSetupBean.isShowDate() && firstPageSetupBean.isShowTime()) {
            
            returnString = "\\begin{center}\n" +
                            "\\begin{tabular}{ccc}\n" +
                            "	\\setlength{\\tabcolsep}{12pt}\n" +
                            "	\\begin{tabular}{|l|l|l|l|l|l|}\n" +
                            "		\\hline\n" +
                            "		\\multicolumn{6}{|c|}{\\bf Roll No.}\\\\\\hline\n" +
                            "		&  &  & &  &  \\\\\n" +
                            "		&  &  & &  &  \\\\\\hline\n" +
                            "	\\end{tabular} \n" +
                            "	& \n" +
                            "	\\bigstrut[b]\n" +
                            "	\\setlength{\\tabcolsep}{7pt}\n" +
                            "	\\begin{tabular}{|l|}\n" +
                            "		\\hline\n" +
                            "		\\multirow{3}{*}{{\\Huge "+firstPageSetupBean.getDivisionName()+" }} \\\\ \n" +
                            "		\\\\\n" +
                            "		\\\\ \\hline\n" +
                            "	\\end{tabular}\n" +
                            "	& \n" +
                            "	\\setlength{\\extrarowheight}{6.5pt}\n" +
                            "	\\setlength{\\tabcolsep}{12pt}\n" +
                            "	\\begin{tabular}{|l|}\\hline\n" +
                            "		\\mbox{\\large{ "+firstPageSetupBean.getSelectedDate()+" }}\\\\\\hline\n" +
                            "		\\mbox{\\large{ "+firstPageSetupBean.getSelectedTime()+" }}\\\\\\hline\n" +
                            "	\\end{tabular}\n" +
                            "\\end{tabular}\n" +
                            "\\end{center}";
                
        } else if(!firstPageSetupBean.isShowDivision() && firstPageSetupBean.isShowDate() && firstPageSetupBean.isShowTime()) {
   
            returnString = "\\begin{center}\n" +
                            "	\\begin{tabular}{ccc}\n" +
                            "		\\setlength{\\tabcolsep}{12pt}\n" +
                            "		\\begin{tabular}{|l|l|l|l|l|l|}\n" +
                            "			\\hline\n" +
                            "			\\multicolumn{6}{|c|}{\\bf Roll No.}\\\\\\hline\n" +
                            "			&  &  & &  &  \\\\\n" +
                            "			&  &  & &  &  \\\\\\hline\n" +
                            "		\\end{tabular} \n" +
                            "		& &\n" +
                            "		\\setlength{\\extrarowheight}{6.5pt}\n" +
                            "		\\setlength{\\tabcolsep}{12pt}\n" +
                            "		\\begin{tabular}{|l|}\\hline\n" +
                            "			\\mbox{\\large{ "+firstPageSetupBean.getSelectedDate()+" }}\\\\\\hline\n" +
                            "			\\mbox{\\large{ "+firstPageSetupBean.getSelectedTime()+" }}\\\\\\hline\n" +
                            "		\\end{tabular}\n" +
                            "	\\end{tabular}\n" +
                            "\\end{center}";
           
        } else if(firstPageSetupBean.isShowDivision() && firstPageSetupBean.isShowDate() && !firstPageSetupBean.isShowTime()) {
            
            returnString = "\\begin{center}\n" +
                            "	\\begin{tabular}{ccc}\n" +
                            "		\\setlength{\\tabcolsep}{12pt}\n" +
                            "		\\begin{tabular}{|l|l|l|l|l|l|}\n" +
                            "			\\hline\n" +
                            "			\\multicolumn{6}{|c|}{\\bf Roll No.}\\\\\\hline\n" +
                            "			&  &  & &  &  \\\\\n" +
                            "			&  &  & &  &  \\\\\\hline\n" +
                            "		\\end{tabular} \n" +
                            "		& \n" +
                            "		\\bigstrut[b]\n" +
                            "		\\setlength{\\tabcolsep}{5pt}\n" +
                            "		\\begin{tabular}{|l|}\n" +
                            "			\\hline\n" +
                            "			\\multirow{3}{*}{{\\LARGE "+firstPageSetupBean.getDivisionName()+" }} \\\\ \n" +
                            "			\\\\\n" +
                            "			\\\\ \\hline\n" +
                            "		\\end{tabular}\n" +
                            "		&\n" +
                            "		\\bigstrut[b]\n" +
                            "		\\setlength{\\tabcolsep}{5pt}\n" +
                            "		\\begin{tabular}{|l|}\n" +
                            "			\\hline\n" +
                            "			\\multirow{3}{*}{{\\LARGE "+firstPageSetupBean.getSelectedDate()+" }} \\\\ \n" +
                            "			\\\\\n" +
                            "			\\\\ \\hline\n" +
                            "		\\end{tabular}\n" +
                            "	\\end{tabular}\n" +
                            "\\end{center}";
            
        } else if(firstPageSetupBean.isShowDivision() && !firstPageSetupBean.isShowDate() && firstPageSetupBean.isShowTime()) {
            
            returnString = "\\begin{center}\n" +
                            "	\\begin{tabular}{ccc}\n" +
                            "		\\setlength{\\tabcolsep}{12pt}\n" +
                            "		\\begin{tabular}{|l|l|l|l|l|l|}\n" +
                            "			\\hline\n" +
                            "			\\multicolumn{6}{|c|}{\\bf Roll No.}\\\\\\hline\n" +
                            "			&  &  & &  &  \\\\\n" +
                            "			&  &  & &  &  \\\\\\hline\n" +
                            "		\\end{tabular} \n" +
                            "		& \n" +
                            "		\\bigstrut[b]\n" +
                            "		\\setlength{\\tabcolsep}{5pt}\n" +
                            "		\\begin{tabular}{|l|}\n" +
                            "			\\hline\n" +
                            "			\\multirow{3}{*}{{\\LARGE "+firstPageSetupBean.getDivisionName()+" }} \\\\ \n" +
                            "			\\\\\n" +
                            "			\\\\ \\hline\n" +
                            "		\\end{tabular}\n" +
                            "		&\n" +
                            "		\\bigstrut[b]\n" +
                            "		\\setlength{\\tabcolsep}{5pt}\n" +
                            "		\\begin{tabular}{|l|}\n" +
                            "			\\hline\n" +
                            "			\\multirow{3}{*}{{\\LARGE "+firstPageSetupBean.getSelectedTime()+" }} \\\\ \n" +
                            "			\\\\\n" +
                            "			\\\\ \\hline\n" +
                            "		\\end{tabular}\n" +
                            "	\\end{tabular}\n" +
                            "\\end{center}";
            
        } else if(firstPageSetupBean.isShowDivision() || firstPageSetupBean.isShowDate() || firstPageSetupBean.isShowTime()) {
            
            String str = "";
            
            if(firstPageSetupBean.isShowDivision())
                str = firstPageSetupBean.getDivisionName();
            else if(firstPageSetupBean.isShowDate())
                str = firstPageSetupBean.getSelectedDate();
            else
                str = firstPageSetupBean.getSelectedTime();
            
            returnString = "\\begin{center}\n" +
                            "\\begin{tabular}{cc}\n" +
                            "	\\setlength{\\tabcolsep}{10pt}\n" +
                            "	\\begin{tabular}{|l|l|l|l|l|l|}\n" +
                            "		\\hline\n" +
                            "		\\multicolumn{6}{|c|}{\\bf Roll No.}\\\\\\hline\n" +
                            "		&  &  & &  &  \\\\\n" +
                            "		&  &  & &  &  \\\\\\hline\n" +
                            "	\\end{tabular} \n" +
                            "	& \n" +
                            "	\\bigstrut[b]\n" +
                            "	\\setlength{\\tabcolsep}{5pt}\n" +
                            "	\\begin{tabular}{|l|}\n" +
                            "		\\hline\n" +
                            "		\\multirow{3}{*}{{\\LARGE "+str+" }} \\\\ \n" +
                            "		\\\\\n" +
                            "		\\\\ \\hline\n" +
                            "	\\end{tabular}\n" +
                            "\\end{tabular}\n" +
                            "\\end{center}";
        } else {
            
            returnString = "\\begin{center}\n" +
                            "	\\begin{tabular}{c}\n" +
                            "		\\setlength{\\tabcolsep}{10pt}\n" +
                            "		\\begin{tabular}{|l|l|l|l|l|l|}\n" +
                            "			\\hline\n" +
                            "			\\multicolumn{6}{|c|}{\\bf Roll No.}\\\\\\hline\n" +
                            "			&  &  & &  &  \\\\\n" +
                            "			&  &  & &  &  \\\\\\hline\n" +
                            "		\\end{tabular} \n" +
                            "	\\end{tabular}\n" +
                            "\\end{center}";
        }
        
        return returnString;
    }
    
    private String getPaperName(PdfFirstFormatBean pdfFirstFormatBean) {
        String returnString = "";
        
        if(pdfFirstFormatBean.isShowPaperName())
            returnString = "\\vspace*{-0.15in}\\begin{center}\\LARGE\\textbf{"+pdfFirstFormatBean.getPaperName() +
                            "}\\end{center}\\vspace*{-0.3in}";
        else
            returnString = "\\vspace*{0.1in}";
        
        return returnString;
    }
    
    private int getQuestionCount(int subjectId,ArrayList<QuestionBean> selectedQuestionList) {
        int returnValue = 0;
        for(QuestionBean bean : selectedQuestionList) {
            if(bean.getSubjectId() == subjectId)
                returnValue += 1;
        }
        return returnValue;
    }
    
    private String getHeaderFooterTextValue(HeaderFooterTextBean headerFooterTextBean,PdfFirstFormatBean pdfFirstFormatBean,String instituteName,ArrayList<SubjectBean> selectedSubjectList,String printingPaperType) {
        String returnValue = "";
        if(!headerFooterTextBean.isCheckBoxSelected()) {
            if(headerFooterTextBean.getTextValue().trim().equalsIgnoreCase("None")) {
                returnValue = " ";
            } else if(headerFooterTextBean.getTextValue().trim().equalsIgnoreCase("Subject/Group")) {
                if(printingPaperType.equalsIgnoreCase("GroupWise") || printingPaperType.equalsIgnoreCase("YearWise")) {
                    for(SubjectBean sb : selectedSubjectList) {
                        returnValue += sb.getSubjectName().substring(0, 1);
                    }
                } else {
                    returnValue = selectedSubjectList.get(0).getSubjectName();
                }
            } else if(headerFooterTextBean.getTextValue().trim().equalsIgnoreCase("Division")) {
                returnValue = pdfFirstFormatBean.getDivisionName();
            } else if(headerFooterTextBean.getTextValue().trim().equalsIgnoreCase("Date")) {
                returnValue = pdfFirstFormatBean.getSelectedDate();
            } else if(headerFooterTextBean.getTextValue().trim().equalsIgnoreCase("Institute Name")) {
                returnValue = instituteName;
            } else if(headerFooterTextBean.getTextValue().trim().equalsIgnoreCase("Page Number")) {
                returnValue = "\\thepage";
            }
        } else {
            returnValue = headerFooterTextBean.getTextValue().trim();
        }
        return returnValue;
    }
    
    private String getTeacherHeaderText(String printingPaperType,ArrayList<SubjectBean> selectedSubjectList,ArrayList<ChapterBean> chapterList) {
        String returnString = "";
        if(printingPaperType.equalsIgnoreCase("ChapterWise")) {
            returnString += selectedSubjectList.get(0).getSubjectName().trim()+" : "+chapterList.get(0).getChapterName().trim();
        } else if(printingPaperType.equalsIgnoreCase("UnitWise")) {
            returnString += selectedSubjectList.get(0).getSubjectName().trim()+" Units Test";
        } else if(printingPaperType.equalsIgnoreCase("SubjectWise")) {
            returnString += selectedSubjectList.get(0).getSubjectName().trim()+" Subject Test";
        } else if(printingPaperType.equalsIgnoreCase("GroupWise")) {
            returnString += new ProcessManager().getGroupName().trim()+" Group Test";
        } else if(printingPaperType.equalsIgnoreCase("YearWise")) {
            returnString += new ProcessManager().getGroupName().trim()+" Old Paper Test";
        }
        return returnString;
    }
}
