/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileSystemView;

/**
 *
 * @author Aniket
 */
public class DefaultFolderLocation {
    
    public String getFolderLocation() {
        String returnString = null;
        BufferedReader br = null;
        try {
            File fl = new File("DefaultPath.txt");
            if(!fl.exists())
                fl.createNewFile();
            //br = new BufferedReader(new FileReader(FILENAME));
            br = new BufferedReader(new FileReader("DefaultPath.txt"));
            System.out.println("br="+br);
            String currentLineText;
            while ((currentLineText = br.readLine()) != null)
                returnString = currentLineText;
        } catch (IOException ex) {
            returnString = null;
            ex.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
                if(returnString == null) {
                    JFileChooser fr = new JFileChooser();
                    FileSystemView fw = fr.getFileSystemView();
                    returnString = fw.getDefaultDirectory().toString();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return returnString;
    }
}
