/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Model;

/**
 *
 * @author Aniket
 */
public class ProcessManager {
    private String processPath;
    private String groupName;
    
    public ProcessManager() {
        setProcessPath("C:/CTP-MHCET-PCB-CLIENTSERVER/CruncherProcess");
        setGroupName("MH-CET");
    }
    
    public String getProcessPath() {
        return processPath;
    }

    public void setProcessPath(String processPath) {
        this.processPath = processPath;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
