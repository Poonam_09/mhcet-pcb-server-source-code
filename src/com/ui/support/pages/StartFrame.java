/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ui.support.pages;

import com.Model.TitleInfo;
import com.Registration.Registration;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;


public class StartFrame {
    static int rollNo;
    
    public static void main(String[] args){
        
        EventQueue.invokeLater(new Runnable() {
            public void run(){
                ImageFrame frame = new ImageFrame();     
                frame.setAlwaysOnTop(true);
                frame.setUndecorated(true);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);     
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);
                frame.setAlwaysOnTop(true);
                Timer timer = new Timer();
                Tasker sound = new Tasker(frame,rollNo);
                timer.schedule(sound, 4500);
            }
        });
    }
}

class ImageFrame extends JFrame{

    public ImageFrame(){
        TitleInfo info = new TitleInfo();
        setTitle(info.getTitle());
        setIconImage(new ImageIcon(getClass().getResource(info.getLogo())).getImage());
        File imagePath = new File("startLogo.gif");
        try {
            JLabel jLabel=new JLabel();
            Image image = ImageIO.read(imagePath);
            setSize(image.getWidth(rootPane), image.getHeight(rootPane));
            jLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/startLogo.gif")));
            this.add(jLabel);
            setLocationRelativeTo(null);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    public static final int DEFAULT_WIDTH = 321;
    public static final int DEFAULT_HEIGHT = 100;
}


class ImageComponent extends JComponent{
    private static final long serialVersionUID = 1L;
    private Image image;
    public ImageComponent(Image image){
            this.image = image;
    }
    public void paintComponent (Graphics g){
        if(image == null) return;
        int imageWidth = image.getWidth(this);
        int imageHeight = image.getHeight(this);

        g.drawImage(image, 0, 0, this);

        for (int i = 0; i*imageWidth <= getWidth(); i++)
            for(int j = 0; j*imageHeight <= getHeight();j++)
                if(i+j>0) g.copyArea(0, 0, imageWidth, imageHeight, i*imageWidth, j*imageHeight);
    }
}

class Tasker extends TimerTask { 
  
  JFrame frame1,frame2; 
  int rollNo;
  
  
  public Tasker(JFrame frame1,int rollno) {
      this.frame1 = frame1;
      this.rollNo=rollno;
  }
 
  public void run() {
      frame1.dispose();
      new Registration();
  }
}