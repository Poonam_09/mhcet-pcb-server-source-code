/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ImagePanel.java
 *
 * Created on Sep 28, 2012, 7:45:12 PM
 */
package com.ui.support.pages;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class ImagePanel1 extends javax.swing.JPanel {

    private BufferedImage image;
    int width;
    int height;

    public ImagePanel1(BufferedImage image,int w,int h) {           
          this.image = image;       
          width=w;
          height=h;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0,width,height, null); // see javadoc for more info on the parameters            
    }
    
    /*public static void main(String[] s)
    {
        JFrame f=new JFrame();
        f.setSize(1500,1500);
        f.add(new ImagePanel1(""));
        f.setVisible(true);
    }*/

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
