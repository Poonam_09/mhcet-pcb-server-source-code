/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.LatexProcessing.pdf;

import com.bean.ChapterBean;
import com.bean.ImageRatioBean;
import com.bean.OptionImageDimensionsBean;
import com.bean.PdfMainStringBean;
import com.bean.PdfPageSetupBean;
import com.bean.QuestionBean;
import com.bean.SubjectBean;
import com.db.operations.ImageRatioOperation;
import com.db.operations.NewAndOldQuestionOperation;
import com.db.operations.OptionImageDimentionOperation;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import javax.imageio.ImageIO;

/**
 *
 * @author Aniket
 */
public class QuestionsProcessingChapterWise {
    private ArrayList<ImageRatioBean> viewImgDimeList;
    private String printingPaperType;
    private int totalMarks;
    private String subjectString;
    
    public PdfMainStringBean getOptimizeMainString(ArrayList<QuestionBean> selectedQuestionsList,ArrayList<SubjectBean> selectedSubjectList,ArrayList<ChapterBean> chapterList,int totalMarks,String printingPaperType
        ,PdfPageSetupBean pdfPageSetupBean,boolean correctAnswerSheet,boolean solutionSheet,boolean solutionSheetTwoColumn) {
        PdfMainStringBean returnBean = null;
        String mainString = "";
        String teacherString = "";
        this.printingPaperType = printingPaperType;
        this.totalMarks = totalMarks;
        String question,optionA,optionB,optionC,optionD,queYear,hint,queImagePath,optionImagePath,hintImagePath;
        boolean isPreviousQuestionPaper = false;
        String mainComplete = "",mainCompleteHint = "";
        NewQuesLatexProcessing newQuesProcess = null;
        NewHintLatexProcessing newHintProcess = null;
        
        OldQuesLatexProcessing oldQuesProcess = null;
        OldHintLatexProcessing oldHintProcess = null;
        
        subjectString = "";
        for(SubjectBean subjectBean : selectedSubjectList) {
            subjectString += subjectBean.getSubjectName() +",";
        }
        
        ArrayList<OptionImageDimensionsBean> optionImageDimentonList = new OptionImageDimentionOperation().getOptionImageDimaentionList(selectedQuestionsList, isPreviousQuestionPaper);
        viewImgDimeList = new ImageRatioOperation().getImageRatioList(selectedQuestionsList);
        ArrayList<Integer> newProccessingQuestionsList = new NewAndOldQuestionOperation().getNewQuestionsLists(selectedQuestionsList, isPreviousQuestionPaper);
        int questionIndex = 0;
          
        for(QuestionBean questionsBean : selectedQuestionsList) {
            question = questionsBean.getQuestion();
            optionA = questionsBean.getOptionA();
            optionB = questionsBean.getOptionB();
            optionC = questionsBean.getOptionC();
            optionD = questionsBean.getOptionD();
            hint = questionsBean.getHint();
            
            int size = pdfPageSetupBean.getFontSize();
            double baseline = size*1.2;
            
//            if(newQuesProcess == null)
//                newQuesProcess = new NewQuesLatexProcessing();
//            if(newHintProcess == null)
//                newHintProcess = new NewHintLatexProcessing();
//                
//            question = newQuesProcess.mboxProcessing(question,true);
//            if(!(optionA.equals("\\mbox{}")))
//                optionA = newQuesProcess.mboxProcessing(optionA,false);
//            if(!(optionB.equals("\\mbox{}")))
//                optionB = newQuesProcess.mboxProcessing(optionB,false);
//            if(!(optionC.equals("\\mbox{}")))
//                optionC = newQuesProcess.mboxProcessing(optionC,false);
//            if(!(optionD.equals("\\mbox{}")))
//                optionD = newQuesProcess.mboxProcessing(optionD,false);
//            if(!(hint.equals("\\mbox{}"))) {
////                hint = hint.replace("$ \\therefore $", "\\\\ $ \\therefore $");
//                hint = newHintProcess.mboxProcessing(hint);
//            }

            if(newProccessingQuestionsList.contains(questionsBean.getQuestionId())) {
                if(newQuesProcess == null)
                    newQuesProcess = new NewQuesLatexProcessing();
                if(newHintProcess == null)
                    newHintProcess = new NewHintLatexProcessing();

                question = newQuesProcess.mboxProcessing(question,true);
                if(!(optionA.equals("\\mbox{}")))
                    optionA = newQuesProcess.mboxProcessing(optionA,false);
                if(!(optionB.equals("\\mbox{}")))
                    optionB = newQuesProcess.mboxProcessing(optionB,false);
                if(!(optionC.equals("\\mbox{}")))
                    optionC = newQuesProcess.mboxProcessing(optionC,false);
                if(!(optionD.equals("\\mbox{}")))
                    optionD = newQuesProcess.mboxProcessing(optionD,false);
                if(!(hint.equals("\\mbox{}"))) 
                    hint = newHintProcess.mboxProcessing(hint);
            } else {
                if(oldQuesProcess == null)
                    oldQuesProcess = new OldQuesLatexProcessing();
                if(oldHintProcess == null)
                    oldHintProcess = new OldHintLatexProcessing();
                
                question = oldQuesProcess.mboxProcessing(question,true);
                if(!(optionA.equals("\\mbox{}")))
                    optionA = oldQuesProcess.mboxProcessing(optionA,false);
                if(!(optionB.equals("\\mbox{}")))
                    optionB = oldQuesProcess.mboxProcessing(optionB,false);
                if(!(optionC.equals("\\mbox{}")))
                    optionC = oldQuesProcess.mboxProcessing(optionC,false);
                if(!(optionD.equals("\\mbox{}")))
                    optionD = oldQuesProcess.mboxProcessing(optionD,false);
                if(!(hint.equals("\\mbox{}")))
                    hint = oldHintProcess.mboxProcessing(hint);
            }

            if(pdfPageSetupBean.isQuestionBold())
                question = "{ \\bf "+ question +"}";

            if(pdfPageSetupBean.isOptionBold()) {
                if(!(optionA.equals("\\mbox{}")))
                    optionA = "{ \\bf "+ optionA +"}";
                if(!(optionB.equals("\\mbox{}")))
                    optionB = "{ \\bf "+ optionB +"}";
                if(!(optionC.equals("\\mbox{}")))
                    optionC = "{ \\bf "+ optionC +"}";
                if(!(optionD.equals("\\mbox{}")))
                    optionD = "{ \\bf "+ optionD +"}";
            }   
            
            queYear = questionsBean.getYear();
            queYear = queYear.replace("&", "\\&");
            if (queYear != null || queYear != "") 
                queYear = queYear.trim();
            else
                queYear = "";
            if (!pdfPageSetupBean.isTwoColumn()) {
                if (pdfPageSetupBean.isPrintYear() && queYear != null && !queYear.equals("")) {
                    mainComplete += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont \\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.94\\textwidth}} \\textbf{" + (questionIndex  + pdfPageSetupBean.getQuestionStartNo()) + ") } & " + question + "    \\textbf{" +  queYear  + "}" + " \\end{tabular}}";
                } else {
                    mainComplete += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont \\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.94\\textwidth}} \\textbf{" + (questionIndex  + pdfPageSetupBean.getQuestionStartNo()) + ") } & " + question + "   \\end{tabular}}";
                }
            } else {
                if (pdfPageSetupBean.isPrintYear() && queYear != null && !queYear.equals("")) {
                    mainComplete += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.44\\textwidth}} \\textbf{" + (questionIndex  + pdfPageSetupBean.getQuestionStartNo()) + ") } & " + question + "    \\textbf{" + queYear + "}" + " \\end{tabular}}";
                } else {
                    mainComplete += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.44\\textwidth}} \\textbf{" + (questionIndex  + pdfPageSetupBean.getQuestionStartNo()) + ") } & " + question + "   \\end{tabular}}";
                }
            }
            
            teacherString += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.44\\textwidth}} \\textbf{" + (questionIndex  + pdfPageSetupBean.getQuestionStartNo()) + ") } & " + question + " \\textbf{" + queYear + "}" + " \\end{tabular}}";
            
            if(questionsBean.isIsQuestionAsImage()) {
                try {
                    queImagePath = questionsBean.getQuestionImagePath();
                    queImagePath = queImagePath.replaceAll("_", "-");
                    float scale;
                    int actualWidth;
                    String latexCode = "";
                    scale = getImageDim(queImagePath);
                    scale = precision(2, scale);
                    scale = (float) (scale-0.2);
                    BufferedImage bufferImg = ImageIO.read(new File(queImagePath));
                    int width = bufferImg.getWidth();
                    actualWidth = (int) (width * scale);
                    if(actualWidth<390)
                        latexCode="[scale="+scale+"]";
                    else 
                        latexCode="[width=0.48\\textwidth]";
                    teacherString += "\\\\{\\centering\\includegraphics"+latexCode+"{" + queImagePath + "}\\\\}";
                    mainComplete += "\\\\{\\centering\\includegraphics"+latexCode+"{" + queImagePath + "}\\\\}";
                    
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            } else {
                queImagePath = "";
            }
            
            if(questionsBean.isIsOptionAsImage()) {
                try {
                    optionImagePath = questionsBean.getOptionImagePath();
                    float scale;
                    int actualWidth;
                    String latexCode = "";
                    scale = getImageDim(optionImagePath);
                    scale = precision(2, scale);
                    scale = (float) (scale-0.2);
                    BufferedImage bimg = ImageIO.read(new File(optionImagePath));
                    int width = bimg.getWidth();
                    actualWidth = (int) (width * scale);
                    if(actualWidth<390)
                        latexCode="[scale="+scale+"]";
                    else
                        latexCode="[width=0.48\\textwidth]";
                    teacherString += "\\\\{\\centering\\includegraphics"+latexCode+"{" + optionImagePath + "}\\\\}";
                    mainComplete += "\\\\{\\centering\\includegraphics"+latexCode+"{" + optionImagePath + "}\\\\}";
                    
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            } else {
                optionImagePath = "";
                OptionImageDimensionsBean optImgDimBean = new OptionImageDimensionsBean();
                for(OptionImageDimensionsBean optBean : optionImageDimentonList){
                    if(optBean.getQuestionId() == questionsBean.getQuestionId()){
                        optImgDimBean = optBean;
                        break;
                    }
                }
                int optionType;
                if (!pdfPageSetupBean.isTwoColumn()) {
                    optionType = 0;
                    if (optImgDimBean.getOptionA() < 90 && optImgDimBean.getOptionB() < 90 && optImgDimBean.getOptionC() < 90 && optImgDimBean.getOptionD() < 90)
                        optionType = 1;
                    else if (optImgDimBean.getOptionA() < 210 && optImgDimBean.getOptionB() < 210 && optImgDimBean.getOptionC() < 210 && optImgDimBean.getOptionD() < 210)
                        optionType = 2;
                    
                    switch (optionType) {
                        case 0:
                            mainComplete += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{0.8}   \\hspace*{0.04\\textwidth}\\begin{tabular}[H]{>{\\raggedright}p{0.01\\textwidth} >{\\raggedright}p{0.95\\textwidth}}{\\textbf{A})  } & " + optionA + " \\tabularnewline {\\textbf{B})  } &" + optionB + " \\tabularnewline {\\textbf{C})  } & " + optionC + "\\tabularnewline {\\textbf{D})  }&" + optionD + " \\end{tabular}}}";
                            break;
                        case 1:
                            mainComplete += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{0.8}  \\hspace*{0.035\\textwidth}  \\begin{tabular}[H] {>{\\raggedright}p{0.23\\textwidth} >{\\raggedright}p{0.22\\textwidth} >{\\raggedright}p{0.22\\textwidth} >{\\raggedright}p{0.22\\textwidth}}{\\textbf{A})  }" + optionA + " & {\\textbf{B})  }" + optionB + " & {\\textbf{C})  }" + optionC + "& {\\textbf{D})  }" + optionD + " \\end{tabular}}}";
                            break;
                        case 2:
                            mainComplete += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{0.8}  \\hspace*{0.035\\textwidth}  \\begin{tabular}[H]  {>{\\raggedright}p{0.45\\textwidth} >{\\raggedright}p{0.45\\textwidth}}{\\textbf{A})  }"+ optionA + " & {\\textbf{B})  }" + optionB + " \\tabularnewline {\\textbf{C})  }" + optionC + "& {\\textbf{D})  }" + optionD + " \\end{tabular}}}";
                            break;
                    }
                    
                    optionType = 0;
                    if(optImgDimBean.getOptionA() < 35 && optImgDimBean.getOptionB() < 35 && optImgDimBean.getOptionC() < 35 && optImgDimBean.getOptionD() < 35) 
                        optionType = 1;
                    else if (optImgDimBean.getOptionA() < 85 && optImgDimBean.getOptionB() < 85 && optImgDimBean.getOptionC() < 85 && optImgDimBean.getOptionD() < 85)
                        optionType = 2;

                    switch (optionType) {
                        case 0:
                            teacherString += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{0.8}   \\hspace*{0.035\\textwidth}\\begin{tabular}[H]{>{\\raggedright}p{0.01\\textwidth} >{\\raggedright}p{0.41\\textwidth}}{\\textbf{A})  } & " + optionA + " \\tabularnewline {\\textbf{B})  } &" + optionB + " \\tabularnewline {\\textbf{C})  } & " + optionC + "\\tabularnewline {\\textbf{D})  }&" + optionD + " \\end{tabular}}}";
                            break;
                        case 1:
                            teacherString += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{0.8}  \\hspace*{0.03\\textwidth}  \\begin{tabular}[H] {>{\\raggedright}p{0.102\\textwidth} >{\\raggedright}p{0.100\\textwidth} >{\\raggedright}p{0.100\\textwidth} >{\\raggedright}p{0.100\\textwidth}}{\\textbf{A})  }" + optionA + " & {\\textbf{B})  }" + optionB + " & {\\textbf{C})  }" + optionC + "& {\\textbf{D})  }" + optionD + "  \\end{tabular}}}";
                            break;
                        case 2:
                            teacherString += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{0.8}  \\hspace*{0.03\\textwidth}  \\begin{tabular}[H]  {>{\\raggedright}p{0.20\\textwidth} >{\\raggedright}p{0.20\\textwidth}}{\\textbf{A})}" + optionA + " & {\\textbf{B})  }" + optionB + " \\tabularnewline {\\textbf{C})  }" + optionC + "& {\\textbf{D})  }" + optionD + " \\end{tabular}}}";
                            break;
                    }
                    
                } else {
                    optionType = 0;
                    if(optImgDimBean.getOptionA() < 35 && optImgDimBean.getOptionB() < 35 && optImgDimBean.getOptionC() < 35 && optImgDimBean.getOptionD() < 35) 
                        optionType = 1;
                    else if (optImgDimBean.getOptionA() < 85 && optImgDimBean.getOptionB() < 85 && optImgDimBean.getOptionC() < 85 && optImgDimBean.getOptionD() < 85)
                        optionType = 2;
                    
                    switch (optionType) {
                        case 0:
                            mainComplete += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{0.8}   \\hspace*{0.035\\textwidth}\\begin{tabular}[H]{>{\\raggedright}p{0.01\\textwidth} >{\\raggedright}p{0.41\\textwidth}}{\\textbf{A})  } & " + optionA + " \\tabularnewline {\\textbf{B})  } &" + optionB + " \\tabularnewline {\\textbf{C})  } & " + optionC + "\\tabularnewline {\\textbf{D})  }&" + optionD + " \\end{tabular}}}";
                            teacherString += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{0.8}   \\hspace*{0.035\\textwidth}\\begin{tabular}[H]{>{\\raggedright}p{0.01\\textwidth} >{\\raggedright}p{0.41\\textwidth}}{\\textbf{A})  } & " + optionA + " \\tabularnewline {\\textbf{B})  } &" + optionB + " \\tabularnewline {\\textbf{C})  } & " + optionC + "\\tabularnewline {\\textbf{D})  }&" + optionD + " \\end{tabular}}}";
                            break;
                        case 1:
                            mainComplete += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{0.8}  \\hspace*{0.03\\textwidth}  \\begin{tabular}[H] {>{\\raggedright}p{0.102\\textwidth} >{\\raggedright}p{0.100\\textwidth} >{\\raggedright}p{0.100\\textwidth} >{\\raggedright}p{0.100\\textwidth}}{\\textbf{A})  }" + optionA + " & {\\textbf{B})  }" + optionB + " & {\\textbf{C})  }" + optionC + "& {\\textbf{D})  }" + optionD + "  \\end{tabular}}}";
                            teacherString += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{0.8}  \\hspace*{0.03\\textwidth}  \\begin{tabular}[H] {>{\\raggedright}p{0.102\\textwidth} >{\\raggedright}p{0.100\\textwidth} >{\\raggedright}p{0.100\\textwidth} >{\\raggedright}p{0.100\\textwidth}}{\\textbf{A})  }" + optionA + " & {\\textbf{B})  }" + optionB + " & {\\textbf{C})  }" + optionC + "& {\\textbf{D})  }" + optionD + "  \\end{tabular}}}";
                            break;
                        case 2:
                            mainComplete += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{0.8}  \\hspace*{0.03\\textwidth}  \\begin{tabular}[H]  {>{\\raggedright}p{0.20\\textwidth} >{\\raggedright}p{0.20\\textwidth}}{\\textbf{A})  }" + optionA + " & {\\textbf{B})  }" + optionB + " \\tabularnewline {\\textbf{C})  }" + optionC + "& {\\textbf{D})  }" + optionD + " \\end{tabular}}}";
                            teacherString += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{0.8}  \\hspace*{0.03\\textwidth}  \\begin{tabular}[H]  {>{\\raggedright}p{0.20\\textwidth} >{\\raggedright}p{0.20\\textwidth}}{\\textbf{A})  }" + optionA + " & {\\textbf{B})  }" + optionB + " \\tabularnewline {\\textbf{C})  }" + optionC + "& {\\textbf{D})  }" + optionD + " \\end{tabular}}}";
                            break;
                    }
                }
            }
            teacherString += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{1.2}  \\hspace*{0.035\\textwidth}\\begin{tabular}[H]{>{\\raggedright}p{0.44\\textwidth} }{\\textbf{Answer : "+questionsBean.getAnswer().trim()+"} } \\end{tabular}}}\\newline";
            
            if((!hint.trim().equals("\\mbox{}") && !(hint.trim().equals(""))) || questionsBean.isIsHintAsImage()) {
                if(pdfPageSetupBean.isSolutionBold())
                    hint = "{\\bf "+ hint +"}";
                if(solutionSheetTwoColumn)
                    mainCompleteHint += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.44\\textwidth}} {\\textbf{"+ (questionIndex  + pdfPageSetupBean.getQuestionStartNo()) +")} & " + hint + "\\end{tabular}}}";
                else
                    mainCompleteHint += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.94\\textwidth}} {\\textbf{"+ (questionIndex  + pdfPageSetupBean.getQuestionStartNo()) +")} & " + hint + "\\end{tabular}}}";
//                mainComplete += "\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.94\\textwidth}}\\textbf{" + (questionIndex  + pdfPageSetupBean.getQuestionStartNo()) + ") } & " + question + "   \\end{tabular}\\newline";
//                mainCompleteHint += "\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.44\\textwidth}}\\textbf{"+ (questionIndex  + pdfPageSetupBean.getQuestionStartNo()) +")} & " + hint + "\\end{tabular}\\\\newline";
                teacherString += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{1.1}\\hspace*{0.035\\textwidth}\\begin{tabular}[H]{>{\\raggedright}p{0.44\\textwidth}}{\\textbf{Solution :}}\\tabularnewline"+ hint +"\\end{tabular}}}\\newline";
            }
            
            if(questionsBean.isIsHintAsImage()) {
                try {    
                    hintImagePath = questionsBean.getHintImagePath();
                    hintImagePath = hintImagePath.replace("\\", "/");
                    float scale;
                    int actualwidth;
                    String latexCode = "";
                    scale = getImageDim(hintImagePath);
                    scale = precision(2, scale);                
                    scale = (float) (scale-0.2);
                    BufferedImage bimg = ImageIO.read(new File(hintImagePath));
                    int width = bimg.getWidth();
                    actualwidth = (int) (width * scale);
                    if(actualwidth<390)
                        latexCode="[scale="+scale+"]";
                    else
                        latexCode="[width=0.48\\textwidth]";
                    mainCompleteHint += "\\\\{\\centering\\includegraphics" + latexCode + "{" + hintImagePath + "}\\\\}";
                    teacherString += "\\\\{\\centering\\includegraphics" + latexCode + "{" + hintImagePath + "}\\\\}";
                    
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            } else {
                hintImagePath = "";
            }
            if((!hint.trim().equals("\\mbox{}") && !(hint.trim().equals(""))) || questionsBean.isIsHintAsImage()) 
//                mainCompleteHint += " \\\\[2.5pt] ";
                mainCompleteHint += " \\\\";
            
            mainComplete += "\\sagar";
            questionIndex ++;
        }
        
        teacherString = "\\begin{multicols*}{2}" + teacherString + "\\end{multicols*}";
        teacherString = teacherString.replace("\\rm", "\\text");
        
        if(pdfPageSetupBean.isTwoColumn()) {
            mainComplete = "\\vspace*{-0.2in}\\begin{multicols*}{2}" + mainComplete + "\\end{multicols*}";
        }
        String chapterName = chapterList.get(0).getChapterName().trim();
        mainComplete = "\\setlength{\\extrarowheight}{7pt}\\begin{tabular}{|>{\\centering}p{19cm}|}\\hline\\large \\bf{"+ chapterName +"}\\tabularnewline\\hline\\end{tabular}\\newline" + mainComplete;
        mainComplete = mainComplete.replace("\\rm", "\\text");
        mainString += mainComplete;
        if(correctAnswerSheet) {
            mainString += "\\newpage" + getAnswerSheet(selectedQuestionsList, pdfPageSetupBean,chapterName);
        }
//        System.out.println("MainComplete Hint:"+mainCompleteHint);
        if(solutionSheet && mainCompleteHint.trim() != "") {
            if(solutionSheetTwoColumn)
                mainCompleteHint = "\\vspace*{-0.1in}\\begin{multicols*}{2}" + mainCompleteHint + "\\end{multicols*}";
//            mainCompleteHint = mainCompleteHint.replaceAll("\\\\ ", "\\\\[2.5pt] ");
            mainCompleteHint=mainCompleteHint.replace("\\rm", "\\text");
            mainString += "\\newpage"+getHintHeaderString(selectedQuestionsList,pdfPageSetupBean,chapterName) + "\\\\"+mainCompleteHint;
        }
//        teacherString = getTeacherCopyHeaderString(selectedQuestionsList, chapterName, "Scholars Katta") + "\\\\" + teacherString;

//        mainString = mainString.replace("â€™", "'");
//        teacherString = teacherString.replace("â€™", "'");
        
        mainString = replaceString(mainString);
        teacherString = replaceString(teacherString);
        
        returnBean = new PdfMainStringBean();
        returnBean.setMainString(mainString);
        returnBean.setTeacherMainString(teacherString);
        
        return returnBean;
    }
    
//   public PdfMainStringBean getMainString(ArrayList<QuestionBean> selectedQuestionsList,ArrayList<SubjectBean> selectedSubjectList,ArrayList<ChapterBean> chapterList,int totalMarks,String printingPaperType
//        ,PdfPageSetupBean pdfPageSetupBean,boolean correctAnswerSheet,boolean solutionSheet,boolean solutionSheetTwoColumn) {
//        PdfMainStringBean returnBean = null;
//        String mainString = "";
//        String teacherString = "";
//        this.printingPaperType = printingPaperType;
//        this.totalMarks = totalMarks;
//        String question,optionA,optionB,optionC,optionD,queYear,hint,queImagePath,optionImagePath,hintImagePath;
//        boolean isPreviousQuestionPaper = false;
//        String mainComplete = "",mainCompleteHint = "";
//        NewQuesLatexProcessing newQuesProcess = null;
//        NewHintLatexProcessing newHintProcess = null;
//        subjectString = "";
//        for(SubjectBean subjectBean : selectedSubjectList) {
//            subjectString += subjectBean.getSubjectName() +",";
//        }
//        
//        ArrayList<OptionImageDimensionsBean> optionImageDimentonList = new OptionImageDimentionOperation().getOptionImageDimaentionList(selectedQuestionsList, isPreviousQuestionPaper);
//        viewImgDimeList = new ImageRatioOperation().getImageRatioList(selectedQuestionsList);
//        int questionIndex = 0;
//          
//        for(QuestionBean questionsBean : selectedQuestionsList) {
//            question = questionsBean.getQuestion();
//            optionA = questionsBean.getOptionA();
//            optionB = questionsBean.getOptionB();
//            optionC = questionsBean.getOptionC();
//            optionD = questionsBean.getOptionD();
//            hint = questionsBean.getHint();
//            
//            int size = pdfPageSetupBean.getFontSize();
//            double baseline = size*1.2;
//            
//            if(newQuesProcess == null)
//                newQuesProcess = new NewQuesLatexProcessing();
//            if(newHintProcess == null)
//                newHintProcess = new NewHintLatexProcessing();
//                
//            question = newQuesProcess.mboxProcessing(question,true);
//            if(!(optionA.equals("\\mbox{}")))
//                optionA = newQuesProcess.mboxProcessing(optionA,false);
//            if(!(optionB.equals("\\mbox{}")))
//                optionB = newQuesProcess.mboxProcessing(optionB,false);
//            if(!(optionC.equals("\\mbox{}")))
//                optionC = newQuesProcess.mboxProcessing(optionC,false);
//            if(!(optionD.equals("\\mbox{}")))
//                optionD = newQuesProcess.mboxProcessing(optionD,false);
//            if(!(hint.equals("\\mbox{}"))) {
////                hint = hint.replace("$ \\therefore $", "\\\\ $ \\therefore $");
//                hint = newHintProcess.mboxProcessing(hint);
//            }
//
//            if(pdfPageSetupBean.isQuestionBold())
//                question = "{\\bf "+ question +"}";
//
//            if(pdfPageSetupBean.isOptionBold()) {
//                if(!(optionA.equals("\\mbox{}")))
//                    optionA = "{\\bf "+ optionA +"}";
//                if(!(optionB.equals("\\mbox{}")))
//                    optionB = "{\\bf "+ optionB +"}";
//                if(!(optionC.equals("\\mbox{}")))
//                    optionC = "{\\bf "+ optionC +"}";
//                if(!(optionD.equals("\\mbox{}")))
//                    optionD = "{\\bf "+ optionD +"}";
//            }   
//            
//            queYear = questionsBean.getYear();
//            queYear = queYear.replace("&", "\\&");
//            if (queYear != null || queYear != "") 
//                queYear = queYear.trim();
//            else
//                queYear = "";
//            if (!pdfPageSetupBean.isTwoColumn()) {
//                if (pdfPageSetupBean.isPrintYear() && queYear != null && !queYear.equals("")) {
//                    mainComplete += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.94\\textwidth}}\\textbf{" + (questionIndex  + pdfPageSetupBean.getQuestionStartNo()) + ") } & " + question + "    \\textbf{" +  queYear  + "}" + " \\end{tabular}}";
//                } else {
//                    mainComplete += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.94\\textwidth}}\\textbf{" + (questionIndex  + pdfPageSetupBean.getQuestionStartNo()) + ") } & " + question + "   \\end{tabular}}";
//                }
//            } else {
//                if (pdfPageSetupBean.isPrintYear() && queYear != null && !queYear.equals("")) {
//                    mainComplete += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.44\\textwidth}}\\textbf{" + (questionIndex  + pdfPageSetupBean.getQuestionStartNo()) + ") } & " + question + "    \\textbf{" + queYear + "}" + " \\end{tabular}}";
//                } else {
//                    mainComplete += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.44\\textwidth}}\\textbf{" + (questionIndex  + pdfPageSetupBean.getQuestionStartNo()) + ") } & " + question + "   \\end{tabular}}";
//                }
//            }
//            
//            teacherString += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.44\\textwidth}}\\textbf{" + (questionIndex  + pdfPageSetupBean.getQuestionStartNo()) + ") } & " + question + " \\textbf{" + queYear + "}" + " \\end{tabular}}";
//            
//            if(questionsBean.isIsQuestionAsImage()) {
//                try {
//                    queImagePath = questionsBean.getQuestionImagePath();
//                    queImagePath = queImagePath.replaceAll("_", "-");
//                    float scale;
//                    int actualWidth;
//                    String latexCode = "";
//                    scale = getImageDim(queImagePath);
//                    scale = precision(2, scale);
//                    scale = (float) (scale-0.2);
//                    BufferedImage bufferImg = ImageIO.read(new File(queImagePath));
//                    int width = bufferImg.getWidth();
//                    actualWidth = (int) (width * scale);
//                    
//                    if(actualWidth<390)
//                        latexCode="[scale="+scale+"]";
//                    else 
//                        latexCode="[width=0.48\\textwidth]";
//                    teacherString += "\\\\{\\centering\\includegraphics"+latexCode+"{" + queImagePath + "}\\\\}";
//                    mainComplete += "\\\\{\\centering\\includegraphics"+latexCode+"{" + queImagePath + "}\\\\}";
//                    
//                    
//                } catch (IOException ex) {
//                    ex.printStackTrace();
//                }
//            } else {
//                queImagePath = "";
//            }
//            
//            if(questionsBean.isIsOptionAsImage()) {
//                try {
//                    optionImagePath = questionsBean.getOptionImagePath();
//                    float scale;
//                    int actualWidth;
//                    String latexCode = "";
//                    scale = getImageDim(optionImagePath);
//                    scale = precision(2, scale);
//                    scale = (float) (scale-0.2);
//                    BufferedImage bimg = ImageIO.read(new File(optionImagePath));
//                    int width = bimg.getWidth();
//                    actualWidth = (int) (width * scale);
//                    
//                    if(actualWidth<390)
//                        latexCode="[scale="+scale+"]";
//                    else
//                        latexCode="[width=0.48\\textwidth]";
//                    teacherString += "\\\\{\\centering\\includegraphics"+latexCode+"{" + optionImagePath + "}\\\\}";
//                    mainComplete += "\\\\{\\centering\\includegraphics"+latexCode+"{" + optionImagePath + "}\\\\}";
//                } catch (IOException ex) {
//                    ex.printStackTrace();
//                }
//            } else {
//                optionImagePath = "";
//                OptionImageDimensionsBean optImgDimBean = new OptionImageDimensionsBean();
//                for(OptionImageDimensionsBean optBean : optionImageDimentonList){
//                    if(optBean.getQuestionId() == questionsBean.getQuestionId()){
//                        optImgDimBean = optBean;
//                        break;
//                    }
//                }
//                int optionType;
//                if (!pdfPageSetupBean.isTwoColumn()) {
//                    optionType = 0;
//                    if (optImgDimBean.getOptionA() < 90 && optImgDimBean.getOptionB() < 90 && optImgDimBean.getOptionC() < 90 && optImgDimBean.getOptionD() < 90)
//                        optionType = 1;
//                    else if (optImgDimBean.getOptionA() < 210 && optImgDimBean.getOptionB() < 210 && optImgDimBean.getOptionC() < 210 && optImgDimBean.getOptionD() < 210)
//                        optionType = 2;
//                    
//                    switch (optionType) {
//                        case 0:
//                            mainComplete += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{1.4}   \\hspace*{0.04\\textwidth}\\begin{tabular}[H]{>{\\raggedright}p{0.01\\textwidth} >{\\raggedright}p{0.95\\textwidth}}{\\textbf{A})  } & " + optionA + " \\tabularnewline {\\textbf{B})  } &" + optionB + " \\tabularnewline {\\textbf{C})  } & " + optionC + "\\tabularnewline {\\textbf{D})  }&" + optionD + " \\end{tabular}}}\\\\";
//                            break;
//                        case 1:
//                            mainComplete += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{1.4}  \\hspace*{0.035\\textwidth}  \\begin{tabular}[H] {>{\\raggedright}p{0.23\\textwidth} >{\\raggedright}p{0.22\\textwidth} >{\\raggedright}p{0.22\\textwidth} >{\\raggedright}p{0.22\\textwidth}}{\\textbf{A})  }" + optionA + " & {\\textbf{B})  }" + optionB + " & {\\textbf{C})  }" + optionC + "& {\\textbf{D})  }" + optionD + " \\end{tabular}}}\\\\";
//                            break;
//                        case 2:
//                            mainComplete += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{1.4}  \\hspace*{0.035\\textwidth}  \\begin{tabular}[H]  {>{\\raggedright}p{0.45\\textwidth} >{\\raggedright}p{0.45\\textwidth}}{\\textbf{A})  }"+ optionA + " & {\\textbf{B})  }" + optionB + " \\tabularnewline {\\textbf{C})  }" + optionC + "& {\\textbf{D})  }" + optionD + " \\end{tabular}}}\\\\";
//                            break;
//                    }
//                    
//                    optionType = 0;
//                    if(optImgDimBean.getOptionA() < 35 && optImgDimBean.getOptionB() < 35 && optImgDimBean.getOptionC() < 35 && optImgDimBean.getOptionD() < 35) 
//                        optionType = 1;
//                    else if (optImgDimBean.getOptionA() < 85 && optImgDimBean.getOptionB() < 85 && optImgDimBean.getOptionC() < 85 && optImgDimBean.getOptionD() < 85)
//                        optionType = 2;
//
//                    switch (optionType) {
//                        case 0:
//                            teacherString += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{1.4}   \\hspace*{0.035\\textwidth}\\begin{tabular}[H]{>{\\raggedright}p{0.01\\textwidth} >{\\raggedright}p{0.41\\textwidth}}{\\textbf{A})  } & " + optionA + " \\tabularnewline {\\textbf{B})  } &" + optionB + " \\tabularnewline {\\textbf{C})  } & " + optionC + "\\tabularnewline {\\textbf{D})  }&" + optionD + " \\end{tabular}}}\\\\";
//                            break;
//                        case 1:
//                            teacherString += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{1.4}  \\hspace*{0.03\\textwidth}  \\begin{tabular}[H] {>{\\raggedright}p{0.102\\textwidth} >{\\raggedright}p{0.100\\textwidth} >{\\raggedright}p{0.100\\textwidth} >{\\raggedright}p{0.100\\textwidth}}{\\textbf{A})  }" + optionA + " & {\\textbf{B})  }" + optionB + " & {\\textbf{C})  }" + optionC + "& {\\textbf{D})  }" + optionD + "  \\end{tabular}}}\\\\";
//                            break;
//                        case 2:
//                            teacherString += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{1.4}  \\hspace*{0.03\\textwidth}  \\begin{tabular}[H]  {>{\\raggedright}p{0.20\\textwidth} >{\\raggedright}p{0.20\\textwidth}}{\\textbf{A})  }" + optionA + " & {\\textbf{B})  }" + optionB + " \\tabularnewline {\\textbf{C})  }" + optionC + "& {\\textbf{D})  }" + optionD + " \\end{tabular}}}\\\\";
//                            break;
//                    }
//                    
//                } else {
//                    optionType = 0;
//                    if(optImgDimBean.getOptionA() < 35 && optImgDimBean.getOptionB() < 35 && optImgDimBean.getOptionC() < 35 && optImgDimBean.getOptionD() < 35) 
//                        optionType = 1;
//                    else if (optImgDimBean.getOptionA() < 85 && optImgDimBean.getOptionB() < 85 && optImgDimBean.getOptionC() < 85 && optImgDimBean.getOptionD() < 85)
//                        optionType = 2;
//                    
//                    switch (optionType) {
//                        case 0:
//                            mainComplete += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{1.4}   \\hspace*{0.035\\textwidth}\\begin{tabular}[H]{>{\\raggedright}p{0.01\\textwidth} >{\\raggedright}p{0.41\\textwidth}}{\\textbf{A})  } & " + optionA + " \\tabularnewline {\\textbf{B})  } &" + optionB + " \\tabularnewline {\\textbf{C})  } & " + optionC + "\\tabularnewline {\\textbf{D})  }&" + optionD + " \\end{tabular}}}\\\\";
//                            teacherString += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{1.4}   \\hspace*{0.035\\textwidth}\\begin{tabular}[H]{>{\\raggedright}p{0.01\\textwidth} >{\\raggedright}p{0.41\\textwidth}}{\\textbf{A})  } & " + optionA + " \\tabularnewline {\\textbf{B})  } &" + optionB + " \\tabularnewline {\\textbf{C})  } & " + optionC + "\\tabularnewline {\\textbf{D})  }&" + optionD + " \\end{tabular}}}\\\\";
//                            break;
//                        case 1:
//                            mainComplete += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{1.4}  \\hspace*{0.03\\textwidth}  \\begin{tabular}[H] {>{\\raggedright}p{0.102\\textwidth} >{\\raggedright}p{0.100\\textwidth} >{\\raggedright}p{0.100\\textwidth} >{\\raggedright}p{0.100\\textwidth}}{\\textbf{A})  }" + optionA + " & {\\textbf{B})  }" + optionB + " & {\\textbf{C})  }" + optionC + "& {\\textbf{D})  }" + optionD + "  \\end{tabular}}}\\\\";
//                            teacherString += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{1.4}  \\hspace*{0.03\\textwidth}  \\begin{tabular}[H] {>{\\raggedright}p{0.102\\textwidth} >{\\raggedright}p{0.100\\textwidth} >{\\raggedright}p{0.100\\textwidth} >{\\raggedright}p{0.100\\textwidth}}{\\textbf{A})  }" + optionA + " & {\\textbf{B})  }" + optionB + " & {\\textbf{C})  }" + optionC + "& {\\textbf{D})  }" + optionD + "  \\end{tabular}}}\\\\";
//                            break;
//                        case 2:
//                            mainComplete += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{1.4}  \\hspace*{0.03\\textwidth}  \\begin{tabular}[H]  {>{\\raggedright}p{0.20\\textwidth} >{\\raggedright}p{0.20\\textwidth}}{\\textbf{A})  }" + optionA + " & {\\textbf{B})  }" + optionB + " \\tabularnewline {\\textbf{C})  }" + optionC + "& {\\textbf{D})  }" + optionD + " \\end{tabular}}}\\\\";
//                            teacherString += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{1.4}  \\hspace*{0.03\\textwidth}  \\begin{tabular}[H]  {>{\\raggedright}p{0.20\\textwidth} >{\\raggedright}p{0.20\\textwidth}}{\\textbf{A})  }" + optionA + " & {\\textbf{B})  }" + optionB + " \\tabularnewline {\\textbf{C})  }" + optionC + "& {\\textbf{D})  }" + optionD + " \\end{tabular}}}\\\\";
//                            break;
//                    }
//                }
//            }
//            teacherString += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{1.2}   \\hspace*{0.035\\textwidth}\\begin{tabular}[H]{>{\\raggedright}p{0.44\\textwidth} }{\\textbf{Answer : "+questionsBean.getAnswer().trim()+"} } \\end{tabular}}}\\newline";
//            
//            if((!hint.trim().equals("\\mbox{}") && !(hint.trim().equals(""))) || questionsBean.isIsHintAsImage()) {
//                if(pdfPageSetupBean.isSolutionBold())
//                    hint = "{\\bf "+ hint +"}";
//                if(solutionSheetTwoColumn)
//                    mainCompleteHint += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.44\\textwidth}}\\textbf{"+ (questionIndex  + pdfPageSetupBean.getQuestionStartNo()) +")} & " + hint + "\\end{tabular}}";
//                else
//                    mainCompleteHint += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.94\\textwidth}}\\textbf{"+ (questionIndex  + pdfPageSetupBean.getQuestionStartNo()) +")} & " + hint + "\\end{tabular}}";
////                mainComplete += "\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.94\\textwidth}}\\textbf{" + (questionIndex  + pdfPageSetupBean.getQuestionStartNo()) + ") } & " + question + "   \\end{tabular}\\newline";
////                mainCompleteHint += "\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.44\\textwidth}}\\textbf{"+ (questionIndex  + pdfPageSetupBean.getQuestionStartNo()) +")} & " + hint + "\\end{tabular}\\\\newline";
//                teacherString += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont{\\renewcommand{\\arraystretch}{1.1}\\hspace*{0.035\\textwidth}\\begin{tabular}[H]{>{\\raggedright}p{0.44\\textwidth}}{\\textbf{Solution :}}\\tabularnewline"+ hint +"\\end{tabular}}}\\newline";
//            }
//            
//            if(questionsBean.isIsHintAsImage()) {
//                try {    
//                    hintImagePath = questionsBean.getHintImagePath();
//                    hintImagePath = hintImagePath.replace("\\", "/");
//                    float scale;
//                    int actualwidth;
//                    String latexCode = "";
//                    scale = getImageDim(hintImagePath);
//                    scale = precision(2, scale);                
//                    scale = (float) (scale-0.2);
//                    BufferedImage bimg = ImageIO.read(new File(hintImagePath));
//                    int width = bimg.getWidth();
//                    actualwidth = (int) (width * scale);
//                    
//                    if(actualwidth<390)
//                        latexCode="[scale="+scale+"]";
//                    else
//                        latexCode="[width=0.48\\textwidth]";
//                    mainCompleteHint += "\\\\{\\centering\\includegraphics" + latexCode + "{" + hintImagePath + "}\\\\}";
//                    teacherString += "\\\\{\\centering\\includegraphics" + latexCode + "{" + hintImagePath + "}\\\\}";
//                    
//                } catch (IOException ex) {
//                    ex.printStackTrace();
//                }
//            } else {
//                hintImagePath = "";
//            }
//            if((!hint.trim().equals("\\mbox{}") && !(hint.trim().equals(""))) || questionsBean.isIsHintAsImage()) 
////                mainCompleteHint += " \\\\[2.5pt] ";
//                mainCompleteHint += " \\\\";
//            
//            mainComplete += "\\sagar";
//            questionIndex ++;
//        }
//        
//        teacherString = "\\begin{multicols*}{2}" + teacherString + "\\end{multicols*}";
//        teacherString = teacherString.replace("\\rm", "\\text");
//        
//        if(pdfPageSetupBean.isTwoColumn()) {
//            mainComplete = "\\vspace*{-0.2in}\\begin{multicols*}{2}" + mainComplete + "\\end{multicols*}";
//        }
//        String chapterName = chapterList.get(0).getChapterName().trim();
//        mainComplete = "\\setlength{\\extrarowheight}{7pt}\\begin{tabular}{|>{\\centering}p{19cm}|}\\hline\\large \\bf{"+ chapterName +"}\\tabularnewline\\hline\\end{tabular}\\newline" + mainComplete;
//        mainComplete = mainComplete.replace("\\rm", "\\text");
//        mainString += mainComplete;
//        if(correctAnswerSheet) {
//            mainString += "\\newpage" + getAnswerSheet(selectedQuestionsList, pdfPageSetupBean,chapterName);
//        }
////        System.out.println("MainComplete Hint:"+mainCompleteHint);
//        if(solutionSheet && mainCompleteHint.trim() != "") {
//            if(solutionSheetTwoColumn)
//                mainCompleteHint = "\\vspace*{-0.1in}\\begin{multicols*}{2}" + mainCompleteHint + "\\end{multicols*}";
////            mainCompleteHint = mainCompleteHint.replaceAll("\\\\ ", "\\\\[2.5pt] ");
//            mainCompleteHint=mainCompleteHint.replace("\\rm", "\\text");
//            mainString += "\\newpage"+getHintHeaderString(selectedQuestionsList,pdfPageSetupBean,chapterName) + "\\\\"+mainCompleteHint;
//        }
////        teacherString = getTeacherCopyHeaderString(selectedQuestionsList, chapterName, "Scholars Katta") + "\\\\" + teacherString;
//
////        mainString = mainString.replace("â€™", "'");
////        teacherString = teacherString.replace("â€™", "'");
//        
//        mainString = replaceString(mainString);
//        teacherString = replaceString(teacherString);
//        
//        returnBean = new PdfMainStringBean();
//        returnBean.setMainString(mainString);
//        returnBean.setTeacherMainString(teacherString);
//        
//        return returnBean;
//    }
    
     public PdfMainStringBean getMainString(ArrayList<QuestionBean> selectedQuestionsList,ArrayList<SubjectBean> selectedSubjectList,ArrayList<ChapterBean> chapterList,int totalMarks,String printingPaperType
        ,PdfPageSetupBean pdfPageSetupBean,boolean correctAnswerSheet,boolean solutionSheet,boolean solutionSheetTwoColumn) {
        PdfMainStringBean returnBean = null;
        String mainString = "";
        String teacherString = "";
        this.printingPaperType = printingPaperType;
        this.totalMarks = totalMarks;
        String question,optionA,optionB,optionC,optionD,queYear,hint,queImagePath,optionImagePath,hintImagePath;
        boolean isPreviousQuestionPaper = false;
        String mainComplete = "",mainCompleteHint = "";
        NewQuesLatexProcessing newQuesProcess = null;
        NewHintLatexProcessing newHintProcess = null;
        OldQuesLatexProcessing oldQuesProcess = null;
        OldHintLatexProcessing oldHintProcess = null;
        subjectString = "";
        for(SubjectBean subjectBean : selectedSubjectList) {
            subjectString += subjectBean.getSubjectName() +",";
        }
        
        ArrayList<OptionImageDimensionsBean> optionImageDimentonList = new OptionImageDimentionOperation().getOptionImageDimaentionList(selectedQuestionsList, isPreviousQuestionPaper);
        viewImgDimeList = new ImageRatioOperation().getImageRatioList(selectedQuestionsList);
        ArrayList<Integer> newProccessingQuestionsList = new NewAndOldQuestionOperation().getNewQuestionsLists(selectedQuestionsList, isPreviousQuestionPaper);
        int questionIndex = 0;
          
        for(QuestionBean questionsBean : selectedQuestionsList) {
            question = questionsBean.getQuestion();
            optionA = questionsBean.getOptionA();
            optionB = questionsBean.getOptionB();
            optionC = questionsBean.getOptionC();
            optionD = questionsBean.getOptionD();
            hint = questionsBean.getHint();
            
            if(newProccessingQuestionsList.contains(questionsBean.getQuestionId())) {
                if(newQuesProcess == null)
                    newQuesProcess = new NewQuesLatexProcessing();
                if(newHintProcess == null)
                    newHintProcess = new NewHintLatexProcessing();

                question = newQuesProcess.mboxProcessing(question,true);
                if(!(optionA.equals("\\mbox{}")))
                    optionA = newQuesProcess.mboxProcessing(optionA,false);
                if(!(optionB.equals("\\mbox{}")))
                    optionB = newQuesProcess.mboxProcessing(optionB,false);
                if(!(optionC.equals("\\mbox{}")))
                    optionC = newQuesProcess.mboxProcessing(optionC,false);
                if(!(optionD.equals("\\mbox{}")))
                    optionD = newQuesProcess.mboxProcessing(optionD,false);
                if(!(hint.equals("\\mbox{}"))) 
                    hint = newHintProcess.mboxProcessing(hint);
            } else {
                if(oldQuesProcess == null)
                    oldQuesProcess = new OldQuesLatexProcessing();
                if(oldHintProcess == null)
                    oldHintProcess = new OldHintLatexProcessing();
                
                question = oldQuesProcess.mboxProcessing(question,true);
                if(!(optionA.equals("\\mbox{}")))
                    optionA = oldQuesProcess.mboxProcessing(optionA,false);
                if(!(optionB.equals("\\mbox{}")))
                    optionB = oldQuesProcess.mboxProcessing(optionB,false);
                if(!(optionC.equals("\\mbox{}")))
                    optionC = oldQuesProcess.mboxProcessing(optionC,false);
                if(!(optionD.equals("\\mbox{}")))
                    optionD = oldQuesProcess.mboxProcessing(optionD,false);
                if(!(hint.equals("\\mbox{}")))
                    hint = oldHintProcess.mboxProcessing(hint);
            }

            if(pdfPageSetupBean.isQuestionBold())
                question = "{\\bf "+ question +"}";

            if(pdfPageSetupBean.isOptionBold()) {
                if(!(optionA.equals("\\mbox{}")))
                    optionA = "{\\bf "+ optionA +"}";
                if(!(optionB.equals("\\mbox{}")))
                    optionB = "{\\bf "+ optionB +"}";
                if(!(optionC.equals("\\mbox{}")))
                    optionC = "{\\bf "+ optionC +"}";
                if(!(optionD.equals("\\mbox{}")))
                    optionD = "{\\bf "+ optionD +"}";
            }   
            
            queYear = questionsBean.getYear();
            queYear = queYear.replace("&", "\\&");
            if (queYear != null || queYear != "") 
                queYear = queYear.trim();
            else
                queYear = "";
            if (!pdfPageSetupBean.isTwoColumn()) {
                if (pdfPageSetupBean.isPrintYear() && queYear != null && !queYear.equals("")) {
                    mainComplete += "\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.94\\textwidth}}\\textbf{" + (questionIndex  + pdfPageSetupBean.getQuestionStartNo()) + ") } & " + question + "    \\textbf{" +  queYear  + "}" + " \\end{tabular}";
                } else {
                    mainComplete += "\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.94\\textwidth}}\\textbf{" + (questionIndex  + pdfPageSetupBean.getQuestionStartNo()) + ") } & " + question + "   \\end{tabular}";
                }
            } else {
                if (pdfPageSetupBean.isPrintYear() && queYear != null && !queYear.equals("")) {
                    mainComplete += "\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.44\\textwidth}}\\textbf{" + (questionIndex  + pdfPageSetupBean.getQuestionStartNo()) + ") } & " + question + "    \\textbf{" + queYear + "}" + " \\end{tabular}";
                } else {
                    mainComplete += "\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.44\\textwidth}}\\textbf{" + (questionIndex  + pdfPageSetupBean.getQuestionStartNo()) + ") } & " + question + "   \\end{tabular}";
                }
            }
            
            teacherString += "\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.44\\textwidth}}\\textbf{" + (questionIndex  + pdfPageSetupBean.getQuestionStartNo()) + ") } & " + question + " \\textbf{" + queYear + "}" + " \\end{tabular}";
            
            if(questionsBean.isIsQuestionAsImage()) {
                try {
                    queImagePath = questionsBean.getQuestionImagePath();
                    queImagePath = queImagePath.replaceAll("_", "-");
                    float scale;
                    int actualWidth;
                    String latexCode = "";
                    scale = getImageDim(queImagePath);
                    scale = precision(2, scale);
                    scale = (float) (scale-0.2);
                    BufferedImage bufferImg = ImageIO.read(new File(queImagePath));
                    int width = bufferImg.getWidth();
                    actualWidth = (int) (width * scale);
                    if(actualWidth<390)
                        latexCode="[scale="+scale+"]";
                    else 
                        latexCode="[width=0.48\\textwidth]";
                    teacherString += "\\\\{\\centering\\includegraphics"+latexCode+"{" + queImagePath + "}\\\\}";
                    mainComplete += "\\\\{\\centering\\includegraphics"+latexCode+"{" + queImagePath + "}\\\\}";
                    
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            } else {
                queImagePath = "";
            }
            
            if(questionsBean.isIsOptionAsImage()) {
                try {
                    optionImagePath = questionsBean.getOptionImagePath();
                    float scale;
                    int actualWidth;
                    String latexCode = "";
                    scale = getImageDim(optionImagePath);
                    scale = precision(2, scale);
                    scale = (float) (scale-0.2);
                    BufferedImage bimg = ImageIO.read(new File(optionImagePath));
                    int width = bimg.getWidth();
                    actualWidth = (int) (width * scale);
                    if(actualWidth<390)
                        latexCode="[scale="+scale+"]";
                    else
                        latexCode="[width=0.48\\textwidth]";
                    teacherString += "\\\\{\\centering\\includegraphics"+latexCode+"{" + optionImagePath + "}\\\\}";
                    mainComplete += "\\\\{\\centering\\includegraphics"+latexCode+"{" + optionImagePath + "}\\\\}";
                    
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            } else {
                optionImagePath = "";
                OptionImageDimensionsBean optImgDimBean = new OptionImageDimensionsBean();
                for(OptionImageDimensionsBean optBean : optionImageDimentonList){
                    if(optBean.getQuestionId() == questionsBean.getQuestionId()){
                        optImgDimBean = optBean;
                        break;
                    }
                }
                int optionType;
                if (!pdfPageSetupBean.isTwoColumn()) {
                    optionType = 0;
                    if (optImgDimBean.getOptionA() < 90 && optImgDimBean.getOptionB() < 90 && optImgDimBean.getOptionC() < 90 && optImgDimBean.getOptionD() < 90)
                        optionType = 1;
                    else if (optImgDimBean.getOptionA() < 210 && optImgDimBean.getOptionB() < 210 && optImgDimBean.getOptionC() < 210 && optImgDimBean.getOptionD() < 210)
                        optionType = 2;
                    
                    switch (optionType) {
                        case 0:
                            mainComplete += "{\\renewcommand{\\arraystretch}{1.4}   \\hspace*{0.04\\textwidth}\\begin{tabular}[H]{>{\\raggedright}p{0.01\\textwidth} >{\\raggedright}p{0.95\\textwidth}}{\\textbf{A})  } & " + optionA + " \\tabularnewline {\\textbf{B})  } &" + optionB + " \\tabularnewline {\\textbf{C})  } & " + optionC + "\\tabularnewline {\\textbf{D})  }&" + optionD + " \\end{tabular}}\\\\";
                            break;
                        case 1:
                            mainComplete += "{\\renewcommand{\\arraystretch}{1.4}  \\hspace*{0.035\\textwidth}  \\begin{tabular}[H] {>{\\raggedright}p{0.23\\textwidth} >{\\raggedright}p{0.22\\textwidth} >{\\raggedright}p{0.22\\textwidth} >{\\raggedright}p{0.22\\textwidth}}{\\textbf{A})  }" + optionA + " & {\\textbf{B})  }" + optionB + " & {\\textbf{C})  }" + optionC + "& {\\textbf{D})  }" + optionD + " \\end{tabular}}\\\\";
                            break;
                        case 2:
                            mainComplete += "{\\renewcommand{\\arraystretch}{1.4}  \\hspace*{0.035\\textwidth}  \\begin{tabular}[H]  {>{\\raggedright}p{0.45\\textwidth} >{\\raggedright}p{0.45\\textwidth}}{\\textbf{A})  }"+ optionA + " & {\\textbf{B})  }" + optionB + " \\tabularnewline {\\textbf{C})  }" + optionC + "& {\\textbf{D})  }" + optionD + " \\end{tabular}}\\\\";
                            break;
                    }
                    
                    optionType = 0;
                    if(optImgDimBean.getOptionA() < 35 && optImgDimBean.getOptionB() < 35 && optImgDimBean.getOptionC() < 35 && optImgDimBean.getOptionD() < 35) 
                        optionType = 1;
                    else if (optImgDimBean.getOptionA() < 85 && optImgDimBean.getOptionB() < 85 && optImgDimBean.getOptionC() < 85 && optImgDimBean.getOptionD() < 85)
                        optionType = 2;

                    switch (optionType) {
                        case 0:
                            teacherString += "{\\renewcommand{\\arraystretch}{1.4}   \\hspace*{0.035\\textwidth}\\begin{tabular}[H]{>{\\raggedright}p{0.01\\textwidth} >{\\raggedright}p{0.41\\textwidth}}{\\textbf{A})  } & " + optionA + " \\tabularnewline {\\textbf{B})  } &" + optionB + " \\tabularnewline {\\textbf{C})  } & " + optionC + "\\tabularnewline {\\textbf{D})  }&" + optionD + " \\end{tabular}}\\\\";
                            break;
                        case 1:
                            teacherString += "{\\renewcommand{\\arraystretch}{1.4}  \\hspace*{0.03\\textwidth}  \\begin{tabular}[H] {>{\\raggedright}p{0.102\\textwidth} >{\\raggedright}p{0.100\\textwidth} >{\\raggedright}p{0.100\\textwidth} >{\\raggedright}p{0.100\\textwidth}}{\\textbf{A})  }" + optionA + " & {\\textbf{B})  }" + optionB + " & {\\textbf{C})  }" + optionC + "& {\\textbf{D})  }" + optionD + "  \\end{tabular}}\\\\";
                            break;
                        case 2:
                            teacherString += "{\\renewcommand{\\arraystretch}{1.4}  \\hspace*{0.03\\textwidth}  \\begin{tabular}[H]  {>{\\raggedright}p{0.20\\textwidth} >{\\raggedright}p{0.20\\textwidth}}{\\textbf{A})  }" + optionA + " & {\\textbf{B})  }" + optionB + " \\tabularnewline {\\textbf{C})  }" + optionC + "& {\\textbf{D})  }" + optionD + " \\end{tabular}}\\\\";
                            break;
                    }
                    
                } else {
                    optionType = 0;
                    if(optImgDimBean.getOptionA() < 35 && optImgDimBean.getOptionB() < 35 && optImgDimBean.getOptionC() < 35 && optImgDimBean.getOptionD() < 35) 
                        optionType = 1;
                    else if (optImgDimBean.getOptionA() < 85 && optImgDimBean.getOptionB() < 85 && optImgDimBean.getOptionC() < 85 && optImgDimBean.getOptionD() < 85)
                        optionType = 2;
                    
                    switch (optionType) {
                        case 0:
                            mainComplete += "{\\renewcommand{\\arraystretch}{1.4}   \\hspace*{0.035\\textwidth}\\begin{tabular}[H]{>{\\raggedright}p{0.01\\textwidth} >{\\raggedright}p{0.41\\textwidth}}{\\textbf{A})  } & " + optionA + " \\tabularnewline {\\textbf{B})  } &" + optionB + " \\tabularnewline {\\textbf{C})  } & " + optionC + "\\tabularnewline {\\textbf{D})  }&" + optionD + " \\end{tabular}}\\\\";
                            teacherString += "{\\renewcommand{\\arraystretch}{1.4}   \\hspace*{0.035\\textwidth}\\begin{tabular}[H]{>{\\raggedright}p{0.01\\textwidth} >{\\raggedright}p{0.41\\textwidth}}{\\textbf{A})  } & " + optionA + " \\tabularnewline {\\textbf{B})  } &" + optionB + " \\tabularnewline {\\textbf{C})  } & " + optionC + "\\tabularnewline {\\textbf{D})  }&" + optionD + " \\end{tabular}}\\\\";
                            break;
                        case 1:
                            mainComplete += "{\\renewcommand{\\arraystretch}{1.4}  \\hspace*{0.03\\textwidth}  \\begin{tabular}[H] {>{\\raggedright}p{0.102\\textwidth} >{\\raggedright}p{0.100\\textwidth} >{\\raggedright}p{0.100\\textwidth} >{\\raggedright}p{0.100\\textwidth}}{\\textbf{A})  }" + optionA + " & {\\textbf{B})  }" + optionB + " & {\\textbf{C})  }" + optionC + "& {\\textbf{D})  }" + optionD + "  \\end{tabular}}\\\\";
                            teacherString += "{\\renewcommand{\\arraystretch}{1.4}  \\hspace*{0.03\\textwidth}  \\begin{tabular}[H] {>{\\raggedright}p{0.102\\textwidth} >{\\raggedright}p{0.100\\textwidth} >{\\raggedright}p{0.100\\textwidth} >{\\raggedright}p{0.100\\textwidth}}{\\textbf{A})  }" + optionA + " & {\\textbf{B})  }" + optionB + " & {\\textbf{C})  }" + optionC + "& {\\textbf{D})  }" + optionD + "  \\end{tabular}}\\\\";
                            break;
                        case 2:
                            mainComplete += "{\\renewcommand{\\arraystretch}{1.4}  \\hspace*{0.03\\textwidth}  \\begin{tabular}[H]  {>{\\raggedright}p{0.20\\textwidth} >{\\raggedright}p{0.20\\textwidth}}{\\textbf{A})  }" + optionA + " & {\\textbf{B})  }" + optionB + " \\tabularnewline {\\textbf{C})  }" + optionC + "& {\\textbf{D})  }" + optionD + " \\end{tabular}}\\\\";
                            teacherString += "{\\renewcommand{\\arraystretch}{1.4}  \\hspace*{0.03\\textwidth}  \\begin{tabular}[H]  {>{\\raggedright}p{0.20\\textwidth} >{\\raggedright}p{0.20\\textwidth}}{\\textbf{A})  }" + optionA + " & {\\textbf{B})  }" + optionB + " \\tabularnewline {\\textbf{C})  }" + optionC + "& {\\textbf{D})  }" + optionD + " \\end{tabular}}\\\\";
                            break;
                    }
                }
            }
            teacherString += "{\\renewcommand{\\arraystretch}{1.2}   \\hspace*{0.035\\textwidth}\\begin{tabular}[H]{>{\\raggedright}p{0.44\\textwidth} }{\\textbf{Answer : "+questionsBean.getAnswer().trim()+"} } \\end{tabular}}\\newline";
            
            if((!hint.trim().equals("\\mbox{}") && !(hint.trim().equals(""))) || questionsBean.isIsHintAsImage()) {
                if(pdfPageSetupBean.isSolutionBold())
                    hint = "{\\bf "+ hint +"}";
                if(solutionSheetTwoColumn)
                    mainCompleteHint += "\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.44\\textwidth}}\\textbf{"+ (questionIndex  + pdfPageSetupBean.getQuestionStartNo()) +")} & " + hint + "\\end{tabular}";
                else
                    mainCompleteHint += "\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.94\\textwidth}}\\textbf{"+ (questionIndex  + pdfPageSetupBean.getQuestionStartNo()) +")} & " + hint + "\\end{tabular}";
//                mainComplete += "\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.94\\textwidth}}\\textbf{" + (questionIndex  + pdfPageSetupBean.getQuestionStartNo()) + ") } & " + question + "   \\end{tabular}\\newline";
//                mainCompleteHint += "\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.44\\textwidth}}\\textbf{"+ (questionIndex  + pdfPageSetupBean.getQuestionStartNo()) +")} & " + hint + "\\end{tabular}\\\\newline";
                teacherString += "{\\renewcommand{\\arraystretch}{1.1}\\hspace*{0.035\\textwidth}\\begin{tabular}[H]{>{\\raggedright}p{0.44\\textwidth}}{\\textbf{Solution :}}\\tabularnewline"+ hint +"\\end{tabular}}\\newline";
            }
            
            if(questionsBean.isIsHintAsImage()) {
                try {    
                    hintImagePath = questionsBean.getHintImagePath();
                    hintImagePath = hintImagePath.replace("\\", "/");
                    float scale;
                    int actualwidth;
                    String latexCode = "";
                    scale = getImageDim(hintImagePath);
                    scale = precision(2, scale);                
                    scale = (float) (scale-0.2);
                    BufferedImage bimg = ImageIO.read(new File(hintImagePath));
                    int width = bimg.getWidth();
                    actualwidth = (int) (width * scale);
                    if(actualwidth<390)
                        latexCode="[scale="+scale+"]";
                    else
                        latexCode="[width=0.48\\textwidth]";
                    mainCompleteHint += "\\\\{\\centering\\includegraphics" + latexCode + "{" + hintImagePath + "}\\\\}";
                    teacherString += "\\\\{\\centering\\includegraphics" + latexCode + "{" + hintImagePath + "}\\\\}";
                    
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            } else {
                hintImagePath = "";
            }
            if((!hint.trim().equals("\\mbox{}") && !(hint.trim().equals(""))) || questionsBean.isIsHintAsImage()) 
//                mainCompleteHint += " \\\\[2.5pt] ";
                mainCompleteHint += " \\\\";
            
            mainComplete += "\\sagar";
            questionIndex ++;
        }
        
        teacherString = "\\begin{multicols*}{2}" + teacherString + "\\end{multicols*}";
        teacherString = teacherString.replace("\\rm", "\\text");
        
        if(pdfPageSetupBean.isTwoColumn()) {
            mainComplete = "\\vspace*{-0.2in}\\begin{multicols*}{2}" + mainComplete + "\\end{multicols*}";
        }
        String chapterName = chapterList.get(0).getChapterName().trim();
        mainComplete = "\\setlength{\\extrarowheight}{7pt}\\begin{tabular}{|>{\\centering}p{19cm}|}\\hline\\large \\bf{"+ chapterName +"}\\tabularnewline\\hline\\end{tabular}\\newline" + mainComplete;
        mainComplete = mainComplete.replace("\\rm", "\\text");
        mainString += mainComplete;
        if(correctAnswerSheet) {
            mainString += "\\newpage" + getAnswerSheet1(selectedQuestionsList, pdfPageSetupBean.getQuestionStartNo(),chapterName);
        }
//        System.out.println("MainComplete Hint:"+mainCompleteHint);
        if(solutionSheet && mainCompleteHint.trim() != "") {
            if(solutionSheetTwoColumn)
                mainCompleteHint = "\\vspace*{-0.1in}\\begin{multicols*}{2}" + mainCompleteHint + "\\end{multicols*}";
//            mainCompleteHint = mainCompleteHint.replaceAll("\\\\ ", "\\\\[2.5pt] ");
            mainCompleteHint=mainCompleteHint.replace("\\rm", "\\text");
            mainString += "\\newpage"+getHintHeaderString1(selectedQuestionsList,chapterName) + "\\\\"+mainCompleteHint;
        }
//        teacherString = getTeacherCopyHeaderString(selectedQuestionsList, chapterName, "Scholars Katta") + "\\\\" + teacherString;

//        mainString = mainString.replace("â€™", "'");
//        teacherString = teacherString.replace("â€™", "'");
        
        mainString = replaceString(mainString);
        teacherString = replaceString(teacherString);
        
        returnBean = new PdfMainStringBean();
        returnBean.setMainString(mainString);
        returnBean.setTeacherMainString(teacherString);
        
        return returnBean;
    }
    
    private String replaceString(String mainString) {
        String returnString = mainString;
        returnString = returnString.replace("â€™", "'");
        returnString = returnString.replace("$$", "");
        returnString = returnString.replace("$ $", "");
        returnString = returnString.replace("$  $", "");
        returnString = returnString.replace("$   $", "");
//        returnString = returnString.replace("\\^", "^");
        return returnString;
    }
    
     private String getAnswerSheet1(ArrayList<QuestionBean> selectedQuestionsList,int startNo,String chapterName) {
        String returnString = "{\\centering\\fbox{\\parbox{\\textwidth}{\\centerline{\\Large \\bf Answer Sheet}}}}\\newline \\newline \\hspace*{0.035 in}";
        returnString += "{\\centerline{ \\Large \\bf "+getSubjectString()+" : "+chapterName+"}\\\\\\hspace*{0.15in} \\Large \\bf "+"Total Questions : "+selectedQuestionsList.size()+"\\hfill \\Large \\bf "+"Total Marks : "+totalMarks+"\\hspace*{0.32in}\\vspace*{0.05in}}\\newline  \\line(1,0){550}";
        returnString += "\\begin{center}" + new AnswerSheetProcessing().getAnswerSheet(selectedQuestionsList,startNo) + "\\end{center}";
        return returnString;
    }
    
    private String getHintHeaderString1(ArrayList<QuestionBean> selectedQuestionsList,String chapterName) {
        String returnString = "{\\centering\\fbox{\\parbox{\\textwidth}{\\centerline{\\Large \\bf Solution Sheet}}}}\\newline \\newline \\hspace*{0.035 in} ";
        returnString += "{\\centerline{ \\Large \\bf "+getSubjectString()+" : "+chapterName+"}\\\\\\hspace*{0.15in} \\Large \\bf "+"Total Questions : "+selectedQuestionsList.size()+"\\hfill \\Large \\bf "+"Total Marks : "+totalMarks+"\\hspace*{0.32in}\\vspace*{0.05in}}\\newline  \\line(1,0){550}";
        return returnString;
    }
    
     private String getAnswerSheet(ArrayList<QuestionBean> selectedQuestionsList,PdfPageSetupBean pdfPageSetupBean,String chapterName) {
        int startNo=pdfPageSetupBean.getQuestionStartNo();
        int size1 = pdfPageSetupBean.getFontSize();
        int resize=size1+4;
        double baseline1 = resize*1.2;
        
        int size = pdfPageSetupBean.getFontSize();
        double baseline = size*1.2;
        
        String returnString = "{\\fontsize{"+resize+"}{"+baseline1+"}\\selectfont{\\centering\\fbox{\\parbox{\\textwidth}{\\centerline{ \\bf Answer Sheet}}}}\\newline \\newline \\hspace*{0.035 in}}";
        returnString += "{\\fontsize{"+resize+"}{"+baseline1+"}\\selectfont{\\centerline{  \\bf "+getSubjectString()+" : "+chapterName+"}\\\\\\hspace*{0.15in} \\bf "+"Total Questions : "+selectedQuestionsList.size()+"\\hfill  \\bf "+"Total Marks : "+totalMarks+"\\hspace*{0.32in}\\vspace*{0.05in}}\\newline  \\line(1,0){550}}";
        returnString += "{\\fontsize{"+size+"}{"+baseline+"}\\selectfont\\begin{center}" + new AnswerSheetProcessing().getAnswerSheet(selectedQuestionsList,startNo) + "\\end{center}}";
        return returnString;
    }
    
    private String getHintHeaderString(ArrayList<QuestionBean> selectedQuestionsList,PdfPageSetupBean pdfPageSetupBean,String chapterName) {
        
        int size1 = pdfPageSetupBean.getFontSize();
        int resize=size1+4;
        double baseline1 = resize*1.2;
        
        String returnString = "{\\fontsize{"+resize+"}{"+baseline1+"}\\selectfont{\\centering\\fbox{\\parbox{\\textwidth}{\\centerline{ \\bf Solution Sheet}}}}\\newline \\newline \\hspace*{0.035 in} }";
        returnString += "{\\fontsize{"+resize+"}{"+baseline1+"}\\selectfont{\\centerline{ \\bf "+getSubjectString()+" : "+chapterName+"}\\\\\\hspace*{0.15in}  \\bf "+"Total Questions : "+selectedQuestionsList.size()+"\\hfill  \\bf "+"Total Marks : "+totalMarks+"\\hspace*{0.32in}\\vspace*{0.05in}}\\newline  \\line(1,0){550}}";
        return returnString;
    }
    
    private float getImageDim(String path) {
        float dim = (float) 1.2;
        for(ImageRatioBean imageRatioBean : viewImgDimeList){
            if(imageRatioBean.getImageName().equals(path)){
                dim = (float) imageRatioBean.getViewDimention();
                break;
            }
        }
        return dim;
    }
    
    private Float precision(int decimalPlace, Float d) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }
    
    private String getSubjectString() {
        String returnString = "";
        String[] subjectArray = subjectString.split(",");
        if(printingPaperType.equalsIgnoreCase("GroupWise") || printingPaperType.equalsIgnoreCase("YearWise")){
            for(String subjectName : subjectArray) 
                returnString += subjectName.substring(0, 1);
        } else {
            for(String subjectName : subjectArray) 
                returnString += subjectName;
        }
        return returnString;
    }
}
