/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.LatexProcessing;

/**
 *
 * @author Aniket
 */
public class MboxSpaceRemoving {
    
    public String removeBeginEndMboxSpace(String mainString) {
        String returnString = mainString;
        
        String tempStr = "";
        if(mainString.contains("\\mbox{")) {
            String[] strArray = returnString.split("\\\\mbox\\{");
            if(strArray.length != 0) {
                tempStr += strArray[0];
                if(strArray[0].equals(""))
                    tempStr += "\\mbox{" + strArray[1].trim();
                else
                    tempStr += "\\mbox{" + strArray[1];
                for(int i=2;i<strArray.length;i++)
                    tempStr += "\\mbox{" + strArray[i];
                returnString = tempStr;
            }
        }
        tempStr = "";
        if(mainString.contains("}")) {
            String[] strArray = returnString.split("}");

            if(strArray.length != 0) {
                for(int i=0;i<strArray.length-1;i++)
                    tempStr += strArray[i] + "}" ;
                if(mainString.charAt(mainString.length()-1) == '}')
                    tempStr += strArray[strArray.length-1].trim() + "}" ;
                else
                    tempStr += strArray[strArray.length-1].trim();
                returnString = tempStr;
            }
        }
        return returnString;
    }
}
