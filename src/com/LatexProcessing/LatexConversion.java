/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.LatexProcessing;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import javax.swing.JLabel;
import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;

/**
 *
 * @author admin
 */
public class LatexConversion {
    private String lblStart;
    private String lblEnd;
    private TeXFormula formula;
    private TeXIcon icon;
    private BufferedImage image;
    private Graphics2D g2;
    
    public LatexConversion(){
        lblStart = "\\begin{array}{l}";
        lblEnd = "\\end{array}";
    }
    
    public void setLableText(JLabel lbl, String str) {
        try {
            if (!str.equals("")) {
                str = str.replace("â€™", "'");
                lbl.setText("");
                JLabel jl;
                str = lblStart + str + lblEnd;
                formula = new TeXFormula(str);
                icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 20);
                icon.setInsets(new Insets(0, 0, 0, 0));
                image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
                g2 = image.createGraphics();
                g2.setColor(Color.white);
                g2.fillRect(0, 0, icon.getIconWidth(), icon.getIconHeight());
                jl = new JLabel();
                jl.setForeground(new Color(0, 0, 0));
                icon.paintIcon(jl, g2, 0, 0);
                lbl.setIcon(icon);
            } else {
                lbl.setIcon(null);
                lbl.setText("");
            }
        } catch (Exception ex) {
            lbl.setIcon(null);
            lbl.setText("");
            ex.printStackTrace();
        }
    }
}
