/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.LatexProcessing.word;
import com.bean.ImageRatioBean;
import com.bean.OptionImageDimensionsBean;
import com.bean.QuestionBean;
import com.bean.SubjectBean;
import com.bean.WordPageSetupBean;
import com.db.operations.ImageRatioOperation;
import com.db.operations.OptionImageDimentionOperation;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Aniket
 */
public class QuestionsProcessing {
    
    private ArrayList<ImageRatioBean> viewImgDimeList;
    private String selectionType;
    private String subjectString;
    
    public String getMainString(ArrayList<QuestionBean> selectedQuestionsList,ArrayList<SubjectBean> selectedSubjectList,String printingPaperType
        ,WordPageSetupBean wordPageSetupBean,boolean twoColumnCompatible,boolean correctAnswerSheet,boolean solutionSheet) {
        String returnString = "";
        String answerString = "";
        int index = 0;
        String question,optionA,optionB,optionC,optionD,queYear,hint,queImagePath,optionImagePath,hintImagePath;
        boolean isPreviousQuestionPaper = false;
        String subjectName = "";
        String mainComplete = "",mainCompleteHint = "";
        ArrayList<Integer> subjectIdList = new ArrayList<Integer>();
        NewQuesLatexProcessing newQuesProcess = null;
        NewHintLatexProcessing newHintProcess = null;
        subjectString = "";
        for(SubjectBean subjectBean : selectedSubjectList) {
            subjectString += subjectBean.getSubjectName() +",";
        }
        
        if(printingPaperType.equalsIgnoreCase("YearWise"))
           isPreviousQuestionPaper = true;
        if(printingPaperType.equalsIgnoreCase("GroupWise") || printingPaperType.equalsIgnoreCase("YearWise")) {
            Collections.sort(selectedQuestionsList);
        }
        
        
        
        
        try {
            File file = new File("wordprocessimages");
            if(!file.exists())
                file.mkdir();
            FileUtils.cleanDirectory(file);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        ArrayList<String> imageList = null;
        
        ArrayList<OptionImageDimensionsBean> optionImageDimentonList = new OptionImageDimentionOperation().getOptionImageDimaentionList(selectedQuestionsList, isPreviousQuestionPaper);
        viewImgDimeList = new ImageRatioOperation().getImageRatioList(selectedQuestionsList);
        int questionIndex = 0;
        for(QuestionBean questionsBean : selectedQuestionsList) {
            //Questions Heading(Subject Name\Test Name) 
            if(printingPaperType.equalsIgnoreCase("GroupWise") || printingPaperType.equalsIgnoreCase("YearWise")){
                if(!subjectIdList.contains(questionsBean.getSubjectId())){
                    subjectIdList.add(questionsBean.getSubjectId());
                    for(SubjectBean subjectBean : selectedSubjectList){
                        if(questionsBean.getSubjectId() == subjectBean.getSubjectId()) {
                            subjectName = subjectBean.getSubjectName();
                            if(subjectIdList.size() !=1)
//                                mainComplete += "\\\\";
                            break;
                        }
                    }
//                    mainComplete += "\\setlength{\\extrarowheight}{6.5pt}\\begin{tabular}{|>{\\centering}p{19cm}|}\\hline\\large "+ subjectName +"\\tabularnewline\\hline\\end{tabular}\\newline\\newline";
                    mainComplete += "\\begin{align} \\textbf{"+ subjectName +"}\\end{align} \\\\";
                }
            }
//            else {
//                if(!subjectIdList.contains(questionsBean.getSubjectId())){
//                    subjectIdList.add(questionsBean.getSubjectId());
//                    for(SubjectBean subjectBean : selectedSubjectList){
//                        if(questionsBean.getSubjectId() == subjectBean.getSubjectId()) {
//                            subjectName = subjectBean.getSubjectName();
//                            break;
//                        }
//                    }
////                    mainComplete += "\\setlength{\\extrarowheight}{7pt}\\begin{tabular}{|>{\\centering}p{19cm}|}\\hline\\large "+ subjectName +"\\tabularnewline\\hline\\end{tabular}\\newline\\newline";
//                    mainComplete += "\\begin{align} \\textbf{"+ subjectName +"}\\end{align} \\\\";
//                }
//            }
            
            question = questionsBean.getQuestion();
            optionA = questionsBean.getOptionA();
            optionB = questionsBean.getOptionB();
            optionC = questionsBean.getOptionC();
            optionD = questionsBean.getOptionD();
            hint = questionsBean.getHint();
            if(newQuesProcess == null)
                newQuesProcess = new NewQuesLatexProcessing();
            if(newHintProcess == null)
                newHintProcess = new NewHintLatexProcessing();
                
            question = newQuesProcess.mboxProcessing(question);

            if(!(optionA.equals("\\mbox{}")))
                optionA = newQuesProcess.mboxProcessing(optionA);
            if(!(optionB.equals("\\mbox{}"))) 
                optionB = newQuesProcess.mboxProcessing(optionB);
            if(!(optionC.equals("\\mbox{}"))) 
                optionC = newQuesProcess.mboxProcessing(optionC);
            if(!(optionD.equals("\\mbox{}")))
                optionD = newQuesProcess.mboxProcessing(optionD);
            if(!(hint.equals("\\mbox{}")))
                hint = newHintProcess.mboxProcessing(hint);
            
            if(wordPageSetupBean.isQuestionBold())
                question = "{\\bf "+ question +"}";
            
            if(wordPageSetupBean.isOptionBold()){
                if(!(optionA.equals("\\mbox{}")))
                    optionA = "{\\bf "+ optionA +"}";
                if(!(optionB.equals("\\mbox{}")))
                    optionB = "{\\bf "+ optionB +"}";
                if(!(optionC.equals("\\mbox{}")))
                    optionC = "{\\bf "+ optionC +"}";
                if(!(optionD.equals("\\mbox{}")))
                    optionD = "{\\bf "+ optionD +"}";
            } 
            
            queYear = questionsBean.getYear();
            if (queYear != null && queYear != "" && !queYear.equalsIgnoreCase("\\mbox{}")) {
                queYear = getRefinedYear(queYear.trim());
            } else {
                queYear = "";
            }
            
            if(wordPageSetupBean.isPrintYear() && queYear != "" && !printingPaperType.equalsIgnoreCase("YearWise")) {
//                mainComplete += "\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.44\\textwidth}}\\textbf{" + (++questionIndex) + ") } & " + question + "    \\newline \\hspace*{2.7in} \\textbf{(" + queYear + ")} \\end{tabular} \\newline";
                mainComplete += "\\begin{table} \\begin{tabular}{ll} \\textbf{" + (questionIndex  + wordPageSetupBean.getQuestionStartNo()) + ") } &  " + question + " \\textbf{"+ queYear +"} \\end{tabular}\\end{table} ";
            } else if(wordPageSetupBean.isPrintYear() && queYear != "" && printingPaperType.equalsIgnoreCase("YearWise")) {
                mainComplete += "\\begin{table} \\begin{tabular}{ll} \\textbf{" + (questionIndex  + wordPageSetupBean.getQuestionStartNo()) + ") } &  " + question + " \\textbf{("+ queYear +")} \\end{tabular}\\end{table} ";
            } else {
                mainComplete += "\\begin{table} \\begin{tabular}{ll} \\textbf{" + (questionIndex  + wordPageSetupBean.getQuestionStartNo()) + ") } &  " + question + "   \\end{tabular}\\end{table} ";
//                mainComplete += "\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.44\\textwidth}}\\textbf{" + (++questionIndex) + ") } & " + question + "   \\end{tabular} \\newline";
            }
            
            if(questionsBean.isIsQuestionAsImage()) {
                try {
                    queImagePath = questionsBean.getQuestionImagePath();
                    queImagePath = queImagePath.replaceAll("_", "-");
                    float scale;
                    int actualWidth;
                    String latexCode = "";
                    scale = getImageDim(queImagePath);
                    scale = precision(2, scale);
                    scale = (float) (scale-0.2);
                    BufferedImage bufferImg = ImageIO.read(new File(queImagePath));
                    int width = bufferImg.getWidth();
                    actualWidth = (int) (width * scale);
                    if(actualWidth<390)
//                        latexCode="[scale="+scale+"]";
                        latexCode ="[width=0.48\\textwidth]";
                    else 
                        latexCode ="[width=0.48\\textwidth]";
                    if(imageList == null)
                        imageList = new ArrayList<String>();
                    imageList.add(queImagePath);
                    mainComplete += "\\begin{table} \\begin{tabular}{lc} & \\includegraphics{wordprocess" + queImagePath + "}    \\end{tabular}\\end{table}";
//                    mainComplete += "\\begin{table} \\begin{tabular}{lc} & \\includegraphics{word" + queImagePath + "}    \\end{tabular}\\end{table}";
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            } else {
                queImagePath = "";
            }
            
            if(questionsBean.isIsOptionAsImage()) {
                try {
                    optionImagePath = questionsBean.getOptionImagePath();
                    float scale;
                    int actualWidth;
                    String latexCode = "";
                    scale = getImageDim(optionImagePath);
                    scale = precision(2, scale);
                    scale = (float) (scale-0.2);
                    BufferedImage bimg = ImageIO.read(new File(optionImagePath));
                    int width = bimg.getWidth();
                    actualWidth = (int) (width * scale);
                    if(actualWidth<390)
                        latexCode="[scale="+scale+"]";
                     else
                        latexCode="[width=0.48\\textwidth]";
                    if(imageList == null)
                        imageList = new ArrayList<String>();
                    imageList.add(optionImagePath);
                    mainComplete += "\\begin{table} \\begin{tabular}{lc} & \\includegraphics{wordprocess" + optionImagePath + "}    \\end{tabular}\\end{table}";
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            } else {
                int onefourth = 78;
                int half = 140;
                 String spaceinoption = " \\ \\ ";
                optionImagePath = "";
                OptionImageDimensionsBean optImgDimBean = new OptionImageDimensionsBean();
                for(OptionImageDimensionsBean optBean : optionImageDimentonList){
                    if(optBean.getQuestionId() == questionsBean.getQuestionId()){
                        optImgDimBean = optBean;
                        break;
                    }
                }
                int optionType = 0;
                
                if (!questionsBean.isIsQuestionAsImage()) {

                    optionType = 0;
                    if (optImgDimBean.getOptionA() < half && optImgDimBean.getOptionB() < half && optImgDimBean.getOptionC() < half && optImgDimBean.getOptionD() < half) {
                        optionType = 2;
                    }
                    if (optImgDimBean.getOptionA() < onefourth && optImgDimBean.getOptionB() < onefourth && optImgDimBean.getOptionC() < onefourth && optImgDimBean.getOptionD() < onefourth) {
                        optionType = 1;
                    }
                }
                
                if(twoColumnCompatible) {
                    if(optionType==2)
                        optionType=0;
                    if(optionType==1)
                        optionType=2;
                }
                
                switch (optionType) {
                    case 0:
                        mainComplete += "\\begin{table} \\begin{tabular}{ll}  & \\textbf{A)}  " + optionA + "  \\end{tabular}\\end{table} ";
                        mainComplete += "\\begin{table} \\begin{tabular}{ll}  & \\textbf{B)}  " + optionB + "  \\end{tabular}\\end{table} ";
                        mainComplete += "\\begin{table} \\begin{tabular}{ll}  & \\textbf{C)}  " + optionC + "  \\end{tabular}\\end{table} ";
                        mainComplete += "\\begin{table} \\begin{tabular}{ll}  & \\textbf{D)}  " + optionD + "  \\end{tabular}\\end{table} ";
                        break;
                    case 1:
                        mainComplete += "\\begin{table} \\begin{tabular}{ll}  & \\textbf{A)}  " + optionA + spaceinoption + " \\textbf{B)}  " + optionB + spaceinoption + "\\textbf{C)}  " + optionC + spaceinoption + "\\textbf{D)}  " + optionD +  "  \\end{tabular}\\end{table} ";
                        break;
                    case 2:
                        mainComplete += "\\begin{table} \\begin{tabular}{ll} & \\textbf{A)}  " + optionA + spaceinoption + " \\textbf{B)}  " + optionB + spaceinoption + "\\\\  & \\textbf{C)}  " + optionC + spaceinoption + "\\textbf{D)}  " + optionD +  "  \\end{tabular}\\end{table} ";
                        break;
                }
            }
            
            if((!hint.trim().equals("\\mbox{}") && !(hint.trim().equals(""))) || questionsBean.isIsHintAsImage())
//                mainCompleteHint += "\\begin{tabular}[H]{>{\\raggedright}p{0.015\\textwidth} >{\\raggedright}p{0.44\\textwidth}}\\textbf{"+ questionIndex +")} & " + hint + "\\end{tabular}";
                if(wordPageSetupBean.isSolutionBold())
                    hint = "{\\bf "+ hint +"}";
                mainCompleteHint += "\\begin{table} \\begin{tabular}{ll} \\textbf{" + (questionIndex  + wordPageSetupBean.getQuestionStartNo()) + ") } & " + hint + "   \\end{tabular}\\end{table}";
            
            if(questionsBean.isIsHintAsImage()) {
                try {    
                    hintImagePath = questionsBean.getHintImagePath();
                    hintImagePath = hintImagePath.replace("\\", "/");
                    float scale;
                    int actualwidth;
                    String latexCode = "";
                    scale = getImageDim(hintImagePath);
                    scale = precision(2, scale);                
                    scale = (float) (scale-0.2);
                    BufferedImage bimg = ImageIO.read(new File(hintImagePath));
                    int width = bimg.getWidth();
                    actualwidth = (int) (width * scale);

                    if(actualwidth<390)
                        latexCode="[scale="+scale+"]";
                    else
                        latexCode="[width=0.48\\textwidth]";
                    if(imageList == null)
                        imageList = new ArrayList<String>();
                    imageList.add(hintImagePath);
                    mainCompleteHint += "\\begin{table} \\begin{tabular}{lc} & \\includegraphics{wordprocess" + hintImagePath + "}    \\end{tabular}\\end{table}";
//                        mainCompleteHint += "\\\\{\\centering\\includegraphics" + latexCode + "{" + hintImagePath + "}\\\\}";
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            } else {
                hintImagePath = "";
            }
            
            if(questionIndex % 10 == 0 && questionIndex > 0) {
               answerString += "\\\\ ";
            }
            
            answerString += manageSpaceNumber((questionIndex  + wordPageSetupBean.getQuestionStartNo()));
            answerString += "\\ \\ ";
            answerString += "\\  "+ questionsBean.getAnswer() +"\\ \\ \\ ";
//            index++;
            questionIndex++;
        }
        
        returnString = mainComplete;
        
        if(correctAnswerSheet)
            returnString += "\\begin{align} \\textbf{Answers}  \\end{align} "+ answerString+" \\\\  ";
        
        if(solutionSheet) 
            returnString += "\\begin{align} \\textbf{Solutions} \\end{align}" + mainCompleteHint;
        
        if(imageList != null)
            makeWordImages(imageList);
        
        returnString = replaceString(returnString);
        
        return returnString;
    }
    
    private String replaceString(String mainString) {
        String returnString = mainString;
        returnString = returnString.replace("â€™", "'");
        returnString = returnString.replace("$$", "");
        returnString = returnString.replace("$ $", "");
        returnString = returnString.replace("$  $", "");
        returnString = returnString.replace("$   $", "");
//        returnString = returnString.replace("\\^", "^");
        return returnString;
    }
    
    private void makeWordImages(ArrayList<String> imageList) {
        File imgFile = null;
        BufferedImage sorce = null;
        for(String imageName : imageList) {
            imgFile = new File(imageName);
            if(imgFile.isFile()) {
                try {
                    sorce = ImageIO.read(imgFile);
                    int destWidth = (int) (sorce.getWidth() * 0.7);
                    int destHeight = (int) (sorce.getHeight() * 0.7);
                    BufferedImage dest = new BufferedImage(destWidth, destHeight,
                    BufferedImage.TYPE_INT_ARGB);

                // Paint source image into the destination, scaling as needed 
                    Graphics2D graphics2D = dest.createGraphics();
                    graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
                    graphics2D.drawImage(sorce, 0, 0, destWidth, destHeight, null);
                    graphics2D.dispose();
                    
                    String[] strArray = imageName.split("/");
                    ImageIO.write(dest, "PNG", new File("wordprocessimages/" + strArray[1].trim()));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
    
    private float getImageDim(String path) {
        float dim = (float) 1.2;
        for(ImageRatioBean imageRatioBean : viewImgDimeList) {
            if(imageRatioBean.getImageName().equals(path)){
                dim = (float) imageRatioBean.getViewDimention();
                break;
            }
        }
        return dim;
    }
    
    private Float precision(int decimalPlace, Float d) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }
    
    private String getRefinedYear(String str) {
        
//        year=year.replace("\\{", "(");
//        year=year.replaceAll("\\}", ")");
//        return year;
        
        if(str.contains("{")) {
            char[] carray = str.toCharArray();
            int clen = carray.length;
            char[] tarray = new char[str.length() + 100];
            for (int i = 0, j = 0; i < clen;) {
                if(carray[i] == '{') {
                    tarray[j]='(';
                    j++;i++;
                } else if(carray[i] == '}') {
                       tarray[j]=')';
                       j++;i++;
                } else {
                    tarray[j]=carray[i];
                    j++;i++;
                }
            }
            
            String s = new String(tarray);
            s = s.trim();
//            System.out.println(s);
            return s;
        }
        return str;
    }
    
    private String manageSpaceNumber(int i) {
        String str = "";
        str = "" + Integer.toString(i);
        int strsize = str.length();
        for(int j=0;j<3-strsize;j++) {
            str = str+"\\ \\ ";
        }
        
        return str;
    }
    
}
