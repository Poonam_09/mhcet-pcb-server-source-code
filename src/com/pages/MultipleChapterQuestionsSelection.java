package com.pages;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.*;
import com.Pdf.Generation.PdfPageSetup;
import com.Word.Generation.WordPageSetup;
import java.util.Collections;
import com.bean.ChapterBean;
import com.bean.CountChapterBean;
import com.bean.ImageRatioBean;
import com.bean.QuestionBean;
import com.bean.SubjectBean;
import com.bean.PrintedTestBean;
import com.db.operations.ChapterOperation;
import com.db.operations.ImageRatioOperation;
import com.db.operations.QuestionOperation;
import java.awt.event.KeyEvent;
import com.ui.support.pages.QuestionPanel;
import com.Model.TitleInfo;
import com.db.operations.ClassSaveTestOperation;
import com.db.operations.PatternStatusOperation;
import ui.SaveTest;

public class MultipleChapterQuestionsSelection extends javax.swing.JFrame {

    private int subjectId,chapterId;
    private ArrayList<QuestionBean> questionsList;
    private ArrayList<ChapterBean> chaptersList;
    private ArrayList<SubjectBean> subjectsList;
    private ArrayList<CountChapterBean> countedChapterList;
    private ArrayList<QuestionBean> sortedQuestionsList; 
    private ArrayList<CountChapterBean> sortedCountChapterList;
    private ArrayList<QuestionBean> selectedQuestionsList = new ArrayList<QuestionBean>();
    private ArrayList<QuestionBean> chapterWiseQuestionsList = new ArrayList<QuestionBean>();
    private ArrayList<QuestionBean> advSelectionQuesList;
    private ArrayList<Integer> addedSequence;
    private QuestionPanel currentPanel = null;
    private javax.swing.JButton[] selectedButtonsArray,nonSelectedButtonsArray;
    private int currentIndex;
    private int shuffleValue;
    private boolean isQuestion;
    private HomePage homePage;
    private MultipleChapterSelection selectionUnitsWise;
    private String printingPaperType;    
    private int totalSelectedQueCount;
    private ArrayList<ImageRatioBean> imageRatioList;
    private PrintedTestBean printedTestBean;
    private ArrayList<Integer> saveTestSelectedQuesIdList;
    private boolean testSaveStatus;
    int animationTime = 1, sec = 0, min = 0, subid, currentIndex1, totalSelectedQueCount1 = 0;
    int currentPatternIndex;
    
    //Complete Subject
    public MultipleChapterQuestionsSelection(int currentPatternIndex,ArrayList<QuestionBean> questionsList,ArrayList<ChapterBean> chaptersList,ArrayList<SubjectBean> subjectsList,ArrayList <CountChapterBean> countedChapterList,HomePage homePage,String printingPaperType){
        initComponents();
        this.currentPatternIndex=currentPatternIndex;
        this.questionsList = questionsList;
        this.chaptersList = chaptersList;
        this.subjectsList = subjectsList;
        this.printingPaperType = printingPaperType;
        sortedCountChapterList = null;
        this.countedChapterList = countedChapterList;
        setInitialValues();
        if (subjectsList.size() != 1) {
            CmbSubject.setVisible(true);
            CmbSubject.removeAllItems();
            LblSubject.setText("Subject : ");
            for(SubjectBean subjectBean : this.subjectsList) {
                CmbSubject.addItem(subjectBean.getSubjectName());
            }
        } else {
            CmbSubject.setVisible(false);
            LblSubject.setText("Subject : "+this.subjectsList.get(0).getSubjectName());
        }
        subjectId = this.subjectsList.get(0).getSubjectId();
        this.homePage = homePage;
        printedTestBean = null;
        testSaveStatus = false;
        saveTestSelectedQuesIdList = null;
        selectionUnitsWise = null;
        BtnChangeChapters.setText("Change Subject");
        setChapterCombo();
        if(currentPatternIndex==0 ||currentPatternIndex==1)
        {
            BtnWord.setVisible(false);
        }
    }
    
    //UnitsWise
    public MultipleChapterQuestionsSelection(int currentPatternIndex,ArrayList<QuestionBean> questionsList,ArrayList<ChapterBean> chaptersList,ArrayList<SubjectBean> subjectsList,ArrayList <CountChapterBean> countedChapterList,MultipleChapterSelection selectionUnitsWise,String printingPaperType) {
        initComponents();
        this.currentPatternIndex=currentPatternIndex;
        this.questionsList = questionsList;
        this.chaptersList = chaptersList;
        this.subjectsList = subjectsList;
        sortedCountChapterList = null;
        this.countedChapterList = countedChapterList;
        setInitialValues();
        if (subjectsList.size() != 1) {
            CmbSubject.setVisible(true);
            CmbSubject.removeAllItems();
            LblSubject.setText("Subject : ");
            for(SubjectBean subjectBean : this.subjectsList) {
                CmbSubject.addItem(subjectBean.getSubjectName());
            }
        } else {
            CmbSubject.setVisible(false);
            LblSubject.setText("Subject : "+this.subjectsList.get(0).getSubjectName());
        }
        subjectId = this.subjectsList.get(0).getSubjectId();
        homePage = null;
        printedTestBean = null;
        testSaveStatus = false;
        saveTestSelectedQuesIdList = null;
        this.printingPaperType = printingPaperType;
        this.selectionUnitsWise = selectionUnitsWise;
        BtnChangeChapters.setText("Change Chapters");
        setChapterCombo();
        if(currentPatternIndex==0 || currentPatternIndex==1)
        {
            BtnWord.setVisible(false);
        }
    }
    
    
    // Saved Test Resume
    public MultipleChapterQuestionsSelection(ArrayList<QuestionBean> questionsList,ArrayList<ChapterBean> chaptersList,ArrayList<SubjectBean> subjectsList,ArrayList <CountChapterBean> countedChapterList,ArrayList<QuestionBean> selectedQuestionsList,String printingPaperType,PrintedTestBean printedTestBean){
        initComponents();
        this.questionsList = questionsList;
        this.chaptersList = chaptersList;
        this.subjectsList = subjectsList;
        this.selectedQuestionsList = selectedQuestionsList;
        sortedCountChapterList = null;
        this.countedChapterList = countedChapterList;
        this.printedTestBean = printedTestBean;
        setInitialValues();
        if (subjectsList.size() != 1) {
            CmbSubject.setVisible(true);
            CmbSubject.removeAllItems();
            LblSubject.setText("Subject : ");
            
            for(SubjectBean subjectBean : this.subjectsList) {
                CmbSubject.addItem(subjectBean.getSubjectName());
            }
            
        } else {
            CmbSubject.setVisible(false);
            LblSubject.setText("Subject : "+this.subjectsList.get(0).getSubjectName());
        }
        
        subjectId = this.subjectsList.get(0).getSubjectId();
        totalSelectedQueCount = selectedQuestionsList.size();
        LblSelectedCount.setText("Selected Questions : " + totalSelectedQueCount);
        
        
        homePage = null;
        this.printingPaperType = printingPaperType;
        selectionUnitsWise = null;
        saveTestSelectedQuesIdList = new ArrayList<Integer>();
        for(QuestionBean bean : selectedQuestionsList) 
            saveTestSelectedQuesIdList.add(bean.getQuestionId());
        testSaveStatus = true; 
        if (printingPaperType.equals("SubjectWise"))
            BtnChangeChapters.setText("Change Subject");
        else    
            BtnChangeChapters.setText("Change Chapters");
        setChapterCombo();
          if(currentPatternIndex==0 || currentPatternIndex==1)
        {
            BtnWord.setVisible(false);
        }
    }
    
    private void setInitialValues() {
        this.getContentPane().setBackground(Color.white);
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        currentPanel = PanelQues1;
        imageRatioList = new ImageRatioOperation().getImageRatioList();
        TxtFldQuestionNumber.setColumns(3);
        TxtFldQuestionNumber.setHorizontalAlignment(JTextField.CENTER);
        totalSelectedQueCount = selectedQuestionsList.size();
        shuffleValue = 0;
    }
    
    private void setQuesType() {
        ArrayList<QuestionBean> tempSortedQue = new ArrayList<QuestionBean>();
        ArrayList<QuestionBean> tempQueL = new ArrayList<QuestionBean>();
        ArrayList<QuestionBean> tempQueT = new ArrayList<QuestionBean>();
        ArrayList<QuestionBean> tempQueU = new ArrayList<QuestionBean>();
        ArrayList<QuestionBean> tempQueP = new ArrayList<QuestionBean>();
        ArrayList<QuestionBean> tempSelection = new ArrayList<QuestionBean>();
        tempSortedQue = chapterWiseQuestionsList;
        
        for(QuestionBean questionsBean:chapterWiseQuestionsList) {    
            if(questionsBean.getChapterId()== chapterId && questionsBean.getSubjectId()== subjectId) {
                if (ChkLevel.isSelected()) {
                    if (CmbLevel.getSelectedIndex() == 0 && questionsBean.getLevel()==0) {
                        tempQueL.add(questionsBean);
                    } else if (CmbLevel.getSelectedIndex() == 1 && questionsBean.getLevel()==1) {
                        tempQueL.add(questionsBean);
                    } else if (CmbLevel.getSelectedIndex() == 2 && questionsBean.getLevel()==2) {
                        tempQueL.add(questionsBean);
                    }
                }
            
                if (ChkType.isSelected()) {
                    if (CmbType.getSelectedIndex() == 0 && questionsBean.getType()==0) {
                        tempQueT.add(questionsBean);
                    } else if (CmbType.getSelectedIndex() == 1 && questionsBean.getType()==1) {
                        tempQueT.add(questionsBean);
                    }
                }
            
                if (ChkUsed.isSelected()) {
                    if (CmbUsed.getSelectedIndex() == 0 && questionsBean.getAttempt()==1) {
                        tempQueU.add(questionsBean);
                    } else if (CmbUsed.getSelectedIndex() == 1 && questionsBean.getAttempt()==0) {
                        tempQueU.add(questionsBean);
                    }   
                }
          
                if (ChkPrevious.isSelected()) {
                    if (CmbPrevious.getSelectedIndex() == 0 && !(questionsBean.getYear().equals("") || questionsBean.getYear().equals(" ") || questionsBean.getYear().equals(null))) {
                        tempQueP.add(questionsBean);
                    } else if (CmbPrevious.getSelectedIndex() == 1 && (questionsBean.getYear().equals("") || questionsBean.getYear().equals(" ") || questionsBean.getYear().equals(null))) {
                        tempQueP.add(questionsBean);
                    }
                }
            }
        }
        
        if(tempSortedQue.size()>0 && ChkLevel.isSelected()) {
            for(QuestionBean bean:tempSortedQue) {
                if(tempQueL.contains(bean)) {
                    tempSelection.add(bean);
                }
            }
            
            tempSortedQue = tempSelection;
            tempSelection=new ArrayList<QuestionBean>();
        }
        
        if(tempSortedQue.size() > 0 && ChkType.isSelected()) {
            for(QuestionBean bean:tempSortedQue) {
                if(tempQueT.contains(bean)) {
                    tempSelection.add(bean);
                }
            }
            tempSortedQue = tempSelection;
            tempSelection = new ArrayList<QuestionBean>();
        }
        
        if(tempSortedQue.size()>0 && ChkUsed.isSelected()) {
            for(QuestionBean bean:tempSortedQue) {
                if(tempQueU.contains(bean)) {
                    tempSelection.add(bean);
                }
            }
            tempSortedQue=tempSelection;
            tempSelection=new ArrayList<QuestionBean>();
        }
        
        if(tempSortedQue.size()>0 && ChkPrevious.isSelected()) {
            for(QuestionBean bean:tempSortedQue) {
                if(tempQueP.contains(bean)) {
                    tempSelection.add(bean);
                }
            }
            tempSortedQue=tempSelection;
            tempSelection=new ArrayList<QuestionBean>();
        }
        
        if(tempSortedQue.size() > 0) 
            isQuestion = true;
        else
            isQuestion = false;
        
        LblCount.setText("COUNT: " + tempSortedQue.size());
            
        addedSequence = new ArrayList<Integer>();
        int i=0;
        sortedQuestionsList = new ArrayList<QuestionBean>();
        for (QuestionBean bean : tempSortedQue) {
            if(selectedQuestionsList.contains(bean)) {
                bean.setSelected(true);
                addedSequence.add(i);
            }
            sortedQuestionsList.add(bean);
            i++;
        }
        
        if(shuffleValue == 1 && addedSequence.size() != 0) {
            Collections.shuffle(addedSequence);
        }
//        currentIndex = 0;
        setPanel(true);
    }
    
    public void setPanel(boolean flag) {
        if(isQuestion) {
            QuestionBean questionBean=sortedQuestionsList.get(currentIndex);
            currentPanel = (currentPanel == PanelQues1) ? PanelQues2 : PanelQues1;
    //        //set Question       
            currentPanel.setQuestionsOnPanel(questionBean, (currentIndex + 1),imageRatioList);
            TxtFldQuestionNumber.setText((currentIndex + 1) + "");
            //slide panel    
            MiddleBodyPanel.nextSlidPanel(1, currentPanel, flag);
            MiddleBodyPanel.refresh();
            if (questionBean.isSelected()) {
                BtnAddRemove.setText("Remove");
                BtnAddRemove.setForeground(Color.red);
            } else {
                BtnAddRemove.setText("Add");
                BtnAddRemove.setForeground(Color.black);
            }
            setButtonOnPanel(RightBodyPanel, LeftBodyPanel);
        } else {
            MiddleBodyPanel.nextSlidPanel(1, noQuestionPanel1, flag);
            MiddleBodyPanel.refresh();
            setButtonOnPanel(RightBodyPanel, LeftBodyPanel);
        }
    }

    public void setQuestion(String actionCommand) {
        currentIndex = Integer.parseInt(actionCommand.split(" ")[1]);
        setPanel(false);
    }

    public void setButtonOnPanel(JPanel allQuestionsPanel, JPanel selectedQuestionsPanel) {
        RightBodyPanel.removeAll();
        LeftBodyPanel.removeAll();
        nonSelectedButtonsArray = new JButton[sortedQuestionsList.size()];
        selectedButtonsArray = new JButton[sortedQuestionsList.size()];
        int i, j;
        i = j = 0;
        for (int x = 0; x < sortedQuestionsList.size(); x++) {
            if (sortedQuestionsList.get(x).isSelected()) {
                selectedButtonsArray[i] = new javax.swing.JButton();
                selectedButtonsArray[i].setActionCommand(subjectId + " " + x);
                selectedButtonsArray[i].setText("" + (x + 1));
                selectedButtonsArray[i].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        setQuestion(e.getActionCommand());
                    }
                });
                i++;
            } else {
                nonSelectedButtonsArray[j] = new javax.swing.JButton();
                nonSelectedButtonsArray[j].setActionCommand(subjectId + " " + x);
                nonSelectedButtonsArray[j].setText("" + (x + 1));
                nonSelectedButtonsArray[j].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        setQuestion(e.getActionCommand());
                    }
                });
                j++;
            }
        }
        GridBagConstraints cons = new GridBagConstraints();
        cons.gridx = 0;
        cons.gridy = 0;
        cons.gridwidth = 1;
        cons.gridheight = 1;
        cons.anchor = GridBagConstraints.BELOW_BASELINE_LEADING;
        cons.weightx = 2;
        cons.weighty = 1;
        cons.insets = new java.awt.Insets(1, 3, 1, 3);
        GridBagLayout layout = new GridBagLayout();
        for (int x = 0; x < i; x++) {
            int uu = addedSequence.get(x);
            uu++;
            String uus = uu + "";
            if (x % 20 == 0) {
                cons.gridx++;
                cons.gridy = 0;
            }
            for (int y = 0; y < addedSequence.size(); y++) {
                if (selectedButtonsArray[y].getText().equalsIgnoreCase(uus)) {
                    layout.setConstraints(selectedButtonsArray[y], cons);
                    selectedQuestionsPanel.setLayout(layout);
                    selectedQuestionsPanel.add(selectedButtonsArray[y], cons);
                    cons.gridy++;
                } else {
                }
            }
        }

        cons.gridx = 0;
        cons.gridy = 0;
        cons.gridwidth = 1;
        cons.gridheight = 1;
        cons.anchor = GridBagConstraints.BELOW_BASELINE_LEADING;
        cons.weightx = 2;
        cons.weighty = 1;
        cons.insets = new java.awt.Insets(1, 3, 1, 3);
        layout = new GridBagLayout();
        for (int x = 0; x < j; x++) {
            if (x % 20 == 0) {
                cons.gridx++;
                cons.gridy = 0;
            }
            layout.setConstraints(nonSelectedButtonsArray[x], cons);
            allQuestionsPanel.setLayout(layout);
            allQuestionsPanel.add(nonSelectedButtonsArray[x], cons);
            cons.gridy++;
        }
        LeftBodyPanel.validate();
        LeftBodyPanel.repaint();
        RightBodyPanel.validate();
        RightBodyPanel.repaint();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGroupAnimation = new javax.swing.ButtonGroup();
        BodyBottomPanel = new javax.swing.JPanel();
        BtnAddRemove = new javax.swing.JButton();
        BtnAdvanceSelection = new javax.swing.JButton();
        BtnShuffle = new javax.swing.JButton();
        BtnHome = new javax.swing.JButton();
        BtnPdf = new javax.swing.JButton();
        BtnWord = new javax.swing.JButton();
        BtnSaveTest = new javax.swing.JButton();
        MiiddleBodyScrollPane = new javax.swing.JScrollPane();
        MiddleBodyPanel = new com.ui.support.pages.JPanelsSliding();
        PanelQues1 = new com.ui.support.pages.QuestionPanel();
        PanelQues2 = new com.ui.support.pages.QuestionPanel();
        noQuestionPanel1 = new com.ui.support.pages.NoQuestionPanel();
        RightBodyScrollPane = new javax.swing.JScrollPane();
        RightBodyPanel = new javax.swing.JPanel();
        LeftBodyScrollPane = new javax.swing.JScrollPane();
        LeftBodyPanel = new javax.swing.JPanel();
        FooterPanel = new javax.swing.JPanel();
        SubFooterPanel = new javax.swing.JPanel();
        TxtFldStart = new javax.swing.JTextField();
        TxtFldEnd = new javax.swing.JTextField();
        BtnRandomAdd = new javax.swing.JButton();
        ChbkRandom = new javax.swing.JCheckBox();
        BtnRemoveAll = new javax.swing.JButton();
        BtnChangeChapters = new javax.swing.JButton();
        BtnModify = new javax.swing.JButton();
        BtnFirst = new javax.swing.JButton();
        BtnPrevious = new javax.swing.JButton();
        TxtFldQuestionNumber = new javax.swing.JTextField();
        BtnNext = new javax.swing.JButton();
        BtnLast = new javax.swing.JButton();
        HeaderPanel = new javax.swing.JPanel();
        SubHeaderPanel = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        CmbLevel = new javax.swing.JComboBox();
        ChkLevel = new javax.swing.JCheckBox();
        LblSort = new javax.swing.JLabel();
        ChkType = new javax.swing.JCheckBox();
        CmbType = new javax.swing.JComboBox();
        ChkUsed = new javax.swing.JCheckBox();
        CmbUsed = new javax.swing.JComboBox();
        ChkPrevious = new javax.swing.JCheckBox();
        CmbPrevious = new javax.swing.JComboBox();
        LblNonSelectedQues = new javax.swing.JLabel();
        CmbChapters = new javax.swing.JComboBox();
        LblChapterName = new javax.swing.JLabel();
        LblCount = new javax.swing.JLabel();
        LblSubject = new javax.swing.JLabel();
        CmbSubject = new javax.swing.JComboBox();
        LblSelectedCount = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("CruncherSoft's Medical CET+JEE Software 2014");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        BodyBottomPanel.setBackground(new java.awt.Color(240, 255, 255));
        BodyBottomPanel.setName("BodyBottomPanel"); // NOI18N

        BtnAddRemove.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        BtnAddRemove.setText("Add");
        BtnAddRemove.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BtnAddRemove.setName("BtnAddRemove"); // NOI18N
        BtnAddRemove.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnAddRemoveMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnAddRemoveMouseExited(evt);
            }
        });
        BtnAddRemove.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                BtnAddRemoveItemStateChanged(evt);
            }
        });
        BtnAddRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAddRemoveActionPerformed(evt);
            }
        });
        BtnAddRemove.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAddRemoveKeyPressed(evt);
            }
        });
        BodyBottomPanel.add(BtnAddRemove);

        BtnAdvanceSelection.setFont(new java.awt.Font("Microsoft JhengHei", 1, 13)); // NOI18N
        BtnAdvanceSelection.setText("Advanced Selection");
        BtnAdvanceSelection.setName("BtnAdvanceSelection"); // NOI18N
        BtnAdvanceSelection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAdvanceSelectionActionPerformed(evt);
            }
        });
        BodyBottomPanel.add(BtnAdvanceSelection);

        BtnShuffle.setFont(new java.awt.Font("Microsoft JhengHei", 1, 13)); // NOI18N
        BtnShuffle.setText("Shuffle");
        BtnShuffle.setName("BtnShuffle"); // NOI18N
        BtnShuffle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnShuffleActionPerformed(evt);
            }
        });
        BodyBottomPanel.add(BtnShuffle);

        BtnHome.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        BtnHome.setText("Home");
        BtnHome.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BtnHome.setName("BtnHome"); // NOI18N
        BtnHome.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                BtnHomeItemStateChanged(evt);
            }
        });
        BtnHome.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnHomeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnHomeMouseExited(evt);
            }
        });
        BtnHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHomeActionPerformed(evt);
            }
        });
        BodyBottomPanel.add(BtnHome);

        BtnPdf.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        BtnPdf.setText("Export Pdf");
        BtnPdf.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BtnPdf.setName("BtnPdf"); // NOI18N
        BtnPdf.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnPdfMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnPdfMouseExited(evt);
            }
        });
        BtnPdf.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                BtnPdfItemStateChanged(evt);
            }
        });
        BtnPdf.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPdfActionPerformed(evt);
            }
        });
        BtnPdf.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPdfKeyPressed(evt);
            }
        });
        BodyBottomPanel.add(BtnPdf);

        BtnWord.setFont(new java.awt.Font("Microsoft JhengHei", 1, 13)); // NOI18N
        BtnWord.setText("Export Word");
        BtnWord.setName("BtnWord"); // NOI18N
        BtnWord.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnWordActionPerformed(evt);
            }
        });
        BodyBottomPanel.add(BtnWord);

        BtnSaveTest.setFont(new java.awt.Font("Microsoft JhengHei", 1, 13)); // NOI18N
        BtnSaveTest.setText("SaveTest");
        BtnSaveTest.setName("BtnSaveTest"); // NOI18N
        BtnSaveTest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSaveTestActionPerformed(evt);
            }
        });
        BodyBottomPanel.add(BtnSaveTest);

        MiiddleBodyScrollPane.setName("MiiddleBodyScrollPane"); // NOI18N

        MiddleBodyPanel.setBackground(new java.awt.Color(233, 238, 255));
        MiddleBodyPanel.setName("MiddleBodyPanel"); // NOI18N
        MiddleBodyPanel.setLayout(new java.awt.CardLayout());

        PanelQues1.setBackground(new java.awt.Color(185, 225, 254));
        PanelQues1.setFont(new java.awt.Font("Centaur", 0, 11)); // NOI18N
        PanelQues1.setName("PanelQues1"); // NOI18N
        MiddleBodyPanel.add(PanelQues1, "card2");

        PanelQues2.setBackground(new java.awt.Color(185, 225, 254));
        PanelQues2.setName("PanelQues2"); // NOI18N
        MiddleBodyPanel.add(PanelQues2, "card3");

        noQuestionPanel1.setName("noQuestionPanel1"); // NOI18N
        MiddleBodyPanel.add(noQuestionPanel1, "card4");

        MiiddleBodyScrollPane.setViewportView(MiddleBodyPanel);

        RightBodyScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        RightBodyScrollPane.setName("RightBodyScrollPane"); // NOI18N

        RightBodyPanel.setBackground(new java.awt.Color(255, 255, 255));
        RightBodyPanel.setName("RightBodyPanel"); // NOI18N

        javax.swing.GroupLayout RightBodyPanelLayout = new javax.swing.GroupLayout(RightBodyPanel);
        RightBodyPanel.setLayout(RightBodyPanelLayout);
        RightBodyPanelLayout.setHorizontalGroup(
            RightBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 150, Short.MAX_VALUE)
        );
        RightBodyPanelLayout.setVerticalGroup(
            RightBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 514, Short.MAX_VALUE)
        );

        RightBodyScrollPane.setViewportView(RightBodyPanel);

        LeftBodyScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        LeftBodyScrollPane.setName("LeftBodyScrollPane"); // NOI18N

        LeftBodyPanel.setBackground(new java.awt.Color(255, 255, 255));
        LeftBodyPanel.setName("LeftBodyPanel"); // NOI18N

        javax.swing.GroupLayout LeftBodyPanelLayout = new javax.swing.GroupLayout(LeftBodyPanel);
        LeftBodyPanel.setLayout(LeftBodyPanelLayout);
        LeftBodyPanelLayout.setHorizontalGroup(
            LeftBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 150, Short.MAX_VALUE)
        );
        LeftBodyPanelLayout.setVerticalGroup(
            LeftBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 514, Short.MAX_VALUE)
        );

        LeftBodyScrollPane.setViewportView(LeftBodyPanel);

        FooterPanel.setBackground(new java.awt.Color(240, 248, 255));
        FooterPanel.setName("FooterPanel"); // NOI18N

        SubFooterPanel.setName("SubFooterPanel"); // NOI18N

        TxtFldStart.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 13)); // NOI18N
        TxtFldStart.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TxtFldStart.setName("TxtFldStart"); // NOI18N
        TxtFldStart.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TxtFldStartKeyPressed(evt);
            }
        });

        TxtFldEnd.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 13)); // NOI18N
        TxtFldEnd.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TxtFldEnd.setName("TxtFldEnd"); // NOI18N
        TxtFldEnd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TxtFldEndKeyPressed(evt);
            }
        });

        BtnRandomAdd.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        BtnRandomAdd.setText("Add");
        BtnRandomAdd.setName("BtnRandomAdd"); // NOI18N
        BtnRandomAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRandomAddActionPerformed(evt);
            }
        });

        ChbkRandom.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        ChbkRandom.setText("Random");
        ChbkRandom.setName("ChbkRandom"); // NOI18N
        ChbkRandom.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChbkRandomItemStateChanged(evt);
            }
        });

        BtnRemoveAll.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        BtnRemoveAll.setText("Remove All");
        BtnRemoveAll.setName("BtnRemoveAll"); // NOI18N
        BtnRemoveAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRemoveAllActionPerformed(evt);
            }
        });

        BtnChangeChapters.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        BtnChangeChapters.setText("Change Chapters");
        BtnChangeChapters.setName("BtnChangeChapters"); // NOI18N
        BtnChangeChapters.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnChangeChaptersActionPerformed(evt);
            }
        });

        BtnModify.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        BtnModify.setText("Modify");
        BtnModify.setName("BtnModify"); // NOI18N
        BtnModify.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnModifyActionPerformed(evt);
            }
        });

        BtnFirst.setBackground(new java.awt.Color(255, 255, 255));
        BtnFirst.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first.png"))); // NOI18N
        BtnFirst.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BtnFirst.setMargin(new java.awt.Insets(0, 0, 0, 0));
        BtnFirst.setName("BtnFirst"); // NOI18N
        BtnFirst.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnFirstMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnFirstMouseExited(evt);
            }
        });
        BtnFirst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnFirstActionPerformed(evt);
            }
        });

        BtnPrevious.setBackground(new java.awt.Color(255, 255, 255));
        BtnPrevious.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/prev.png"))); // NOI18N
        BtnPrevious.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BtnPrevious.setIconTextGap(0);
        BtnPrevious.setMargin(new java.awt.Insets(0, 0, 0, 0));
        BtnPrevious.setName("BtnPrevious"); // NOI18N
        BtnPrevious.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnPreviousMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnPreviousMouseExited(evt);
            }
        });
        BtnPrevious.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPreviousActionPerformed(evt);
            }
        });

        TxtFldQuestionNumber.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 13)); // NOI18N
        TxtFldQuestionNumber.setText("0000");
        TxtFldQuestionNumber.setName("TxtFldQuestionNumber"); // NOI18N
        TxtFldQuestionNumber.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                TxtFldQuestionNumberMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                TxtFldQuestionNumberMouseExited(evt);
            }
        });
        TxtFldQuestionNumber.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TxtFldQuestionNumberKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtFldQuestionNumberKeyReleased(evt);
            }
        });

        BtnNext.setBackground(new java.awt.Color(255, 255, 255));
        BtnNext.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/next.png"))); // NOI18N
        BtnNext.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BtnNext.setMargin(new java.awt.Insets(0, 0, 0, 0));
        BtnNext.setName("BtnNext"); // NOI18N
        BtnNext.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnNextMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnNextMouseExited(evt);
            }
        });
        BtnNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnNextActionPerformed(evt);
            }
        });

        BtnLast.setBackground(new java.awt.Color(255, 255, 255));
        BtnLast.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last.png"))); // NOI18N
        BtnLast.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BtnLast.setMargin(new java.awt.Insets(0, 0, 0, 0));
        BtnLast.setName("BtnLast"); // NOI18N
        BtnLast.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnLastMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnLastMouseExited(evt);
            }
        });
        BtnLast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnLastActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout SubFooterPanelLayout = new javax.swing.GroupLayout(SubFooterPanel);
        SubFooterPanel.setLayout(SubFooterPanelLayout);
        SubFooterPanelLayout.setHorizontalGroup(
            SubFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, SubFooterPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(BtnFirst)
                .addGap(5, 5, 5)
                .addComponent(BtnPrevious)
                .addGap(5, 5, 5)
                .addComponent(TxtFldQuestionNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(BtnNext)
                .addGap(5, 5, 5)
                .addComponent(BtnLast)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ChbkRandom)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(TxtFldStart, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TxtFldEnd, javax.swing.GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE)
                .addGap(5, 5, 5)
                .addComponent(BtnRandomAdd)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BtnRemoveAll)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BtnChangeChapters)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BtnModify)
                .addGap(5, 5, 5))
        );
        SubFooterPanelLayout.setVerticalGroup(
            SubFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, SubFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(BtnFirst)
                .addComponent(BtnPrevious)
                .addGroup(SubFooterPanelLayout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(TxtFldQuestionNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addComponent(BtnNext)
                .addComponent(BtnLast))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, SubFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(SubFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BtnRemoveAll)
                    .addComponent(BtnRandomAdd)
                    .addComponent(BtnChangeChapters)
                    .addComponent(BtnModify))
                .addGroup(SubFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TxtFldStart, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TxtFldEnd, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ChbkRandom)))
        );

        FooterPanel.add(SubFooterPanel);

        HeaderPanel.setBackground(new java.awt.Color(240, 255, 255));
        HeaderPanel.setName("HeaderPanel"); // NOI18N

        SubHeaderPanel.setName("SubHeaderPanel"); // NOI18N

        jPanel7.setName("jPanel7"); // NOI18N

        CmbLevel.setFont(new java.awt.Font("Microsoft JhengHei", 1, 14)); // NOI18N
        CmbLevel.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Easy", "Medium", "Hard" }));
        CmbLevel.setEnabled(false);
        CmbLevel.setName("CmbLevel"); // NOI18N
        CmbLevel.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmbLevelItemStateChanged(evt);
            }
        });
        CmbLevel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CmbLevelActionPerformed(evt);
            }
        });

        ChkLevel.setFont(new java.awt.Font("Microsoft JhengHei", 1, 14)); // NOI18N
        ChkLevel.setForeground(new java.awt.Color(255, 0, 0));
        ChkLevel.setText("Level :");
        ChkLevel.setName("ChkLevel"); // NOI18N
        ChkLevel.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkLevelItemStateChanged(evt);
            }
        });

        LblSort.setFont(new java.awt.Font("Microsoft JhengHei", 1, 14)); // NOI18N
        LblSort.setForeground(new java.awt.Color(255, 0, 0));
        LblSort.setText("Sort :");
        LblSort.setName("LblSort"); // NOI18N

        ChkType.setFont(new java.awt.Font("Microsoft JhengHei", 1, 14)); // NOI18N
        ChkType.setForeground(new java.awt.Color(255, 0, 0));
        ChkType.setText("Type :");
        ChkType.setName("ChkType"); // NOI18N
        ChkType.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkTypeItemStateChanged(evt);
            }
        });

        CmbType.setFont(new java.awt.Font("Microsoft JhengHei", 1, 14)); // NOI18N
        CmbType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Theoretical", "Numerical" }));
        CmbType.setEnabled(false);
        CmbType.setName("CmbType"); // NOI18N
        CmbType.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmbTypeItemStateChanged(evt);
            }
        });

        ChkUsed.setFont(new java.awt.Font("Microsoft JhengHei", 1, 14)); // NOI18N
        ChkUsed.setForeground(new java.awt.Color(255, 0, 0));
        ChkUsed.setText("Used :");
        ChkUsed.setName("ChkUsed"); // NOI18N
        ChkUsed.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkUsedItemStateChanged(evt);
            }
        });

        CmbUsed.setFont(new java.awt.Font("Microsoft JhengHei", 1, 14)); // NOI18N
        CmbUsed.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Used", "Not Used" }));
        CmbUsed.setEnabled(false);
        CmbUsed.setName("CmbUsed"); // NOI18N
        CmbUsed.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmbUsedItemStateChanged(evt);
            }
        });

        ChkPrevious.setFont(new java.awt.Font("Microsoft JhengHei", 1, 14)); // NOI18N
        ChkPrevious.setForeground(new java.awt.Color(255, 0, 0));
        ChkPrevious.setText("Previously Asked");
        ChkPrevious.setName("ChkPrevious"); // NOI18N
        ChkPrevious.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkPreviousItemStateChanged(evt);
            }
        });

        CmbPrevious.setFont(new java.awt.Font("Microsoft JhengHei", 1, 14)); // NOI18N
        CmbPrevious.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Asked", "Not Asked" }));
        CmbPrevious.setEnabled(false);
        CmbPrevious.setName("CmbPrevious"); // NOI18N
        CmbPrevious.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmbPreviousItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(LblSort)
                .addGap(18, 18, 18)
                .addComponent(ChkLevel)
                .addGap(18, 18, 18)
                .addComponent(CmbLevel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(ChkType)
                .addGap(18, 18, 18)
                .addComponent(CmbType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(ChkUsed)
                .addGap(18, 18, 18)
                .addComponent(CmbUsed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(ChkPrevious)
                .addGap(18, 18, 18)
                .addComponent(CmbPrevious, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblSort)
                    .addComponent(CmbLevel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ChkType)
                    .addComponent(CmbType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ChkUsed)
                    .addComponent(CmbUsed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ChkPrevious)
                    .addComponent(CmbPrevious, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ChkLevel))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        SubHeaderPanel.add(jPanel7);

        LblNonSelectedQues.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 12)); // NOI18N
        LblNonSelectedQues.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblNonSelectedQues.setText("Non Selected Questions");
        LblNonSelectedQues.setName("LblNonSelectedQues"); // NOI18N

        CmbChapters.setFont(new java.awt.Font("Microsoft JhengHei", 1, 14)); // NOI18N
        CmbChapters.setName("CmbChapters"); // NOI18N
        CmbChapters.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmbChaptersItemStateChanged(evt);
            }
        });
        CmbChapters.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
                CmbChaptersCaretPositionChanged(evt);
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
            }
        });

        LblChapterName.setFont(new java.awt.Font("Microsoft JhengHei", 1, 18)); // NOI18N
        LblChapterName.setText("Chapters :");
        LblChapterName.setName("LblChapterName"); // NOI18N

        LblCount.setFont(new java.awt.Font("Microsoft JhengHei", 1, 18)); // NOI18N
        LblCount.setText("Count :");
        LblCount.setName("LblCount"); // NOI18N

        LblSubject.setFont(new java.awt.Font("Microsoft JhengHei", 1, 18)); // NOI18N
        LblSubject.setText("Subject :");
        LblSubject.setName("LblSubject"); // NOI18N

        CmbSubject.setFont(new java.awt.Font("Microsoft JhengHei", 1, 14)); // NOI18N
        CmbSubject.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Physics", "Chemistry", "Mathematics" }));
        CmbSubject.setMinimumSize(new java.awt.Dimension(88, 26));
        CmbSubject.setName("CmbSubject"); // NOI18N
        CmbSubject.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmbSubjectItemStateChanged(evt);
            }
        });

        LblSelectedCount.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        LblSelectedCount.setText("Selected Questions: 0");
        LblSelectedCount.setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
        LblSelectedCount.setName("LblSelectedCount"); // NOI18N
        LblSelectedCount.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                LblSelectedCountMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                LblSelectedCountMouseExited(evt);
            }
        });

        javax.swing.GroupLayout HeaderPanelLayout = new javax.swing.GroupLayout(HeaderPanel);
        HeaderPanel.setLayout(HeaderPanelLayout);
        HeaderPanelLayout.setHorizontalGroup(
            HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(HeaderPanelLayout.createSequentialGroup()
                .addGroup(HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(HeaderPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, HeaderPanelLayout.createSequentialGroup()
                                .addComponent(LblSubject)
                                .addGap(18, 18, 18)
                                .addComponent(CmbSubject, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(LblChapterName)
                                .addGap(18, 18, 18)
                                .addComponent(CmbChapters, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 514, Short.MAX_VALUE)
                                .addComponent(LblCount))
                            .addComponent(SubHeaderPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                    .addGroup(HeaderPanelLayout.createSequentialGroup()
                        .addComponent(LblSelectedCount)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(LblNonSelectedQues)))
                .addContainerGap())
        );
        HeaderPanelLayout.setVerticalGroup(
            HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(HeaderPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CmbChapters, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblChapterName)
                    .addComponent(LblCount)
                    .addComponent(LblSubject)
                    .addComponent(CmbSubject, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(SubHeaderPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(HeaderPanelLayout.createSequentialGroup()
                        .addComponent(LblNonSelectedQues, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, HeaderPanelLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(LblSelectedCount))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(FooterPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(HeaderPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(LeftBodyScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(MiiddleBodyScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(RightBodyScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(BodyBottomPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(HeaderPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(LeftBodyScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE)
                    .addComponent(RightBodyScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE)
                    .addComponent(MiiddleBodyScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BodyBottomPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(FooterPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnNextActionPerformed(java.awt.event.ActionEvent evt, boolean b) {
        int last=sortedQuestionsList.size()-1;
        if (currentIndex == last) {
            currentIndex = 0;
            setPanel(false);
        } else {
            JOptionPane.showMessageDialog(null, "This is Last Question.");
        }
    }

private void BtnAddRemoveItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_BtnAddRemoveItemStateChanged
}//GEN-LAST:event_BtnAddRemoveItemStateChanged
private void BtnAddRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAddRemoveActionPerformed
    if(isQuestion) {
        int last=sortedQuestionsList.size()-1;
        int i = currentIndex;
        if ((currentIndex <= last) && (currentIndex >= 0)) {
            if (BtnAddRemove.getText().equals("Add")) {
                sortedQuestionsList.get(currentIndex).setSelected(true);
                totalSelectedQueCount++;
                addedSequence.add(currentIndex);
                selectedQuestionsList.add(sortedQuestionsList.get(currentIndex));
            } else {
                sortedQuestionsList.get(currentIndex).setSelected(false);
                int index = selectedQuestionsList.indexOf(sortedQuestionsList.get(currentIndex));
                selectedQuestionsList.remove(index);
                int iii = addedSequence.indexOf(currentIndex);
                addedSequence.remove(iii);
                totalSelectedQueCount--;
                if(selectedQuestionsList.isEmpty())
                    shuffleValue = 0;
            }
            LblSelectedCount.setText("Selected Questions : " + totalSelectedQueCount);
            setButtonOnPanel(RightBodyPanel, LeftBodyPanel);
            LeftBodyPanel.validate();
            LeftBodyPanel.repaint();
            RightBodyPanel.validate();
            RightBodyPanel.repaint();
        }
        if (i < last) {
            BtnNextActionPerformed(evt);
        } else if (i == last) {
            boolean b = true;
            BtnNextActionPerformed(evt, b);
        }
    }
}//GEN-LAST:event_BtnAddRemoveActionPerformed
private void BtnHomeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_BtnHomeItemStateChanged
// TODO add your handling code here:
}//GEN-LAST:event_BtnHomeItemStateChanged
private void BtnHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHomeActionPerformed
    new HomePage().setVisible(true);
    this.dispose();
}//GEN-LAST:event_BtnHomeActionPerformed
private void BtnAddRemoveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnAddRemoveMouseEntered
    BtnAddRemove.setForeground(Color.red);
}//GEN-LAST:event_BtnAddRemoveMouseEntered
private void BtnAddRemoveMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnAddRemoveMouseExited
    BtnAddRemove.setForeground(Color.black);
}//GEN-LAST:event_BtnAddRemoveMouseExited
private void BtnHomeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnHomeMouseEntered
    BtnHome.setForeground(Color.red);
}//GEN-LAST:event_BtnHomeMouseEntered
private void BtnHomeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnHomeMouseExited
    BtnHome.setForeground(Color.black);
}//GEN-LAST:event_BtnHomeMouseExited
private void BtnAddRemoveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAddRemoveKeyPressed
}//GEN-LAST:event_BtnAddRemoveKeyPressed

private void LblSelectedCountMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblSelectedCountMouseEntered
}//GEN-LAST:event_LblSelectedCountMouseEntered

private void LblSelectedCountMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblSelectedCountMouseExited
}//GEN-LAST:event_LblSelectedCountMouseExited

private void BtnPdfMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnPdfMouseEntered
// TODO add your handling code here:
}//GEN-LAST:event_BtnPdfMouseEntered

private void BtnPdfMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnPdfMouseExited
// TODO add your handling code here:
}//GEN-LAST:event_BtnPdfMouseExited

private void BtnPdfItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_BtnPdfItemStateChanged
// TODO add your handling code here:
}//GEN-LAST:event_BtnPdfItemStateChanged

private void BtnPdfActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPdfActionPerformed
    if(!selectedQuestionsList.isEmpty()) {
        ArrayList<Integer> chapterIdList = null;
        for(QuestionBean bean : selectedQuestionsList) {
            if(chapterIdList == null)
                chapterIdList = new ArrayList<Integer>();
            if(!chapterIdList.contains(bean.getChapterId()))
                chapterIdList.add(bean.getChapterId());
        }

        ArrayList<ChapterBean> sortedChapterList = null;
        if(chapterIdList != null)
            sortedChapterList = new ChapterOperation().getChaptersList(chapterIdList);
        if(shuffleValue == 1)
            Collections.shuffle(selectedQuestionsList);
        new PdfPageSetup(selectedQuestionsList,subjectsList,sortedChapterList,(Object)this, questionsList, printingPaperType).setVisible(true);
        this.setEnabled(false);
    } else {
        JOptionPane.showMessageDialog(this, "Please Add Questions for Printing.");
    }
}//GEN-LAST:event_BtnPdfActionPerformed

private void BtnPdfKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPdfKeyPressed
// TODO add your handling code here:
}//GEN-LAST:event_BtnPdfKeyPressed

private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
    Object[] options = {"YES", "CANCEL"};
    int i = JOptionPane.showOptionDialog(null, "Are You Sure to Cancel Test?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
    if (i == 0) {
        new HomePage().setVisible(true);
        this.dispose();
    }
}//GEN-LAST:event_formWindowClosing

private void ChkLevelItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkLevelItemStateChanged
    if (ChkLevel.isSelected()) {
        CmbLevel.setEnabled(true);
    } else {
        CmbLevel.setEnabled(false);
    }
    currentIndex = 0;
    setQuesType();
}//GEN-LAST:event_ChkLevelItemStateChanged

private void ChkTypeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkTypeItemStateChanged
    if (ChkType.isSelected()) {
        CmbType.setEnabled(true);
    } else {
        CmbType.setEnabled(false);
    }
    currentIndex = 0;
    setQuesType();
}//GEN-LAST:event_ChkTypeItemStateChanged

private void ChkUsedItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkUsedItemStateChanged
    if (ChkUsed.isSelected()) {
        CmbUsed.setEnabled(true);
    } else {
        CmbUsed.setEnabled(false);
    }
    currentIndex = 0;
    setQuesType();

}//GEN-LAST:event_ChkUsedItemStateChanged

private void ChkPreviousItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkPreviousItemStateChanged
    if (ChkPrevious.isSelected()) {
        CmbPrevious.setEnabled(true);
    } else {
        CmbPrevious.setEnabled(false);
    }
    currentIndex = 0;
    setQuesType();
}//GEN-LAST:event_ChkPreviousItemStateChanged

private void CmbTypeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmbTypeItemStateChanged
    if (ChkType.isSelected()) {
        currentIndex = 0;
        setQuesType();
    }
}//GEN-LAST:event_CmbTypeItemStateChanged

private void CmbUsedItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmbUsedItemStateChanged
    if (ChkUsed.isSelected()) {
        currentIndex = 0;
        setQuesType();
    }
}//GEN-LAST:event_CmbUsedItemStateChanged

private void CmbPreviousItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmbPreviousItemStateChanged
    if (ChkPrevious.isSelected()) {
        currentIndex = 0;
        setQuesType();
    }
}//GEN-LAST:event_CmbPreviousItemStateChanged

private void CmbLevelItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmbLevelItemStateChanged
    if (ChkLevel.isSelected()) {
        currentIndex = 0;
        setQuesType();
    }
}//GEN-LAST:event_CmbLevelItemStateChanged

private void CmbChaptersItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmbChaptersItemStateChanged
    if (evt.getStateChange() == java.awt.event.ItemEvent.SELECTED) {
        if(CmbChapters.getSelectedIndex() != -1) {
            chapterWiseQuestionsList.clear();
            chapterId = sortedCountChapterList.get(CmbChapters.getSelectedIndex()).getChapterBean().getChapterId();
            for(QuestionBean questionBean : questionsList) {
                if(questionBean.getChapterId() == chapterId && questionBean.getSubjectId() == subjectId)
                    chapterWiseQuestionsList.add(questionBean);
            }
            currentIndex = 0;
            setQuesType();
        }
    }
    
    /*
    if(CmbChapters.getSelectedIndex()!=-1) {
        
            chapterWiseQuestionsList.clear();
            String str = CmbChapters.getSelectedItem().toString();
        
            String[] tmpStr = str.split("\\(");
            str = tmpStr[0].trim();
        
            for(ChapterBean bean : chaptersList) {
                if(bean.getChapterName().equals(str)) {
                    chapterId = bean.getChapterId();
                    break;
                }
            }
            
            for(QuestionBean questionBean : questionsList) {
                if(questionBean.getChapterId() == chapterId && questionBean.getSubjectId() == subjectId)
                    chapterWiseQuestionsList.add(questionBean);
            }
            setQuesType();
        }
        setButtonOnPanel(RightBodyPanel, LeftBodyPanel);
        LeftBodyPanel.validate();
        LeftBodyPanel.repaint();
        RightBodyPanel.validate();
        RightBodyPanel.repaint();*/
}//GEN-LAST:event_CmbChaptersItemStateChanged

    private void BtnShuffleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnShuffleActionPerformed
        if(isQuestion) {
            if (addedSequence.size() > 1) {
                Object[] options = {"YES", "NO"};
                int i = JOptionPane.showOptionDialog(null, "Sequence of question will be changed? Are you sure to continue?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
                if (i == 0) {
                    shuffleValue = 1;
                    Collections.shuffle(addedSequence);
                    setButtonOnPanel(RightBodyPanel, LeftBodyPanel);
                    JOptionPane.showMessageDialog(null, "Questions are shuffled.");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Unable to Shuffle, please add more questions.");
            }
        }
    }//GEN-LAST:event_BtnShuffleActionPerformed

    private void BtnAdvanceSelectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAdvanceSelectionActionPerformed
        if(isQuestion) {
            String message;
            if(selectedQuestionsList.size() != 0)
                message = "Removed All Currently Selected Questions? Are you Sure to Continue?";
            else 
                message = "Are you Sure to Continue?";
            
            Object[] options = {"YES", "CANCEL"};
            int oo = JOptionPane.showOptionDialog(this,message, "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
            if (oo == 0) {
                advSelectionQuesList = new ArrayList<QuestionBean>(); 
                for(CountChapterBean countChapterBean : countedChapterList){
                    ChapterBean chapterBean = countChapterBean.getChapterBean();
                    for(QuestionBean questionBean : questionsList) {
                        if(chapterBean.getChapterId() == questionBean.getChapterId())
                            advSelectionQuesList.add(questionBean);
                    }   
                }
            
                if(selectedQuestionsList.size() != 0)
                    removeAllQues();
            
                new AdvanceQuestionSelection(countedChapterList, advSelectionQuesList,this).setVisible(true);
                this.setEnabled(false);
            }
        }
    }//GEN-LAST:event_BtnAdvanceSelectionActionPerformed

    private void BtnWordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnWordActionPerformed
        // TODO add your handling code here:
        if(!selectedQuestionsList.isEmpty()) {
            if(shuffleValue == 1) 
                Collections.shuffle(selectedQuestionsList);
            new WordPageSetup(selectedQuestionsList, subjectsList, chaptersList,(Object)this, questionsList, printingPaperType).setVisible(true);
            this.setEnabled(false);
        } else {    
            JOptionPane.showMessageDialog(this, "Please Add Questions for Printing.");
        }
    }//GEN-LAST:event_BtnWordActionPerformed

    private void CmbSubjectItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmbSubjectItemStateChanged
        if (evt.getStateChange() == java.awt.event.ItemEvent.SELECTED) {
            if(CmbSubject.getSelectedIndex() != -1) {
                subjectId = subjectsList.get(CmbSubject.getSelectedIndex()).getSubjectId();
//                CmbChapters.removeAllItems();
                setChapterCombo();
            }
        }
//        if(CmbChapters.getSelectedIndex()!= -1) {
//            subjectId = subjectsList.get(CmbSubject.getSelectedIndex()).getSubjectId();
//            setChapterCombo();
//            CmbChapters.removeAllItems();
//            for(CountChapterBean countChapterBean :countedChapterList){
//                ChapterBean bean = countChapterBean.getChapterBean();
//                if(bean.getSubjectId() == subjectId){
//                    CmbChapters.addItem(bean.getChapterName()+" ("+countChapterBean.getTotalQuestions()+")");
//                }
//            }
//        }
    }//GEN-LAST:event_CmbSubjectItemStateChanged

    private void CmbLevelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CmbLevelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CmbLevelActionPerformed

    private void BtnLastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnLastActionPerformed
        if(isQuestion) {
            int last=sortedQuestionsList.size()-1;
            if (currentIndex < last) {
                currentIndex = last;
                setPanel(false);
            } else {
                JOptionPane.showMessageDialog(null, "This is Last Question.");
            }
        }
    }//GEN-LAST:event_BtnLastActionPerformed

    private void BtnLastMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnLastMouseExited

    }//GEN-LAST:event_BtnLastMouseExited

    private void BtnLastMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnLastMouseEntered

    }//GEN-LAST:event_BtnLastMouseEntered

    private void BtnNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnNextActionPerformed
        if(isQuestion) {
            int last=sortedQuestionsList.size()-1;
            if (currentIndex < last) {
                currentIndex++;
                setPanel(false);
            } else {
                JOptionPane.showMessageDialog(null, "This is Last Question.");
            }
        }
    }//GEN-LAST:event_BtnNextActionPerformed

    private void BtnNextMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnNextMouseExited

    }//GEN-LAST:event_BtnNextMouseExited

    private void BtnNextMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnNextMouseEntered

    }//GEN-LAST:event_BtnNextMouseEntered

    private void TxtFldQuestionNumberKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtFldQuestionNumberKeyReleased
        if (evt.getKeyCode() == 10) {
            if(!TxtFldQuestionNumber.getText().isEmpty()) {
                int i = Integer.parseInt(TxtFldQuestionNumber.getText());
                int size = sortedQuestionsList.size();
                if ((i < 1) || (i > size))
                    JOptionPane.showMessageDialog(rootPane, "Invalid Question Number");
                else
                    currentIndex = i - 1;
            } else {
                JOptionPane.showMessageDialog(rootPane, "Invalid Question Number");
            }
            setPanel(false);
        }
    }//GEN-LAST:event_TxtFldQuestionNumberKeyReleased

    private void TxtFldQuestionNumberMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TxtFldQuestionNumberMouseExited

    }//GEN-LAST:event_TxtFldQuestionNumberMouseExited

    private void TxtFldQuestionNumberMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TxtFldQuestionNumberMouseEntered

    }//GEN-LAST:event_TxtFldQuestionNumberMouseEntered

    private void BtnPreviousActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPreviousActionPerformed
        if(isQuestion) {
            if (currentIndex != 0) {
                currentIndex--;
                setPanel(true);
            } else {
                JOptionPane.showMessageDialog(null, "This is First Question.");
            }
        }
    }//GEN-LAST:event_BtnPreviousActionPerformed

    private void BtnPreviousMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnPreviousMouseExited

    }//GEN-LAST:event_BtnPreviousMouseExited

    private void BtnPreviousMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnPreviousMouseEntered

    }//GEN-LAST:event_BtnPreviousMouseEntered

    private void BtnFirstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnFirstActionPerformed
        if(isQuestion) {
            if (currentIndex != 0) {
                currentIndex = 0;
                setPanel(true);
            } else {
                JOptionPane.showMessageDialog(null, "This is First Question.");
            }
        }
    }//GEN-LAST:event_BtnFirstActionPerformed

    private void BtnFirstMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnFirstMouseExited

    }//GEN-LAST:event_BtnFirstMouseExited

    private void BtnFirstMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnFirstMouseEntered

    }//GEN-LAST:event_BtnFirstMouseEntered

    private void BtnModifyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnModifyActionPerformed
        if (isQuestion) {
            QuestionBean questionsBean = sortedQuestionsList.get(currentIndex);
            new ModifyQuestion(questionsBean , this , currentIndex).setVisible(true);
            this.setVisible(false);
        }
    }//GEN-LAST:event_BtnModifyActionPerformed

    public void loadAfterModification(QuestionBean questionsBean,boolean ratioUpdate) {
        if(questionsBean != null) {
            QuestionBean oldQuesBean = sortedQuestionsList.get(currentIndex); 
            questionsBean.setSelected(oldQuesBean.isSelected());
            int index1 = questionsList.indexOf(oldQuesBean);
            sortedQuestionsList.set(currentIndex, questionsBean);
            questionsList.set(index1, questionsBean);
            int index2 = selectedQuestionsList.indexOf(oldQuesBean);
            if(index2 != -1)
                selectedQuestionsList.set(index2, questionsBean);
            int index3 = chapterWiseQuestionsList.indexOf(oldQuesBean);
                chapterWiseQuestionsList.set(index3, questionsBean);
            if(ratioUpdate)
                imageRatioList = new ImageRatioOperation().getImageRatioList();
//            setPanel(false);
            setQuesType();
        }
    }
    
    private void BtnRemoveAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRemoveAllActionPerformed
        if(isQuestion) {
            if(selectedQuestionsList.size() != 0) {
                for (int i = 0; i < questionsList.size(); i++) {
                    questionsList.get(i).setSelected(false);
                }
                for(int i=0; i< sortedQuestionsList.size(); i++) {
                    sortedQuestionsList.get(i).setSelected(false);
                }
                shuffleValue = 0;
                totalSelectedQueCount = 0;
//                selectedQuestionsList = new ArrayList<QuestionsBean>();
                selectedQuestionsList.clear();
                BtnAddRemove.setText("Add");
                BtnAddRemove.setForeground(Color.black);
                TxtFldStart.setText("");
                TxtFldEnd.setText("");
//                addedSequence = new ArrayList<Integer>();
                addedSequence.clear();
                LblSelectedCount.setText("Selected Questions : " + totalSelectedQueCount);
                setButtonOnPanel(RightBodyPanel, LeftBodyPanel);
                LeftBodyPanel.validate();
                LeftBodyPanel.repaint();
                RightBodyPanel.validate();
                RightBodyPanel.repaint();
                BodyBottomPanel.validate();
                BodyBottomPanel.revalidate();
                BodyBottomPanel.repaint();
            } else {
                JOptionPane.showMessageDialog(null, "You Are Not Selected Any Questions.");
            }
        }
    }//GEN-LAST:event_BtnRemoveAllActionPerformed

    private void ChbkRandomItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChbkRandomItemStateChanged
        // TODO add your handling code here:
        if(isQuestion) {
            if (ChbkRandom.isSelected()) {
                TxtFldStart.setEnabled(false);
            } else {
                TxtFldStart.setEnabled(true);
            }
        }
    }//GEN-LAST:event_ChbkRandomItemStateChanged

    private void BtnRandomAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRandomAddActionPerformed
        if(isQuestion) {
            int strt = 0, end = 0;
            String str1 = TxtFldStart.getText();
            String str2 = TxtFldEnd.getText();
            TxtFldStart.setText("");
            TxtFldEnd.setText("");
            if (ChbkRandom.isSelected()) {
                try {
                    end = Integer.parseInt(str2);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "Please enter valid number");
                }

                ArrayList<QuestionBean> tempRemainingList = new ArrayList<QuestionBean>();
                for(QuestionBean bean : sortedQuestionsList) {
                    if(!bean.isSelected())
                    tempRemainingList.add(bean);
                }
                Collections.shuffle(tempRemainingList);

                if(end > 0 && selectedQuestionsList.size()==0 && end < sortedQuestionsList.size()+1) {
                    for(int i=0;i<end;i++) {
                        tempRemainingList.get(i).setSelected(true);
                        selectedQuestionsList.add(tempRemainingList.get(i));
                        int index1 = sortedQuestionsList.indexOf(tempRemainingList.get(i));
                        sortedQuestionsList.get(index1).setSelected(true);
                        addedSequence.add(index1);
                        totalSelectedQueCount++;
                    }
                } else {
                    if(end < tempRemainingList.size()+1 ) {
                        for(int i=0;i<end;i++) {
                            selectedQuestionsList.add(tempRemainingList.get(i));
                            int index1 = sortedQuestionsList.indexOf(tempRemainingList.get(i));
                            sortedQuestionsList.get(index1).setSelected(true);
                            addedSequence.add(index1);
                            totalSelectedQueCount++;
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Invalid range.\nYou Can Add ("+tempRemainingList.size()+") Questions.");
                    }
                }

                LblSelectedCount.setText("Selected Questions : " + totalSelectedQueCount);
                setButtonOnPanel(RightBodyPanel, LeftBodyPanel);
                LeftBodyPanel.validate();
                LeftBodyPanel.repaint();
                RightBodyPanel.validate();
                RightBodyPanel.repaint();
                setPanel(false);
                BodyBottomPanel.validate();
                BodyBottomPanel.revalidate();
                BodyBottomPanel.repaint();
            } else {
                try {
                    strt = Integer.parseInt(str1);
                    end = Integer.parseInt(str2);
                } catch (Exception e) {
                }

                if (strt > 0 && end > 0) {
                    if (strt < end) {
                        if(end < sortedQuestionsList.size()+1) {
                            for (int i = strt - 1; i < end; i++) {
                                if(!(selectedQuestionsList.contains(sortedQuestionsList.get(i)))) {
                                    sortedQuestionsList.get(i).setSelected(true);
                                    totalSelectedQueCount++;
                                    selectedQuestionsList.add(sortedQuestionsList.get(i));
                                    int index = questionsList.indexOf(sortedQuestionsList.get(i));
                                    questionsList.get(index).setSelected(true);
                                    addedSequence.add(i);
                                }
                            }
                            LblSelectedCount.setText("Selected Questions : " + totalSelectedQueCount);
                            setPanel(false);
                            setButtonOnPanel(RightBodyPanel,LeftBodyPanel);
                            LeftBodyPanel.validate();
                            LeftBodyPanel.repaint();
                            RightBodyPanel.validate();
                            RightBodyPanel.repaint();

                            BodyBottomPanel.validate();
                            BodyBottomPanel.revalidate();
                            BodyBottomPanel.repaint();
                        } else {
                            JOptionPane.showMessageDialog(null, "Entered Count is out of range.");
                        }
                    } else {
                        if (strt < sortedQuestionsList.size()+1) {
                            for (int i = end - 1; i < strt; i++) {
                                if(!(selectedQuestionsList.contains(sortedQuestionsList.get(i)))){
                                    sortedQuestionsList.get(i).setSelected(true);
                                    totalSelectedQueCount++;
                                    selectedQuestionsList.add(sortedQuestionsList.get(i));
                                    int index = questionsList.indexOf(sortedQuestionsList.get(i));
                                    questionsList.get(index).setSelected(true);
                                    addedSequence.add(i);
                                }
                            }
                            LblSelectedCount.setText("Selected Questions : " + totalSelectedQueCount);
                            setPanel(false);
                            setButtonOnPanel(RightBodyPanel, LeftBodyPanel);
                            LeftBodyPanel.validate();
                            LeftBodyPanel.repaint();
                            RightBodyPanel.validate();
                            RightBodyPanel.repaint();

                            BodyBottomPanel.validate();
                            BodyBottomPanel.revalidate();
                            BodyBottomPanel.repaint();
                        } else {
                            JOptionPane.showMessageDialog(null, "Entered Count is out of range.");
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Please enter valid range");
                }
            }
        }
    }//GEN-LAST:event_BtnRandomAddActionPerformed

    private void BtnChangeChaptersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnChangeChaptersActionPerformed
        if(printingPaperType.equals("UnitWise")) {
            new MultipleChapterSelection(currentPatternIndex,subjectsList.get(0),printingPaperType).setVisible(true);
        } else if(printingPaperType.equals("SubjectWise")) {
            new HomePage(printingPaperType).setVisible(true);
        } else if(printingPaperType.equals("GroupWise")) {
            new MultipleChapterSelection(currentPatternIndex,subjectsList.get(0),subjectsList.get(1),subjectsList.get(2),printingPaperType).setVisible(true);
        }
            
        if(homePage != null)
            homePage.dispose();
        this.dispose();
    }//GEN-LAST:event_BtnChangeChaptersActionPerformed

    private void CmbChaptersCaretPositionChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_CmbChaptersCaretPositionChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_CmbChaptersCaretPositionChanged

    private void TxtFldQuestionNumberKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtFldQuestionNumberKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() != 10) {
            keyPressed(evt, TxtFldQuestionNumber);
        }
    }//GEN-LAST:event_TxtFldQuestionNumberKeyPressed

    private void TxtFldStartKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtFldStartKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() != 10) {
            keyPressed(evt, TxtFldStart);
        }
    }//GEN-LAST:event_TxtFldStartKeyPressed

    private void TxtFldEndKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtFldEndKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() != 10) {
            keyPressed(evt, TxtFldEnd);
        }
    }//GEN-LAST:event_TxtFldEndKeyPressed

    private void BtnSaveTestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSaveTestActionPerformed
        // TODO add your handling code here:
        
////        subid=subjectBean.getSubjectId();
//        String CurrentPatternName=new PatternStatusOperation().getCurrentPatternName();
//        subid=0;
//        System.out.println("subjectsList.get(0).getSubjectId()=" +subjectsList.get(0).getSubjectId());
//        String t = JOptionPane.showInputDialog("Enter Test Time In Minutes.");
//        int testTime = Integer.parseInt(t);     
//        String TestName = JOptionPane.showInputDialog("Enter Test Name.");
//        int testId = new ClassSaveTestOperation().saveNewClassTest(selectedQuestionsList, subid, testTime,TestName,CurrentPatternName);
//        if (testId == -1) {
//        JOptionPane.showMessageDialog(null, "Error In Saving Test Please Try Again.");
//        } else {
//        JOptionPane.showMessageDialog(null, "Your Test Id Is : " + testId);
//        new ClassSaveTestOperation().insertintoTest_And_Set(testId);
//        new HomePage().setVisible(true);
//        this.dispose();
//        new SetOnlineTestSubjectMark(subjectsList.get(0).getSubjectName()).setVisible(true);
//        }

          new SaveTest(subjectsList,selectedQuestionsList,questionsList,printingPaperType,(Object)this).setVisible(true);
          
    }//GEN-LAST:event_BtnSaveTestActionPerformed
    
    private void keyPressed(KeyEvent evt, JTextField text) {
        if ((evt.getKeyChar() >= '0' && evt.getKeyChar() <= '9') || evt.getKeyCode() == 8 || evt.getKeyCode() == 9 || evt.getKeyCode() == 13 || evt.getKeyCode() == 16 || evt.getKeyCode() == 17 || evt.getKeyCode() == 20 || evt.getKeyCode() == 27 || evt.getKeyCode() == 37 || evt.getKeyCode() == 38 || evt.getKeyCode() == 39 || evt.getKeyCode() == 40 || evt.getKeyCode() == 46) {
            text.setEditable(true);
        } else {
            text.setEditable(false);
            JOptionPane.showMessageDialog(null, "Please Enter Numbers Only.");
            text.setEditable(true);
        }
    }
    
    private void removeAllQues() {
        for (int i = 0; i < questionsList.size(); i++) {
            questionsList.get(i).setSelected(false);
        }
        
        for(int i=0; i< sortedQuestionsList.size(); i++) {
            sortedQuestionsList.get(i).setSelected(false);
        }
        
        totalSelectedQueCount = 0;
        selectedQuestionsList = new ArrayList<QuestionBean>();
        BtnAddRemove.setText("Add");
        BtnAddRemove.setForeground(Color.black);
        TxtFldStart.setText("");
        TxtFldEnd.setText("");
        addedSequence = new ArrayList<Integer>();
        LblSelectedCount.setText("Selected Questions : " + totalSelectedQueCount);
        setButtonOnPanel(RightBodyPanel, LeftBodyPanel);
        LeftBodyPanel.validate();
        LeftBodyPanel.repaint();
        RightBodyPanel.validate();
        RightBodyPanel.repaint();
        BodyBottomPanel.validate();
        BodyBottomPanel.revalidate();
        BodyBottomPanel.repaint();
    }
    
    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(SelectQuestionForTest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(SelectQuestionForTest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(SelectQuestionForTest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(SelectQuestionForTest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
//                new SelectQuestionForTest("Gravitation",1,1).setVisible(true);[
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel BodyBottomPanel;
    private javax.swing.JButton BtnAddRemove;
    private javax.swing.JButton BtnAdvanceSelection;
    private javax.swing.JButton BtnChangeChapters;
    private javax.swing.JButton BtnFirst;
    private javax.swing.JButton BtnHome;
    private javax.swing.JButton BtnLast;
    private javax.swing.JButton BtnModify;
    private javax.swing.JButton BtnNext;
    private javax.swing.JButton BtnPdf;
    private javax.swing.JButton BtnPrevious;
    private javax.swing.JButton BtnRandomAdd;
    private javax.swing.JButton BtnRemoveAll;
    private javax.swing.JButton BtnSaveTest;
    private javax.swing.JButton BtnShuffle;
    private javax.swing.JButton BtnWord;
    private javax.swing.JCheckBox ChbkRandom;
    private javax.swing.JCheckBox ChkLevel;
    private javax.swing.JCheckBox ChkPrevious;
    private javax.swing.JCheckBox ChkType;
    private javax.swing.JCheckBox ChkUsed;
    private javax.swing.JComboBox CmbChapters;
    private javax.swing.JComboBox CmbLevel;
    private javax.swing.JComboBox CmbPrevious;
    private javax.swing.JComboBox CmbSubject;
    private javax.swing.JComboBox CmbType;
    private javax.swing.JComboBox CmbUsed;
    private javax.swing.JPanel FooterPanel;
    private javax.swing.JPanel HeaderPanel;
    private javax.swing.JLabel LblChapterName;
    private javax.swing.JLabel LblCount;
    private javax.swing.JLabel LblNonSelectedQues;
    private javax.swing.JLabel LblSelectedCount;
    private javax.swing.JLabel LblSort;
    private javax.swing.JLabel LblSubject;
    private javax.swing.JPanel LeftBodyPanel;
    private javax.swing.JScrollPane LeftBodyScrollPane;
    private com.ui.support.pages.JPanelsSliding MiddleBodyPanel;
    private javax.swing.JScrollPane MiiddleBodyScrollPane;
    private com.ui.support.pages.QuestionPanel PanelQues1;
    private com.ui.support.pages.QuestionPanel PanelQues2;
    private javax.swing.JPanel RightBodyPanel;
    private javax.swing.JScrollPane RightBodyScrollPane;
    private javax.swing.JPanel SubFooterPanel;
    private javax.swing.JPanel SubHeaderPanel;
    private javax.swing.JTextField TxtFldEnd;
    private javax.swing.JTextField TxtFldQuestionNumber;
    private javax.swing.JTextField TxtFldStart;
    private javax.swing.ButtonGroup btnGroupAnimation;
    private javax.swing.JPanel jPanel7;
    private com.ui.support.pages.NoQuestionPanel noQuestionPanel1;
    // End of variables declaration//GEN-END:variables

    
    public void setSelectionPanelQuestions(int easyQues,int mediumQues,int hardQues) {
        totalSelectedQueCount = easyQues+mediumQues+hardQues;
        Collections.shuffle(advSelectionQuesList);
        for(QuestionBean questionsBean : advSelectionQuesList) {
            if(questionsBean.getLevel() == 0 && easyQues != 0) {
                selectedQuestionsList.add(questionsBean);
                easyQues--;
            } else if(questionsBean.getLevel() == 1 && mediumQues != 0) {
                selectedQuestionsList.add(questionsBean);
                mediumQues--;
            } else if(questionsBean.getLevel() == 2 && hardQues != 0) {
                selectedQuestionsList.add(questionsBean);
                hardQues--;
            }
        }
        
        LblSelectedCount.setText("Selected Questions : " + totalSelectedQueCount);
        currentIndex = 0;
        setQuesType();
        setButtonOnPanel(RightBodyPanel, LeftBodyPanel);
        LeftBodyPanel.validate();
        LeftBodyPanel.repaint();
        RightBodyPanel.validate();
        RightBodyPanel.repaint();
    }
    
    public void setSelectionPanelTableQuestions(ArrayList<Integer> userCountedChapterList) {
        totalSelectedQueCount = 0;
        Collections.shuffle(advSelectionQuesList);
        int index = 0;
        for(CountChapterBean countChapterBean : countedChapterList) {
            ChapterBean chapterBean = countChapterBean.getChapterBean();
            int counter = userCountedChapterList.get(index);
            if (counter != 0) {
                for (QuestionBean questionsBean : advSelectionQuesList) {
                    if (questionsBean.getChapterId() == chapterBean.getChapterId()) {
                        totalSelectedQueCount++;
                        selectedQuestionsList.add(questionsBean);
                        counter--;
                    }
                    
                    if (counter == 0)
                        break;
                }
            }
            index++;
        }
        
        Collections.shuffle(selectedQuestionsList);
        LblSelectedCount.setText("Selected Questions : " + totalSelectedQueCount);
        currentIndex = 0;
        setQuesType();
        setButtonOnPanel(RightBodyPanel, LeftBodyPanel);
        LeftBodyPanel.validate();
        LeftBodyPanel.repaint();
        RightBodyPanel.validate();
        RightBodyPanel.repaint();
    }
    
    private void setChapterCombo() {
        CmbChapters.removeAllItems();
        
        if(sortedCountChapterList == null)
            sortedCountChapterList = new ArrayList<CountChapterBean>();
        else
            sortedCountChapterList.clear();
        
        for(CountChapterBean countChapterBean :countedChapterList) {
            ChapterBean bean = countChapterBean.getChapterBean();
            if(bean.getSubjectId() == subjectId) {
                sortedCountChapterList.add(countChapterBean);
            }
        }

        if(sortedCountChapterList.size() != 1) {
            if(!CmbChapters.isVisible())
                CmbChapters.setVisible(true);
            LblChapterName.setText("Chapters :");
            for(CountChapterBean countChapterBean : sortedCountChapterList)
                CmbChapters.addItem(countChapterBean.getChapterBean().getChapterName()+" ("+countChapterBean.getTotalQuestions()+")");
        } else {
            chapterWiseQuestionsList.clear();
            CmbChapters.setVisible(false);
            LblChapterName.setText("Chapter : "+sortedCountChapterList.get(0).getChapterBean().getChapterName()
                                    +" ("+sortedCountChapterList.get(0).getTotalQuestions()+")");

            chapterId = sortedCountChapterList.get(0).getChapterBean().getChapterId();
            for(QuestionBean questionBean : questionsList) {
                if(questionBean.getChapterId() == chapterId && questionBean.getSubjectId() == subjectId)
                    chapterWiseQuestionsList.add(questionBean);
            }
            currentIndex = 0;
            setQuesType();
        } 
    }
    
    //Getter And Setters
    public boolean isTestSaveStatus() {
        return testSaveStatus;
    }

    public void setTestSaveStatus(boolean testSaveStatus) {
        this.testSaveStatus = testSaveStatus;
    }

    public ArrayList<Integer> getSaveTestSelectedQuesIdList() {
        return saveTestSelectedQuesIdList;
    }

    public void setSaveTestSelectedQuesIdList(ArrayList<Integer> saveTestSelectedQuesIdList) {
        this.saveTestSelectedQuesIdList = saveTestSelectedQuesIdList;
    }

    public PrintedTestBean getPrintedTestBean() {
        return printedTestBean;
    }

    public void setPrintedTestBean(PrintedTestBean printedTestBean) {
        this.printedTestBean = printedTestBean;
    }
}
