package com.pages;

import com.Model.ProcessManager;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.InputEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import com.bean.ChapterBean;
import com.bean.ImageRatioBean;
import com.bean.QuestionBean;
import com.bean.SubjectBean;
import com.db.operations.ChapterOperation;
import com.db.operations.ImageRatioOperation;
import com.db.operations.MasterImageRationOperation;
import com.db.operations.MasterOptionImageDimentionOperation;
import com.db.operations.MasterQuestionOperation;
import com.db.operations.OptionImageDimentionOperation;
import com.db.operations.QuestionOperation;
import com.db.operations.SubjectOperation;
import com.ui.support.pages.PreviewImage;
import org.scilab.forge.jlatexmath.*;
import com.Model.TitleInfo;

public class ModifyQuestion extends javax.swing.JFrame {

    private QuestionBean selectedQuestionBean;
    private int currentIndex;
    private SubjectBean selectedSubjectBean;
    private SingleChapterQuestionsSelection selectChapterWise;
    private MultipleChapterQuestionsSelection selectQuestionsUnitsWise;
    private ArrayList<ChapterBean> chapterList;
    private BufferedImage existImgQuestion, existImgOption, existImgHint;
    private float questionRatio,optionRatio,hintRatio;
    private BufferedImage bufferImgQuestion, bufferImgOption, bufferImgHint;
    private ArrayList<ImageRatioBean> imageRatioList;
    private String processPath;
    private QuestionBean selectedMasterQuestionBean;
    
    //ChapterWise
    public ModifyQuestion(QuestionBean selectedQuestionBean,SingleChapterQuestionsSelection selectChapterWise,int currentIndex) {
        initComponents();
        this.selectedQuestionBean = selectedQuestionBean;
        selectQuestionsUnitsWise = null;
        this.selectChapterWise = selectChapterWise;
        this.currentIndex = currentIndex;
        loadInitialValues();
        setInitialValues();
    }
    
    //UnitWise
    public ModifyQuestion(QuestionBean selectedQuestionBean,MultipleChapterQuestionsSelection selectQuestionsUnitsWise,int currentIndex) {
        initComponents();
        this.selectedQuestionBean = selectedQuestionBean;
        selectChapterWise = null;
        this.selectQuestionsUnitsWise = selectQuestionsUnitsWise;
        this.currentIndex = currentIndex;
        loadInitialValues();
        setInitialValues();
    }
    
    private void loadInitialValues() {
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        this.setTitle("Modify Question");
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        selectedSubjectBean = new SubjectOperation().getSubjectBean(selectedQuestionBean.getSubjectId());
        imageRatioList = new ImageRatioOperation().getImageRatioList();
        chapterList = new ChapterOperation().getChapterList(selectedQuestionBean.getSubjectId());
        selectedMasterQuestionBean = new MasterQuestionOperation().getMasterQuestionId(selectedQuestionBean);
    }
    
    private void setInitialValues() {
        existImgQuestion = null;
        existImgOption = null;
        existImgHint = null;
        bufferImgQuestion = null;
        bufferImgOption = null;
        bufferImgHint = null;
        processPath = null;
        rdoOptionA.setActionCommand("A");
        rdoOptionB.setActionCommand("B");
        rdoOptionC.setActionCommand("C");
        rdoOptionD.setActionCommand("D");
        buttonGroup1.add(rdoOptionA);
        buttonGroup1.add(rdoOptionB);
        buttonGroup1.add(rdoOptionC);
        buttonGroup1.add(rdoOptionD);
        
        LblQuestionPreview.setVisible(false);
        LblOptionPreview.setVisible(false);
        LblHintPreview.setVisible(false);
        
        cmbChapter.removeAllItems();
        int index = 0;
        for(int i=0;i<chapterList.size();i++) {
            cmbChapter.addItem(chapterList.get(i).getChapterName());
            if(chapterList.get(i).getChapterId() == selectedQuestionBean.getChapterId()) {
                index = i;
            }
        }
        cmbChapter.setSelectedIndex(index);
        LblTitle.setText(selectedSubjectBean.getSubjectName() + " : " + chapterList.get(index).getChapterName());
        lblHint.setText("");
        setQuestionPanel();
    }

    private Image getImageFromClipboard() {
        Transferable transferable = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
        if (transferable != null && transferable.isDataFlavorSupported(DataFlavor.imageFlavor)) {
            try {
                JOptionPane.showMessageDialog(rootPane, "got image ");
                return (Image) transferable.getTransferData(DataFlavor.imageFlavor);
            } catch (UnsupportedFlavorException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
    private BufferedImage getBufferedImage(String imagePath) {
        BufferedImage returnBufferedImage = null;
        try {
            returnBufferedImage = ImageIO.read(new File(imagePath));
        } catch (IOException ex) {
            returnBufferedImage = null;
            ex.printStackTrace();
        }
        return returnBufferedImage;
    }
    
    private void loadImage(BufferedImage bufferedImage,JLabel lbl) {
        ImageIcon icon = null;
        Image img = bufferedImage.getScaledInstance((int) bufferedImage.getWidth(), (int) bufferedImage.getHeight(), Image.SCALE_AREA_AVERAGING);
        icon = new ImageIcon(img);
        lbl.setIcon(icon);
        lbl.setText("");
    }
    
    private void txtOptionStateChanged(boolean value) {
        lblOption.setVisible(value);
        lblA.setVisible(value);
        lblOptionA.setVisible(value);
        lblB.setVisible(value);
        lblOptionB.setVisible(value);
        lblC.setVisible(value);
        lblOptionC.setVisible(value);
        lblD.setVisible(value);
        lblOptionD.setVisible(value);
        if(!BtnModify.isEnabled()) {
            txtOptionA.setVisible(value);
            btnAddNewLineA.setVisible(value);
            txtOptionB.setVisible(value);
            btnAddNewLineB.setVisible(value);
            txtOptionC.setVisible(value);
            btnAddNewLineC.setVisible(value);
            txtOptionD.setVisible(value);
            btnAddNewLineD.setVisible(value);
        }
    }
    
    private void txtBoxesStateChanged(boolean value) {
        BtnModify.setEnabled(!value);
        BtnHide.setEnabled(value);
        txtQuestion.setVisible(value);
        btnAddNewLine.setVisible(value);
        txtYear.setVisible(value);
        btnYear.setVisible(value);
        txtHint.setVisible(value);
        btnAddNewLineHint.setVisible(value);
        boolean optionValue = value;
        if(value) 
            optionValue = !chkOptionImage.isSelected();
        txtOptionA.setVisible(optionValue);
        txtOptionB.setVisible(optionValue);
        txtOptionC.setVisible(optionValue);
        txtOptionD.setVisible(optionValue);
        
        btnAddNewLineA.setVisible(optionValue);
        btnAddNewLineB.setVisible(optionValue);
        btnAddNewLineC.setVisible(optionValue);
        btnAddNewLineD.setVisible(optionValue);
    }
    
    private void setLableText(JLabel l, String str) {
        try {
            if (str.equals("")) {
            } else {
                l.setText("");
                str = str.replace("â€™", "'");
                TeXFormula formula;
                TeXIcon icon;
                BufferedImage image;
                Graphics2D g2;
                JLabel jl;
//                start = "\\begin{array}{l}";
//                end = "\\end{array}";

                str = "\\begin{array}{l}" + str + "\\end{array}";
                formula = new TeXFormula(str);
                icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 20);
                icon.setInsets(new Insets(0, 0, 0, 0));
                image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
                g2 = image.createGraphics();
                g2.setColor(Color.white);
                g2.fillRect(0, 0, icon.getIconWidth(), icon.getIconHeight());
                jl = new JLabel();
                jl.setForeground(new Color(0, 0, 0));
                icon.paintIcon(jl, g2, 0, 0);
                l.setIcon(icon);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(rootPane, e.getMessage());
        }
    }

    private void setQuestionPanel() {
        String question,answer,optionA,optionB,optionC,optionD,hints;
        
        lblQuestion.setIcon(null);
        lblOptionA.setIcon(null);
        lblOptionB.setIcon(null);
        lblOptionC.setIcon(null);
        lblOptionD.setIcon(null);
        lblHint.setIcon(null);
        txtYear.setText("");
        chkTheory.setSelected(false);
        chkNumerical.setSelected(false);
        
        answer = selectedQuestionBean.getAnswer();
        
        if (answer.equals("A"))
            rdoOptionA.setSelected(true);
        else if (answer.equals("B"))
            rdoOptionB.setSelected(true);
        else if (answer.equals("C"))
            rdoOptionC.setSelected(true);
        else
            rdoOptionD.setSelected(true);
        
        txtYear.setText(selectedQuestionBean.getYear());
        cmbLevel.setSelectedIndex(selectedQuestionBean.getLevel());
        
        question = selectedQuestionBean.getQuestion();
        optionA = selectedQuestionBean.getOptionA();
        optionB = selectedQuestionBean.getOptionB();
        optionC = selectedQuestionBean.getOptionC();
        optionD = selectedQuestionBean.getOptionD();
        hints = selectedQuestionBean.getHint();
        
        if(selectedQuestionBean.getType() == 0) {
            chkTheory.setSelected(true);
            chkNumerical.setSelected(false);
        } else if (selectedQuestionBean.getType() == 1) {
            chkTheory.setSelected(false);
            chkNumerical.setSelected(true);
        }

        if (selectedQuestionBean.isIsQuestionAsImage()) {
            existImgQuestion = getBufferedImage(selectedQuestionBean.getQuestionImagePath());
            if(existImgQuestion != null) {
                chkQueImage.setSelected(true);
                LblQuestionPreview.setVisible(true);
                loadImage(existImgQuestion, lblQuestionImageView);
                setQuestionRatio(getImageRatio(selectedQuestionBean.getQuestionImagePath()));
            }
        } else {
            chkQueImage.setSelected(false);
        }
        
        boolean optionStateValue = false;
        if (selectedQuestionBean.isIsOptionAsImage()) {
            existImgOption = getBufferedImage(selectedQuestionBean.getOptionImagePath());
            if(existImgOption != null) {
                chkOptionImage.setSelected(true);
                LblOptionPreview.setVisible(true);
                optionStateValue = false;
                loadImage(existImgOption, lblOptionImageView);
                setOptionRatio(getImageRatio(selectedQuestionBean.getOptionImagePath()));
            }
        } else {
            chkOptionImage.setSelected(false);
            optionStateValue = true;
            optionA = replace0(optionA);
            optionB = replace0(optionB);
            optionC = replace0(optionC);
            optionD = replace0(optionD);
        }
        if (selectedQuestionBean.isIsHintAsImage()) {
            existImgHint = getBufferedImage(selectedQuestionBean.getHintImagePath());
            if(existImgHint != null) {
                chkHintImage.setSelected(true);
                LblHintPreview.setVisible(true);
                loadImage(existImgHint, lblHintImageView);
                setHintRatio(getImageRatio(selectedQuestionBean.getHintImagePath()));
            }
        } else {
            chkHintImage.setSelected(false);
        }
        
        txtOptionStateChanged(true);
        question = replace0(question);
        hints = replace0(hints);
        
        txtQuestion.setText(question);
        txtOptionA.setText(optionA);
        txtOptionB.setText(optionB);
        txtOptionC.setText(optionC);
        txtOptionD.setText(optionD);
        txtHint.setText(hints);
        
        setLableText(lblQuestion, question);
        setLableText(lblHint, hints);
        setLableText(lblOptionA, optionA);
        setLableText(lblOptionB, optionB);
        setLableText(lblOptionC, optionC);
        setLableText(lblOptionD, optionD);
        
        lblQuestionNo.setText("Q. " + (currentIndex + 1));
        txtBoxesStateChanged(false);
        txtOptionStateChanged(optionStateValue);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jFrame1 = new javax.swing.JFrame();
        jPanel3 = new javax.swing.JPanel();
        RightBodyScrollPane = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        chkQueImage = new javax.swing.JCheckBox();
        btnQuePaste = new javax.swing.JButton();
        panelQueImage = new javax.swing.JPanel();
        lblQuestionImageView = new javax.swing.JLabel();
        btnHintPaste = new javax.swing.JButton();
        btnOptionPaste = new javax.swing.JButton();
        panelHintImage = new javax.swing.JPanel();
        lblHintImageView = new javax.swing.JLabel();
        chkHintImage = new javax.swing.JCheckBox();
        panelOptionImage = new javax.swing.JPanel();
        lblOptionImageView = new javax.swing.JLabel();
        chkOptionImage = new javax.swing.JCheckBox();
        LblQuestionPreview = new javax.swing.JLabel();
        LblOptionPreview = new javax.swing.JLabel();
        LblHintPreview = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel4 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        lblQuestionNo = new javax.swing.JLabel();
        lblQuestion = new javax.swing.JLabel();
        txtQuestion = new javax.swing.JTextField();
        btnAddNewLine = new javax.swing.JButton();
        btnYear = new javax.swing.JButton();
        txtYear = new javax.swing.JTextField();
        txtOptionA = new javax.swing.JTextField();
        btnAddNewLineA = new javax.swing.JButton();
        txtOptionB = new javax.swing.JTextField();
        btnAddNewLineB = new javax.swing.JButton();
        txtOptionC = new javax.swing.JTextField();
        btnAddNewLineC = new javax.swing.JButton();
        txtHint = new javax.swing.JTextField();
        txtOptionD = new javax.swing.JTextField();
        btnAddNewLineD = new javax.swing.JButton();
        btnAddNewLineHint = new javax.swing.JButton();
        lblOption = new javax.swing.JLabel();
        lblA = new javax.swing.JLabel();
        lblOptionA = new javax.swing.JLabel();
        lblB = new javax.swing.JLabel();
        lblOptionB = new javax.swing.JLabel();
        lblC = new javax.swing.JLabel();
        lblOptionC = new javax.swing.JLabel();
        lblD = new javax.swing.JLabel();
        lblOptionD = new javax.swing.JLabel();
        lhint = new javax.swing.JLabel();
        lblHint = new javax.swing.JLabel();
        lblQuestionNo5 = new javax.swing.JLabel();
        rdoOptionA = new javax.swing.JRadioButton();
        rdoOptionB = new javax.swing.JRadioButton();
        rdoOptionC = new javax.swing.JRadioButton();
        rdoOptionD = new javax.swing.JRadioButton();
        lblChapterTopic = new javax.swing.JLabel();
        cmbChapter = new javax.swing.JComboBox<>();
        chkTheory = new javax.swing.JCheckBox();
        txtLevel = new javax.swing.JLabel();
        cmbLevel = new javax.swing.JComboBox();
        chkNumerical = new javax.swing.JCheckBox();
        jLabel2 = new javax.swing.JLabel();
        LblTitle = new javax.swing.JLabel();
        FooterPanel = new javax.swing.JPanel();
        BtnModify = new javax.swing.JButton();
        BtnHide = new javax.swing.JButton();
        BtnAddToFinalDB = new javax.swing.JButton();
        BtnBack = new javax.swing.JButton();
        BtnReset = new javax.swing.JButton();

        javax.swing.GroupLayout jFrame1Layout = new javax.swing.GroupLayout(jFrame1.getContentPane());
        jFrame1.getContentPane().setLayout(jFrame1Layout);
        jFrame1Layout.setHorizontalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jFrame1Layout.setVerticalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(153, 102, 255));
        jPanel3.setPreferredSize(new java.awt.Dimension(241, 444));

        RightBodyScrollPane.setPreferredSize(new java.awt.Dimension(345, 444));

        chkQueImage.setText("Question Image");
        chkQueImage.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                chkQueImageItemStateChanged(evt);
            }
        });

        btnQuePaste.setText("Paste");
        btnQuePaste.setEnabled(false);
        btnQuePaste.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQuePasteActionPerformed(evt);
            }
        });

        panelQueImage.setBackground(new java.awt.Color(255, 255, 255));
        panelQueImage.setPreferredSize(new java.awt.Dimension(309, 170));

        lblQuestionImageView.setBackground(new java.awt.Color(255, 255, 255));
        lblQuestionImageView.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblQuestionImageView.setPreferredSize(new java.awt.Dimension(309, 170));

        javax.swing.GroupLayout panelQueImageLayout = new javax.swing.GroupLayout(panelQueImage);
        panelQueImage.setLayout(panelQueImageLayout);
        panelQueImageLayout.setHorizontalGroup(
            panelQueImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblQuestionImageView, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        panelQueImageLayout.setVerticalGroup(
            panelQueImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblQuestionImageView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        btnHintPaste.setText("Paste");
        btnHintPaste.setEnabled(false);
        btnHintPaste.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHintPasteActionPerformed(evt);
            }
        });

        btnOptionPaste.setText("Paste");
        btnOptionPaste.setEnabled(false);
        btnOptionPaste.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOptionPasteActionPerformed(evt);
            }
        });

        panelHintImage.setBackground(new java.awt.Color(255, 255, 255));

        lblHintImageView.setBackground(new java.awt.Color(255, 255, 255));
        lblHintImageView.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblHintImageView.setPreferredSize(new java.awt.Dimension(309, 170));

        javax.swing.GroupLayout panelHintImageLayout = new javax.swing.GroupLayout(panelHintImage);
        panelHintImage.setLayout(panelHintImageLayout);
        panelHintImageLayout.setHorizontalGroup(
            panelHintImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblHintImageView, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        panelHintImageLayout.setVerticalGroup(
            panelHintImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblHintImageView, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        chkHintImage.setText("Hint Image");
        chkHintImage.setPreferredSize(new java.awt.Dimension(101, 23));
        chkHintImage.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                chkHintImageItemStateChanged(evt);
            }
        });

        panelOptionImage.setBackground(new java.awt.Color(255, 255, 255));
        panelOptionImage.setPreferredSize(new java.awt.Dimension(309, 170));

        lblOptionImageView.setBackground(new java.awt.Color(255, 255, 255));
        lblOptionImageView.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblOptionImageView.setPreferredSize(new java.awt.Dimension(309, 170));

        javax.swing.GroupLayout panelOptionImageLayout = new javax.swing.GroupLayout(panelOptionImage);
        panelOptionImage.setLayout(panelOptionImageLayout);
        panelOptionImageLayout.setHorizontalGroup(
            panelOptionImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblOptionImageView, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        panelOptionImageLayout.setVerticalGroup(
            panelOptionImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelOptionImageLayout.createSequentialGroup()
                .addComponent(lblOptionImageView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        chkOptionImage.setText("Option Image");
        chkOptionImage.setPreferredSize(new java.awt.Dimension(101, 23));
        chkOptionImage.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                chkOptionImageItemStateChanged(evt);
            }
        });

        LblQuestionPreview.setForeground(new java.awt.Color(51, 0, 255));
        LblQuestionPreview.setText("Preview");
        LblQuestionPreview.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        LblQuestionPreview.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblQuestionPreviewMouseClicked(evt);
            }
        });

        LblOptionPreview.setForeground(new java.awt.Color(51, 0, 255));
        LblOptionPreview.setText("Preview");
        LblOptionPreview.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        LblOptionPreview.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblOptionPreviewMouseClicked(evt);
            }
        });

        LblHintPreview.setForeground(new java.awt.Color(51, 0, 255));
        LblHintPreview.setText("Preview");
        LblHintPreview.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        LblHintPreview.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblHintPreviewMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelHintImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(chkOptionImage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(101, 101, 101)
                                .addComponent(LblOptionPreview)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnOptionPaste))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(chkQueImage)
                                .addGap(101, 101, 101)
                                .addComponent(LblQuestionPreview)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnQuePaste))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(chkHintImage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(101, 101, 101)
                                .addComponent(LblHintPreview)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnHintPaste))
                            .addComponent(panelQueImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelOptionImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chkQueImage)
                    .addComponent(btnQuePaste)
                    .addComponent(LblQuestionPreview))
                .addGap(10, 10, 10)
                .addComponent(panelQueImage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnOptionPaste)
                    .addComponent(chkOptionImage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblOptionPreview))
                .addGap(10, 10, 10)
                .addComponent(panelOptionImage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chkHintImage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnHintPaste)
                    .addComponent(LblHintPreview))
                .addGap(10, 10, 10)
                .addComponent(panelHintImage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        RightBodyScrollPane.setViewportView(jPanel1);

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/Line.png"))); // NOI18N

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/Line.png"))); // NOI18N

        lblQuestionNo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblQuestionNo.setText("Q. 1");

        lblQuestion.setText("Question"); // NOI18N

        txtQuestion.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtQuestion.setMinimumSize(new java.awt.Dimension(584, 20));
        txtQuestion.setPreferredSize(new java.awt.Dimension(584, 20));
        txtQuestion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtQuestionKeyReleased(evt);
            }
        });

        btnAddNewLine.setText("Add \\mbox");
        btnAddNewLine.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddNewLineActionPerformed(evt);
            }
        });

        btnYear.setText("For Year");
        btnYear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnYearActionPerformed(evt);
            }
        });

        txtYear.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtYear.setPreferredSize(new java.awt.Dimension(584, 20));

        txtOptionA.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtOptionA.setPreferredSize(new java.awt.Dimension(584, 20));
        txtOptionA.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtOptionAKeyReleased(evt);
            }
        });

        btnAddNewLineA.setText("Add \\mbox");
        btnAddNewLineA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddNewLineAActionPerformed(evt);
            }
        });

        txtOptionB.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtOptionB.setPreferredSize(new java.awt.Dimension(584, 20));
        txtOptionB.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtOptionBKeyReleased(evt);
            }
        });

        btnAddNewLineB.setText("Add \\mbox");
        btnAddNewLineB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddNewLineBActionPerformed(evt);
            }
        });

        txtOptionC.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtOptionC.setPreferredSize(new java.awt.Dimension(584, 20));
        txtOptionC.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtOptionCKeyReleased(evt);
            }
        });

        btnAddNewLineC.setText("Add \\mbox");
        btnAddNewLineC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddNewLineCActionPerformed(evt);
            }
        });

        txtHint.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtHint.setPreferredSize(new java.awt.Dimension(584, 20));
        txtHint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtHintKeyReleased(evt);
            }
        });

        txtOptionD.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtOptionD.setPreferredSize(new java.awt.Dimension(584, 20));
        txtOptionD.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtOptionDKeyReleased(evt);
            }
        });

        btnAddNewLineD.setText("Add \\mbox");
        btnAddNewLineD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddNewLineDActionPerformed(evt);
            }
        });

        btnAddNewLineHint.setText("Add \\mbox");
        btnAddNewLineHint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddNewLineHintActionPerformed(evt);
            }
        });

        lblOption.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblOption.setText("Options :");

        lblA.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        lblA.setText("A :");

        lblOptionA.setText("jLabel7");

        lblB.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        lblB.setText("B :");

        lblOptionB.setText("jLabel7");

        lblC.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        lblC.setText("C :");

        lblOptionC.setText("jLabel7");

        lblD.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        lblD.setText("D :");

        lblOptionD.setText("jLabel7");

        lhint.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        lhint.setText("Hint :");

        lblHint.setText("jLabel7");

        lblQuestionNo5.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblQuestionNo5.setText("Select Answer:");

        rdoOptionA.setText("A");
        rdoOptionA.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdoOptionAItemStateChanged(evt);
            }
        });

        rdoOptionB.setText("B");

        rdoOptionC.setText("C");

        rdoOptionD.setText("D");

        lblChapterTopic.setText("Chapter + Topic:");

        cmbChapter.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        chkTheory.setText("Theorotical");
        chkTheory.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                chkTheoryItemStateChanged(evt);
            }
        });

        txtLevel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtLevel.setText("Select Level:"); // NOI18N

        cmbLevel.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Easy", "Medium", "Hard" }));

        chkNumerical.setText("Numerical");
        chkNumerical.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                chkNumericalItemStateChanged(evt);
            }
        });

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Type:");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(lblQuestionNo)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(jLabel4))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblQuestion)
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(txtHint, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtOptionD, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtOptionC, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtOptionB, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtOptionA, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtYear, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtQuestion, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(btnAddNewLine)
                                            .addComponent(btnYear, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(btnAddNewLineA)
                                            .addComponent(btnAddNewLineB)
                                            .addComponent(btnAddNewLineC)
                                            .addComponent(btnAddNewLineD)
                                            .addComponent(btnAddNewLineHint, javax.swing.GroupLayout.Alignment.TRAILING)))))))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(112, 112, 112)
                        .addComponent(jLabel5))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(lblOption)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addComponent(lblB)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lblOptionB))
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addComponent(lblC)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lblOptionC))
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addComponent(lblD)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lblOptionD))
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addComponent(lblA)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lblOptionA))))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGap(67, 67, 67)
                                .addComponent(lhint)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblHint))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(lblQuestionNo5, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(rdoOptionA)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rdoOptionB, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rdoOptionC)
                                .addGap(2, 2, 2)
                                .addComponent(rdoOptionD)
                                .addGap(104, 104, 104)
                                .addComponent(lblChapterTopic, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cmbChapter, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(103, 103, 103)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtLevel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(chkTheory)
                                .addGap(6, 6, 6)
                                .addComponent(chkNumerical))
                            .addComponent(cmbLevel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblQuestion))
                    .addComponent(lblQuestionNo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtQuestion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAddNewLine))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnYear))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtOptionA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAddNewLineA))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtOptionB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAddNewLineB))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtOptionC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAddNewLineC))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtOptionD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAddNewLineD))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtHint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAddNewLineHint))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblOption)
                    .addComponent(lblA)
                    .addComponent(lblOptionA))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblB)
                    .addComponent(lblOptionB))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblC)
                    .addComponent(lblOptionC))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblD)
                    .addComponent(lblOptionD))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lhint)
                    .addComponent(lblHint))
                .addGap(38, 38, 38)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblQuestionNo5, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rdoOptionA, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rdoOptionB, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rdoOptionC, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rdoOptionD, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblChapterTopic, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbChapter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtLevel)
                    .addComponent(cmbLevel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chkNumerical)
                    .addComponent(jLabel2)
                    .addComponent(chkTheory))
                .addContainerGap(12, Short.MAX_VALUE))
        );

        jScrollPane1.setViewportView(jPanel4);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 802, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(RightBodyScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(RightBodyScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 426, Short.MAX_VALUE)))
        );

        LblTitle.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        LblTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblTitle.setText("Subject");

        FooterPanel.setBackground(new java.awt.Color(204, 204, 0));
        FooterPanel.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(51, 51, 255)));

        BtnModify.setText("Modify");
        BtnModify.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnModifyActionPerformed(evt);
            }
        });
        FooterPanel.add(BtnModify);

        BtnHide.setText("Hide");
        BtnHide.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHideActionPerformed(evt);
            }
        });
        FooterPanel.add(BtnHide);

        BtnAddToFinalDB.setText("Update");
        BtnAddToFinalDB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAddToFinalDBActionPerformed(evt);
            }
        });
        FooterPanel.add(BtnAddToFinalDB);

        BtnBack.setText("Back");
        BtnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBackActionPerformed(evt);
            }
        });
        FooterPanel.add(BtnBack);

        BtnReset.setText("Reset");
        BtnReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnResetActionPerformed(evt);
            }
        });
        FooterPanel.add(BtnReset);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(LblTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(FooterPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1153, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(LblTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 433, Short.MAX_VALUE)
                .addGap(7, 7, 7)
                .addComponent(FooterPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void BtnModifyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnModifyActionPerformed
//    showTextBoxes();
    txtBoxesStateChanged(true);
}//GEN-LAST:event_BtnModifyActionPerformed

private void BtnHideActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHideActionPerformed
//    hideTextBoxes();
    txtBoxesStateChanged(false);
}//GEN-LAST:event_BtnHideActionPerformed

private void BtnAddToFinalDBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAddToFinalDBActionPerformed
    Object[] options = {"YES", "CANCEL"};
    int i = JOptionPane.showOptionDialog(null, "Do You Want to Update Data?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
    if (i == 0) {
        if(checkInsertStatuss()) {
            boolean quesUpdate = getUpdateStatuss();
            boolean dimUpdate = getDimentionUpdateStatus();
            boolean ratioUpdate = false;
            QuestionBean questionsBean = null;
            QuestionBean masterQuestionBean = null;
            if(quesUpdate || dimUpdate) {
                ArrayList<String> updateImgRatioList = new ArrayList<String>();
                ArrayList<String> masterUpdateImgRatioList = new ArrayList<String>();
                
                questionsBean = new QuestionBean();
                questionsBean.setQuestionId(selectedQuestionBean.getQuestionId());//1
                questionsBean.setQuestion(txtQuestion.getText().trim());//2
                questionsBean.setAnswer(buttonGroup1.getSelection().getActionCommand());//7
                questionsBean.setYear(txtYear.getText().trim());//21
                if(txtHint.getText().trim().isEmpty())//8
                    questionsBean.setHint("\\mbox{}");
                else
                    questionsBean.setHint(txtHint.getText().trim());
                questionsBean.setLevel(cmbLevel.getSelectedIndex());//9
                questionsBean.setSubjectId(selectedQuestionBean.getSubjectId());//10'
                questionsBean.setAttempt(selectedQuestionBean.getAttempt());//19
                questionsBean.setType(getTypeOfQuestion());//20
                int chapterId = chapterList.get(cmbChapter.getSelectedIndex()).getChapterId();
                questionsBean.setChapterId(chapterId);//11
                questionsBean.setTopicId(chapterId);//12
                
                //Master Update
                int masterChapterId = 0;
                if(selectedMasterQuestionBean != null) {
                    masterQuestionBean = new QuestionBean();
                    masterQuestionBean.setQuestionId(selectedMasterQuestionBean.getQuestionId());//1
                    masterQuestionBean.setQuestion(txtQuestion.getText().trim());//2
                    masterQuestionBean.setAnswer(buttonGroup1.getSelection().getActionCommand());//7
                    masterQuestionBean.setYear(txtYear.getText().trim());//21
                    if(txtHint.getText().trim().isEmpty())//8
                        masterQuestionBean.setHint("\\mbox{}");
                    else
                        masterQuestionBean.setHint(txtHint.getText().trim());
                    masterQuestionBean.setLevel(selectedMasterQuestionBean.getLevel());//9
                    masterQuestionBean.setSubjectId(selectedMasterQuestionBean.getSubjectId());//10'
                    masterQuestionBean.setAttempt(selectedMasterQuestionBean.getAttempt());//19
                    masterQuestionBean.setType(selectedMasterQuestionBean.getType());//20
                    
                    masterChapterId = selectedMasterQuestionBean.getChapterId();
                    masterQuestionBean.setChapterId(masterChapterId);//11
                    masterQuestionBean.setTopicId(masterChapterId);//12
                }
                System.out.println("MasterBean : "+selectedMasterQuestionBean);
                String queImagePath = "";
                String masterQueImagePath = "";
                if(existImgQuestion != null && bufferImgQuestion != null) {
                    if(chapterId != selectedQuestionBean.getChapterId()) {
                        deleteFile(selectedQuestionBean.getQuestionImagePath());
                        queImagePath = "images/"+chapterId+"Question"+selectedQuestionBean.getQuestionId()+".png";
                        updateImgRatioList.add(selectedQuestionBean.getQuestionImagePath()+"##"+queImagePath+"##"+getQuestionRatio());
                    } else {
                        queImagePath = selectedQuestionBean.getQuestionImagePath();
                        updateImgRatioList.add(queImagePath+"##"+queImagePath+"##"+getQuestionRatio());
                    }
                    
                    questionsBean.setIsQuestionAsImage(true);
                    questionsBean.setQuestionImagePath(queImagePath);
                    saveImage(queImagePath,bufferImgQuestion);
                    
                    //Master
                    if(selectedMasterQuestionBean != null) {
                        masterQueImagePath = selectedMasterQuestionBean.getQuestionImagePath();
                        masterUpdateImgRatioList.add(masterQueImagePath+"##"+masterQueImagePath+"##"+getQuestionRatio());
                        masterQuestionBean.setIsQuestionAsImage(true);
                        masterQuestionBean.setQuestionImagePath(masterQueImagePath);
                        masterSaveImage(masterQueImagePath,bufferImgQuestion);
                    }
                    
                } else if(existImgQuestion == null && bufferImgQuestion != null) {
                    queImagePath = "images/"+chapterId+"Question"+selectedQuestionBean.getQuestionId()+".png";
                    saveImage(queImagePath,bufferImgQuestion);
                    updateImgRatioList.add(queImagePath+"##"+getQuestionRatio());
                    questionsBean.setIsQuestionAsImage(true);
                    questionsBean.setQuestionImagePath(queImagePath);
                    
                    //Master
                    if(selectedMasterQuestionBean != null) {
                        masterQueImagePath = "masterImages/"+masterChapterId+"Question"+selectedMasterQuestionBean.getQuestionId()+".png";
                        masterSaveImage(masterQueImagePath,bufferImgQuestion);
                        masterUpdateImgRatioList.add(masterQueImagePath+"##"+getQuestionRatio());
                        masterQuestionBean.setIsQuestionAsImage(true);
                        masterQuestionBean.setQuestionImagePath(masterQueImagePath);
                    }
                } else if(existImgQuestion != null && !chkQueImage.isSelected()) {
                    deleteFile(selectedQuestionBean.getQuestionImagePath());
                    updateImgRatioList.add(selectedQuestionBean.getQuestionImagePath());
                    questionsBean.setIsQuestionAsImage(false);
                    questionsBean.setQuestionImagePath(queImagePath);
                    
                    //Master
                    if(selectedMasterQuestionBean != null) {
                        deleteFile(selectedMasterQuestionBean.getQuestionImagePath());
                        masterUpdateImgRatioList.add(selectedMasterQuestionBean.getQuestionImagePath());
                        masterQuestionBean.setIsQuestionAsImage(false);
                        masterQuestionBean.setQuestionImagePath(masterQueImagePath);
                    }
                    
                } else if(existImgQuestion != null && bufferImgQuestion == null && chapterId != selectedQuestionBean.getChapterId()) {
                    deleteFile(selectedQuestionBean.getQuestionImagePath());
                    queImagePath = "images/"+chapterId+"Question"+selectedQuestionBean.getQuestionId()+".png";
                    updateImgRatioList.add(selectedQuestionBean.getQuestionImagePath()+"##"+queImagePath+"##"+getQuestionRatio());

                    questionsBean.setIsQuestionAsImage(true);
                    questionsBean.setQuestionImagePath(queImagePath);
                    saveImage(queImagePath,existImgQuestion);
                    
                } else if(existImgQuestion != null && bufferImgQuestion == null && getQuestionRatio() != getImageRatio(selectedQuestionBean.getQuestionImagePath())) {
                    queImagePath = selectedQuestionBean.getQuestionImagePath();
                    updateImgRatioList.add(queImagePath+"##"+queImagePath+"##"+getQuestionRatio());
                    questionsBean.setIsQuestionAsImage(true);
                    questionsBean.setQuestionImagePath(queImagePath);
                    
                    //Master
                    if(selectedMasterQuestionBean != null) {
                        masterQueImagePath = selectedMasterQuestionBean.getQuestionImagePath();
                        masterUpdateImgRatioList.add(masterQueImagePath+"##"+masterQueImagePath+"##"+getQuestionRatio());
                        masterQuestionBean.setIsQuestionAsImage(true);
                        masterQuestionBean.setQuestionImagePath(masterQueImagePath);
                    }
                    
                } else if(existImgQuestion != null && bufferImgQuestion == null) {
                    queImagePath = selectedQuestionBean.getQuestionImagePath();
                    questionsBean.setIsQuestionAsImage(true);
                    questionsBean.setQuestionImagePath(queImagePath);    
                    
                    //Master
                    if(selectedMasterQuestionBean != null) {
                        masterQueImagePath = selectedMasterQuestionBean.getQuestionImagePath();
                        masterQuestionBean.setIsQuestionAsImage(true);
                        masterQuestionBean.setQuestionImagePath(masterQueImagePath);
                    }
                } else if(!chkQueImage.isSelected()) {
                    questionsBean.setIsQuestionAsImage(false);
                    questionsBean.setQuestionImagePath(queImagePath);
                    
                    //Master
                    if(selectedMasterQuestionBean != null) {
                        masterQuestionBean.setIsQuestionAsImage(false);
                        masterQuestionBean.setQuestionImagePath(masterQueImagePath);
                    }
                 }
                
                String hintImagePath = "";
                String masterHintImagePath = "";
                
                if(existImgHint != null && bufferImgHint != null) {
                    if(chapterId != selectedQuestionBean.getChapterId()) {
                        deleteFile(selectedQuestionBean.getHintImagePath());
                        hintImagePath = "images/"+chapterId+"Hint"+selectedQuestionBean.getQuestionId()+".png";
                        updateImgRatioList.add(selectedQuestionBean.getHintImagePath()+"##"+hintImagePath+"##"+getHintRatio());
                    } else {
                        hintImagePath = selectedQuestionBean.getHintImagePath();
                        updateImgRatioList.add(hintImagePath+"##"+hintImagePath+"##"+getHintRatio());
                    }
                    
                    questionsBean.setIsHintAsImage(true);
                    questionsBean.setHintImagePath(hintImagePath);
                    saveImage(hintImagePath,bufferImgHint);
                    
                    //Master
                    if(selectedMasterQuestionBean != null) {
                        masterHintImagePath = selectedMasterQuestionBean.getHintImagePath();
                        masterUpdateImgRatioList.add(masterHintImagePath+"##"+masterHintImagePath+"##"+getHintRatio());
                        masterQuestionBean.setIsHintAsImage(true);
                        masterQuestionBean.setHintImagePath(masterHintImagePath);
                        masterSaveImage(masterHintImagePath,bufferImgHint);
                    }
                } else if(existImgHint == null && bufferImgHint != null) {
                    hintImagePath = "images/"+chapterId+"Hint"+selectedQuestionBean.getQuestionId()+".png";
                    saveImage(hintImagePath,bufferImgHint);
                    updateImgRatioList.add(hintImagePath+"##"+getHintRatio());
                    questionsBean.setIsHintAsImage(true);
                    questionsBean.setHintImagePath(hintImagePath);
                    
                    //Master
                    if(selectedMasterQuestionBean != null) {
                        masterHintImagePath = "masterImages/"+masterChapterId+"Hint"+selectedMasterQuestionBean.getQuestionId()+".png";
                        masterSaveImage(masterHintImagePath,bufferImgHint);
                        masterUpdateImgRatioList.add(masterHintImagePath+"##"+getHintRatio());
                        masterQuestionBean.setIsHintAsImage(true);
                        masterQuestionBean.setHintImagePath(masterHintImagePath);
                    }
                } else if(existImgHint != null && !chkHintImage.isSelected()) {
                    deleteFile(selectedQuestionBean.getHintImagePath());
                    updateImgRatioList.add(selectedQuestionBean.getHintImagePath());
                    questionsBean.setIsHintAsImage(false);
                    questionsBean.setHintImagePath(hintImagePath);
                    
                    //Master
                    if(selectedMasterQuestionBean != null) {
                        deleteFile(selectedMasterQuestionBean.getHintImagePath());
                        masterUpdateImgRatioList.add(selectedMasterQuestionBean.getHintImagePath());
                        masterQuestionBean.setIsHintAsImage(false);
                        masterQuestionBean.setHintImagePath(masterHintImagePath);
                    }
                } else if(existImgHint != null && bufferImgHint == null && chapterId != selectedQuestionBean.getChapterId()) {
                    deleteFile(selectedQuestionBean.getHintImagePath());
                    hintImagePath = "images/"+chapterId+"Hint"+selectedQuestionBean.getQuestionId()+".png";
                    updateImgRatioList.add(selectedQuestionBean.getHintImagePath()+"##"+hintImagePath+"##"+getHintRatio());

                    questionsBean.setIsHintAsImage(true);
                    questionsBean.setHintImagePath(hintImagePath);
                    saveImage(hintImagePath,existImgHint);
                } else if(existImgHint != null && bufferImgHint == null && getHintRatio() != getImageRatio(selectedQuestionBean.getHintImagePath())) {
                    hintImagePath = selectedQuestionBean.getHintImagePath();
                    updateImgRatioList.add(hintImagePath+"##"+hintImagePath+"##"+getHintRatio());
                    questionsBean.setIsHintAsImage(true);
                    questionsBean.setHintImagePath(hintImagePath);
                    
                    //Master
                    if(selectedMasterQuestionBean != null) {
                        masterHintImagePath = selectedMasterQuestionBean.getHintImagePath();
                        masterUpdateImgRatioList.add(masterHintImagePath+"##"+masterHintImagePath+"##"+getHintRatio());
                        masterQuestionBean.setIsHintAsImage(true);
                        masterQuestionBean.setHintImagePath(masterHintImagePath);
                    }
                    
                } else if(existImgHint != null && bufferImgHint == null) {
                    hintImagePath = selectedQuestionBean.getHintImagePath();
                    questionsBean.setIsHintAsImage(true);
                    questionsBean.setHintImagePath(hintImagePath);    
                    
                    //Master
                    if(selectedMasterQuestionBean != null) {
                        masterHintImagePath = selectedMasterQuestionBean.getHintImagePath();
                        masterQuestionBean.setIsHintAsImage(true);
                        masterQuestionBean.setHintImagePath(masterHintImagePath);
                    }
                    
                } else if(!chkHintImage.isSelected()) {
                    questionsBean.setIsHintAsImage(false);
                    questionsBean.setHintImagePath(hintImagePath);
                    
                    //Master
                    if(selectedMasterQuestionBean != null) {
                        masterQuestionBean.setIsHintAsImage(false);
                        masterQuestionBean.setHintImagePath(masterHintImagePath);
                    }
                }
                
                String optionImagePath = "";
                String masterOptionImagePath = "";
                
                if(existImgOption != null && bufferImgOption != null) {
                    if(chapterId != selectedQuestionBean.getChapterId()) {
                        deleteFile(selectedQuestionBean.getOptionImagePath());
                        optionImagePath = "images/"+chapterId+"Option"+selectedQuestionBean.getQuestionId()+".png";
                        updateImgRatioList.add(selectedQuestionBean.getOptionImagePath()+"##"+optionImagePath+"##"+getOptionRatio());
                    } else {
                        optionImagePath = selectedQuestionBean.getOptionImagePath();
                        updateImgRatioList.add(optionImagePath+"##"+optionImagePath+"##"+getOptionRatio());
                    }
                    saveImage(optionImagePath,bufferImgOption);
                    questionsBean.setIsOptionAsImage(true);
                    questionsBean.setOptionImagePath(optionImagePath);
                    
                    questionsBean.setOptionA("\\mbox{}");
                    questionsBean.setOptionB("\\mbox{}");
                    questionsBean.setOptionC("\\mbox{}");
                    questionsBean.setOptionD("\\mbox{}");
                    
                    //Master
                    if(selectedMasterQuestionBean != null) {
                        masterOptionImagePath = selectedMasterQuestionBean.getOptionImagePath();
                        masterUpdateImgRatioList.add(masterOptionImagePath+"##"+masterOptionImagePath+"##"+getOptionRatio());
                        masterQuestionBean.setIsOptionAsImage(true);
                        masterQuestionBean.setOptionImagePath(masterOptionImagePath);
                        masterSaveImage(masterOptionImagePath,bufferImgOption);

                        masterQuestionBean.setOptionA("\\mbox{}");
                        masterQuestionBean.setOptionB("\\mbox{}");
                        masterQuestionBean.setOptionC("\\mbox{}");
                        masterQuestionBean.setOptionD("\\mbox{}");
                    }
                } else if(existImgOption == null && bufferImgOption != null) {
                    optionImagePath = "images/"+chapterId+"Option"+selectedQuestionBean.getQuestionId()+".png";
                    saveImage(optionImagePath,bufferImgOption);
                    updateImgRatioList.add(optionImagePath+"##"+getOptionRatio());
                    questionsBean.setIsOptionAsImage(true);
                    questionsBean.setOptionImagePath(optionImagePath);
                    questionsBean.setOptionA("\\mbox{}");
                    questionsBean.setOptionB("\\mbox{}");
                    questionsBean.setOptionC("\\mbox{}");
                    questionsBean.setOptionD("\\mbox{}");
                    
                    //Master
                    if(selectedMasterQuestionBean != null) {
                        masterOptionImagePath = "masterImages/"+masterChapterId+"Option"+selectedMasterQuestionBean.getQuestionId()+".png";
                        masterSaveImage(masterOptionImagePath,bufferImgOption);
                        masterUpdateImgRatioList.add(masterOptionImagePath+"##"+getOptionRatio());
                        masterQuestionBean.setIsOptionAsImage(true);
                        masterQuestionBean.setOptionImagePath(masterOptionImagePath);

                        masterQuestionBean.setOptionA("\\mbox{}");
                        masterQuestionBean.setOptionB("\\mbox{}");
                        masterQuestionBean.setOptionC("\\mbox{}");
                        masterQuestionBean.setOptionD("\\mbox{}");
                    }
                } else if(existImgOption != null && !chkOptionImage.isSelected()) {
                    deleteFile(selectedQuestionBean.getOptionImagePath());
                    updateImgRatioList.add(selectedQuestionBean.getOptionImagePath());
                    questionsBean.setIsOptionAsImage(false);
                    questionsBean.setOptionImagePath(optionImagePath);
                    questionsBean.setOptionA(txtOptionA.getText().trim());
                    questionsBean.setOptionB(txtOptionB.getText().trim());
                    questionsBean.setOptionC(txtOptionC.getText().trim());
                    questionsBean.setOptionD(txtOptionD.getText().trim());
                    
                    //Master
                    if(selectedMasterQuestionBean != null) {
                        deleteFile(selectedMasterQuestionBean.getOptionImagePath());
                        masterUpdateImgRatioList.add(selectedMasterQuestionBean.getOptionImagePath());
                        masterQuestionBean.setIsOptionAsImage(false);
                        masterQuestionBean.setOptionImagePath(masterOptionImagePath);
                    
                        masterQuestionBean.setOptionA("\\mbox{}");
                        masterQuestionBean.setOptionB("\\mbox{}");
                        masterQuestionBean.setOptionC("\\mbox{}");
                        masterQuestionBean.setOptionD("\\mbox{}");
                    }
                } else if(existImgOption != null && bufferImgOption == null && chapterId != selectedQuestionBean.getChapterId()) {
                    deleteFile(selectedQuestionBean.getOptionImagePath());
                    optionImagePath = "images/"+chapterId+"Option"+selectedQuestionBean.getQuestionId()+".png";
                    updateImgRatioList.add(selectedQuestionBean.getOptionImagePath()+"##"+optionImagePath+"##"+getOptionRatio());
                    saveImage(optionImagePath,existImgOption);
                    questionsBean.setIsOptionAsImage(true);
                    questionsBean.setOptionImagePath(optionImagePath);
                    
                    questionsBean.setOptionA("\\mbox{}");
                    questionsBean.setOptionB("\\mbox{}");
                    questionsBean.setOptionC("\\mbox{}");
                    questionsBean.setOptionD("\\mbox{}");
                }  else if(existImgOption != null && bufferImgOption == null && getOptionRatio() != getImageRatio(selectedQuestionBean.getOptionImagePath())) {
                    optionImagePath = selectedQuestionBean.getOptionImagePath();
                    updateImgRatioList.add(optionImagePath+"##"+optionImagePath+"##"+getOptionRatio());
                    questionsBean.setIsOptionAsImage(true);
                    questionsBean.setOptionImagePath(optionImagePath);
                    questionsBean.setOptionA("\\mbox{}");
                    questionsBean.setOptionB("\\mbox{}");
                    questionsBean.setOptionC("\\mbox{}");
                    questionsBean.setOptionD("\\mbox{}");
                    
                    //Master
                    if(selectedMasterQuestionBean != null) {
                        masterOptionImagePath = selectedMasterQuestionBean.getOptionImagePath();
                        masterUpdateImgRatioList.add(masterOptionImagePath+"##"+masterOptionImagePath+"##"+getOptionRatio());
                        masterQuestionBean.setIsOptionAsImage(true);
                        masterQuestionBean.setOptionImagePath(masterOptionImagePath);

                        masterQuestionBean.setOptionA("\\mbox{}");
                        masterQuestionBean.setOptionB("\\mbox{}");
                        masterQuestionBean.setOptionC("\\mbox{}");
                        masterQuestionBean.setOptionD("\\mbox{}");
                    }
                } else if(existImgOption != null && bufferImgOption == null) {
                    optionImagePath = selectedQuestionBean.getOptionImagePath();
                    questionsBean.setIsOptionAsImage(true);
                    questionsBean.setOptionImagePath(optionImagePath);
                    questionsBean.setOptionA("\\mbox{}");
                    questionsBean.setOptionB("\\mbox{}");
                    questionsBean.setOptionC("\\mbox{}");
                    questionsBean.setOptionD("\\mbox{}");
                    
                    //Master
                    if(selectedMasterQuestionBean != null) {
                        masterOptionImagePath = selectedMasterQuestionBean.getOptionImagePath();
                        masterQuestionBean.setIsOptionAsImage(true);
                        masterQuestionBean.setOptionImagePath(masterOptionImagePath);

                        masterQuestionBean.setOptionA("\\mbox{}");
                        masterQuestionBean.setOptionB("\\mbox{}");
                        masterQuestionBean.setOptionC("\\mbox{}");
                        masterQuestionBean.setOptionD("\\mbox{}");
                    }
                } else if(!chkOptionImage.isSelected()) {
                    questionsBean.setIsOptionAsImage(false);
                    questionsBean.setOptionImagePath(optionImagePath);
                    questionsBean.setOptionA(txtOptionA.getText().trim());
                    questionsBean.setOptionB(txtOptionB.getText().trim());
                    questionsBean.setOptionC(txtOptionC.getText().trim());
                    questionsBean.setOptionD(txtOptionD.getText().trim());
                    
                    //Master
                    if(selectedMasterQuestionBean != null) {
                        masterQuestionBean.setIsOptionAsImage(false);
                        masterQuestionBean.setOptionImagePath(masterOptionImagePath);

                        masterQuestionBean.setOptionA(txtOptionA.getText().trim());
                        masterQuestionBean.setOptionB(txtOptionB.getText().trim());
                        masterQuestionBean.setOptionC(txtOptionC.getText().trim());
                        masterQuestionBean.setOptionD(txtOptionD.getText().trim());
                    }
                }
                
                new QuestionOperation().updateModifyQuestion(questionsBean);
                new OptionImageDimentionOperation().updateOptionImageDimentions(questionsBean, false);
                
                if(updateImgRatioList.size() != 0) {
                    new ImageRatioOperation().updateModifyQuesImageRatio(updateImgRatioList);
                    ratioUpdate = true;
                }
                
                //Master
                if(selectedMasterQuestionBean != null) {
                    new MasterQuestionOperation().updateModifyQuestion(masterQuestionBean);
                    new MasterOptionImageDimentionOperation().updateOptionImageDimentions(masterQuestionBean);
                    
                    if(masterUpdateImgRatioList.size() != 0)
                        new MasterImageRationOperation().updateModifyQuesImageRatio(masterUpdateImgRatioList);
                }
                
                
                if(chapterId != selectedQuestionBean.getChapterId()) {
                    JOptionPane.showMessageDialog(rootPane, "Data Updated Successfully.\nYour Changed Chapter that Reason You Are Redirected to Home Page.");
                    new HomePage().setVisible(true);
                    this.dispose();
                } else {
                    JOptionPane.showMessageDialog(rootPane, "Data Updated Successfully.");
                    returnPageCall(questionsBean,ratioUpdate);
                }
            }
            
            if(!quesUpdate && !dimUpdate) {
                JOptionPane.showMessageDialog(rootPane, "No Updation Found.");
                returnPageCall(questionsBean,ratioUpdate);
            }
        } 
    }
}//GEN-LAST:event_BtnAddToFinalDBActionPerformed

private void returnPageCall(QuestionBean questionsBean,boolean ratioUpdate) {
    if(selectChapterWise != null) {
        selectChapterWise.setVisible(true);
        selectChapterWise.loadAfterModification(questionsBean,ratioUpdate);
    } else if(selectQuestionsUnitsWise != null) {
        selectQuestionsUnitsWise.setVisible(true);
        selectQuestionsUnitsWise.loadAfterModification(questionsBean,ratioUpdate);
    }
        
    this.dispose();
}

private boolean getUpdateStatuss() {
    boolean returnStatus = false;
    
    if(!selectedQuestionBean.getQuestion().trim().equalsIgnoreCase(txtQuestion.getText().trim()) ||
       !selectedQuestionBean.getYear().trim().equalsIgnoreCase(txtYear.getText().trim()) ||
       !selectedQuestionBean.getHint().trim().equalsIgnoreCase(txtHint.getText().trim()) ||
       !selectedQuestionBean.getAnswer().trim().equals(buttonGroup1.getSelection().getActionCommand().trim()) ||
        selectedQuestionBean.getType() != getTypeOfQuestion() || selectedQuestionBean.getLevel() != cmbLevel.getSelectedIndex() ||
        selectedQuestionBean.getChapterId() != chapterList.get(cmbChapter.getSelectedIndex()).getChapterId()) 
        returnStatus = true;
    
    if(existImgQuestion == null && bufferImgQuestion != null)
        returnStatus = true;
    else if(existImgQuestion != null && bufferImgQuestion != null)
        returnStatus = true;
    else if(existImgQuestion != null && !chkQueImage.isSelected())
        returnStatus = true;
    
    if(existImgHint == null && bufferImgHint != null)
        returnStatus = true;
    else if(existImgHint != null && bufferImgHint != null)
        returnStatus = true;
    else if(existImgHint != null && !chkHintImage.isSelected())
        returnStatus = true;
    
    if(existImgOption == null && bufferImgOption != null) {
        returnStatus = true;
    } else if(existImgOption != null && bufferImgOption != null) {
        returnStatus = true;
    } else if(existImgOption != null && !chkOptionImage.isSelected()) {
        returnStatus = true;
    } else if(existImgOption == null && bufferImgOption == null) {
        if(!selectedQuestionBean.getOptionA().trim().equalsIgnoreCase(txtOptionA.getText().trim()) ||
           !selectedQuestionBean.getOptionB().trim().equalsIgnoreCase(txtOptionB.getText().trim()) ||
           !selectedQuestionBean.getOptionC().trim().equalsIgnoreCase(txtOptionC.getText().trim()) ||
           !selectedQuestionBean.getOptionD().trim().equalsIgnoreCase(txtOptionD.getText().trim()))
        returnStatus = true;
    }
    
    return returnStatus;
}

private boolean getDimentionUpdateStatus() {
    boolean returnStatus = false;

    if(existImgQuestion != null && bufferImgQuestion == null) {
        if(getImageRatio(selectedQuestionBean.getQuestionImagePath()) != getQuestionRatio())
            returnStatus = true;
    } else if(existImgQuestion != null && bufferImgQuestion != null) {
        returnStatus = true;
    } else if(existImgQuestion == null && bufferImgQuestion != null) {
        returnStatus = true;
    } else if(existImgQuestion != null && !chkQueImage.isSelected()) {
        returnStatus = true;
    }
    
    if(existImgOption != null && bufferImgOption == null) {
        if(getImageRatio(selectedQuestionBean.getOptionImagePath()) != getOptionRatio())
            returnStatus = true;
    } else if(existImgOption != null && bufferImgOption != null) {
        returnStatus = true;
    } else if(existImgOption == null && bufferImgOption != null) {
        returnStatus = true;
    } else if(existImgOption != null && !chkOptionImage.isSelected()) {
        returnStatus = true;
    }
    
    if(existImgHint != null && bufferImgHint == null) {
        if(getImageRatio(selectedQuestionBean.getHintImagePath()) != getHintRatio())
            returnStatus = true;
    } else if(existImgHint != null && bufferImgHint != null) {
        returnStatus = true;
    } else if(existImgHint == null && bufferImgHint != null) {
        returnStatus = true;
    } else if(existImgHint != null && !chkHintImage.isSelected()) {
        returnStatus = true;
    }
    
    return returnStatus;
}

private boolean checkInsertStatuss() {
    boolean returnValue = true;
    String textErrorMessage = "";
    String imageErrorMessage = "";
    
    if(txtQuestion.getText().trim().isEmpty() || txtQuestion.getText().trim().equalsIgnoreCase("\\mbox{}")) {
        returnValue = false;
        textErrorMessage += "Question,";
    }
    
    if(chkQueImage.isSelected() && existImgQuestion == null && bufferImgQuestion == null) {
        returnValue = false;
        imageErrorMessage += "Question Image,";
    } else if(chkQueImage.isSelected() && existImgQuestion != null && bufferImgQuestion == null && lblQuestionImageView.getIcon() == null) {
        returnValue = false;
        imageErrorMessage += "Question Image,";
    }
    
    if((txtOptionA.getText().trim().isEmpty() || txtOptionB.getText().trim().isEmpty() || 
        txtOptionC.getText().trim().isEmpty() || txtOptionD.getText().trim().isEmpty() ||
        txtOptionA.getText().trim().equalsIgnoreCase("\\mbox{}") || txtOptionB.getText().trim().equalsIgnoreCase("\\mbox{}") ||
        txtOptionC.getText().trim().equalsIgnoreCase("\\mbox{}") || txtOptionD.getText().trim().equalsIgnoreCase("\\mbox{}")) && 
        !chkOptionImage.isSelected()) {
        returnValue = false;
        textErrorMessage += "Options,";
    } else if(chkOptionImage.isSelected() && existImgOption == null && bufferImgOption == null){
        returnValue = false;
        imageErrorMessage += "Option Image,";
    } else if(chkOptionImage.isSelected() && existImgOption != null && bufferImgOption == null && lblOptionImageView.getIcon() == null){
        returnValue = false;
        imageErrorMessage += "Option Image,";
    } 
    
    if(chkHintImage.isSelected() && existImgHint == null && bufferImgHint == null) {
        returnValue = false;
        imageErrorMessage += "Hint Image,";
    } else if(chkHintImage.isSelected() && existImgHint != null && bufferImgHint == null && lblHintImageView.getIcon() == null) {
        returnValue = false;
        imageErrorMessage += "Hint Image,";
    }
    
    if(!returnValue) {
        String message = "";
        if(textErrorMessage.length() > 0) {
            message += "Please Enter "+ textErrorMessage.substring(0,textErrorMessage.length()-1) +" Data.\n";
        }
        if(imageErrorMessage.length() > 0) {
            message += "Please Add "+ imageErrorMessage.substring(0,imageErrorMessage.length()-1) +".";
        }
        JOptionPane.showMessageDialog(rootPane, message);
    }
    
    return returnValue;
    
}

    private void sett(JTextField jt, JLabel st, int o) {
        StringBuilder sb = new StringBuilder(jt.getText());
        if (o == 0) {
            sb.insert(jt.getCaretPosition(), "}\\\\\\mbox{");
        }
        jt.setText(sb.toString());
        try {
            String finalStr = "";
            finalStr = jt.getText();
            finalStr = "\\begin{array}{l}" + finalStr + "\\end{array}";
            TeXFormula formula = new TeXFormula(finalStr);
            TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 20);
            icon.setInsets(new Insets(0, 0, 0, 0));
            BufferedImage image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
            Graphics2D g2 = image.createGraphics();
            g2.setColor(Color.white);
            g2.fillRect(0, 0, icon.getIconWidth(), icon.getIconHeight());
            JLabel jl = new JLabel();
            jl.setForeground(new Color(0, 0, 0));
            icon.paintIcon(jl, g2, 0, 0);
            st.setIcon(icon);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(rootPane, e.getMessage());
        }
    }

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        Object[] options = {"YES", "CANCEL"};
        int i = JOptionPane.showOptionDialog(null, "Do you want to close this task?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (i == 0) {
            new HomePage().setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_formWindowClosing

    private void BtnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBackActionPerformed
        if(selectChapterWise != null)
            selectChapterWise.setVisible(true);
        else if(selectQuestionsUnitsWise != null)
            selectQuestionsUnitsWise.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_BtnBackActionPerformed

    private void btnQuePasteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQuePasteActionPerformed
        bufferImgQuestion = (BufferedImage) getImageFromClipboard();
        lblQuestionImageView.setIcon(null);
        setQuestionRatio(1.0f);
        if (bufferImgQuestion != null) {
            loadImage(bufferImgQuestion, lblQuestionImageView);
            LblQuestionPreview.setVisible(true);
        } else {
            LblQuestionPreview.setVisible(false);
            JOptionPane.showMessageDialog(rootPane, "Invalid Copied Image");
        }
    }//GEN-LAST:event_btnQuePasteActionPerformed

    private void chkQueImageItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_chkQueImageItemStateChanged
        LblQuestionPreview.setVisible(false);
        lblQuestionImageView.setIcon(null);
        bufferImgQuestion = null;
        btnQuePaste.setEnabled(chkQueImage.isSelected());
        setQuestionRatio(1.0f);
        if(existImgQuestion != null && chkQueImage.isSelected()) {
            LblQuestionPreview.setVisible(true);
            loadImage(existImgQuestion, lblQuestionImageView);
            setQuestionRatio(getImageRatio(selectedQuestionBean.getQuestionImagePath()));
        }
    }//GEN-LAST:event_chkQueImageItemStateChanged

    private void btnHintPasteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHintPasteActionPerformed
        // TODO add your handling code here:
        bufferImgHint = (BufferedImage) getImageFromClipboard();
        lblHintImageView.setIcon(null);
        setHintRatio(1.0f);
        if (bufferImgHint != null) {
            loadImage(bufferImgHint, lblHintImageView);
            LblHintPreview.setVisible(true);
        } else {
            LblHintPreview.setVisible(false);
            JOptionPane.showMessageDialog(rootPane, "Invalid Copied Image");
        }
    }//GEN-LAST:event_btnHintPasteActionPerformed

    private void btnOptionPasteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOptionPasteActionPerformed
        // TODO add your handling code here:
        bufferImgOption = (BufferedImage) getImageFromClipboard();
        lblOptionImageView.setIcon(null);
        setOptionRatio(1.0f);
        if (bufferImgOption != null) {
            loadImage(bufferImgOption, lblOptionImageView);
            LblOptionPreview.setVisible(true);
        } else {
            LblOptionPreview.setVisible(false);
            JOptionPane.showMessageDialog(rootPane, "Invalid Copied Image");
        }
    }//GEN-LAST:event_btnOptionPasteActionPerformed

    private void chkHintImageItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_chkHintImageItemStateChanged
        // TODO add your handling code here:
        LblHintPreview.setVisible(false);
        lblHintImageView.setIcon(null);
        bufferImgHint = null;
        btnHintPaste.setEnabled(chkHintImage.isSelected());
        setHintRatio(1.0f);
        if(existImgHint != null && chkHintImage.isSelected()) {
            LblHintPreview.setVisible(true);
            loadImage(existImgHint, lblHintImageView);
            setHintRatio(getImageRatio(selectedQuestionBean.getHintImagePath()));
        }
    }//GEN-LAST:event_chkHintImageItemStateChanged

    private void chkOptionImageItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_chkOptionImageItemStateChanged
        // TODO add your handling code here:
        txtOptionStateChanged(!chkOptionImage.isSelected());
        LblOptionPreview.setVisible(false);
        lblOptionImageView.setIcon(null);
        bufferImgOption = null;
        btnOptionPaste.setEnabled(chkOptionImage.isSelected());
        setOptionRatio(1.0f);
        if(existImgOption != null && chkOptionImage.isSelected()) {
            LblOptionPreview.setVisible(true);
            loadImage(existImgOption, lblOptionImageView);
            setOptionRatio(getImageRatio(selectedQuestionBean.getOptionImagePath()));
        }
    }//GEN-LAST:event_chkOptionImageItemStateChanged
    
    private void chkNumericalItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_chkNumericalItemStateChanged
        if (chkNumerical.isSelected()) {
            chkTheory.setSelected(false);
        }
    }//GEN-LAST:event_chkNumericalItemStateChanged

    private void chkTheoryItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_chkTheoryItemStateChanged
        if (chkTheory.isSelected()) {
            chkNumerical.setSelected(false);
        }
    }//GEN-LAST:event_chkTheoryItemStateChanged

    private void rdoOptionAItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdoOptionAItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_rdoOptionAItemStateChanged

    private void btnAddNewLineHintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddNewLineHintActionPerformed
        sett(txtHint, lblHint, 0);
    }//GEN-LAST:event_btnAddNewLineHintActionPerformed

    private void btnAddNewLineDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddNewLineDActionPerformed
        sett(txtOptionD, lblOptionD, 0);
    }//GEN-LAST:event_btnAddNewLineDActionPerformed

    private void txtOptionDKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtOptionDKeyReleased
        if (evt.getKeyCode() == 10) {
            sett(txtOptionD, lblOptionD, 1);
        }
    }//GEN-LAST:event_txtOptionDKeyReleased

    private void txtHintKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtHintKeyReleased
        if (evt.getKeyCode() == 10) {
            sett(txtHint, lblHint, 1);
        }
    }//GEN-LAST:event_txtHintKeyReleased

    private void btnAddNewLineCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddNewLineCActionPerformed
        sett(txtOptionC, lblOptionC, 0);
    }//GEN-LAST:event_btnAddNewLineCActionPerformed

    private void txtOptionCKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtOptionCKeyReleased
        if (evt.getKeyCode() == 10) {
            sett(txtOptionC, lblOptionC, 1);
        }
    }//GEN-LAST:event_txtOptionCKeyReleased

    private void btnAddNewLineBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddNewLineBActionPerformed
        sett(txtOptionB, lblOptionB, 0);
    }//GEN-LAST:event_btnAddNewLineBActionPerformed

    private void txtOptionBKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtOptionBKeyReleased
        if (evt.getKeyCode() == 10) {
            sett(txtOptionB, lblOptionB, 1);
        }
    }//GEN-LAST:event_txtOptionBKeyReleased

    private void btnAddNewLineAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddNewLineAActionPerformed
        sett(txtOptionA, lblOptionA, 0);
    }//GEN-LAST:event_btnAddNewLineAActionPerformed

    private void txtOptionAKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtOptionAKeyReleased
        if (evt.getKeyCode() == 10) {
            sett(txtOptionA, lblOptionA, 1);
        }
    }//GEN-LAST:event_txtOptionAKeyReleased

    private void btnYearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnYearActionPerformed
        String str = txtQuestion.getText();
        String s = "", s1 = "", sss = "", e = "";
        char temp[] = str.toCharArray();
        char que[] = new char[temp.length];
        char end[] = new char[temp.length];
        for (int i = temp.length - 1, t = 0, k = 0; i >= 0; i--) {
            if (temp[i] == ']') {
                que[t] = temp[i];
                while (temp[i] != '[') {
                    i--;
                    t++;
                    que[t] = temp[i];
                }
                break;
            } else if (temp[i] == ')') {
            que[t] = temp[i];
            while (temp[i] != '(') {
                i--;
                t++;
                que[t] = temp[i];
            }
            break;
        } else {
            end[k] = temp[i];
            k++;
        }
        }
        for (int i = end.length - 1; i >= 0; i--) {
            if (end[i] == ' ') {
            } else {
                System.out.println(end[i]);
                e = e + end[i];
            }
        }
        System.out.println(e);
        for (int i = 0; i < que.length; i++) {
            if (que[i] == '[') {
                s = s + que[i];
                break;
            } else if (que[i] == '(') {
                s = s + que[i];
                break;
            }
            s = s + que[i];
        }
        s = s.trim();
        que = s.toCharArray();
        if (s.length() > 0) {
            for (int i = s.length() - 1; i >= 0; i--) {
                s1 = s1 + que[i];
            }
            StringBuilder sb = new StringBuilder(txtQuestion.getText());
            sb.delete(txtQuestion.getCaretPosition(), str.length());
            s = sb.toString();
            sss = s + e;
            s1 = s1.trim();
            txtYear.setText(s1);
            txtQuestion.setText(sss);
            try {
                String finalStr = sss;
                TeXFormula formula = new TeXFormula(finalStr);
                TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 20);
                icon.setInsets(new Insets(0, 0, 0, 0));

                BufferedImage image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
                Graphics2D g2 = image.createGraphics();
                g2.setColor(Color.white);
                g2.fillRect(0, 0, icon.getIconWidth(), icon.getIconHeight());
                JLabel jl = new JLabel();
                jl.setForeground(new Color(0, 0, 0));
                icon.paintIcon(jl, g2, 0, 0);
                lblQuestion.setIcon(icon);
            } catch (Exception easd) {
                JOptionPane.showMessageDialog(rootPane, easd.getMessage());
            }
        }
    }//GEN-LAST:event_btnYearActionPerformed

    private void btnAddNewLineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddNewLineActionPerformed
        sett(txtQuestion, lblQuestion, 0);
    }//GEN-LAST:event_btnAddNewLineActionPerformed

    private void txtQuestionKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtQuestionKeyReleased
        if (evt.getKeyCode() == 10) {
            sett(txtQuestion, lblQuestion, 1);
        }
    }//GEN-LAST:event_txtQuestionKeyReleased

    private void LblQuestionPreviewMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblQuestionPreviewMouseClicked
        switch (evt.getModifiers()) {
            case InputEvent.BUTTON1_MASK: {
                if(bufferImgQuestion != null)
                    new PreviewImage(bufferImgQuestion,"Question",this).setVisible(true); 
                else if(existImgQuestion != null)
                    new PreviewImage(existImgQuestion,"Question",this).setVisible(true); 
                this.setEnabled(false);
                break;
            }
        }
    }//GEN-LAST:event_LblQuestionPreviewMouseClicked

    private void LblOptionPreviewMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblOptionPreviewMouseClicked
        // TODO add your handling code here:
        switch (evt.getModifiers()) {
            case InputEvent.BUTTON1_MASK: {
                if(bufferImgOption != null)
                    new PreviewImage(bufferImgOption,"Option",this).setVisible(true); 
                else if(existImgOption != null)
                    new PreviewImage(existImgOption,"Option",this).setVisible(true); 
                this.setEnabled(false);
                break;
            }
        }
    }//GEN-LAST:event_LblOptionPreviewMouseClicked

    private void LblHintPreviewMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblHintPreviewMouseClicked
        // TODO add your handling code here:
        switch (evt.getModifiers()) {
            case InputEvent.BUTTON1_MASK: {
                if(bufferImgHint != null)
                    new PreviewImage(bufferImgHint,"Hint",this).setVisible(true); 
                else if(existImgHint != null)
                    new PreviewImage(existImgHint,"Hint",this).setVisible(true); 
                this.setEnabled(false);
                break;
            }
        }
    }//GEN-LAST:event_LblHintPreviewMouseClicked

    private void BtnResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnResetActionPerformed
        // TODO add your handling code here:
        Object[] options = {"YES", "CANCEL"};
        int i = JOptionPane.showOptionDialog(null, "Do You Want to Reset Page?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (i == 0) {
            setInitialValues();
        }
    }//GEN-LAST:event_BtnResetActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ModifyQuestion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ModifyQuestion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ModifyQuestion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ModifyQuestion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnAddToFinalDB;
    private javax.swing.JButton BtnBack;
    private javax.swing.JButton BtnHide;
    private javax.swing.JButton BtnModify;
    private javax.swing.JButton BtnReset;
    private javax.swing.JPanel FooterPanel;
    private javax.swing.JLabel LblHintPreview;
    private javax.swing.JLabel LblOptionPreview;
    private javax.swing.JLabel LblQuestionPreview;
    private javax.swing.JLabel LblTitle;
    private javax.swing.JScrollPane RightBodyScrollPane;
    private javax.swing.JButton btnAddNewLine;
    private javax.swing.JButton btnAddNewLineA;
    private javax.swing.JButton btnAddNewLineB;
    private javax.swing.JButton btnAddNewLineC;
    private javax.swing.JButton btnAddNewLineD;
    private javax.swing.JButton btnAddNewLineHint;
    private javax.swing.JButton btnHintPaste;
    private javax.swing.JButton btnOptionPaste;
    private javax.swing.JButton btnQuePaste;
    private javax.swing.JButton btnYear;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JCheckBox chkHintImage;
    private javax.swing.JCheckBox chkNumerical;
    private javax.swing.JCheckBox chkOptionImage;
    private javax.swing.JCheckBox chkQueImage;
    private javax.swing.JCheckBox chkTheory;
    private javax.swing.JComboBox<String> cmbChapter;
    private javax.swing.JComboBox cmbLevel;
    private javax.swing.JFrame jFrame1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblA;
    private javax.swing.JLabel lblB;
    private javax.swing.JLabel lblC;
    private javax.swing.JLabel lblChapterTopic;
    private javax.swing.JLabel lblD;
    private javax.swing.JLabel lblHint;
    private javax.swing.JLabel lblHintImageView;
    private javax.swing.JLabel lblOption;
    private javax.swing.JLabel lblOptionA;
    private javax.swing.JLabel lblOptionB;
    private javax.swing.JLabel lblOptionC;
    private javax.swing.JLabel lblOptionD;
    private javax.swing.JLabel lblOptionImageView;
    private javax.swing.JLabel lblQuestion;
    private javax.swing.JLabel lblQuestionImageView;
    private javax.swing.JLabel lblQuestionNo;
    private javax.swing.JLabel lblQuestionNo5;
    private javax.swing.JLabel lhint;
    private javax.swing.JPanel panelHintImage;
    private javax.swing.JPanel panelOptionImage;
    private javax.swing.JPanel panelQueImage;
    private javax.swing.JRadioButton rdoOptionA;
    private javax.swing.JRadioButton rdoOptionB;
    private javax.swing.JRadioButton rdoOptionC;
    private javax.swing.JRadioButton rdoOptionD;
    private javax.swing.JTextField txtHint;
    private javax.swing.JLabel txtLevel;
    private javax.swing.JTextField txtOptionA;
    private javax.swing.JTextField txtOptionB;
    private javax.swing.JTextField txtOptionC;
    private javax.swing.JTextField txtOptionD;
    private javax.swing.JTextField txtQuestion;
    private javax.swing.JTextField txtYear;
    // End of variables declaration//GEN-END:variables

    private void deleteFile (String path) {
        if(processPath == null)
            processPath = new ProcessManager().getProcessPath();
        File f1 = new File(path);
        File f2 = new File(processPath+"/"+path);
        f1.delete();
        f2.delete();
    }
    
    private void masterSaveImage(String path,BufferedImage bufferedImage) {
        try {
            File newFile1 = new File(path);
            ImageIO.write(bufferedImage, "png", newFile1);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    private void saveImage(String path,BufferedImage bufferedImage) {
        if(processPath == null)
            processPath = new ProcessManager().getProcessPath();
        try {
            File newFile1 = new File(path);
            File newFile2 = new File(processPath+"/"+path);
            ImageIO.write(bufferedImage, "png", newFile1);
            ImageIO.write(bufferedImage, "png", newFile2);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    private int getTypeOfQuestion() {
        int questiontype = 0;
        if (chkTheory.isSelected()) {
            questiontype = 0;
        } else if (chkNumerical.isSelected()) {
            questiontype = 1;
        } else {
            JOptionPane.showMessageDialog(null, "Please select type of question.");
        }
        return questiontype;
    }

    private String replace0(String que) {
        String str = que;
        System.out.println("S:"+str);
        ArrayList<Character> limit = new ArrayList<Character>();
        limit.add('0');
        limit.add('1');
        limit.add('2');
        limit.add('3');
        limit.add('4');
        limit.add('5');
        limit.add('6');
        limit.add('7');
        limit.add('8');
        limit.add('9');
        char c[] = new char[str.length()];
        c = str.toCharArray();
        char temp[] = new char[str.length()];
        for (int i = 0, j = 0; i < str.length(); i++, j++) {
            if (c[i] == '0') {
                if (i < c.length - 1) {
                    if (c[i + 1] == '\\') {
                        if (c[i + 2] == '_') {
                            if (limit.contains(c[i + 3])) {
                                temp[j] = 'O';
                            } else {
                                temp[j] = c[j];
                            }
                        } else {
                            temp[j] = c[j];
                        }
                    } else {
                        temp[j] = c[j];
                    }
                }
            } else {
                temp[j] = c[j];
            }
        }
        str = "";
        for (int i = 0; i < temp.length; i++) {
            str = str + temp[i];
        }
        str = str.replace("-", "{\\minus}");
        str.trim();
        str = str.replace(" ", " ");
        System.out.println("R:"+str);
        return str;
    }
    
    private float getImageRatio(String imgPath) {
        float returnValue = 1.0f;
        if(imageRatioList != null) {
            for(ImageRatioBean imageRatioBean : imageRatioList) {
                if(imageRatioBean.getImageName().trim().equalsIgnoreCase(imgPath)) {
                    returnValue = (float)imageRatioBean.getViewDimention();
                    break;
                }
            }
        }
        return returnValue;
    }
    
    //Setters And Getters
    public float getQuestionRatio() {
        return questionRatio;
    }

    public void setQuestionRatio(float questionRatio) {
        this.questionRatio = questionRatio;
    }

    public float getOptionRatio() {
        return optionRatio;
    }

    public void setOptionRatio(float optionRatio) {
        this.optionRatio = optionRatio;
    }

    public float getHintRatio() {
        return hintRatio;
    }

    public void setHintRatio(float hintRatio) {
        this.hintRatio = hintRatio;
    }
}
