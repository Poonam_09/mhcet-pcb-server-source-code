/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pages;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import com.bean.CountYearBean;
import com.bean.QuestionBean;
import com.bean.SubjectBean;
import com.bean.ViewQuestionPaperBean;
import com.db.operations.QuestionPaperOperation;
import com.db.operations.SubjectOperation;
import com.Model.TitleInfo;
/**
 *
 * @author admin
 */
public class MultipleYearSelection extends javax.swing.JFrame {
    private javax.swing.JCheckBox[] chkboxArrayYear;
    private ArrayList<SubjectBean> subjectList;
    private ArrayList<ViewQuestionPaperBean> viewQuestionPaperYearList;
    private SubjectBean subjectFirstBean;
    private SubjectBean subjectSecondBean;
    private SubjectBean subjectThirdBean;
    private ArrayList<String> selectedYearList;
    private String printingPaperType;
    private static int currentPatternIndex;
    
    public MultipleYearSelection(int currentPatternIndex,String printingPaperType) {
        this.currentPatternIndex=currentPatternIndex;
        initComponents();
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        setLocationRelativeTo(null);
        BtnAll.setVisible(true);
        selectedYearList = null;
        this.printingPaperType = printingPaperType;
        viewQuestionPaperYearList = new QuestionPaperOperation().getViewQuestionPaperList();
        if (viewQuestionPaperYearList != null) {
            subjectList = new SubjectOperation().getSubjectsList();
            int index = 0;
            for (SubjectBean sb : subjectList) {
                if (index == 0) {
                    subjectFirstBean = sb;
                } else if (index == 1) {
                    subjectSecondBean = sb;
                } else if (index == 2) {
                    subjectThirdBean = sb;
                }
                index++;
            }
            setQuestionYearPanel();
        }
        setExtendedState(MAXIMIZED_BOTH);
    }
    
    private void setQuestionYearPanel() {
        PanelBody.removeAll();
        chkboxArrayYear = new JCheckBox[viewQuestionPaperYearList.size()];
        for (int x = 0; x < viewQuestionPaperYearList.size(); x++) {
            chkboxArrayYear[x] = new JCheckBox();
            chkboxArrayYear[x].setVerticalTextPosition(SwingConstants.TOP);
            chkboxArrayYear[x].setText("<html>"
                    + "<b><center><font size=6 color=#EF9433>" + viewQuestionPaperYearList.get(x).getPreviousYear()
                    + "</font></center></b><hr>Total Questions: " + viewQuestionPaperYearList.get(x).getTotalQues()
                    + "<br />" + subjectFirstBean.getSubjectName() + " Count: " + viewQuestionPaperYearList.get(x).getSubjectFirstQues()
                    + "<br />" + subjectSecondBean.getSubjectName() + " Count: " + viewQuestionPaperYearList.get(x).getSubjectSecondQues()
                    + "<br />" + subjectThirdBean.getSubjectName() + " Count: " + viewQuestionPaperYearList.get(x).getSubjectThirdQues()
                    + "<br />Used Quetions: " + viewQuestionPaperYearList.get(x).getUsedQues()
                    + "</html>");
            chkboxArrayYear[x].setMaximumSize(new java.awt.Dimension(180, 160));
            chkboxArrayYear[x].setMinimumSize(new java.awt.Dimension(180, 160));
            chkboxArrayYear[x].setPreferredSize(new java.awt.Dimension(180, 160));
//            chkboxArrayYear[x].setBorderPainted(false);
            chkboxArrayYear[x].setBackground(new java.awt.Color(11, 45, 55));
            chkboxArrayYear[x].setFont(new java.awt.Font("Microsoft YaHei UI", 0, 14)); // NOI18N
            chkboxArrayYear[x].setForeground(new java.awt.Color(255, 255, 255));
            chkboxArrayYear[x].setVerticalAlignment(1);
        }
        
        GridBagConstraints cons = new GridBagConstraints();
        cons.gridx = 0;
        cons.gridy = 0;
        cons.gridwidth = 1;
        cons.gridheight = 1;
        cons.anchor = GridBagConstraints.ABOVE_BASELINE;
        cons.weightx = 2;
        cons.weighty = 1;
        cons.insets = new java.awt.Insets(10, 10, 10, 10);
        GridBagLayout layout = new GridBagLayout();
        for (int x = 0; x < viewQuestionPaperYearList.size(); x++) {
            if (x % 6 == 0) {
                cons.gridy++;
                cons.gridx = 0;
            }
            PanelBody.setLayout(layout);
            PanelBody.add(chkboxArrayYear[x], cons);
            cons.gridx++;
        }
        PanelBody.validate();
        PanelBody.repaint();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BodyPanelScrollPane = new javax.swing.JScrollPane();
        PanelBody = new javax.swing.JPanel();
        PanelHeader = new javax.swing.JPanel();
        SubPanelHeader1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        SubPanelHeader2 = new javax.swing.JPanel();
        BtnBack = new javax.swing.JButton();
        BtnStart = new javax.swing.JButton();
        BtnAll = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        BodyPanelScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        BodyPanelScrollPane.setName("BodyPanelScrollPane"); // NOI18N

        PanelBody.setBackground(new java.awt.Color(0, 102, 102));
        PanelBody.setName("PanelBody"); // NOI18N

        javax.swing.GroupLayout PanelBodyLayout = new javax.swing.GroupLayout(PanelBody);
        PanelBody.setLayout(PanelBodyLayout);
        PanelBodyLayout.setHorizontalGroup(
            PanelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1138, Short.MAX_VALUE)
        );
        PanelBodyLayout.setVerticalGroup(
            PanelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 220, Short.MAX_VALUE)
        );

        BodyPanelScrollPane.setViewportView(PanelBody);

        PanelHeader.setName("PanelHeader"); // NOI18N

        SubPanelHeader1.setBackground(new java.awt.Color(0, 102, 102));
        SubPanelHeader1.setName("SubPanelHeader1"); // NOI18N

        jLabel1.setFont(new java.awt.Font("Microsoft JhengHei", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Previous Years Question Papers");
        jLabel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 204), new java.awt.Color(255, 255, 255), new java.awt.Color(204, 255, 204), new java.awt.Color(255, 255, 255)));
        jLabel1.setName("jLabel1"); // NOI18N

        javax.swing.GroupLayout SubPanelHeader1Layout = new javax.swing.GroupLayout(SubPanelHeader1);
        SubPanelHeader1.setLayout(SubPanelHeader1Layout);
        SubPanelHeader1Layout.setHorizontalGroup(
            SubPanelHeader1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 413, Short.MAX_VALUE)
        );
        SubPanelHeader1Layout.setVerticalGroup(
            SubPanelHeader1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 61, Short.MAX_VALUE)
        );

        SubPanelHeader2.setBackground(new java.awt.Color(0, 102, 102));
        SubPanelHeader2.setName("SubPanelHeader2"); // NOI18N
        SubPanelHeader2.setLayout(new java.awt.GridLayout(1, 0));

        BtnBack.setFont(new java.awt.Font("Microsoft JhengHei", 1, 18)); // NOI18N
        BtnBack.setForeground(new java.awt.Color(255, 255, 255));
        BtnBack.setText("Back");
        BtnBack.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 204), new java.awt.Color(255, 255, 255), new java.awt.Color(204, 255, 204), new java.awt.Color(255, 255, 255)));
        BtnBack.setContentAreaFilled(false);
        BtnBack.setName("BtnBack"); // NOI18N
        BtnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBackActionPerformed(evt);
            }
        });
        SubPanelHeader2.add(BtnBack);

        BtnStart.setFont(new java.awt.Font("Microsoft JhengHei", 1, 18)); // NOI18N
        BtnStart.setForeground(new java.awt.Color(255, 255, 255));
        BtnStart.setText("Start");
        BtnStart.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 204), new java.awt.Color(255, 255, 255), new java.awt.Color(204, 255, 204), new java.awt.Color(255, 255, 255)));
        BtnStart.setContentAreaFilled(false);
        BtnStart.setName("BtnStart"); // NOI18N
        BtnStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnStartActionPerformed(evt);
            }
        });
        SubPanelHeader2.add(BtnStart);

        BtnAll.setFont(new java.awt.Font("Microsoft JhengHei", 1, 18)); // NOI18N
        BtnAll.setForeground(new java.awt.Color(255, 255, 255));
        BtnAll.setText("All");
        BtnAll.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 255, 204), new java.awt.Color(255, 255, 255), new java.awt.Color(204, 255, 204), java.awt.Color.white));
        BtnAll.setContentAreaFilled(false);
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        SubPanelHeader2.add(BtnAll);

        javax.swing.GroupLayout PanelHeaderLayout = new javax.swing.GroupLayout(PanelHeader);
        PanelHeader.setLayout(PanelHeaderLayout);
        PanelHeaderLayout.setHorizontalGroup(
            PanelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 413, Short.MAX_VALUE)
            .addGroup(PanelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(SubPanelHeader1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(PanelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(SubPanelHeader2, javax.swing.GroupLayout.DEFAULT_SIZE, 413, Short.MAX_VALUE))
        );
        PanelHeaderLayout.setVerticalGroup(
            PanelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 104, Short.MAX_VALUE)
            .addGroup(PanelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(PanelHeaderLayout.createSequentialGroup()
                    .addComponent(SubPanelHeader1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(43, Short.MAX_VALUE)))
            .addGroup(PanelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelHeaderLayout.createSequentialGroup()
                    .addContainerGap(61, Short.MAX_VALUE)
                    .addComponent(SubPanelHeader2, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(PanelHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(BodyPanelScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 413, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(PanelHeader, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(224, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addGap(107, 107, 107)
                    .addComponent(BodyPanelScrollPane)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void BtnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBackActionPerformed
    new HomePage().setVisible(true);
    this.dispose();
}//GEN-LAST:event_BtnBackActionPerformed

private void BtnStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnStartActionPerformed
    boolean selectedStatus = false;
    for (int i = 0; i < chkboxArrayYear.length; i++) {
        if (chkboxArrayYear[i].isSelected()) {
            selectedStatus = true;
            break;
        }
    }
    
    if (selectedStatus) {
        if (viewQuestionPaperYearList != null) {
            for (int i = 0; i < chkboxArrayYear.length; i++) {
                if (chkboxArrayYear[i].isSelected()) {
                    String getYear = chkboxArrayYear[i].getActionCommand();
                    String strSplt[] = getYear.split("EF9433>");
                    String tempSplt = strSplt[1];
                    strSplt = tempSplt.split("<");
                    tempSplt = strSplt[0];
                    if (selectedYearList == null) {
                        selectedYearList = new ArrayList<String>();
                    }
                    selectedYearList.add(tempSplt);
                }
            }
            nextCall();
        }
    } else {
        JOptionPane.showMessageDialog(null, "Please Select Atleast One Paper");
    }
}//GEN-LAST:event_BtnStartActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        new HomePage().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_formWindowClosing

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        Object[] options = {"YES", "NO"};
        int oo = JOptionPane.showOptionDialog(null, "Are You Sure to Select all Question Papers?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (oo == 0) {
            if (viewQuestionPaperYearList != null) {
                selectedYearList = new ArrayList<String>();
                for (ViewQuestionPaperBean viewQuestionPaperBean : viewQuestionPaperYearList) {
                    selectedYearList.add(viewQuestionPaperBean.getPreviousYear());
                }
                nextCall();
            }
        }
    }//GEN-LAST:event_BtnAllActionPerformed
    
    private void nextCall() {
        ArrayList<QuestionBean> selectedQuestionsList = new QuestionPaperOperation().getPreviousYearWiseQuestionList(selectedYearList);
        ArrayList<CountYearBean> selectedYearCountList = null;
        CountYearBean countYearBean = null;
        for (String year : selectedYearList) {
            int totalQues = 0;
            
            for (QuestionBean questionsBean : selectedQuestionsList) {
                if (questionsBean.getYear().trim().equals(year.trim()))
                    totalQues += 1;
            }
            
            countYearBean = new CountYearBean();
            countYearBean.setYear(year);
            countYearBean.setTotalQuestions(totalQues);
            
            if (selectedYearCountList == null) 
                selectedYearCountList = new ArrayList<CountYearBean>();
            
            selectedYearCountList.add(countYearBean);
        }
        
        new MultipleYearQuestionsSelection(currentPatternIndex,selectedYearCountList, selectedQuestionsList,printingPaperType).setVisible(true);
        this.dispose();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(PracticeChapterSelection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(PracticeChapterSelection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(PracticeChapterSelection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(PracticeChapterSelection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MultipleYearSelection(currentPatternIndex,"Paper").setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane BodyPanelScrollPane;
    private javax.swing.JButton BtnAll;
    private javax.swing.JButton BtnBack;
    private javax.swing.JButton BtnStart;
    private javax.swing.JPanel PanelBody;
    private javax.swing.JPanel PanelHeader;
    private javax.swing.JPanel SubPanelHeader1;
    private javax.swing.JPanel SubPanelHeader2;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}
