
package com.pages;

import com.Model.TestUtility;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.*;
import com.Pdf.Generation.PdfPageSetup;
import com.Word.Generation.WordPageSetup;
import com.bean.ChapterBean;
import com.bean.ImageRatioBean;
import java.util.Collections;
import com.bean.QuestionBean;
import com.bean.SubjectBean;
import com.bean.PrintedTestBean;
import com.db.operations.ImageRatioOperation;
import com.db.operations.MasterQuestionOperation;
import com.db.operations.QuestionOperation;
import com.ui.support.pages.PleaseWaitPanel;
import java.awt.event.KeyEvent;
import com.ui.support.pages.QuestionPanel;
import com.Model.TitleInfo;
import com.db.operations.ClassSaveTestOperation;
import com.db.operations.PatternStatusOperation;
import ui.SaveTest;

public class SingleChapterQuestionsSelection extends javax.swing.JFrame {

    private int currentIndex, totalSelectedQueCount;
    private ArrayList<QuestionBean> questionsList;
    private ArrayList<QuestionBean> sortedQuestionsList; 
    private ArrayList<QuestionBean> selectedQuestionsList;
    private ArrayList<Integer> addedSequence;
    private QuestionPanel currentPanel = null;
    private javax.swing.JButton[] nonSelectedButtonsArray,selectedButtonsArray;
    private int shuffleValue;
    private boolean testSaveStatus;
    private ChapterBean chapterBean;
    private SubjectBean subjectBean;
    private String printingPaperType;
    private ArrayList<Integer> saveTestSelectedQuesIdList;
    private PrintedTestBean printedTestBean;
    private boolean isQuestion;
    private ArrayList<ImageRatioBean> imageRatioList;
    int animationTime = 1, sec = 0, min = 0, subid, currentIndex1, totalSelectedQueCount1 = 0;
    ArrayList<Integer> selectedQuestionIds = new ArrayList<Integer>();
    int currentPatternIndex;
    
    public SingleChapterQuestionsSelection() {
        initComponents();
    }
    
    //Save Test Questions
    public SingleChapterQuestionsSelection(ChapterBean chapterBean,SubjectBean subjectBean,ArrayList<QuestionBean> questionsList,ArrayList<QuestionBean> selectedQuestionsList,String printingPaperType,PrintedTestBean printedTestBean) {
        initComponents();
        this.chapterBean = chapterBean;
        this.subjectBean = subjectBean;
        this.printingPaperType = printingPaperType;
        this.questionsList = questionsList;
        this.printedTestBean = printedTestBean;
        this.selectedQuestionsList = selectedQuestionsList;
        saveTestSelectedQuesIdList = new ArrayList<Integer>();
        for(QuestionBean bean : selectedQuestionsList) 
            saveTestSelectedQuesIdList.add(bean.getQuestionId());
        testSaveStatus = true;  
        
    }
    
    //Selection ChapterWise
    public SingleChapterQuestionsSelection(int currentPatternIndex,ChapterBean chapterBean,SubjectBean subjectBean,ArrayList<QuestionBean> questionsList,String printingPaperType) {
        initComponents();
        this.chapterBean = chapterBean;
        this.subjectBean = subjectBean;
        this.printingPaperType = printingPaperType;
        this.questionsList = questionsList;
        this.currentPatternIndex=currentPatternIndex;
        printedTestBean = null;
        testSaveStatus = false;
        saveTestSelectedQuesIdList = null;
        selectedQuestionsList = new ArrayList<QuestionBean>();
        setInitialValues();
         if(currentPatternIndex==0|| currentPatternIndex==1)
        {
            BtnWord.setVisible(false);
        }
    }
    
    private void setInitialValues() {
        this.getContentPane().setBackground(Color.white);
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        currentPanel = PanelQues1;
        imageRatioList = new ImageRatioOperation().getImageRatioList();
        TxtFldQuestionNumber.setColumns(3);
        TxtFldQuestionNumber.setHorizontalAlignment(JTextField.CENTER);
        LblChapterName.setText(this.subjectBean.getSubjectName() + " : " + this.chapterBean.getChapterName() + " (" + this.questionsList.size() + ")");
        totalSelectedQueCount = selectedQuestionsList.size();
        
        LblSelectedCount.setText("Selected Questions : " + totalSelectedQueCount);
        shuffleValue = 0;
        currentIndex = 0;
        setQuesType();
    }
    
    private void setQuesType() {
        ArrayList<QuestionBean> tempSortedQue = new ArrayList<QuestionBean>();
        ArrayList<QuestionBean> tempQueL = new ArrayList<QuestionBean>();
        ArrayList<QuestionBean> tempQueT = new ArrayList<QuestionBean>();
        ArrayList<QuestionBean> tempQueU = new ArrayList<QuestionBean>();
        ArrayList<QuestionBean> tempQueP = new ArrayList<QuestionBean>();
        ArrayList<QuestionBean> tempSelection = new ArrayList<QuestionBean>();
        tempSortedQue = questionsList;
        //System.out.println("tempSortedQue="+tempSortedQue.get(0).getQuestionId());
        for(QuestionBean questionsBean:questionsList) {
            if (ChkLevel.isSelected()) {
                if (CmbLevel.getSelectedIndex() == 0 && questionsBean.getLevel() == 0) {
                    tempQueL.add(questionsBean);
                } else if (CmbLevel.getSelectedIndex() == 1 && questionsBean.getLevel() == 1) {
                    tempQueL.add(questionsBean);
                } else if (CmbLevel.getSelectedIndex() == 2 && questionsBean.getLevel() == 2) {
                    tempQueL.add(questionsBean);
                }
            }
            
            if (ChkType.isSelected()) {
                if (CmbType.getSelectedIndex() == 0 && questionsBean.getType()==0) {
                    tempQueT.add(questionsBean);
                } else if (CmbType.getSelectedIndex() == 1 && questionsBean.getType()==1) {
                    tempQueT.add(questionsBean);
                }
            }
            
            if (ChkUsed.isSelected()) {
                if (CmbUsed.getSelectedIndex() == 0 && questionsBean.getAttempt()==1) {
                    tempQueU.add(questionsBean);
                } else if (CmbUsed.getSelectedIndex() == 1 && questionsBean.getAttempt()==0){
                    tempQueU.add(questionsBean);
                }
            }
          
            if (ChkPrevious.isSelected()) {
                if (CmbPrevious.getSelectedIndex() == 0 && !(questionsBean.getYear().equals("") || questionsBean.getYear().equals(" ") || questionsBean.getYear().equals(null))) {
                    tempQueP.add(questionsBean);
                } else if (CmbPrevious.getSelectedIndex() == 1 && (questionsBean.getYear().equals("") || questionsBean.getYear().equals(" ") || questionsBean.getYear().equals(null))) {
                    tempQueP.add(questionsBean);
                }
            }
        }
        
        if(tempSortedQue.size()>0 && ChkLevel.isSelected()) {
            for(QuestionBean bean:tempSortedQue) {
                if(tempQueL.contains(bean)) {
                    tempSelection.add(bean);
                }
            }
            tempSortedQue = tempSelection;
            tempSelection=new ArrayList<QuestionBean>();
        }
        
        if(tempSortedQue.size()>0 && ChkType.isSelected()) {
            for(QuestionBean bean:tempSortedQue) {
                if(tempQueT.contains(bean)) {
                    tempSelection.add(bean);
                }
            }
            tempSortedQue=tempSelection;
            tempSelection=new ArrayList<QuestionBean>();
        }
        
        if(tempSortedQue.size()>0 && ChkUsed.isSelected()) {
            for(QuestionBean bean:tempSortedQue) {
                if(tempQueU.contains(bean)) {
                    tempSelection.add(bean);
                }
            }
            tempSortedQue=tempSelection;
            tempSelection=new ArrayList<QuestionBean>();
        }
        
        if(tempSortedQue.size()>0 && ChkPrevious.isSelected()) {
            for(QuestionBean bean:tempSortedQue) {
                if(tempQueP.contains(bean)) {
                    tempSelection.add(bean);
                }
            }
            tempSortedQue=tempSelection;
            tempSelection=new ArrayList<QuestionBean>();
        } 
        
        if(tempSortedQue.size() > 0) 
            isQuestion = true;
        else
            isQuestion = false;

        LblCount.setText("COUNT: " + tempSortedQue.size());
            
        addedSequence = new ArrayList<Integer>();
        int i=0;
        sortedQuestionsList = new ArrayList<QuestionBean>();
        for(QuestionBean bean:tempSortedQue) {
            if(selectedQuestionsList.contains(bean)) {
                bean.setSelected(true);
                addedSequence.add(i);
            }
            sortedQuestionsList.add(bean);
            i++;
        }
        if(shuffleValue == 1 && addedSequence.size() != 0){
            Collections.shuffle(addedSequence);
        }
        //currentIndex = 0;
        setPanel(true);
    }
    
    private void setPanel(boolean flag) {
        if(isQuestion) {
            QuestionBean questionBean=sortedQuestionsList.get(currentIndex);
            currentPanel = (currentPanel == PanelQues1) ? PanelQues2 : PanelQues1;
            //set Question       
//            System.out.println("QuestionId:"+questionBean.getQuestionId());
            currentPanel.setQuestionsOnPanel(questionBean, (currentIndex + 1),imageRatioList);
            TxtFldQuestionNumber.setText((currentIndex + 1) + "");
            //slide panel    
            MiddleBodyPanel.nextSlidPanel(1, currentPanel, flag);
            MiddleBodyPanel.refresh();
            if (questionBean.isSelected()) {
                BtnAddRemove.setText("Remove");
                BtnAddRemove.setForeground(Color.red);
            } else {
                BtnAddRemove.setText("Add");
                BtnAddRemove.setForeground(Color.black);
            }
            setButtonOnPanel(RightBodyPanel, LeftBodyPanel);
        } else {
            MiddleBodyPanel.nextSlidPanel(1, noQuestionPanel1, flag);
            MiddleBodyPanel.refresh();
            setButtonOnPanel(RightBodyPanel, LeftBodyPanel);
        }
    }

    public void setQuestion(String actionCommand) {
        currentIndex = Integer.parseInt(actionCommand.split(" ")[1]);
        setPanel(false);
    }

    public void setButtonOnPanel(JPanel allQuestionsPanel, JPanel selectedQuestionsPanel) {
        RightBodyPanel.removeAll();
        LeftBodyPanel.removeAll();
        nonSelectedButtonsArray = new JButton[sortedQuestionsList.size()];
        selectedButtonsArray = new JButton[sortedQuestionsList.size()];
        int i, j;
        i = j = 0;
        for (int x = 0; x < sortedQuestionsList.size(); x++) {
            
            if (sortedQuestionsList.get(x).isSelected()) {
                selectedButtonsArray[i] = new javax.swing.JButton();
                selectedButtonsArray[i].setActionCommand(subjectBean.getSubjectId() + " " + x);
                selectedButtonsArray[i].setText("" + (x + 1));
                selectedButtonsArray[i].setToolTipText("" + (x + 1));
                selectedButtonsArray[i].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        setQuestion(e.getActionCommand());
                    }
                });
                i++;
            } else {
                nonSelectedButtonsArray[j] = new javax.swing.JButton();
                nonSelectedButtonsArray[j].setActionCommand(subjectBean.getSubjectId() + " " + x);
                nonSelectedButtonsArray[j].setText("" + (x + 1));
                nonSelectedButtonsArray[j].setToolTipText("" + (x + 1));
                nonSelectedButtonsArray[j].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        setQuestion(e.getActionCommand());
                    }
                });
                j++;
            }
        }
        GridBagConstraints cons = new GridBagConstraints();
        cons.gridx = 0;
        cons.gridy = 0;
        cons.gridwidth = 1;
        cons.gridheight = 1;
        cons.anchor = GridBagConstraints.BELOW_BASELINE_LEADING;
        cons.weightx = 2;
        cons.weighty = 1;
        cons.insets = new java.awt.Insets(1, 3, 1, 3);
        GridBagLayout layout = new GridBagLayout();
        for (int x = 0; x < i; x++) {
            int uu = addedSequence.get(x);
            uu++;
            String uus = uu + "";
            if (x % 20 == 0) {
                cons.gridx++;
                cons.gridy = 0;
            }
            for (int y = 0; y < addedSequence.size(); y++) {
                if (selectedButtonsArray[y].getText().equalsIgnoreCase(uus)) {
                    layout.setConstraints(selectedButtonsArray[y], cons);
                    selectedQuestionsPanel.setLayout(layout);
                    selectedQuestionsPanel.add(selectedButtonsArray[y], cons);
                    cons.gridy++;
                } else {
                
                }
            }
        }
        
        cons.gridx = 0;
        cons.gridy = 0;
        cons.gridwidth = 1;
        cons.gridheight = 1;
        cons.anchor = GridBagConstraints.BELOW_BASELINE_LEADING;
        cons.weightx = 2;
        cons.weighty = 1;
        cons.insets = new java.awt.Insets(1, 3, 1, 3);
        layout = new GridBagLayout();
        
        for (int x = 0; x < j; x++) {
            if (x % 20 == 0) {
                cons.gridx++;
                cons.gridy = 0;
            }
            layout.setConstraints(nonSelectedButtonsArray[x], cons);
            allQuestionsPanel.setLayout(layout);
            allQuestionsPanel.add(nonSelectedButtonsArray[x], cons);
            cons.gridy++;
        }
        
        LeftBodyPanel.validate();
        LeftBodyPanel.repaint();
        RightBodyPanel.validate();
        RightBodyPanel.repaint();
    }
       
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGroupAnimation = new javax.swing.ButtonGroup();
        HeaderPanel = new javax.swing.JPanel();
        LblChapterName = new javax.swing.JLabel();
        SubHeaderPanel = new javax.swing.JPanel();
        SortPanel = new javax.swing.JPanel();
        CmbLevel = new javax.swing.JComboBox();
        ChkLevel = new javax.swing.JCheckBox();
        LblSort = new javax.swing.JLabel();
        ChkType = new javax.swing.JCheckBox();
        CmbType = new javax.swing.JComboBox();
        ChkUsed = new javax.swing.JCheckBox();
        CmbUsed = new javax.swing.JComboBox();
        ChkPrevious = new javax.swing.JCheckBox();
        CmbPrevious = new javax.swing.JComboBox();
        LblNonSelectedQue = new javax.swing.JLabel();
        LblCount = new javax.swing.JLabel();
        LblSelectedCount = new javax.swing.JLabel();
        LeftBodyScrollPane = new javax.swing.JScrollPane();
        LeftBodyPanel = new javax.swing.JPanel();
        MiddleBodyScrollPane = new javax.swing.JScrollPane();
        MiddleBodyPanel = new com.ui.support.pages.JPanelsSliding();
        PanelQues2 = new com.ui.support.pages.QuestionPanel();
        noQuestionPanel1 = new com.ui.support.pages.NoQuestionPanel();
        PanelQues1 = new com.ui.support.pages.QuestionPanel();
        RightBodyScrollPane = new javax.swing.JScrollPane();
        RightBodyPanel = new javax.swing.JPanel();
        BodyBottomPanel = new javax.swing.JPanel();
        BtnAddRemove = new javax.swing.JButton();
        BtnHome = new javax.swing.JButton();
        BtnShuffle = new javax.swing.JButton();
        BtnPdf = new javax.swing.JButton();
        BtnWord = new javax.swing.JButton();
        BtnChangeChapter = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        FooterPanel = new javax.swing.JPanel();
        SubFooterPanel = new javax.swing.JPanel();
        TxtFldStart = new javax.swing.JTextField();
        TxtFldEnd = new javax.swing.JTextField();
        BtnRandomAdd = new javax.swing.JButton();
        ChbkRandom = new javax.swing.JCheckBox();
        BtnRemoveAll = new javax.swing.JButton();
        BtnDelete = new javax.swing.JButton();
        BtnModify = new javax.swing.JButton();
        BtnFirst = new javax.swing.JButton();
        BtnPrevious = new javax.swing.JButton();
        BtnNext = new javax.swing.JButton();
        TxtFldQuestionNumber = new javax.swing.JTextField();
        btnLast = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("CruncherSoft's Medical CET+JEE Software 2014");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        HeaderPanel.setBackground(new java.awt.Color(240, 255, 255));
        HeaderPanel.setName("HeaderPanel"); // NOI18N

        LblChapterName.setFont(new java.awt.Font("Microsoft JhengHei", 1, 18)); // NOI18N
        LblChapterName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblChapterName.setText("Name of Chapter");
        LblChapterName.setName("LblChapterName"); // NOI18N

        SubHeaderPanel.setName("SubHeaderPanel"); // NOI18N

        SortPanel.setName("SortPanel"); // NOI18N

        CmbLevel.setFont(new java.awt.Font("Microsoft JhengHei", 1, 14)); // NOI18N
        CmbLevel.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Easy", "Medium", "Hard" }));
        CmbLevel.setEnabled(false);
        CmbLevel.setName("CmbLevel"); // NOI18N
        CmbLevel.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmbLevelItemStateChanged(evt);
            }
        });

        ChkLevel.setFont(new java.awt.Font("Microsoft JhengHei", 1, 14)); // NOI18N
        ChkLevel.setForeground(new java.awt.Color(255, 0, 0));
        ChkLevel.setText("Level :");
        ChkLevel.setName("ChkLevel"); // NOI18N
        ChkLevel.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkLevelItemStateChanged(evt);
            }
        });

        LblSort.setFont(new java.awt.Font("Microsoft JhengHei", 1, 14)); // NOI18N
        LblSort.setForeground(new java.awt.Color(255, 0, 0));
        LblSort.setText("Sort :");
        LblSort.setName("LblSort"); // NOI18N

        ChkType.setFont(new java.awt.Font("Microsoft JhengHei", 1, 14)); // NOI18N
        ChkType.setForeground(new java.awt.Color(255, 0, 0));
        ChkType.setText("Type :");
        ChkType.setName("ChkType"); // NOI18N
        ChkType.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkTypeItemStateChanged(evt);
            }
        });

        CmbType.setFont(new java.awt.Font("Microsoft JhengHei", 1, 14)); // NOI18N
        CmbType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Theoretical", "Numerical" }));
        CmbType.setEnabled(false);
        CmbType.setName("CmbType"); // NOI18N
        CmbType.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmbTypeItemStateChanged(evt);
            }
        });

        ChkUsed.setFont(new java.awt.Font("Microsoft JhengHei", 1, 14)); // NOI18N
        ChkUsed.setForeground(new java.awt.Color(255, 0, 0));
        ChkUsed.setText("Used :");
        ChkUsed.setName("ChkUsed"); // NOI18N
        ChkUsed.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkUsedItemStateChanged(evt);
            }
        });

        CmbUsed.setFont(new java.awt.Font("Microsoft JhengHei", 1, 14)); // NOI18N
        CmbUsed.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Used", "Not Used" }));
        CmbUsed.setEnabled(false);
        CmbUsed.setName("CmbUsed"); // NOI18N
        CmbUsed.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmbUsedItemStateChanged(evt);
            }
        });

        ChkPrevious.setFont(new java.awt.Font("Microsoft JhengHei", 1, 14)); // NOI18N
        ChkPrevious.setForeground(new java.awt.Color(255, 0, 0));
        ChkPrevious.setText("Previously Asked");
        ChkPrevious.setName("ChkPrevious"); // NOI18N
        ChkPrevious.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkPreviousItemStateChanged(evt);
            }
        });

        CmbPrevious.setFont(new java.awt.Font("Microsoft JhengHei", 1, 14)); // NOI18N
        CmbPrevious.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Asked", "Not Asked" }));
        CmbPrevious.setEnabled(false);
        CmbPrevious.setName("CmbPrevious"); // NOI18N
        CmbPrevious.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmbPreviousItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout SortPanelLayout = new javax.swing.GroupLayout(SortPanel);
        SortPanel.setLayout(SortPanelLayout);
        SortPanelLayout.setHorizontalGroup(
            SortPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SortPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(LblSort)
                .addGap(18, 18, 18)
                .addComponent(ChkLevel)
                .addGap(18, 18, 18)
                .addComponent(CmbLevel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(ChkType)
                .addGap(18, 18, 18)
                .addComponent(CmbType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(ChkUsed)
                .addGap(18, 18, 18)
                .addComponent(CmbUsed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(ChkPrevious)
                .addGap(18, 18, 18)
                .addComponent(CmbPrevious, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        SortPanelLayout.setVerticalGroup(
            SortPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SortPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(SortPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblSort)
                    .addComponent(CmbLevel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ChkType)
                    .addComponent(CmbType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ChkUsed)
                    .addComponent(CmbUsed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ChkPrevious)
                    .addComponent(CmbPrevious, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ChkLevel))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        SubHeaderPanel.add(SortPanel);

        LblNonSelectedQue.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 12)); // NOI18N
        LblNonSelectedQue.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblNonSelectedQue.setText("Non Selected Questions");
        LblNonSelectedQue.setName("LblNonSelectedQue"); // NOI18N

        LblCount.setFont(new java.awt.Font("Microsoft JhengHei", 1, 18)); // NOI18N
        LblCount.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblCount.setText("Count");
        LblCount.setToolTipText("");
        LblCount.setName("LblCount"); // NOI18N

        LblSelectedCount.setFont(new java.awt.Font("Microsoft YaHei UI", 1, 13)); // NOI18N
        LblSelectedCount.setText("Selected Question : 0");
        LblSelectedCount.setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
        LblSelectedCount.setName("LblSelectedCount"); // NOI18N
        LblSelectedCount.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                LblSelectedCountMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                LblSelectedCountMouseExited(evt);
            }
        });

        javax.swing.GroupLayout HeaderPanelLayout = new javax.swing.GroupLayout(HeaderPanel);
        HeaderPanel.setLayout(HeaderPanelLayout);
        HeaderPanelLayout.setHorizontalGroup(
            HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(HeaderPanelLayout.createSequentialGroup()
                .addGroup(HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(HeaderPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(SubHeaderPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(HeaderPanelLayout.createSequentialGroup()
                                .addComponent(LblChapterName)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(LblCount))))
                    .addGroup(HeaderPanelLayout.createSequentialGroup()
                        .addComponent(LblSelectedCount)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(LblNonSelectedQue, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        HeaderPanelLayout.setVerticalGroup(
            HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(HeaderPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(LblCount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblChapterName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(SubHeaderPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(HeaderPanelLayout.createSequentialGroup()
                        .addComponent(LblNonSelectedQue)
                        .addContainerGap())
                    .addComponent(LblSelectedCount, javax.swing.GroupLayout.Alignment.TRAILING)))
        );

        LeftBodyScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        LeftBodyScrollPane.setName("LeftBodyScrollPane"); // NOI18N

        LeftBodyPanel.setBackground(new java.awt.Color(255, 255, 255));
        LeftBodyPanel.setName("LeftBodyPanel"); // NOI18N

        javax.swing.GroupLayout LeftBodyPanelLayout = new javax.swing.GroupLayout(LeftBodyPanel);
        LeftBodyPanel.setLayout(LeftBodyPanelLayout);
        LeftBodyPanelLayout.setHorizontalGroup(
            LeftBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 150, Short.MAX_VALUE)
        );
        LeftBodyPanelLayout.setVerticalGroup(
            LeftBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 514, Short.MAX_VALUE)
        );

        LeftBodyScrollPane.setViewportView(LeftBodyPanel);

        MiddleBodyScrollPane.setName("MiddleBodyScrollPane"); // NOI18N

        MiddleBodyPanel.setBackground(new java.awt.Color(233, 238, 255));
        MiddleBodyPanel.setName("MiddleBodyPanel"); // NOI18N
        MiddleBodyPanel.setLayout(new java.awt.CardLayout());

        PanelQues2.setBackground(new java.awt.Color(185, 225, 254));
        PanelQues2.setName("PanelQues2"); // NOI18N
        MiddleBodyPanel.add(PanelQues2, "card3");

        noQuestionPanel1.setName("noQuestionPanel1"); // NOI18N
        MiddleBodyPanel.add(noQuestionPanel1, "noQues");

        PanelQues1.setBackground(new java.awt.Color(185, 225, 254));
        PanelQues1.setFont(new java.awt.Font("Centaur", 0, 11)); // NOI18N
        PanelQues1.setName("PanelQues1"); // NOI18N
        PanelQues1.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                PanelQues1AncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        MiddleBodyPanel.add(PanelQues1, "panelQues");

        MiddleBodyScrollPane.setViewportView(MiddleBodyPanel);

        RightBodyScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        RightBodyScrollPane.setName("RightBodyScrollPane"); // NOI18N

        RightBodyPanel.setBackground(new java.awt.Color(255, 255, 255));
        RightBodyPanel.setName("RightBodyPanel"); // NOI18N

        javax.swing.GroupLayout RightBodyPanelLayout = new javax.swing.GroupLayout(RightBodyPanel);
        RightBodyPanel.setLayout(RightBodyPanelLayout);
        RightBodyPanelLayout.setHorizontalGroup(
            RightBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 150, Short.MAX_VALUE)
        );
        RightBodyPanelLayout.setVerticalGroup(
            RightBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 514, Short.MAX_VALUE)
        );

        RightBodyScrollPane.setViewportView(RightBodyPanel);

        BodyBottomPanel.setBackground(new java.awt.Color(240, 255, 255));
        BodyBottomPanel.setName("BodyBottomPanel"); // NOI18N

        BtnAddRemove.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        BtnAddRemove.setText("Add");
        BtnAddRemove.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BtnAddRemove.setName("BtnAddRemove"); // NOI18N
        BtnAddRemove.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnAddRemoveMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnAddRemoveMouseExited(evt);
            }
        });
        BtnAddRemove.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                BtnAddRemoveItemStateChanged(evt);
            }
        });
        BtnAddRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAddRemoveActionPerformed(evt);
            }
        });
        BtnAddRemove.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAddRemoveKeyPressed(evt);
            }
        });
        BodyBottomPanel.add(BtnAddRemove);

        BtnHome.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        BtnHome.setText("Home");
        BtnHome.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BtnHome.setName("BtnHome"); // NOI18N
        BtnHome.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnHomeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnHomeMouseExited(evt);
            }
        });
        BtnHome.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                BtnHomeItemStateChanged(evt);
            }
        });
        BtnHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHomeActionPerformed(evt);
            }
        });
        BodyBottomPanel.add(BtnHome);

        BtnShuffle.setFont(new java.awt.Font("Microsoft JhengHei", 1, 13)); // NOI18N
        BtnShuffle.setText("Shuffle");
        BtnShuffle.setName("BtnShuffle"); // NOI18N
        BtnShuffle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnShuffleActionPerformed(evt);
            }
        });
        BodyBottomPanel.add(BtnShuffle);

        BtnPdf.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        BtnPdf.setText("Export Pdf");
        BtnPdf.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BtnPdf.setName("BtnPdf"); // NOI18N
        BtnPdf.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnPdfMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnPdfMouseExited(evt);
            }
        });
        BtnPdf.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                BtnPdfItemStateChanged(evt);
            }
        });
        BtnPdf.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPdfActionPerformed(evt);
            }
        });
        BtnPdf.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPdfKeyPressed(evt);
            }
        });
        BodyBottomPanel.add(BtnPdf);

        BtnWord.setFont(new java.awt.Font("Microsoft JhengHei", 1, 13)); // NOI18N
        BtnWord.setText("Export Word");
        BtnWord.setActionCommand("Export To Word");
        BtnWord.setName("BtnWord"); // NOI18N
        BtnWord.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnWordActionPerformed(evt);
            }
        });
        BodyBottomPanel.add(BtnWord);

        BtnChangeChapter.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        BtnChangeChapter.setText("Change Chapter");
        BtnChangeChapter.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BtnChangeChapter.setName("BtnChangeChapter"); // NOI18N
        BtnChangeChapter.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnChangeChapterMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnChangeChapterMouseExited(evt);
            }
        });
        BtnChangeChapter.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                BtnChangeChapterItemStateChanged(evt);
            }
        });
        BtnChangeChapter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnChangeChapterActionPerformed(evt);
            }
        });
        BtnChangeChapter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnChangeChapterKeyPressed(evt);
            }
        });
        BodyBottomPanel.add(BtnChangeChapter);

        jButton1.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        jButton1.setText("SaveTest");
        jButton1.setName("jButton1"); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        BodyBottomPanel.add(jButton1);

        FooterPanel.setBackground(new java.awt.Color(240, 248, 255));
        FooterPanel.setName("FooterPanel"); // NOI18N

        SubFooterPanel.setName("SubFooterPanel"); // NOI18N

        TxtFldStart.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 12)); // NOI18N
        TxtFldStart.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TxtFldStart.setName("TxtFldStart"); // NOI18N
        TxtFldStart.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TxtFldStartKeyPressed(evt);
            }
        });

        TxtFldEnd.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 12)); // NOI18N
        TxtFldEnd.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TxtFldEnd.setName("TxtFldEnd"); // NOI18N
        TxtFldEnd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TxtFldEndKeyPressed(evt);
            }
        });

        BtnRandomAdd.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        BtnRandomAdd.setText("Add");
        BtnRandomAdd.setName("BtnRandomAdd"); // NOI18N
        BtnRandomAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRandomAddActionPerformed(evt);
            }
        });

        ChbkRandom.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        ChbkRandom.setText("Random");
        ChbkRandom.setName("ChbkRandom"); // NOI18N
        ChbkRandom.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChbkRandomItemStateChanged(evt);
            }
        });

        BtnRemoveAll.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        BtnRemoveAll.setText("Remove All");
        BtnRemoveAll.setName("BtnRemoveAll"); // NOI18N
        BtnRemoveAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRemoveAllActionPerformed(evt);
            }
        });

        BtnDelete.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        BtnDelete.setText("Delete Question");
        BtnDelete.setName("BtnDelete"); // NOI18N
        BtnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDeleteActionPerformed(evt);
            }
        });

        BtnModify.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        BtnModify.setText("Modify");
        BtnModify.setName("BtnModify"); // NOI18N
        BtnModify.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnModifyActionPerformed(evt);
            }
        });

        BtnFirst.setBackground(new java.awt.Color(255, 255, 255));
        BtnFirst.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first.png"))); // NOI18N
        BtnFirst.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BtnFirst.setMargin(new java.awt.Insets(0, 0, 0, 0));
        BtnFirst.setName("BtnFirst"); // NOI18N
        BtnFirst.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnFirstMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnFirstMouseExited(evt);
            }
        });
        BtnFirst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnFirstActionPerformed(evt);
            }
        });

        BtnPrevious.setBackground(new java.awt.Color(255, 255, 255));
        BtnPrevious.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/prev.png"))); // NOI18N
        BtnPrevious.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BtnPrevious.setIconTextGap(0);
        BtnPrevious.setMargin(new java.awt.Insets(0, 0, 0, 0));
        BtnPrevious.setName("BtnPrevious"); // NOI18N
        BtnPrevious.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnPreviousMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnPreviousMouseExited(evt);
            }
        });
        BtnPrevious.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPreviousActionPerformed(evt);
            }
        });

        BtnNext.setBackground(new java.awt.Color(255, 255, 255));
        BtnNext.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/next.png"))); // NOI18N
        BtnNext.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BtnNext.setMargin(new java.awt.Insets(0, 0, 0, 0));
        BtnNext.setName("BtnNext"); // NOI18N
        BtnNext.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnNextMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnNextMouseExited(evt);
            }
        });
        BtnNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnNextActionPerformed(evt);
            }
        });

        TxtFldQuestionNumber.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 12)); // NOI18N
        TxtFldQuestionNumber.setText("0000");
        TxtFldQuestionNumber.setName("TxtFldQuestionNumber"); // NOI18N
        TxtFldQuestionNumber.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                TxtFldQuestionNumberMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                TxtFldQuestionNumberMouseExited(evt);
            }
        });
        TxtFldQuestionNumber.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TxtFldQuestionNumberKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtFldQuestionNumberKeyReleased(evt);
            }
        });

        btnLast.setBackground(new java.awt.Color(255, 255, 255));
        btnLast.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last.png"))); // NOI18N
        btnLast.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnLast.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnLast.setName("btnLast"); // NOI18N
        btnLast.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnLastMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnLastMouseExited(evt);
            }
        });
        btnLast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLastActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout SubFooterPanelLayout = new javax.swing.GroupLayout(SubFooterPanel);
        SubFooterPanel.setLayout(SubFooterPanelLayout);
        SubFooterPanelLayout.setHorizontalGroup(
            SubFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, SubFooterPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(BtnFirst)
                .addGap(5, 5, 5)
                .addComponent(BtnPrevious)
                .addGap(5, 5, 5)
                .addComponent(TxtFldQuestionNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(BtnNext)
                .addGap(5, 5, 5)
                .addComponent(btnLast)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ChbkRandom)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TxtFldStart, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(TxtFldEnd, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BtnRandomAdd)
                .addGap(10, 10, 10)
                .addComponent(BtnRemoveAll)
                .addGap(10, 10, 10)
                .addComponent(BtnDelete)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BtnModify)
                .addGap(5, 5, 5))
        );
        SubFooterPanelLayout.setVerticalGroup(
            SubFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, SubFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(BtnFirst)
                .addComponent(BtnPrevious)
                .addGroup(SubFooterPanelLayout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(TxtFldQuestionNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addComponent(BtnNext)
                .addComponent(btnLast))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, SubFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(TxtFldStart, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(BtnRandomAdd)
                .addComponent(TxtFldEnd, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(ChbkRandom)
                .addComponent(BtnRemoveAll)
                .addComponent(BtnDelete)
                .addComponent(BtnModify))
        );

        FooterPanel.add(SubFooterPanel);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(FooterPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(HeaderPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(LeftBodyScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(MiddleBodyScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(RightBodyScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(BodyBottomPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(HeaderPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(LeftBodyScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE)
                    .addComponent(RightBodyScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE)
                    .addComponent(MiddleBodyScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BodyBottomPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(FooterPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
private void BtnFirstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnFirstActionPerformed
    if(isQuestion) {
        if (currentIndex != 0) {
            currentIndex = 0;
            setPanel(true);
        } else {
            JOptionPane.showMessageDialog(null, "This is First Question.");
        }
    }
}//GEN-LAST:event_BtnFirstActionPerformed
private void BtnPreviousActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPreviousActionPerformed
    if(isQuestion) {
        if (currentIndex != 0) {
            currentIndex--;
            setPanel(true);
        } else {
            JOptionPane.showMessageDialog(null, "This is First Question.");
        }
    }
}//GEN-LAST:event_BtnPreviousActionPerformed
private void TxtFldQuestionNumberKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtFldQuestionNumberKeyReleased
    if (evt.getKeyCode() == 10) {
        if(!TxtFldQuestionNumber.getText().isEmpty()) {
            int i = Integer.parseInt(TxtFldQuestionNumber.getText());
            int size = sortedQuestionsList.size();
            if ((i < 1) || (i > size))
                JOptionPane.showMessageDialog(rootPane, "Invalid Question Number");
            else
                currentIndex = i - 1;
        } else {
            JOptionPane.showMessageDialog(rootPane, "Invalid Question Number");
        }
        setPanel(false);
    }
}//GEN-LAST:event_TxtFldQuestionNumberKeyReleased

       private void BtnNextActionPerformed(java.awt.event.ActionEvent evt, boolean b) {
        int last=sortedQuestionsList.size()-1;
        if (currentIndex == last) {
            currentIndex = 0;
            setPanel(false);
        } else {
            JOptionPane.showMessageDialog(null, "This is Last Question.");
        }
    }

private void BtnNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnNextActionPerformed
    if(isQuestion) {
        int last=sortedQuestionsList.size()-1;
        if (currentIndex < last) {
            currentIndex++;
            setPanel(false);
        } else {
            JOptionPane.showMessageDialog(null, "This is Last Question.");
        }
    }
}//GEN-LAST:event_BtnNextActionPerformed
private void btnLastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLastActionPerformed
    if(isQuestion) {
        int last=sortedQuestionsList.size()-1;
        if (currentIndex < last) {
            currentIndex = last;
            setPanel(false);
        } else {
            JOptionPane.showMessageDialog(null, "This is Last Question.");
        }
    }
}//GEN-LAST:event_btnLastActionPerformed
private void BtnAddRemoveItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_BtnAddRemoveItemStateChanged
}//GEN-LAST:event_BtnAddRemoveItemStateChanged
private void BtnAddRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAddRemoveActionPerformed
    if(isQuestion) {
        int last = sortedQuestionsList.size()-1;
        int i = currentIndex;
        if ((currentIndex <= last) && (currentIndex >= 0)) {
            if (BtnAddRemove.getText().equals("Add")) {
                sortedQuestionsList.get(currentIndex).setSelected(true);
                totalSelectedQueCount++;
                addedSequence.add(currentIndex);
                selectedQuestionsList.add(sortedQuestionsList.get(currentIndex));
            } else {
                sortedQuestionsList.get(currentIndex).setSelected(false);
                int index = selectedQuestionsList.indexOf(sortedQuestionsList.get(currentIndex));
                selectedQuestionsList.remove(index);
                int iii = addedSequence.indexOf(currentIndex);
                addedSequence.remove(iii);
                totalSelectedQueCount--;
                if(selectedQuestionsList.isEmpty())
                    shuffleValue = 0;
            }
            LblSelectedCount.setText("Selected Questions : " + totalSelectedQueCount);
            setButtonOnPanel(RightBodyPanel, LeftBodyPanel);
            LeftBodyPanel.validate();
            LeftBodyPanel.repaint();
            RightBodyPanel.validate();
            RightBodyPanel.repaint();
        }

        if (i < last) {
            BtnNextActionPerformed(evt);
        } else if (i == last) {
            boolean b = true;
            BtnNextActionPerformed(evt, b);
        }
    }
}//GEN-LAST:event_BtnAddRemoveActionPerformed
private void BtnHomeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_BtnHomeItemStateChanged
// TODO add your handling code here:
}//GEN-LAST:event_BtnHomeItemStateChanged
private void BtnHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHomeActionPerformed
    new HomePage().setVisible(true);
    this.dispose();
}//GEN-LAST:event_BtnHomeActionPerformed
private void BtnAddRemoveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnAddRemoveMouseEntered
    BtnAddRemove.setForeground(Color.red);
}//GEN-LAST:event_BtnAddRemoveMouseEntered
private void BtnAddRemoveMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnAddRemoveMouseExited
    BtnAddRemove.setForeground(Color.black);
}//GEN-LAST:event_BtnAddRemoveMouseExited
private void BtnHomeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnHomeMouseEntered
    BtnHome.setForeground(Color.red);
}//GEN-LAST:event_BtnHomeMouseEntered
private void BtnHomeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnHomeMouseExited
    BtnHome.setForeground(Color.black);
}//GEN-LAST:event_BtnHomeMouseExited
private void BtnAddRemoveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAddRemoveKeyPressed
}//GEN-LAST:event_BtnAddRemoveKeyPressed

private void BtnFirstMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnFirstMouseEntered
}//GEN-LAST:event_BtnFirstMouseEntered

private void BtnFirstMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnFirstMouseExited
}//GEN-LAST:event_BtnFirstMouseExited

private void BtnPreviousMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnPreviousMouseEntered
}//GEN-LAST:event_BtnPreviousMouseEntered

private void BtnPreviousMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnPreviousMouseExited
}//GEN-LAST:event_BtnPreviousMouseExited

private void BtnNextMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnNextMouseEntered
}//GEN-LAST:event_BtnNextMouseEntered

private void btnLastMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnLastMouseEntered
}//GEN-LAST:event_btnLastMouseEntered

private void BtnNextMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnNextMouseExited
}//GEN-LAST:event_BtnNextMouseExited

private void btnLastMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnLastMouseExited
}//GEN-LAST:event_btnLastMouseExited

private void TxtFldQuestionNumberMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TxtFldQuestionNumberMouseEntered
}//GEN-LAST:event_TxtFldQuestionNumberMouseEntered

private void TxtFldQuestionNumberMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TxtFldQuestionNumberMouseExited
}//GEN-LAST:event_TxtFldQuestionNumberMouseExited

private void LblSelectedCountMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblSelectedCountMouseEntered
}//GEN-LAST:event_LblSelectedCountMouseEntered

private void LblSelectedCountMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblSelectedCountMouseExited
}//GEN-LAST:event_LblSelectedCountMouseExited

private void BtnPdfMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnPdfMouseEntered
// TODO add your handling code here:
}//GEN-LAST:event_BtnPdfMouseEntered

private void BtnPdfMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnPdfMouseExited
// TODO add your handling code here:
}//GEN-LAST:event_BtnPdfMouseExited

private void BtnPdfItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_BtnPdfItemStateChanged
// TODO add your handling code here:
}//GEN-LAST:event_BtnPdfItemStateChanged

private void BtnPdfActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPdfActionPerformed
    if (!selectedQuestionsList.isEmpty()) {
        if(shuffleValue == 1) 
            Collections.shuffle(selectedQuestionsList);
        ArrayList<ChapterBean> chapterList = new ArrayList<ChapterBean>();
        chapterList.add(chapterBean);
        ArrayList<SubjectBean> selectedSubjectList = new ArrayList<SubjectBean>();
        selectedSubjectList.add(subjectBean);
        new PdfPageSetup(selectedQuestionsList,selectedSubjectList,chapterList,(Object)this,questionsList,printingPaperType).setVisible(true);
        this.setEnabled(false);
    } else {
        JOptionPane.showMessageDialog(this, "Please Add Questions for Printing.");
    }
}//GEN-LAST:event_BtnPdfActionPerformed

private void BtnPdfKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPdfKeyPressed
// TODO add your handling code here:
}//GEN-LAST:event_BtnPdfKeyPressed

private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
    Object[] options = {"YES", "CANCEL"};
    int i = JOptionPane.showOptionDialog(null, "Are You Sure to Cancel Test?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
    if (i == 0) {
         new HomePage().setVisible(true);
        this.dispose();
    }
}//GEN-LAST:event_formWindowClosing

private void BtnRemoveAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRemoveAllActionPerformed
    if(isQuestion) {
        if(selectedQuestionsList.size()!=0) {
            for (int i = 0; i < questionsList.size(); i++)
                questionsList.get(i).setSelected(false);
            for(int i=0; i< sortedQuestionsList.size(); i++)
                sortedQuestionsList.get(i).setSelected(false);
            shuffleValue = 0;
            totalSelectedQueCount = 0;
//            selectedQuestionsList = new ArrayList<QuestionsBean>();
            selectedQuestionsList.clear();
            BtnAddRemove.setText("Add");
            BtnAddRemove.setForeground(Color.black);
            TxtFldStart.setText("");
            TxtFldEnd.setText("");
//            addedSequence = new ArrayList<Integer>();
            addedSequence.clear();
            LblSelectedCount.setText("Selected Questions : " + totalSelectedQueCount);
            setButtonOnPanel(RightBodyPanel, LeftBodyPanel);
            LeftBodyPanel.validate();
            LeftBodyPanel.repaint();
            RightBodyPanel.validate();
            RightBodyPanel.repaint();
            BodyBottomPanel.validate();
            BodyBottomPanel.revalidate();
            BodyBottomPanel.repaint();
        } else {
            JOptionPane.showMessageDialog(null, "You Are Not Selected Any Questions.");
        }
    }
}//GEN-LAST:event_BtnRemoveAllActionPerformed

private void BtnRandomAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRandomAddActionPerformed
    if(isQuestion) {
        int strt = 0, end = 0;
        String str1 = TxtFldStart.getText();
        String str2 = TxtFldEnd.getText();
        TxtFldStart.setText("");
        TxtFldEnd.setText("");
        
        if (ChbkRandom.isSelected()) {
            try {
                end = Integer.parseInt(str2);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Please enter valid number");
            }

            ArrayList<QuestionBean> tempRemainingList = new ArrayList<QuestionBean>();
            for(QuestionBean bean : sortedQuestionsList) {
                if(!bean.isSelected())
                    tempRemainingList.add(bean);
            }
            
            Collections.shuffle(tempRemainingList);

            if(end > 0 && selectedQuestionsList.size() == 0 && end < sortedQuestionsList.size()+1) {
                for(int i=0;i<end;i++) {
                    tempRemainingList.get(i).setSelected(true);
                    selectedQuestionsList.add(tempRemainingList.get(i));
                    int index1 = sortedQuestionsList.indexOf(tempRemainingList.get(i));
                    sortedQuestionsList.get(index1).setSelected(true);
                    addedSequence.add(index1);
                    totalSelectedQueCount ++;
                }
            } else {
                if(end < tempRemainingList.size()+1 ) {
                    for(int i=0;i<end;i++) {
                        selectedQuestionsList.add(tempRemainingList.get(i));
                        int index1 = sortedQuestionsList.indexOf(tempRemainingList.get(i));
                        sortedQuestionsList.get(index1).setSelected(true);
                        addedSequence.add(index1);
                        totalSelectedQueCount++;
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Invalid range.\nYou Can Add ("+tempRemainingList.size()+") Questions.");
                } 
            }

            LblSelectedCount.setText("Selected Questions : " + totalSelectedQueCount);
            setButtonOnPanel(RightBodyPanel, LeftBodyPanel);
            LeftBodyPanel.validate();
            LeftBodyPanel.repaint();
            RightBodyPanel.validate();
            RightBodyPanel.repaint();
            setPanel(false);
            BodyBottomPanel.validate();
            BodyBottomPanel.revalidate();
            BodyBottomPanel.repaint();
        } else {
            try {
                strt = Integer.parseInt(str1);
                end = Integer.parseInt(str2);
            } catch (Exception e) {
            }

            if (strt > 0 && end > 0) {
                if (strt < end) {
                    if(end < sortedQuestionsList.size()+1) {    
                        for (int i = strt - 1; i < end; i++) {
                            if(!(selectedQuestionsList.contains(sortedQuestionsList.get(i)))){
                                sortedQuestionsList.get(i).setSelected(true);
                                totalSelectedQueCount++;
                                selectedQuestionsList.add(sortedQuestionsList.get(i));
                                int index = questionsList.indexOf(sortedQuestionsList.get(i));
                                questionsList.get(index).setSelected(true);
                                addedSequence.add(i);
                            }
                        }
                        LblSelectedCount.setText("Selected Questions : " + totalSelectedQueCount);
                        setPanel(false);
                        setButtonOnPanel(RightBodyPanel, LeftBodyPanel);
                        LeftBodyPanel.validate();
                        LeftBodyPanel.repaint();
                        RightBodyPanel.validate();
                        RightBodyPanel.repaint();

                        BodyBottomPanel.validate();
                        BodyBottomPanel.revalidate();
                        BodyBottomPanel.repaint();
                    } else {
                        JOptionPane.showMessageDialog(null, "Entered Count is out of range.");
                    }
                } else {
                    if (strt < sortedQuestionsList.size()+1) {
                        for (int i = end - 1; i < strt; i++) {
                            if(!(selectedQuestionsList.contains(sortedQuestionsList.get(i)))) {
                                sortedQuestionsList.get(i).setSelected(true);
                                totalSelectedQueCount++;
                                selectedQuestionsList.add(sortedQuestionsList.get(i));
                                int index = questionsList.indexOf(sortedQuestionsList.get(i));
                                questionsList.get(index).setSelected(true);
                                addedSequence.add(i);
                            }
                        }
                        LblSelectedCount.setText("Selected Questions : " + totalSelectedQueCount);
                        setPanel(false);
                        setButtonOnPanel(RightBodyPanel, LeftBodyPanel);
                        LeftBodyPanel.validate();
                        LeftBodyPanel.repaint();
                        RightBodyPanel.validate();
                        RightBodyPanel.repaint();

                        BodyBottomPanel.validate();
                        BodyBottomPanel.revalidate();
                        BodyBottomPanel.repaint();
                    } else {
                        JOptionPane.showMessageDialog(null, "Entered Count is out of range.");
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Please enter valid range");
            }
        }
    }
}//GEN-LAST:event_BtnRandomAddActionPerformed

private void ChkLevelItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkLevelItemStateChanged
    if (ChkLevel.isSelected()) {
        CmbLevel.setEnabled(true);
    } else {
        CmbLevel.setEnabled(false);
    }
    currentIndex = 0;
    setQuesType();
    
}//GEN-LAST:event_ChkLevelItemStateChanged

private void ChkTypeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkTypeItemStateChanged
    if (ChkType.isSelected()) {
        CmbType.setEnabled(true);
    } else {
        CmbType.setEnabled(false);
    }
    currentIndex = 0;
    setQuesType();
}//GEN-LAST:event_ChkTypeItemStateChanged

private void ChkUsedItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkUsedItemStateChanged
   
    if (ChkUsed.isSelected()) {
        CmbUsed.setEnabled(true);
    } else {
        CmbUsed.setEnabled(false);
    }
    currentIndex = 0;
    setQuesType();
}//GEN-LAST:event_ChkUsedItemStateChanged

private void ChkPreviousItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkPreviousItemStateChanged
    
    if (ChkPrevious.isSelected()) {
        CmbPrevious.setEnabled(true);
    } else {
        CmbPrevious.setEnabled(false);
    }
    currentIndex = 0;
    setQuesType();
}//GEN-LAST:event_ChkPreviousItemStateChanged

private void CmbTypeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmbTypeItemStateChanged
    if (ChkType.isSelected()) {
        currentIndex = 0;
        setQuesType();
    }
}//GEN-LAST:event_CmbTypeItemStateChanged

private void CmbUsedItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmbUsedItemStateChanged
    if (ChkUsed.isSelected()) {
        currentIndex = 0;
        setQuesType();
    }
}//GEN-LAST:event_CmbUsedItemStateChanged

private void CmbPreviousItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmbPreviousItemStateChanged
    if (ChkPrevious.isSelected()) {
        currentIndex = 0;
        setQuesType();
    }
}//GEN-LAST:event_CmbPreviousItemStateChanged

private void CmbLevelItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmbLevelItemStateChanged
    if (ChkLevel.isSelected()) {
        currentIndex = 0;
        setQuesType();
    }
}//GEN-LAST:event_CmbLevelItemStateChanged

private void BtnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDeleteActionPerformed
    /*if(isQuestion) {
        Object[] options = {"YES", "CANCEL"};
        int i = JOptionPane.showOptionDialog(null, "Are You Sure to delete question?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (i == 0) {
            new PleaseWaitPanel(this).setVisible(true);
            this.setEnabled(false);
        }
    }*/
    if(isQuestion) {
        Object[] options = {"YES", "CANCEL"};
        int i = JOptionPane.showOptionDialog(rootPane, "Are You Sure to delete question?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (i == 0) {
            int last = sortedQuestionsList.size()-1;
            QuestionBean questionBean = sortedQuestionsList.get(currentIndex);
            if(new QuestionOperation().deleteQue(questionBean)) {
                if(selectedQuestionsList.contains(questionBean)) {
                    int index2 = selectedQuestionsList.indexOf(questionBean);
                    selectedQuestionsList.remove(index2);
                }
                
                sortedQuestionsList.remove(currentIndex);
                questionsList.remove(currentIndex);
                
                if(last == currentIndex)
                    currentIndex--;
                loadAfterDeletion();
            } else {
                JOptionPane.showMessageDialog(rootPane, "Error In Question Deletion");
            }
        }
    }
}//GEN-LAST:event_BtnDeleteActionPerformed
    
    public void loadAfterDeletion() {
        int ii = 0;
        addedSequence = new ArrayList<Integer>();
        for(QuestionBean bean:sortedQuestionsList) {
            if(selectedQuestionsList.contains(bean)) {
                addedSequence.add(ii);
            }
            ii++;
        }

        if(shuffleValue == 1 && addedSequence.size() != 0){
            Collections.shuffle(addedSequence);
        }

        totalSelectedQueCount = selectedQuestionsList.size();
        LblSelectedCount.setText("Selected Question : " + totalSelectedQueCount);
        LblChapterName.setText(this.subjectBean.getSubjectName() + " : " + this.chapterBean.getChapterName() + " (" + this.questionsList.size() + ")");

        if (sortedQuestionsList.size() == 0) {
            JOptionPane.showMessageDialog(null,"Selected Chapter have no Questions for Combinations");
            new SingleChapterSelection(currentPatternIndex,subjectBean,printingPaperType).setVisible(true);
            this.dispose();
        }
        
        LblCount.setText("COUNT: " + sortedQuestionsList.size());
        setPanel(false);
    }
    
    private void BtnModifyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnModifyActionPerformed
        if (isQuestion) {
            QuestionBean questionsBean = sortedQuestionsList.get(currentIndex);
            new ModifyQuestion(questionsBean , this , currentIndex).setVisible(true);
            this.setVisible(false);
        }
    }//GEN-LAST:event_BtnModifyActionPerformed

    private void BtnChangeChapterMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnChangeChapterMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnChangeChapterMouseEntered

    private void BtnChangeChapterMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnChangeChapterMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnChangeChapterMouseExited

    private void BtnChangeChapterItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_BtnChangeChapterItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnChangeChapterItemStateChanged

    private void BtnChangeChapterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnChangeChapterActionPerformed
//        new SingleChapterSelection(subjectId, selectiontype).setVisible(true);
        new SingleChapterSelection(currentPatternIndex,subjectBean,printingPaperType).setVisible(true);
        this.dispose();
    }//GEN-LAST:event_BtnChangeChapterActionPerformed

    private void BtnChangeChapterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnChangeChapterKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnChangeChapterKeyPressed

    private void ChbkRandomItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChbkRandomItemStateChanged
        // TODO add your handling code here:
        if(isQuestion) {
            if (ChbkRandom.isSelected()) {
                TxtFldStart.setEnabled(false);
            } else {
                TxtFldStart.setEnabled(true);
            }
        }
    }//GEN-LAST:event_ChbkRandomItemStateChanged

    private void BtnShuffleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnShuffleActionPerformed
        if(isQuestion) {
            if(addedSequence.size() > 1) {
                Object[] options = {"YES", "NO"};
                int i = JOptionPane.showOptionDialog(null, "Sequence of question will be changed? Are you sure to continue?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
                if (i == 0) {
                    shuffleValue = 1;
                    Collections.shuffle(addedSequence);
                    setButtonOnPanel(RightBodyPanel, LeftBodyPanel);
                    JOptionPane.showMessageDialog(null, "Questions are shuffled.");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Unable to Shuffle, please add more questions.");
            }
        }
    }//GEN-LAST:event_BtnShuffleActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        // TODO add your handling code here:
    }//GEN-LAST:event_formWindowClosed

    private void BtnWordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnWordActionPerformed
        // TODO add your handling code here:
        if(!selectedQuestionsList.isEmpty()) {
            if(shuffleValue == 1) 
                Collections.shuffle(selectedQuestionsList);
            ArrayList<ChapterBean> chapterList = new ArrayList<ChapterBean>();
            chapterList.add(chapterBean);
            ArrayList<SubjectBean> selectedSubjectList = new ArrayList<SubjectBean>();
            selectedSubjectList.add(subjectBean);
            new WordPageSetup(selectedQuestionsList, selectedSubjectList, chapterList,(Object)this, questionsList, printingPaperType).setVisible(true);
            this.setEnabled(false);
        } else {    
            JOptionPane.showMessageDialog(this, "Please Add Questions for Printing.");
        }
    }//GEN-LAST:event_BtnWordActionPerformed

    private void TxtFldQuestionNumberKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtFldQuestionNumberKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() != 10) {
            keyPressed(evt, TxtFldQuestionNumber);
        }
    }//GEN-LAST:event_TxtFldQuestionNumberKeyPressed

    private void TxtFldStartKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtFldStartKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() != 10) {
            keyPressed(evt, TxtFldStart);
        }
    }//GEN-LAST:event_TxtFldStartKeyPressed

    private void TxtFldEndKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtFldEndKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() != 10) {
            keyPressed(evt, TxtFldEnd);
        }
    }//GEN-LAST:event_TxtFldEndKeyPressed

    private void PanelQues1AncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_PanelQues1AncestorAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_PanelQues1AncestorAdded

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
//        String CurrentPatternName=new PatternStatusOperation().getCurrentPatternName();
//        subid=subjectBean.getSubjectId();
//        String t = JOptionPane.showInputDialog("Enter Test Time In Minutes.");
//        int testTime = Integer.parseInt(t); 
//        String TestName = JOptionPane.showInputDialog("Enter Test Name.");
//        int testId = new ClassSaveTestOperation().saveNewClassTest(selectedQuestionsList, subid, testTime, TestName,CurrentPatternName);
//        if (testId == -1) {
//        JOptionPane.showMessageDialog(null, "Error In Saving Test Please Try Again.");
//        } else {
//        JOptionPane.showMessageDialog(null, "Your Test Id Is : " + testId);
//        new ClassSaveTestOperation().insertintoTest_And_Set(testId);
//        new HomePage().setVisible(true);
//        this.dispose();
//        new SetOnlineTestSubjectMark(subjectBean.getSubjectName()).setVisible(true);
//        }
      ArrayList<SubjectBean> selectedSubjectList = new ArrayList<SubjectBean>();
          selectedSubjectList.add(subjectBean);
          new SaveTest(subjectBean,selectedSubjectList,selectedQuestionsList,questionsList,printingPaperType,(Object)this).setVisible(true);
         
    }//GEN-LAST:event_jButton1ActionPerformed
    
    private void keyPressed(KeyEvent evt, JTextField text) {
        if ((evt.getKeyChar() >= '0' && evt.getKeyChar() <= '9') || evt.getKeyCode() == 8 || evt.getKeyCode() == 9 || evt.getKeyCode() == 13 || evt.getKeyCode() == 16 || evt.getKeyCode() == 17 || evt.getKeyCode() == 20 || evt.getKeyCode() == 27 || evt.getKeyCode() == 37 || evt.getKeyCode() == 38 || evt.getKeyCode() == 39 || evt.getKeyCode() == 40 || evt.getKeyCode() == 46) {
            text.setEditable(true);
        } else {
            text.setEditable(false);
            JOptionPane.showMessageDialog(null, "Please Enter Numbers Only.");
            text.setEditable(true);
        }
    }
    
    public void loadAfterModification(QuestionBean questionsBean,boolean ratioUpdate) {
        if(questionsBean != null) {
            QuestionBean oldQuesBean = sortedQuestionsList.get(currentIndex); 
            questionsBean.setSelected(oldQuesBean.isSelected());
            int index1 = questionsList.indexOf(oldQuesBean);
            sortedQuestionsList.set(currentIndex, questionsBean);
            questionsList.set(index1, questionsBean);
            int index2 = selectedQuestionsList.indexOf(oldQuesBean);
            if(index2 != -1)
                selectedQuestionsList.set(index2, questionsBean);
            if(ratioUpdate)
                imageRatioList = new ImageRatioOperation().getImageRatioList();
            setQuesType();
        }
    }
    
    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(SingleChapterQuestionsSelection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(SingleChapterQuestionsSelection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(SingleChapterQuestionsSelection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(SingleChapterQuestionsSelection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SingleChapterQuestionsSelection().setVisible(true);
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel BodyBottomPanel;
    private javax.swing.JButton BtnAddRemove;
    private javax.swing.JButton BtnChangeChapter;
    private javax.swing.JButton BtnDelete;
    private javax.swing.JButton BtnFirst;
    private javax.swing.JButton BtnHome;
    private javax.swing.JButton BtnModify;
    private javax.swing.JButton BtnNext;
    private javax.swing.JButton BtnPdf;
    private javax.swing.JButton BtnPrevious;
    private javax.swing.JButton BtnRandomAdd;
    private javax.swing.JButton BtnRemoveAll;
    private javax.swing.JButton BtnShuffle;
    private javax.swing.JButton BtnWord;
    private javax.swing.JCheckBox ChbkRandom;
    private javax.swing.JCheckBox ChkLevel;
    private javax.swing.JCheckBox ChkPrevious;
    private javax.swing.JCheckBox ChkType;
    private javax.swing.JCheckBox ChkUsed;
    private javax.swing.JComboBox CmbLevel;
    private javax.swing.JComboBox CmbPrevious;
    private javax.swing.JComboBox CmbType;
    private javax.swing.JComboBox CmbUsed;
    private javax.swing.JPanel FooterPanel;
    private javax.swing.JPanel HeaderPanel;
    private javax.swing.JLabel LblChapterName;
    private javax.swing.JLabel LblCount;
    private javax.swing.JLabel LblNonSelectedQue;
    private javax.swing.JLabel LblSelectedCount;
    private javax.swing.JLabel LblSort;
    private javax.swing.JPanel LeftBodyPanel;
    private javax.swing.JScrollPane LeftBodyScrollPane;
    private com.ui.support.pages.JPanelsSliding MiddleBodyPanel;
    private javax.swing.JScrollPane MiddleBodyScrollPane;
    private com.ui.support.pages.QuestionPanel PanelQues1;
    private com.ui.support.pages.QuestionPanel PanelQues2;
    private javax.swing.JPanel RightBodyPanel;
    private javax.swing.JScrollPane RightBodyScrollPane;
    private javax.swing.JPanel SortPanel;
    private javax.swing.JPanel SubFooterPanel;
    private javax.swing.JPanel SubHeaderPanel;
    private javax.swing.JTextField TxtFldEnd;
    private javax.swing.JTextField TxtFldQuestionNumber;
    private javax.swing.JTextField TxtFldStart;
    private javax.swing.ButtonGroup btnGroupAnimation;
    private javax.swing.JButton btnLast;
    private javax.swing.JButton jButton1;
    private com.ui.support.pages.NoQuestionPanel noQuestionPanel1;
    // End of variables declaration//GEN-END:variables

    //Getter And Setters
    public boolean isTestSaveStatus() {
        return testSaveStatus;
    }

    public void setTestSaveStatus(boolean testSaveStatus) {
        this.testSaveStatus = testSaveStatus;
    }

    public ArrayList<Integer> getSaveTestSelectedQuesIdList() {
        return saveTestSelectedQuesIdList;
    }

    public void setSaveTestSelectedQuesIdList(ArrayList<Integer> saveTestSelectedQuesIdList) {
        this.saveTestSelectedQuesIdList = saveTestSelectedQuesIdList;
    }

    public PrintedTestBean getPrintedTestBean() {
        return printedTestBean;
    }

    public void setPrintedTestBean(PrintedTestBean printedTestBean) {
        this.printedTestBean = printedTestBean;
    }
    
    //Delete
    public int getCurrentIndex() {
        return currentIndex;
    }

    public void setCurrentIndex(int currentIndex) {
        this.currentIndex = currentIndex;
    }

    public ArrayList<QuestionBean> getQuestionsList() {
        return questionsList;
    }

    public void setQuestionsList(ArrayList<QuestionBean> questionsList) {
        this.questionsList = questionsList;
    }

    public ArrayList<QuestionBean> getSortedQuestionsList() {
        return sortedQuestionsList;
    }

    public void setSortedQuestionsList(ArrayList<QuestionBean> sortedQuestionsList) {
        this.sortedQuestionsList = sortedQuestionsList;
    }

    public ArrayList<QuestionBean> getSelectedQuestionsList() {
        return selectedQuestionsList;
    }

    public void setSelectedQuestionsList(ArrayList<QuestionBean> selectedQuestionsList) {
        this.selectedQuestionsList = selectedQuestionsList;
    }

    public ChapterBean getChapterBean() {
        return chapterBean;
    }

    public void setChapterBean(ChapterBean chapterBean) {
        this.chapterBean = chapterBean;
    }
}