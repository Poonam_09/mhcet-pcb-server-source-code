
package com.pages;

import com.Model.PdfOpener;
import com.bean.ChapterBean;
import com.bean.SubjectBean;
import com.db.operations.ChapterOperation;
import java.awt.Color;
import java.awt.Dimension;
import static java.awt.Frame.MAXIMIZED_BOTH;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import com.Model.TitleInfo;

public class ChapterSummary extends javax.swing.JFrame {
    private ArrayList<ChapterBean> chapterList; 
    private javax.swing.JButton[] chapterButtonArray;
    private HomePage homePage;
    private String printingPaperType;
    
    public ChapterSummary() {
        initComponents();
        setExtendedState(MAXIMIZED_BOTH);
        chapterList = new ChapterOperation().getChapterList(1);
        LblHeaderMessage.setText("Chapter-Wise Summery of Physics");
        setChapter();
    }
    
    public ChapterSummary(SubjectBean subjectBean,HomePage homePage) {
        initComponents();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(screenSize);
//        pnlSubUnitTest.setSize(screenSize);
        getContentPane().setBackground(Color.white);
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        this.setExtendedState(MAXIMIZED_BOTH);
        this.setLocationRelativeTo(null);
        this.homePage = homePage;
        chapterList = new ChapterOperation().getChapterSummaryList(subjectBean.getSubjectId());
//        chapterList = new ChapterOperation().getChapterList(subjectBean.getSubjectId());
        LblHeaderMessage.setText("Chapter-Wise Summary of " + subjectBean.getSubjectName());
//        this.setTitle("Chapter-Wise Summery of " + subjectBean.getSubjectName());
        setChapter();
    }
    
    public ChapterSummary(SubjectBean subjectBean,String printingPaperType) {
        initComponents();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(screenSize);
//        pnlSubUnitTest.setSize(screenSize);
        getContentPane().setBackground(Color.white);
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        this.setExtendedState(MAXIMIZED_BOTH);
        this.setLocationRelativeTo(null);
        this.printingPaperType = printingPaperType;
        chapterList = new ChapterOperation().getChapterList(subjectBean.getSubjectId());
        LblHeaderMessage.setText("Chapter-Wise Summary of " + subjectBean.getSubjectName());
//        this.setTitle("Chapter-Wise Summery of " + subjectBean.getSubjectName());
        setChapter();
    }
    
    public void setChapter() {
        chapterButtonArray = new javax.swing.JButton[chapterList.size()];
        for (int x = 0; x < chapterList.size(); x++) {
            chapterButtonArray[x] = new javax.swing.JButton();
            chapterButtonArray[x].setText("<html><b><center>" + chapterList.get(x).getChapterName()+"</center></b></html>");
            chapterButtonArray[x].setMaximumSize(new java.awt.Dimension(170, 100));
            chapterButtonArray[x].setMinimumSize(new java.awt.Dimension(170, 100));
            chapterButtonArray[x].setPreferredSize(new java.awt.Dimension(170, 100));
            chapterButtonArray[x].setBorderPainted(true);
            chapterButtonArray[x].setBackground(new java.awt.Color(11,45,45));
            chapterButtonArray[x].setFont(new java.awt.Font("Microsoft JhengHei", 0, 14));
            chapterButtonArray[x].setForeground(new java.awt.Color(240,240,240));
            chapterButtonArray[x].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                   setChapterName(e.getActionCommand());
                }
            });
        }
        GridBagConstraints cons = new GridBagConstraints();
        cons.gridx = 2;
        cons.gridy = 2;
//        cons.gridwidth = 2;
//        cons.gridheight = 2;
        cons.anchor = GridBagConstraints.BELOW_BASELINE_LEADING;
//        cons.weightx =1;
//        cons.weighty = 2;
        cons.insets = new java.awt.Insets(5,5,5,5);
        GridBagLayout layout = new GridBagLayout();
        for (int x = 0; x < chapterList.size(); x++) {
            if (x % 7 == 0) {
                cons.gridy ++;
                cons.gridx = 0;
            }
            
            jPanel1.setLayout(layout);
            jPanel1.add(chapterButtonArray[x], cons);
            cons.gridx++;
        }   
    }
    
    public void setChapterName(String actionCommand) {
        String strSplt[] = actionCommand.split("<center>");
        String tempSplt = strSplt[1];
        strSplt = tempSplt.split("<");
        tempSplt = strSplt[0];
        String chapterName = null;
        for(ChapterBean chapterBean : chapterList) {
            if(chapterBean.getChapterName().equals(tempSplt.trim()))
                chapterName = chapterBean.getChapterName();
        }
        
        if(chapterName != null) {
            chapterName = chapterName.replace(":", "");
            try {
                PdfOpener pdof=new PdfOpener();
                pdof.openPdf(chapterName);
            } catch(Exception ex) {
                JOptionPane.showMessageDialog(rootPane, "Summary not found");
                ex.printStackTrace();
            }
        } else {
            JOptionPane.showMessageDialog(rootPane, "Summary not found");
        }
   }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        LblHeaderMessage = new javax.swing.JLabel();
        BtnBack = new javax.swing.JToggleButton();
        ChapterNote = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jPanel3.setBackground(new java.awt.Color(0, 20, 72));

        LblHeaderMessage.setFont(new java.awt.Font("Microsoft JhengHei", 1, 30)); // NOI18N
        LblHeaderMessage.setForeground(new java.awt.Color(255, 255, 255));
        LblHeaderMessage.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        BtnBack.setBackground(new java.awt.Color(102, 102, 102));
        BtnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/common/images/BackImg2.png"))); // NOI18N
        BtnBack.setBorderPainted(false);
        BtnBack.setContentAreaFilled(false);
        BtnBack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnBackMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnBackMouseExited(evt);
            }
        });
        BtnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBackActionPerformed(evt);
            }
        });

        ChapterNote.setForeground(new java.awt.Color(255, 255, 102));
        ChapterNote.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChapterNote.setText("(Note: We Provide Same Chapters Summary For All Patterns.)");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(BtnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(LblHeaderMessage, javax.swing.GroupLayout.DEFAULT_SIZE, 245, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ChapterNote)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(ChapterNote)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(LblHeaderMessage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(BtnBack, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jScrollPane1.setBorder(null);

        jPanel1.setBackground(new java.awt.Color(0, 102, 102));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jScrollPane1.setViewportView(jPanel1);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane1)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 64, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnBackMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnBackMouseExited
        BtnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/common/images/BackImg2.png")));
    }//GEN-LAST:event_BtnBackMouseExited

    private void BtnBackMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnBackMouseEntered
        BtnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/common/images/BackImg1.png")));
    }//GEN-LAST:event_BtnBackMouseEntered

    private void BtnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBackActionPerformed
        if(homePage != null) {
            homePage.setVisible(true);
            homePage.setNextPageCallStatus(false);
            this.dispose();
        }
    }//GEN-LAST:event_BtnBackActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
        Object[] options = {"YES", "CANCEL"};
        int i = JOptionPane.showOptionDialog(null, "Are You Sure Back to Home?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (i == 0) {
            new HomePage().setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_formWindowClosing

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ChapterSummary.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ChapterSummary.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ChapterSummary.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ChapterSummary.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ChapterSummary().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton BtnBack;
    private javax.swing.JLabel ChapterNote;
    private javax.swing.JLabel LblHeaderMessage;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
