/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pages;

import com.Model.SmsOperation;
import com.Model.TitleInfo;
import com.bean.RankBean;
import com.bean.SMSHistoryBean;
import com.bean.SenderIdBean;
import com.bean.StudentBean;
import com.db.operations.ClassSaveTestOperation;
import com.db.operations.SMSHistoryOperation;
import com.db.operations.SenderIdOperation;
import com.db.operations.StudentRegistrationOperation;
import com.db.operations.SubjectOperation;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import ui.RankGeneration;
import ui.RankGenerationGroup;

/**
 *
 * @author Aniket
 */
public class SendGroupTestResult extends javax.swing.JFrame {

    /**
     * Creates new form ViewStudents
     */
    ArrayList<RankBean> rankList= new ArrayList<RankBean>();
    private RankGenerationGroup rankGenerationGroup;
    private String instituteName;
    StudentBean studentBean=null;
    ArrayList<StudentBean> studentBeanList= new ArrayList<StudentBean>();    
    SenderIdBean senderIdBean=null;  
    private ArrayList<SenderIdBean> senderIdList;
    
    public SendGroupTestResult(ArrayList<RankBean> rankList ,String instituteName,RankGenerationGroup rankGenerationGroup) {
        initComponents();
//        this.setLocationRelativeTo(null);
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize(screenSize.width, (screenSize.height * 95) / 100);
        this.rankGenerationGroup = rankGenerationGroup;      
        this.rankList = rankList;

        ResultTable.getColumnModel().getColumn(0).setPreferredWidth(50);
        ResultTable.getColumnModel().getColumn(1).setPreferredWidth(300);
        ResultTable.getColumnModel().getColumn(2).setPreferredWidth(100);
        ResultTable.getColumnModel().getColumn(3).setPreferredWidth(50);
        this.instituteName = instituteName;
        loadResult();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        HeaderPanel = new javax.swing.JPanel();
        LblHeader = new javax.swing.JLabel();
        BodyPanel = new javax.swing.JPanel();
        StudentTableScrollPane = new javax.swing.JScrollPane();
        ResultTable = new javax.swing.JTable();
        SortPanel = new javax.swing.JPanel();
        ChkSelectAll = new javax.swing.JCheckBox();
        BtnSendSMS = new javax.swing.JButton();
        BtnCancel = new javax.swing.JButton();
        btnSendSMSHistory = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        HeaderPanel.setBackground(new java.awt.Color(0, 0, 0));

        LblHeader.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 20)); // NOI18N
        LblHeader.setForeground(new java.awt.Color(255, 255, 255));
        LblHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblHeader.setText("Send Group Test Result");

        javax.swing.GroupLayout HeaderPanelLayout = new javax.swing.GroupLayout(HeaderPanel);
        HeaderPanel.setLayout(HeaderPanelLayout);
        HeaderPanelLayout.setHorizontalGroup(
            HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(LblHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        HeaderPanelLayout.setVerticalGroup(
            HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(LblHeader, javax.swing.GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE)
        );

        BodyPanel.setPreferredSize(new java.awt.Dimension(719, 450));

        StudentTableScrollPane.setBackground(new java.awt.Color(102, 102, 102));
        StudentTableScrollPane.setPreferredSize(new java.awt.Dimension(717, 430));

        ResultTable.setBackground(new java.awt.Color(102, 102, 102));
        ResultTable.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        ResultTable.setForeground(new java.awt.Color(255, 255, 255));
        ResultTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Sr No", "Student Name", "Mobile Number", "Select Sender"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        ResultTable.setMinimumSize(new java.awt.Dimension(60, 300));
        ResultTable.setRowHeight(24);
        ResultTable.setRowSelectionAllowed(false);
        ResultTable.setSelectionBackground(new java.awt.Color(128, 128, 128));
        ResultTable.setSelectionForeground(new java.awt.Color(255, 255, 102));
        StudentTableScrollPane.setViewportView(ResultTable);

        javax.swing.GroupLayout BodyPanelLayout = new javax.swing.GroupLayout(BodyPanel);
        BodyPanel.setLayout(BodyPanelLayout);
        BodyPanelLayout.setHorizontalGroup(
            BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 719, Short.MAX_VALUE)
            .addGroup(BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(StudentTableScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 1052, Short.MAX_VALUE))
        );
        BodyPanelLayout.setVerticalGroup(
            BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 446, Short.MAX_VALUE)
            .addGroup(BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(StudentTableScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 630, Short.MAX_VALUE))
        );

        SortPanel.setBackground(new java.awt.Color(0, 51, 102));

        ChkSelectAll.setBackground(new java.awt.Color(0, 51, 102));
        ChkSelectAll.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        ChkSelectAll.setForeground(new java.awt.Color(255, 0, 0));
        ChkSelectAll.setText("Select All");
        ChkSelectAll.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkSelectAllItemStateChanged(evt);
            }
        });

        BtnSendSMS.setBackground(new java.awt.Color(208, 87, 96));
        BtnSendSMS.setFont(new java.awt.Font("Calibri", 0, 16)); // NOI18N
        BtnSendSMS.setForeground(new java.awt.Color(255, 255, 255));
        BtnSendSMS.setText("Send SMS");
        BtnSendSMS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSendSMSActionPerformed(evt);
            }
        });

        BtnCancel.setBackground(new java.awt.Color(208, 87, 96));
        BtnCancel.setFont(new java.awt.Font("Calibri", 0, 16)); // NOI18N
        BtnCancel.setForeground(new java.awt.Color(255, 255, 255));
        BtnCancel.setText("Cancel");
        BtnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCancelActionPerformed(evt);
            }
        });

        btnSendSMSHistory.setBackground(new java.awt.Color(208, 87, 96));
        btnSendSMSHistory.setFont(new java.awt.Font("Calibri Light", 0, 16)); // NOI18N
        btnSendSMSHistory.setForeground(new java.awt.Color(255, 255, 255));
        btnSendSMSHistory.setText("Send Sms History");
        btnSendSMSHistory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSendSMSHistoryActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout SortPanelLayout = new javax.swing.GroupLayout(SortPanel);
        SortPanel.setLayout(SortPanelLayout);
        SortPanelLayout.setHorizontalGroup(
            SortPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SortPanelLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(ChkSelectAll)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 245, Short.MAX_VALUE)
                .addComponent(BtnCancel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSendSMSHistory)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BtnSendSMS)
                .addGap(30, 30, 30))
        );
        SortPanelLayout.setVerticalGroup(
            SortPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SortPanelLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(SortPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ChkSelectAll, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BtnSendSMS)
                    .addComponent(BtnCancel)
                    .addComponent(btnSendSMSHistory, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE))
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(SortPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(BodyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(HeaderPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(HeaderPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(SortPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(BodyPanel, 446, 446, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
        Object[] options = {"YES", "CANCEL"};
        int i = JOptionPane.showOptionDialog(null, "Are You Sure Back to Home Page?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (i == 0) {
            new HomePage().setVisible(true);
            rankGenerationGroup.dispose();
            this.dispose();
        }
    }//GEN-LAST:event_formWindowClosing

    private void BtnSendSMSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSendSMSActionPerformed
        // TODO add your handling code here:
        if (rankList != null) {
            ArrayList<RankBean> selectedList = null;
            DefaultTableModel model = (DefaultTableModel) ResultTable.getModel();
            for(int i=0;i<rankList.size();i++) {
                if((boolean)model.getValueAt(i, 3)) {
                    if(selectedList == null)
                        selectedList = new ArrayList<RankBean>();
                    selectedList.add(rankList.get(i));
                }
            }
            if(selectedList != null) {
                String textMessage = "";
                String smsResponse = "";
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                
                try {
//                    senderIdList= new SenderIdOperation().getSenderIdList();
                     senderIdList= new SenderIdOperation().getSenderIdbyflagList();
                    for(SenderIdBean senderIdBean : senderIdList) 
                    {
                        this.senderIdBean=senderIdBean;                      
                        System.out.println("senderIdBean.getSenderId()"+senderIdBean.getSenderId());
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                String UserName=senderIdBean.getUserName().trim();
                String Password=senderIdBean.getPassword().trim();
                String SenderId=senderIdBean.getSenderId().trim();
                System.out.println(UserName+"...."+Password+"...."+SenderId);
                
                
                for(RankBean rankBean : selectedList) {
//                    String SubjectName=new SubjectOperation().getSubject(rankBean.getSubject_Id());
                    String testName=new ClassSaveTestOperation().getTestName(rankBean.getUnitTestId());
                    
                    studentBeanList=new StudentRegistrationOperation().getAllInfoStudent(rankBean.getRollNo());
                    
                    for(StudentBean studentBean : studentBeanList) {
                        
                    textMessage = "";
                    textMessage += instituteName.trim() + "\n";
                    textMessage += "Name :"+studentBean.getStudentName().trim() + "\n";
                    textMessage += "Subject Name :"+ "MH-CET-PCB" + ".\n";
                    textMessage += "Test Id :"+rankBean.getUnitTestId()+ "\n";
                    textMessage += "Test Name :"+ testName+ "\n";
                    textMessage += "Class/Stand :"+studentBean.getStandard().trim() +"/"+studentBean.getDivision().trim()+ "\n";                   
                    textMessage += "Phy Correct/Incorrect :"+rankBean.getPHY_CORRECT_QUESTIONS()+"/"+rankBean.getPHY_INCORRECT_QUESTIONS()+"\n";
                    textMessage += "Chem Correct/Incorrect :"+rankBean.getCHEM_CORRECT_QUESTIONS()+"/"+rankBean.getCHEM_INCORRECT_QUESTIONS()+"\n";
                    textMessage += "Bio Correct/Incorrect :"+rankBean.getBIO_CORRECT_QUESTIONS()+"/"+rankBean.getBIO_INCORRECT_QUESTIONS()+"\n";                  
                    textMessage += "Total Correct/Incorrect:"+rankBean.getCorrect_Questions()+"/"+rankBean.getIncorrect_Questions()+"\n";
                    textMessage += "Phy Marks :"+rankBean.getTOTAL_PHY_MARK()+"/"+rankBean.getTOTAL_PHY_QUESTIONS()+"\n";
                    textMessage += "Chem Marks :"+rankBean.getTOTAL_CHEM_MARK()+"/"+rankBean.getTOTAL_CHEM_QUESTIONS()+"\n";
                    textMessage += "Bio Marks :"+rankBean.getTOTAL_BIO_MARK()+"/"+rankBean.getTOTAL_BIO_QUESTIONS()+"\n";                  
                    textMessage += "Total Marks :"+rankBean.getObtainMark()+"/"+rankBean.getTotalMark()+"\n";
//                    textMessage += "Date : "+sdf.format(new Date(rankBean.getTimeinstring().trim()));        

//                    smsResponse +=  new SmsOperation().sendMessages1(UserName,Password,textMessage,SenderId,studentBean.getMobileNo().trim())+"," ;
//                    smsResponse +=new SmsOperation().sendSMS(UserName,Password,textMessage,SenderId,studentBean.getMobileNo().trim())+"," ;
                   new SmsOperation().sendSMS(studentBean.getStudentName(),rankBean.getUnitTestId(),testName,rankBean.getRollNo(),UserName,Password,textMessage,SenderId,studentBean.getMobileNo().trim());
                    System.out.println(smsResponse);
                     
                    }
                }

            } else {
                JOptionPane.showMessageDialog(rootPane, "Select Atleast One Sender.");
            }
  
            
//            this coading for Route SMS Service
//            if(selectedList != null) {
//                String textMessage = "";
//                String smsResponse = "";
//                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//                for(RankBean rankBean : selectedList) {
//                    for(StudentBean studentBean : studentBeanList) {
//                    textMessage = "";
//                    textMessage += instituteName.trim() + ".\n";
//                    textMessage += studentBean.getStudentName().trim() + ".\n";
////                    textMessage += rankBean.getClassTestBean().getTitle().trim() + "\n";
//                    textMessage += "Class : "+studentBean.getStandard().trim() +"/"+studentBean.getDivision().trim()+ "\n";
//                    textMessage += "Marks : "+rankBean.getObtainMark()+"/"+rankBean.getTotalMark()+"\n";
////                    textMessage += "Date : "+sdf.format(new Date(rankBean.getTimeinstring().trim()));            
//                    smsResponse +=  new SmsOperation().sendMessages(textMessage, studentBean.getMobileNo().trim())+"," ;
//                    System.out.println(smsResponse);
//                    }
//                }
//
//                String message = "";
//                String[] responseArray = smsResponse.split(",");
//                
//                for(String response : responseArray) {
//                    String[] tempArray = response.split("\\|");
//                    System.out.println("tempArray"+tempArray);
//                    tempArray = tempArray[1].split(":");
//                    String mobileNumber = tempArray[0].substring(2, tempArray[0].length());
//                    if(response.startsWith("1701")) {
//                        message += mobileNumber + " Message Send Successfully...!!!\n";
//                    } else if(response.startsWith("1706")) {
//                        message += mobileNumber + " Invalid Destination...!!!\n";
//                    } else if(response.startsWith("1025")) {
//                        message += mobileNumber + " Insufficient Credit...!!!\n";
//                    } else if(response.startsWith("1032")) {
//                        message += mobileNumber + " in DND...!!!\n";
//                    } else {
//                        message += mobileNumber + " Message Sending Error...!!!\n";
//                    }
//                }
//                JOptionPane.showMessageDialog(null, message);
//            } else {
//                JOptionPane.showMessageDialog(rootPane, "Select Atleast One Sender.");
//            }
        }
    }//GEN-LAST:event_BtnSendSMSActionPerformed

    private void ChkSelectAllItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkSelectAllItemStateChanged
        // TODO add your handling code here:
        if (rankList != null) {
            boolean selected = ChkSelectAll.isSelected();
            DefaultTableModel model = (DefaultTableModel) ResultTable.getModel();
            for(int i=0;i<rankList.size();i++)
                model.setValueAt(selected, i, 3);
        }
    }//GEN-LAST:event_ChkSelectAllItemStateChanged

    private void BtnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCancelActionPerformed
        // TODO add your handling code here:
        rankGenerationGroup.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_BtnCancelActionPerformed

    private void btnSendSMSHistoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSendSMSHistoryActionPerformed
        try {
            // TODO add your handling code here:

//            ArrayList<SMSHistoryBean> smsHistoryBeanList =new SMSHistoryOperation().getSMSHistoryList();
           ArrayList<SMSHistoryBean> smsHistoryBeanList =new SMSHistoryOperation().getSMSHistoryByTestId(rankList.get(0).getUnitTestId());
            
            new SendGroupSMSHistory(smsHistoryBeanList,this).setVisible(true);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }//GEN-LAST:event_btnSendSMSHistoryActionPerformed

                                         

     private void loadResult() {
        DefaultTableModel model = (DefaultTableModel) ResultTable.getModel();
        clearTableRow(model);
        System.out.println("rankList.size()"+rankList.size());
        for (int i = 0; i < rankList.size(); i++) {
            studentBeanList=new StudentRegistrationOperation().getAllInfoStudent(rankList.get(i).getRollNo());
            if (studentBeanList != null) {
                for (StudentBean studentBean : studentBeanList) {
                    model.addRow(new Object[]{(i+1), studentBean.getStudentName().trim(),
                    studentBean.getMobileNo().trim(),false});
                }
            }
        }     
    }

    private void clearTableRow(DefaultTableModel model) {
        int rowCount = model.getRowCount();
        for (int i = rowCount - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel BodyPanel;
    private javax.swing.JButton BtnCancel;
    private javax.swing.JButton BtnSendSMS;
    private javax.swing.JCheckBox ChkSelectAll;
    private javax.swing.JPanel HeaderPanel;
    private javax.swing.JLabel LblHeader;
    private javax.swing.JTable ResultTable;
    private javax.swing.JPanel SortPanel;
    private javax.swing.JScrollPane StudentTableScrollPane;
    private javax.swing.JButton btnSendSMSHistory;
    // End of variables declaration//GEN-END:variables
}
