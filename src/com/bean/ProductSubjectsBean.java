/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

/**
 *
 * @author Aniket
 */
public class ProductSubjectsBean {
    private int productSubjectId;
    private int productId;
    private String productSubjects;

    public int getProductSubjectId() {
        return productSubjectId;
    }

    public void setProductSubjectId(int productSubjectId) {
        this.productSubjectId = productSubjectId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductSubjects() {
        return productSubjects;
    }

    public void setProductSubjects(String productSubjects) {
        this.productSubjects = productSubjects;
    }
    
    
}
