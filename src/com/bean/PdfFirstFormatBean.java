/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

/**
 *
 * @author Aniket
 */
public class PdfFirstFormatBean {
    private String subjectName;
    private String divisionName;
    private String paperName;
    private String testId;
    private String departmentName;
    private int marksPerQueSubjectOne;
    private int marksPerQueSubjectTwo;
    private int marksPerQueSubjectThree;
    private String selectedDate;
    private String selectedTime;
    
    private boolean showSubjectName;
    private boolean showTotalQuestions;
    private boolean showDivision;
    private boolean showPaperName;
    private boolean showTestId;
    private boolean showDepartmentName;
    private boolean showTotal;
    private boolean showDate;
    private boolean showTime;
    
    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }
    
    public String getPaperName() {
        return paperName;
    }

    public void setPaperName(String paperName) {
        this.paperName = paperName;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public int getMarksPerQueSubjectOne() {
        return marksPerQueSubjectOne;
    }

    public void setMarksPerQueSubjectOne(int marksPerQueSubjectOne) {
        this.marksPerQueSubjectOne = marksPerQueSubjectOne;
    }

    public int getMarksPerQueSubjectTwo() {
        return marksPerQueSubjectTwo;
    }

    public void setMarksPerQueSubjectTwo(int marksPerQueSubjectTwo) {
        this.marksPerQueSubjectTwo = marksPerQueSubjectTwo;
    }

    public int getMarksPerQueSubjectThree() {
        return marksPerQueSubjectThree;
    }

    public void setMarksPerQueSubjectThree(int marksPerQueSubjectThree) {
        this.marksPerQueSubjectThree = marksPerQueSubjectThree;
    }

    public String getSelectedDate() {
        return selectedDate;
    }

    public void setSelectedDate(String selectedDate) {
        this.selectedDate = selectedDate;
    }

    public String getSelectedTime() {
        return selectedTime;
    }

    public void setSelectedTime(String selectedTime) {
        this.selectedTime = selectedTime;
    }

    public boolean isShowSubjectName() {
        return showSubjectName;
    }

    public void setShowSubjectName(boolean showSubjectName) {
        this.showSubjectName = showSubjectName;
    }

    public boolean isShowTotalQuestions() {
        return showTotalQuestions;
    }

    public void setShowTotalQuestions(boolean showTotalQuestions) {
        this.showTotalQuestions = showTotalQuestions;
    }

    public boolean isShowDivision() {
        return showDivision;
    }

    public void setShowDivision(boolean showDivision) {
        this.showDivision = showDivision;
    }

    public boolean isShowPaperName() {
        return showPaperName;
    }

    public void setShowPaperName(boolean showPaperName) {
        this.showPaperName = showPaperName;
    }

    public boolean isShowTestId() {
        return showTestId;
    }

    public void setShowTestId(boolean showTestId) {
        this.showTestId = showTestId;
    }

    public boolean isShowDepartmentName() {
        return showDepartmentName;
    }

    public void setShowDepartmentName(boolean showDepartmentName) {
        this.showDepartmentName = showDepartmentName;
    }

    public boolean isShowTotal() {
        return showTotal;
    }

    public void setShowTotal(boolean showTotal) {
        this.showTotal = showTotal;
    }

    public boolean isShowDate() {
        return showDate;
    }

    public void setShowDate(boolean showDate) {
        this.showDate = showDate;
    }

    public boolean isShowTime() {
        return showTime;
    }

    public void setShowTime(boolean showTime) {
        this.showTime = showTime;
    }
}
