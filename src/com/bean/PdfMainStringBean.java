/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

/**
 *
 * @author Aniket
 */
public class PdfMainStringBean {
    private String mainString;
    private String teacherMainString;

    public String getMainString() {
        return mainString;
    }

    public void setMainString(String mainString) {
        this.mainString = mainString;
    }

    public String getTeacherMainString() {
        return teacherMainString;
    }

    public void setTeacherMainString(String teacherMainString) {
        this.teacherMainString = teacherMainString;
    }
    
}
