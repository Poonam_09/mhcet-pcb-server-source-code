/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

/**
 *
 * @author Aniket
 */
public class SMSHistoryBean {
    private int Sid,TestId,RollNo;
    private String StudentName,MobileNo,MessageText,TestName;
    private boolean DeliveryReport;
    private String Send_Sms_Date;
    
    
    
    
    public String getSend_Sms_Date() {
        return Send_Sms_Date;
    }

    public void setSend_Sms_Date(String Send_Sms_Date) {
        this.Send_Sms_Date = Send_Sms_Date;
    }
    
    public int getSid() {
        return Sid;
    }

    public void setSid(int Sid) {
        this.Sid = Sid;
    }

    public int getTestId() {
        return TestId;
    }

    public void setTestId(int TestId) {
        this.TestId = TestId;
    }

    public String getStudentName() {
        return StudentName;
    }

    public void setStudentName(String StudentName) {
        this.StudentName = StudentName;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String MobileNo) {
        this.MobileNo = MobileNo;
    }

    public String getMessageText() {
        return MessageText;
    }

    public void setMessageText(String MessageText) {
        this.MessageText = MessageText;
    }

    public String getTestName() {
        return TestName;
    }

    public void setTestName(String TestName) {
        this.TestName = TestName;
    }

    public boolean isDeliveryReport() {
        return DeliveryReport;
    }

    public void setDeliveryReport(boolean DeliveryReport) {
        this.DeliveryReport = DeliveryReport;
    }
    
     public int getRollNo() {
        return RollNo;
    }

    public void setRollNo(int RollNo) {
        this.RollNo = RollNo;
    }
}
