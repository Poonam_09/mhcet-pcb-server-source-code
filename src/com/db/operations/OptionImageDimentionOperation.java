/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.ChangeIdReferenceBean;
import com.bean.OptionImageDimensionsBean;
import com.bean.PatternOptionImageDimensionsBean;
import com.bean.QuestionBean;
import com.db.DbConnection;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.JLabel;
import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;

/**
 *
 * @author Aniket
 */
public class OptionImageDimentionOperation {
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    
    public ArrayList<OptionImageDimensionsBean> getOptionImageDimaentionList(ArrayList<QuestionBean> selectedQuestionsList,
            boolean isPreviousPaperQuestions){
        ArrayList<OptionImageDimensionsBean> returnList = new ArrayList<OptionImageDimensionsBean>();
        try{
            conn = new DbConnection().getConnection();
            String query = "";
            OptionImageDimensionsBean optImgDimensionsBean;
            if(isPreviousPaperQuestions)
                query = "Select * from OPTION_IMAGE_DIMENSIONSFORPAPER where QUEST_ID = ?";
            else
                query = "Select * from OPTION_IMAGE_DIMENSIONS where QUEST_ID = ?";
            ps=conn.prepareStatement(query);
            for(QuestionBean questionsBean : selectedQuestionsList){
                int quesId =  questionsBean.getQuestionId();
                ps.setInt(1, quesId);
                rs=ps.executeQuery();
                while(rs.next()) {
                    optImgDimensionsBean = new OptionImageDimensionsBean();
                    String a, b, c, d;
                    a = rs.getString(2);
                    String[] strFirst = a.split("X");
                    b = rs.getString(3);
                    String[] strSecond = b.split("X");
                    c = rs.getString(4);
                    String[] strThird = c.split("X");
                    d = rs.getString(5);
                    String[] strFourth = d.split("X");
                    int widthA = 0; int widthB = 0; int widthC = 0; int widthD=0;
                    try{
                        widthA=Integer.parseInt(strFirst[0]);
                    } catch(Exception ex) {
                        widthA=1000;
                    }
                    try{
                        widthB=Integer.parseInt(strSecond[0]);
                    } catch(Exception ex) {
                        widthB=1000;
                    }
                    try{
                        widthC=Integer.parseInt(strThird[0]);
                    } catch(Exception ex) {
                        widthC=1000;
                    }
                    try{
                        widthD=Integer.parseInt(strFourth[0]);
                    } catch(Exception ex) {
                        widthD=1000;
                    }
                    optImgDimensionsBean.setQuestionId(rs.getInt(1));
                    optImgDimensionsBean.setOptionA(widthA);
                    optImgDimensionsBean.setOptionB(widthB);
                    optImgDimensionsBean.setOptionC(widthC);
                    optImgDimensionsBean.setOptionD(widthD);
                    returnList.add(optImgDimensionsBean);
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnList;
    }
    
    public PatternOptionImageDimensionsBean insertOptionImageDimentions(QuestionBean questionsBean,boolean isPreviousPaperQuestions) {
        String optionA = makeImage(questionsBean.getOptionA(), questionsBean.getQuestionId(), "a");
        String optionB = makeImage(questionsBean.getOptionB(), questionsBean.getQuestionId(), "b");
        String optionC = makeImage(questionsBean.getOptionC(), questionsBean.getQuestionId(), "c");
        String optionD = makeImage(questionsBean.getOptionD(), questionsBean.getQuestionId(), "d");
        PatternOptionImageDimensionsBean returnBean = null;
        try {
            conn = new DbConnection().getConnection();
            String query = "";
            if(isPreviousPaperQuestions) {
                query = "INSERT INTO OPTION_IMAGE_DIMENSIONSFORPAPER VALUES(?,?,?,?,?)";
            } else {
                query = "INSERT INTO OPTION_IMAGE_DIMENSIONS VALUES(?,?,?,?,?)";
            }
            ps = conn.prepareStatement(query);
            ps.setInt(1, questionsBean.getQuestionId());
            ps.setString(2, optionA);
            ps.setString(3, optionB);
            ps.setString(4, optionC);
            ps.setString(5, optionD);
            ps.executeUpdate();
            
            returnBean = new PatternOptionImageDimensionsBean();
            returnBean.setQuestionId(questionsBean.getQuestionId());
            returnBean.setOptionA(optionA);
            returnBean.setOptionB(optionB);
            returnBean.setOptionC(optionC);
            returnBean.setOptionD(optionD);
            
        } catch (SQLException ex) {
            returnBean = null;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnBean;
    }
    
    public boolean updateOptionImageDimentions(QuestionBean questionsBean,boolean isPreviousPaperQuestions) {
        String optionA = makeImage(questionsBean.getOptionA(), questionsBean.getQuestionId(), "a");
        String optionB = makeImage(questionsBean.getOptionB(), questionsBean.getQuestionId(), "b");
        String optionC = makeImage(questionsBean.getOptionC(), questionsBean.getQuestionId(), "c");
        String optionD = makeImage(questionsBean.getOptionD(), questionsBean.getQuestionId(), "d");
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "";
            if(isPreviousPaperQuestions) {
                query = "UPDATE OPTION_IMAGE_DIMENSIONSFORPAPER SET OPTA = ?,OPTB = ?,OPTC = ?,OPTD = ? WHERE QUEST_ID = ?";
            } else {
                query = "UPDATE OPTION_IMAGE_DIMENSIONS SET OPTA = ?,OPTB = ?,OPTC = ?,OPTD = ? WHERE QUEST_ID = ?";
            }
            ps = conn.prepareStatement(query);
            ps.setString(1, optionA);
            ps.setString(2, optionB);
            ps.setString(3, optionC);
            ps.setString(4, optionD);
            ps.setInt(5, questionsBean.getQuestionId());
            ps.executeUpdate();
            returnValue = true;
        } catch (SQLException ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnValue;
    }
    
    private String makeImage(String a, int qid, String opt) {
        String returnValue = "";
        File fl = new File("newimages");
        if(!fl.exists())
            fl.mkdir();
        
        String imgQuestionFileName = "newimages/" + (qid + opt) + ".png";
        try {
            TeXFormula formula = new TeXFormula();
//            formula = new TeXFormula(a);
            try {
                formula = new TeXFormula(a);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
//            BufferedImage image=image = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
            TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 10);
            icon.setInsets(new Insets(0, 0, 0, 0));
            int wi = icon.getIconWidth();
            int he = icon.getIconHeight();
            if (wi <= 0 || he <= 0) {
            } else {
                BufferedImage image = new BufferedImage(wi, he, BufferedImage.TYPE_INT_ARGB);
//                System.out.println("Image Exception: "+newid);
                Graphics2D g2 = image.createGraphics();
                //g2.setColor(Color.black);
                g2.fillRect(0, 0, icon.getIconWidth(), icon.getIconHeight());
                JLabel jl = new JLabel();
                jl.setForeground(new Color(0, 0, 0));
                icon.paintIcon(jl, g2, 0, 0);
                File outputfile = new File(imgQuestionFileName);
                ImageIO.write(image, "png", outputfile);
                returnValue = icon.getIconWidth() + "X" + icon.getIconHeight();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnValue;
    }
    
    //Pattern Process
    public boolean insertNewAndOldQuestion(ArrayList<PatternOptionImageDimensionsBean> optionImageDimensionsList,ArrayList<ChangeIdReferenceBean> referenceList) {
        boolean returnValue = false;
        int questionId;
        try {
            conn = new DbConnection().getConnection();
            String query = "INSERT INTO OPTION_IMAGE_DIMENSIONS VALUES(?,?,?,?,?)";
            ps = conn.prepareStatement(query);
            for(PatternOptionImageDimensionsBean optionImageDimensionsBean : optionImageDimensionsList) {
                questionId = 0;
                for(ChangeIdReferenceBean idReferenceBean : referenceList) {
                    if(idReferenceBean.getOldId() == optionImageDimensionsBean.getQuestionId()) {
                        questionId = idReferenceBean.getNewId();
                    }
                }
                ps.setInt(1, questionId);
                ps.setString(2, optionImageDimensionsBean.getOptionA());
                ps.setString(3, optionImageDimensionsBean.getOptionB());
                ps.setString(4, optionImageDimensionsBean.getOptionC());
                ps.setString(5, optionImageDimensionsBean.getOptionD());
                ps.executeUpdate();
            }
            returnValue = true;
        } catch (SQLException ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnValue;
    }
    
    private void sqlClose() {
        try {
            if(rs != null)
                rs.close();
            if(ps != null)
                ps.close();
            if(conn != null)
                conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
