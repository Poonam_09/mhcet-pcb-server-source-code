/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.MasterSubjectBean;
import com.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class MasterSubjectOperation {
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    
    public ArrayList<MasterSubjectBean> getSubjectList() {
        ArrayList<MasterSubjectBean> returnList = null;
        conn = new DbConnection().getConnection();
        try {
            String query = "SELECT * FROM MASTER_SUBJECT_INFO";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            MasterSubjectBean subjectBean = null;
            while(rs.next()) {
                subjectBean = new MasterSubjectBean();
                subjectBean.setSubjectId(rs.getInt(1));
                subjectBean.setSubjectName(rs.getString(2));
                
                if(returnList == null)
                    returnList = new ArrayList<MasterSubjectBean>();
                
                returnList.add(subjectBean);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnList = null;
        } finally {
            sqlClose();
        }    
        return returnList;
    }
    private void sqlClose() {
        try {
            if(rs != null)
                rs.close();
            if(ps != null)
                ps.close();
            if(conn != null)
                conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
