/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.GroupBean;
import com.bean.SubMasterChapterBean;
import com.bean.MasterSubjectBean;
import com.db.DbConnection;
import com.id.operations.NewIdOperation;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JOptionPane;

/**
 *
 * @author Aniket
 */
public class SubMasterChapterOperation {
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
     ArrayList<SubMasterChapterBean>SubMasterchapterList;
    private ArrayList<SubMasterChapterBean> updatedChapterList;
    
    public ArrayList<Object[]> getGroupList1(int groupId,int subjectId)
     {  
          int SrNo = 1;
        ArrayList<Object[]> group = new ArrayList<Object[]>();
        conn = new DbConnection().getConnection();
       
        try {
            String query = "SELECT * FROM SUB_MASTER_CHAPTER_INFO WHERE GROUP_ID=? AND SUBJECT_ID=?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, groupId);
             ps.setInt(2, subjectId);
            rs = ps.executeQuery();
             while(rs.next()) {
                
                group.add(new Object[]{SrNo++,rs.getString(2),rs.getInt(4)});
            }
           return group;
        } catch(Exception ex) {
           
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return null;
     }
public ArrayList<SubMasterChapterBean> getSubMasterChapterList1(int currentPattern) {
        ArrayList<SubMasterChapterBean> returnList = null;
        ArrayList<MasterSubjectBean> subjectList = new MasterSubjectOperation().getSubjectList();
        ArrayList<GroupBean> groupList = new MasterGroupOperation().getGroupList();
        conn = new DbConnection().getConnection();
        
        try {
            String query = "SELECT * FROM SUB_MASTER_CHAPTER_INFO WHERE GROUP_ID= " +currentPattern;
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            SubMasterChapterBean subMasterChapterBean = null;
            while(rs.next()) {
                subMasterChapterBean = new SubMasterChapterBean();
                subMasterChapterBean.setChapterId(rs.getInt(1));
                subMasterChapterBean.setChapterName(rs.getString(2));
                for(MasterSubjectBean bean : subjectList) {
                    if(bean.getSubjectId() == rs.getInt(3)) {
                        subMasterChapterBean.setSubjectBean(bean);
                        break;
                    }
                }
                subMasterChapterBean.setWeightage(rs.getInt(4));
                subMasterChapterBean.setChapterIds(rs.getString(5));
                for(GroupBean bean : groupList) {
                    if(bean.getGroupId() == rs.getInt(6)) {
                        subMasterChapterBean.setGroupBean(bean);
                    }
                }
                
                if(returnList == null)
                    returnList = new ArrayList<SubMasterChapterBean>();
                
                returnList.add(subMasterChapterBean);
            }
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            returnList = null;
        }
        finally {
            sqlClose();
        }    
         return returnList;
    }
public ArrayList<SubMasterChapterBean> getSubMasterChapterList(GroupBean groupBean) {
        ArrayList<SubMasterChapterBean> returnList = null;
        ArrayList<MasterSubjectBean> subjectList = new MasterSubjectOperation().getSubjectList();
        ArrayList<GroupBean> groupList = new MasterGroupOperation().getGroupList();
        conn = new DbConnection().getConnection();
        
        try {
            String query = "SELECT * FROM SUB_MASTER_CHAPTER_INFO WHERE GROUP_ID="+groupBean.getGroupId();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            SubMasterChapterBean subMasterChapterBean = null;
            while(rs.next()) {
                
                subMasterChapterBean = new SubMasterChapterBean();
           
                subMasterChapterBean.setChapterId(rs.getInt(1));
            
                subMasterChapterBean.setChapterName(rs.getString(2));
                for(MasterSubjectBean bean : subjectList) {
                    if(bean.getSubjectId() == rs.getInt(3)) {
                        subMasterChapterBean.setSubjectBean(bean);
                        break;
                    }
                }
                subMasterChapterBean.setWeightage(rs.getInt(4));
                subMasterChapterBean.setChapterIds(rs.getString(5));
                for(GroupBean bean : groupList) 
                {
                    if(bean.getGroupId() == rs.getInt(6)) 
                    {
                        subMasterChapterBean.setGroupBean(bean);
                        break;
                    }
                }
                
                if(returnList == null)
                    returnList = new ArrayList<SubMasterChapterBean>();
                
                returnList.add(subMasterChapterBean);
            }
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            returnList = null;
        }
        finally {
            sqlClose();
        }    
         return returnList;
    }
    public ArrayList<SubMasterChapterBean> getSubMasterChapterList() {
        ArrayList<SubMasterChapterBean> returnList = null;
        ArrayList<MasterSubjectBean> subjectList = new MasterSubjectOperation().getSubjectList();
        ArrayList<GroupBean> groupList = new MasterGroupOperation().getGroupList();
        conn = new DbConnection().getConnection();
        try {
            String query = "SELECT * FROM SUB_MASTER_CHAPTER_INFO";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            SubMasterChapterBean subMasterChapterBean = null;
            while(rs.next()) {
                subMasterChapterBean = new SubMasterChapterBean();
                subMasterChapterBean.setChapterId(rs.getInt(1));
                subMasterChapterBean.setChapterName(rs.getString(2));
                for(MasterSubjectBean bean : subjectList) {
                    if(bean.getSubjectId() == rs.getInt(3)) {
                        subMasterChapterBean.setSubjectBean(bean);
                        break;
                    }
                }
                subMasterChapterBean.setWeightage(rs.getInt(4));
                subMasterChapterBean.setChapterIds(rs.getString(5));
                for(GroupBean bean : groupList) {
                    if(bean.getGroupId() == rs.getInt(6)) {
                        subMasterChapterBean.setGroupBean(bean);
                    }
                }
                
                if(returnList == null)
                    returnList = new ArrayList<SubMasterChapterBean>();
                
                returnList.add(subMasterChapterBean);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnList = null;
        } finally {
            sqlClose();
        }    
         return returnList;
    }
    public ArrayList<SubMasterChapterBean> getSubMasterChapterList1(int currentPattern,int subjectId) {
        ArrayList<SubMasterChapterBean> returnList = null;
        ArrayList<MasterSubjectBean> subjectList = new MasterSubjectOperation().getSubjectList();
        ArrayList<GroupBean> groupList = new MasterGroupOperation().getGroupList();
        conn = new DbConnection().getConnection();
        
        try {
            String query = "SELECT * FROM SUB_MASTER_CHAPTER_INFO WHERE GROUP_ID= " +currentPattern;
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            SubMasterChapterBean subMasterChapterBean = null;
            while(rs.next()) {
                subMasterChapterBean = new SubMasterChapterBean();
                subMasterChapterBean.setChapterId(rs.getInt(1));
                subMasterChapterBean.setChapterName(rs.getString(2));
                for(MasterSubjectBean bean : subjectList) {
                    if(bean.getSubjectId() == rs.getInt(3)) {
                        subMasterChapterBean.setSubjectBean(bean);
                        break;
                    }
                }
                subMasterChapterBean.setWeightage(rs.getInt(4));
                subMasterChapterBean.setChapterIds(rs.getString(5));
                for(GroupBean bean : groupList) {
                    if(bean.getGroupId() == rs.getInt(6)) {
                        subMasterChapterBean.setGroupBean(bean);
                    }
                }
                
                if(returnList == null)
                    returnList = new ArrayList<SubMasterChapterBean>();
                
                returnList.add(subMasterChapterBean);
            }
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            returnList = null;
        }
        finally {
            sqlClose();
        }    
         return returnList;
    }
    
    public void updateChapterWeight(int subjectId,Vector rows, ArrayList<SubMasterChapterBean>SubMasterchapterList,int currentPatternIndex) {
          updatedChapterList=null;
        boolean returnValue = false;
         if (rows != null) {
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE SUB_MASTER_CHAPTER_INFO SET WEIGTHAGE =? WHERE CHAPTER_NAME =? AND GROUP_ID="+currentPatternIndex+"AND SUBJECT_ID="+subjectId;
                ps = conn.prepareStatement(query);
                     
                  int size=rows.size();
           for (int i = 0; i < size; i++) 
            {  
              Vector row = (Vector) rows.elementAt(i);
                   SubMasterChapterBean scb=null;
                    scb = new SubMasterChapterBean();
                    for(SubMasterChapterBean chapterBean : SubMasterchapterList) 
                    { 
                      if(chapterBean.getSubjectBean().getSubjectId()==subjectId)
                      {
                       if(chapterBean.getChapterName().equals(row.elementAt(1).toString())) 
                         {
                            if(chapterBean.getWeightage() != (Integer) row.elementAt(2)) 
                               {
                                   scb.setChapterId(chapterBean.getChapterId());
                                   scb.setChapterName(chapterBean.getChapterName().trim());
                                   scb.setWeightage((Integer) row.elementAt(2));
                                
                                   ps.setInt(1,(Integer) row.elementAt(2));
                                   ps.setString(2,row.elementAt(1).toString());
                                   ps.executeUpdate();
                                   if(updatedChapterList == null)
                                       updatedChapterList = new ArrayList<SubMasterChapterBean>();
                                       updatedChapterList.add(scb);
                                       returnValue = true;
                               }
                         }
                      }
                    }
            }    
            
            if(returnValue==true)
            {
                JOptionPane.showMessageDialog(null, "Settings Updated...!!!");
                new ChapterOperation().updateChapterWeightage(rows,SubMasterchapterList,currentPatternIndex);
            }
        } catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
      
      }
        
    }
    
    public boolean insertSubMasterChapter(SubMasterChapterBean subMasterChapterBean) {
        boolean returnValue = false;
        int chapterId = new NewIdOperation().getNewId("SUB_MASTER_CHAPTER_INFO");
        conn = new DbConnection().getConnection();
        try {
            String query = "INSERT INTO SUB_MASTER_CHAPTER_INFO VALUES(?,?,?,?,?,?)";
            ps = conn.prepareStatement(query);
            ps.setInt(1, chapterId);
            ps.setString(2, subMasterChapterBean.getChapterName());
            ps.setInt(3, subMasterChapterBean.getSubjectBean().getSubjectId());
            ps.setInt(4, subMasterChapterBean.getWeightage());
            ps.setString(5, subMasterChapterBean.getChapterIds());
            ps.setInt(6, subMasterChapterBean.getGroupBean().getGroupId());
            ps.executeUpdate();
            returnValue = true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnValue = false;
        } finally {
            sqlClose();
        }    
        return returnValue;
    }
    
    private void sqlClose() {
        try {
            if(rs != null)
                rs.close();
            if(ps != null)
                ps.close();
            if(conn != null)
                conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
