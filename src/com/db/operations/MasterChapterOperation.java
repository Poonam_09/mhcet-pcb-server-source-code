/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.MasterBookBean;
import com.bean.MasterChapterBean;
import com.bean.MasterChapterCountBean;
import com.bean.MasterSubjectBean;
import com.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class MasterChapterOperation {
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    
    public ArrayList<MasterChapterBean> getMasterChapterList() {
        ArrayList<MasterChapterBean> returnList = null;
        ArrayList<MasterSubjectBean> subjectList = new MasterSubjectOperation().getSubjectList();
        ArrayList<MasterBookBean> bookList = new MasterBookOperation().getBookList();
        conn = new DbConnection().getConnection();
        try {
            String query = "SELECT * FROM MASTER_CHAPTER_INFO";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            MasterChapterBean masterChapterBean = null;
            while(rs.next()) {
                masterChapterBean = new MasterChapterBean();
                masterChapterBean.setChapterId(rs.getInt(1));
                for(MasterSubjectBean bean : subjectList) {
                    if(bean.getSubjectId() == rs.getInt(2)) {
                        masterChapterBean.setSubjectBean(bean);
                        break;
                    }
                }
                masterChapterBean.setChapterName(rs.getString(3));
                for(MasterBookBean bean : bookList) {
                    if(bean.getBookId() == rs.getInt(4)) {
                        masterChapterBean.setBookBean(bean);
                        break;
                    }
                }
                if(returnList == null)
                    returnList = new ArrayList<MasterChapterBean>();
                
                returnList.add(masterChapterBean);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnList = null;
        } finally {
            sqlClose();
        }    
         return returnList;
    }
    
    public boolean insertMasterChapter(ArrayList<MasterChapterBean> masterChapterList) {
        boolean returnValue = false;
        
        conn = new DbConnection().getConnection();
        try {
            String query = "INSERT INTO MASTER_CHAPTER_INFO VALUES(?,?,?,?)";
            ps = conn.prepareStatement(query);
            for(MasterChapterBean masterChapterBean : masterChapterList) {
                ps.setInt(1, masterChapterBean.getChapterId());
                ps.setInt(2, masterChapterBean.getSubjectBean().getSubjectId());
                ps.setString(3, masterChapterBean.getChapterName());
                ps.setInt(4, masterChapterBean.getBookBean().getBookId());
                ps.executeUpdate();
            }
            returnValue = true;
        } catch (SQLException ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }    
        return returnValue;
    }
    
    public boolean updateMasterChapter(MasterChapterBean masterChapterBean) {
        boolean returnValue = false;
        conn = new DbConnection().getConnection();
        try {
            String query = "UPDATE MASTER_CHAPTER_INFO SET SUBJECT_ID = ?,CHAPTER_NAME = ?,BOOK_ID = ? WHERE CHAPTER_ID = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, masterChapterBean.getSubjectBean().getSubjectId());
            ps.setString(2, masterChapterBean.getChapterName());
            ps.setInt(3, masterChapterBean.getBookBean().getBookId());
            ps.setInt(4, masterChapterBean.getChapterId());
            ps.executeUpdate();
            returnValue = true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnValue = false;
        } finally {
            sqlClose();
        }    
        return returnValue;
    }
    
    public ArrayList<MasterChapterCountBean> getMasterQuesCountList(){
        ArrayList<MasterChapterCountBean> returnList = null;
        conn = new DbConnection().getConnection();
        MasterChapterCountBean masterChapterCountBean = null;
        try {
            String query = "SELECT CHAPTER_ID,COUNT(*) AS TOTAL_QUES FROM MASTER_QUESTION_INFO GROUP BY CHAPTER_ID";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while(rs.next()) {
                masterChapterCountBean = new MasterChapterCountBean();
                masterChapterCountBean.setChapterId(rs.getInt(1));
                masterChapterCountBean.setTotalQuetions(rs.getInt(2));
                if(returnList == null)
                    returnList = new ArrayList<MasterChapterCountBean>();
                returnList.add(masterChapterCountBean);
            }
        } catch (SQLException ex) {
        ex.printStackTrace();
        returnList = null;
        } finally {
            sqlClose();
        } 
        return returnList;
    }
    
    private void sqlClose() {
        try {
            if(rs != null)
                rs.close();
            if(ps != null)
                ps.close();
            if(conn != null)
                conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
