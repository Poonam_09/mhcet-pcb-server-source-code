/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.Pdf.Generation;

import com.Model.ProcessManager;
import com.Model.Utilities;
import com.bean.ChapterBean;
import com.bean.PdfFirstFormatBean;
import com.bean.PdfPageSetupBean;
import com.bean.QuestionBean;
import com.bean.SubjectBean;
import com.pages.HomePage;
import com.pages.MultipleChapterQuestionsSelection;
import com.pages.MultipleYearQuestionsSelection;
import com.pages.SingleChapterQuestionsSelection;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.ImageIcon;
import com.Model.TitleInfo;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Sagar
 */
public class PdfFormatFirst extends javax.swing.JFrame {

    
    private ArrayList<QuestionBean> selectedQuestionList;
    private ArrayList<QuestionBean> questionsList;
    private ArrayList<ChapterBean> chaptersList;
    private PdfPageSetupBean pdfPageSetupBean;
    private ArrayList<SubjectBean> subjectList;
    private int subFirstQuesCount,subSecondQuesCount,subThirdQuesCount;
    private PdfFirstFormatBean pdfFirstFormatBean;
    private String printingPaperType;
    private int totalMarks;
    private PdfPageSetup pdfPageSetup;
    private String groupName;
    private Object quesPageObject;

    /**
     * Creates new form PdfFormatFirst
     */
    public PdfFormatFirst() {
        initComponents();
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        setLocation(0, 0);
        setLocationRelativeTo(null);
        this.getContentPane().setBackground(new Color(204, 204, 204));
    }


    
    //Aniket
    public PdfFormatFirst(ArrayList<QuestionBean> selectedQuestionList,ArrayList<SubjectBean> selectedSubjectList,ArrayList<ChapterBean> chaptersList,PdfPageSetupBean pageSetupBean,ArrayList<QuestionBean> questionsList,String printingPaperType,Object quesPageObject,PdfPageSetup pdfPageSetup) {
        initComponents();
        setLocation(0, 0);
        setLocationRelativeTo(null);
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        this.selectedQuestionList = selectedQuestionList;
        this.questionsList = questionsList;
        this.chaptersList = chaptersList;
        this.pdfPageSetupBean = pageSetupBean;
        subjectList = selectedSubjectList;
        this.printingPaperType = printingPaperType;
//        this.quesFrame = quesFrame;
        this.quesPageObject = quesPageObject;
        totalMarks = 0;
        pdfFirstFormatBean = null;
        this.pdfPageSetup = pdfPageSetup;
        groupName = new ProcessManager().getGroupName().trim();
        setDefaultSettings();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        HeaderPanel = new javax.swing.JPanel();
        LblHeader = new javax.swing.JLabel();
        LeftBodyPanel = new javax.swing.JPanel();
        ChkTotalQuestion = new javax.swing.JCheckBox();
        ChkDivision = new javax.swing.JCheckBox();
        CmbDivision = new javax.swing.JComboBox();
        ChkPaperName = new javax.swing.JCheckBox();
        CmbPaperName = new javax.swing.JComboBox();
        ChkDepartment = new javax.swing.JCheckBox();
        CmbDepartment = new javax.swing.JComboBox();
        ChkSubject = new javax.swing.JCheckBox();
        jPanel1 = new javax.swing.JPanel();
        CmbSubject = new javax.swing.JComboBox();
        MiddleBodyPanel = new javax.swing.JPanel();
        LblPaperImage = new javax.swing.JLabel();
        FooterPanel = new javax.swing.JPanel();
        BtnBack = new javax.swing.JButton();
        BtnNext = new javax.swing.JButton();
        RightBodyPanel = new javax.swing.JPanel();
        ChkTotalMarks = new javax.swing.JCheckBox();
        FirstSubTotalPanel = new javax.swing.JPanel();
        LblFirstSubjectTotalMsg = new javax.swing.JLabel();
        LblFirstMultiply = new javax.swing.JLabel();
        CmbFirstSubjectTotal = new javax.swing.JComboBox();
        LblFirstEqual = new javax.swing.JLabel();
        TxtFirstSubTotal = new javax.swing.JTextField();
        LblSubFirstTotal = new javax.swing.JLabel();
        ChkDate = new javax.swing.JCheckBox();
        DatePicker = new org.jdesktop.swingx.JXDatePicker();
        ChkTime = new javax.swing.JCheckBox();
        CmbStartHour = new javax.swing.JComboBox();
        LblFirstColon = new javax.swing.JLabel();
        CmbStartMinute = new javax.swing.JComboBox();
        LblTo = new javax.swing.JLabel();
        CmbEndHour = new javax.swing.JComboBox();
        LblSecondColon = new javax.swing.JLabel();
        CmbEndMinute = new javax.swing.JComboBox();
        CmbStartAM = new javax.swing.JComboBox<>();
        CmbEndAM = new javax.swing.JComboBox<>();
        SecondSubTotalPanel = new javax.swing.JPanel();
        LblSecondSubjectTotalMsg = new javax.swing.JLabel();
        LblSecondMultiply = new javax.swing.JLabel();
        CmbSecondSubjectTotal = new javax.swing.JComboBox();
        LblSecondEqual = new javax.swing.JLabel();
        TxtSecondSubTotal = new javax.swing.JTextField();
        LblSubSecondTotal = new javax.swing.JLabel();
        ThirdSubTotalPanel = new javax.swing.JPanel();
        LblThirdSubjectTotalMsg = new javax.swing.JLabel();
        LblThirdMultiply = new javax.swing.JLabel();
        CmbThirdSubjectTotal = new javax.swing.JComboBox();
        LblThirdEqual = new javax.swing.JLabel();
        TxtThirdSubTotal = new javax.swing.JTextField();
        LblSubThirdTotal = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setAlwaysOnTop(true);
        setBackground(new java.awt.Color(255, 255, 255));
        setFocusCycleRoot(false);
        setForeground(new java.awt.Color(255, 255, 255));
        setResizable(false);
        setType(java.awt.Window.Type.UTILITY);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        HeaderPanel.setBackground(new java.awt.Color(11, 45, 55));
        HeaderPanel.setName("HeaderPanel"); // NOI18N

        LblHeader.setBackground(new java.awt.Color(11, 45, 55));
        LblHeader.setFont(new java.awt.Font("Calibri", 0, 32)); // NOI18N
        LblHeader.setForeground(new java.awt.Color(255, 255, 255));
        LblHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblHeader.setText("Customize First Page");
        LblHeader.setName("LblHeader"); // NOI18N

        javax.swing.GroupLayout HeaderPanelLayout = new javax.swing.GroupLayout(HeaderPanel);
        HeaderPanel.setLayout(HeaderPanelLayout);
        HeaderPanelLayout.setHorizontalGroup(
            HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(LblHeader, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        HeaderPanelLayout.setVerticalGroup(
            HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, HeaderPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(LblHeader, javax.swing.GroupLayout.DEFAULT_SIZE, 54, Short.MAX_VALUE)
                .addContainerGap())
        );

        LeftBodyPanel.setBackground(new java.awt.Color(11, 45, 55));
        LeftBodyPanel.setForeground(new java.awt.Color(255, 255, 255));
        LeftBodyPanel.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        LeftBodyPanel.setName("LeftBodyPanel"); // NOI18N

        ChkTotalQuestion.setBackground(new java.awt.Color(11, 45, 55));
        ChkTotalQuestion.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        ChkTotalQuestion.setForeground(new java.awt.Color(255, 255, 255));
        ChkTotalQuestion.setSelected(true);
        ChkTotalQuestion.setText("Show Total Questions");
        ChkTotalQuestion.setName("ChkTotalQuestion"); // NOI18N
        ChkTotalQuestion.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkTotalQuestionItemStateChanged(evt);
            }
        });

        ChkDivision.setBackground(new java.awt.Color(11, 45, 55));
        ChkDivision.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        ChkDivision.setForeground(new java.awt.Color(255, 255, 255));
        ChkDivision.setSelected(true);
        ChkDivision.setText("Show Division");
        ChkDivision.setName("ChkDivision"); // NOI18N
        ChkDivision.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkDivisionItemStateChanged(evt);
            }
        });

        CmbDivision.setEditable(true);
        CmbDivision.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        CmbDivision.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "XII - A Div", "XII - B Div", "XII - C Div", "XII - D Div", "XII - E Div", "XII - F Div", "XII - G Div", "XII - H Div" }));
        CmbDivision.setName("CmbDivision"); // NOI18N

        ChkPaperName.setBackground(new java.awt.Color(11, 45, 55));
        ChkPaperName.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        ChkPaperName.setForeground(new java.awt.Color(255, 255, 255));
        ChkPaperName.setSelected(true);
        ChkPaperName.setText("Show Paper Name");
        ChkPaperName.setName("ChkPaperName"); // NOI18N
        ChkPaperName.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkPaperNameItemStateChanged(evt);
            }
        });

        CmbPaperName.setEditable(true);
        CmbPaperName.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        CmbPaperName.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "MH CET", "JEE", "NEET", "Medical CET", "JEE MAIN", "JEE Advance" }));
        CmbPaperName.setName("CmbPaperName"); // NOI18N

        ChkDepartment.setBackground(new java.awt.Color(11, 45, 55));
        ChkDepartment.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        ChkDepartment.setForeground(new java.awt.Color(255, 255, 255));
        ChkDepartment.setSelected(true);
        ChkDepartment.setText("Show Department Name");
        ChkDepartment.setName("ChkDepartment"); // NOI18N
        ChkDepartment.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkDepartmentItemStateChanged(evt);
            }
        });

        CmbDepartment.setEditable(true);
        CmbDepartment.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        CmbDepartment.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "CET CELL", "Physics Department", "Chemistry Department", "Mathematics Department", "Biology Department" }));
        CmbDepartment.setName("CmbDepartment"); // NOI18N

        ChkSubject.setBackground(new java.awt.Color(11, 45, 55));
        ChkSubject.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        ChkSubject.setForeground(new java.awt.Color(255, 255, 255));
        ChkSubject.setSelected(true);
        ChkSubject.setText("Show Subject Name");
        ChkSubject.setName("ChkSubject"); // NOI18N
        ChkSubject.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkSubjectItemStateChanged(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(11, 45, 55));
        jPanel1.setName("jPanel1"); // NOI18N

        CmbSubject.setEditable(true);
        CmbSubject.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        CmbSubject.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Physics", "Chemistry", "Mathematics", "PCM", "Physics I", "Physics II", "Mathematics I", "Mathematics II", "Chemistry I", "Chemistry II" }));
        CmbSubject.setAutoscrolls(true);
        CmbSubject.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        CmbSubject.setFocusCycleRoot(true);
        CmbSubject.setName("CmbSubject"); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(CmbSubject, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(CmbSubject, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5))
        );

        javax.swing.GroupLayout LeftBodyPanelLayout = new javax.swing.GroupLayout(LeftBodyPanel);
        LeftBodyPanel.setLayout(LeftBodyPanelLayout);
        LeftBodyPanelLayout.setHorizontalGroup(
            LeftBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LeftBodyPanelLayout.createSequentialGroup()
                .addContainerGap(10, Short.MAX_VALUE)
                .addGroup(LeftBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, LeftBodyPanelLayout.createSequentialGroup()
                        .addGap(0, 33, Short.MAX_VALUE)
                        .addGroup(LeftBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(CmbPaperName, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(CmbDepartment, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(CmbDivision, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31))
                    .addGroup(LeftBodyPanelLayout.createSequentialGroup()
                        .addGroup(LeftBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ChkDepartment, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ChkPaperName, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addGroup(LeftBodyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(LeftBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ChkDivision)
                    .addComponent(ChkSubject)
                    .addComponent(ChkTotalQuestion))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, LeftBodyPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );
        LeftBodyPanelLayout.setVerticalGroup(
            LeftBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LeftBodyPanelLayout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(ChkSubject)
                .addGap(7, 7, 7)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(ChkTotalQuestion)
                .addGap(15, 15, 15)
                .addComponent(ChkDivision, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(CmbDivision, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(ChkPaperName, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(CmbPaperName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addComponent(ChkDepartment, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(CmbDepartment, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(173, 173, 173))
        );

        MiddleBodyPanel.setBackground(new java.awt.Color(255, 255, 255));
        MiddleBodyPanel.setName("MiddleBodyPanel"); // NOI18N

        LblPaperImage.setBackground(new java.awt.Color(255, 255, 255));
        LblPaperImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/FormatFirstSingle.png"))); // NOI18N
        LblPaperImage.setName("LblPaperImage"); // NOI18N

        javax.swing.GroupLayout MiddleBodyPanelLayout = new javax.swing.GroupLayout(MiddleBodyPanel);
        MiddleBodyPanel.setLayout(MiddleBodyPanelLayout);
        MiddleBodyPanelLayout.setHorizontalGroup(
            MiddleBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(LblPaperImage, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 531, Short.MAX_VALUE)
        );
        MiddleBodyPanelLayout.setVerticalGroup(
            MiddleBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(LblPaperImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        FooterPanel.setBackground(new java.awt.Color(11, 45, 55));
        FooterPanel.setName("FooterPanel"); // NOI18N

        BtnBack.setBackground(new java.awt.Color(208, 87, 96));
        BtnBack.setFont(new java.awt.Font("Calibri", 0, 18)); // NOI18N
        BtnBack.setForeground(new java.awt.Color(255, 255, 255));
        BtnBack.setText("Back");
        BtnBack.setName("BtnBack"); // NOI18N
        BtnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBackActionPerformed(evt);
            }
        });

        BtnNext.setBackground(new java.awt.Color(208, 87, 96));
        BtnNext.setFont(new java.awt.Font("Calibri", 0, 18)); // NOI18N
        BtnNext.setForeground(new java.awt.Color(255, 255, 255));
        BtnNext.setText("Next");
        BtnNext.setName("BtnNext"); // NOI18N
        BtnNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnNextActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout FooterPanelLayout = new javax.swing.GroupLayout(FooterPanel);
        FooterPanel.setLayout(FooterPanelLayout);
        FooterPanelLayout.setHorizontalGroup(
            FooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FooterPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(BtnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(428, 428, 428)
                .addComponent(BtnNext, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(191, 191, 191))
        );
        FooterPanelLayout.setVerticalGroup(
            FooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FooterPanelLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(FooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BtnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BtnNext, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        RightBodyPanel.setBackground(new java.awt.Color(11, 45, 55));
        RightBodyPanel.setName("RightBodyPanel"); // NOI18N

        ChkTotalMarks.setBackground(new java.awt.Color(11, 45, 55));
        ChkTotalMarks.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        ChkTotalMarks.setForeground(new java.awt.Color(255, 255, 255));
        ChkTotalMarks.setSelected(true);
        ChkTotalMarks.setText("Show Total Marks");
        ChkTotalMarks.setName("ChkTotalMarks"); // NOI18N
        ChkTotalMarks.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkTotalMarksItemStateChanged(evt);
            }
        });

        FirstSubTotalPanel.setBackground(new java.awt.Color(11, 45, 55));
        FirstSubTotalPanel.setName("FirstSubTotalPanel"); // NOI18N

        LblFirstSubjectTotalMsg.setBackground(new java.awt.Color(255, 255, 255));
        LblFirstSubjectTotalMsg.setFont(new java.awt.Font("Verdana", 1, 13)); // NOI18N
        LblFirstSubjectTotalMsg.setForeground(new java.awt.Color(255, 255, 255));
        LblFirstSubjectTotalMsg.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LblFirstSubjectTotalMsg.setText("Physics");
        LblFirstSubjectTotalMsg.setName("LblFirstSubjectTotalMsg"); // NOI18N
        LblFirstSubjectTotalMsg.setPreferredSize(new java.awt.Dimension(94, 24));

        LblFirstMultiply.setBackground(new java.awt.Color(255, 255, 255));
        LblFirstMultiply.setFont(new java.awt.Font("Verdana", 1, 13)); // NOI18N
        LblFirstMultiply.setForeground(new java.awt.Color(255, 255, 255));
        LblFirstMultiply.setText("*");
        LblFirstMultiply.setName("LblFirstMultiply"); // NOI18N

        CmbFirstSubjectTotal.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        CmbFirstSubjectTotal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
        CmbFirstSubjectTotal.setToolTipText("Marks Per Question");
        CmbFirstSubjectTotal.setName("CmbFirstSubjectTotal"); // NOI18N
        CmbFirstSubjectTotal.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmbFirstSubjectTotalItemStateChanged(evt);
            }
        });

        LblFirstEqual.setBackground(new java.awt.Color(255, 255, 255));
        LblFirstEqual.setFont(new java.awt.Font("Verdana", 1, 13)); // NOI18N
        LblFirstEqual.setForeground(new java.awt.Color(255, 255, 255));
        LblFirstEqual.setText("=");
        LblFirstEqual.setName("LblFirstEqual"); // NOI18N

        TxtFirstSubTotal.setEditable(false);
        TxtFirstSubTotal.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        TxtFirstSubTotal.setName("TxtFirstSubTotal"); // NOI18N

        LblSubFirstTotal.setFont(new java.awt.Font("Verdana", 1, 13)); // NOI18N
        LblSubFirstTotal.setForeground(new java.awt.Color(255, 255, 255));
        LblSubFirstTotal.setText("999");
        LblSubFirstTotal.setName("LblSubFirstTotal"); // NOI18N
        LblSubFirstTotal.setPreferredSize(new java.awt.Dimension(27, 27));

        javax.swing.GroupLayout FirstSubTotalPanelLayout = new javax.swing.GroupLayout(FirstSubTotalPanel);
        FirstSubTotalPanel.setLayout(FirstSubTotalPanelLayout);
        FirstSubTotalPanelLayout.setHorizontalGroup(
            FirstSubTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FirstSubTotalPanelLayout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(LblFirstSubjectTotalMsg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblSubFirstTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblFirstMultiply)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(CmbFirstSubjectTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblFirstEqual)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TxtFirstSubTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        FirstSubTotalPanelLayout.setVerticalGroup(
            FirstSubTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FirstSubTotalPanelLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addGroup(FirstSubTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblFirstMultiply, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CmbFirstSubjectTotal)
                    .addComponent(TxtFirstSubTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblFirstEqual, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblFirstSubjectTotalMsg, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblSubFirstTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4))
        );

        LblFirstSubjectTotalMsg.getAccessibleContext().setAccessibleName("");
        LblFirstSubjectTotalMsg.getAccessibleContext().setAccessibleDescription("");
        LblFirstMultiply.getAccessibleContext().setAccessibleName("");
        LblFirstMultiply.getAccessibleContext().setAccessibleDescription("");
        CmbFirstSubjectTotal.getAccessibleContext().setAccessibleName("");
        CmbFirstSubjectTotal.getAccessibleContext().setAccessibleDescription("");
        LblFirstEqual.getAccessibleContext().setAccessibleName("");
        LblFirstEqual.getAccessibleContext().setAccessibleDescription("");
        TxtFirstSubTotal.getAccessibleContext().setAccessibleName("");
        TxtFirstSubTotal.getAccessibleContext().setAccessibleDescription("");

        ChkDate.setBackground(new java.awt.Color(11, 45, 55));
        ChkDate.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        ChkDate.setForeground(new java.awt.Color(255, 255, 255));
        ChkDate.setSelected(true);
        ChkDate.setText("Show Date");
        ChkDate.setName("ChkDate"); // NOI18N

        DatePicker.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        DatePicker.setName("DatePicker"); // NOI18N
        DatePicker.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DatePickerActionPerformed(evt);
            }
        });

        ChkTime.setBackground(new java.awt.Color(11, 45, 55));
        ChkTime.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        ChkTime.setForeground(new java.awt.Color(255, 255, 255));
        ChkTime.setSelected(true);
        ChkTime.setText("Show Time");
        ChkTime.setName("ChkTime"); // NOI18N
        ChkTime.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkTimeItemStateChanged(evt);
            }
        });

        CmbStartHour.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        CmbStartHour.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" }));
        CmbStartHour.setName("CmbStartHour"); // NOI18N
        CmbStartHour.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmbStartHourItemStateChanged(evt);
            }
        });

        LblFirstColon.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        LblFirstColon.setForeground(new java.awt.Color(255, 255, 255));
        LblFirstColon.setText(":");
        LblFirstColon.setName("LblFirstColon"); // NOI18N

        CmbStartMinute.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        CmbStartMinute.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "05", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55" }));
        CmbStartMinute.setName("CmbStartMinute"); // NOI18N

        LblTo.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        LblTo.setForeground(new java.awt.Color(255, 255, 255));
        LblTo.setText("To");
        LblTo.setName("LblTo"); // NOI18N

        CmbEndHour.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        CmbEndHour.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" }));
        CmbEndHour.setName("CmbEndHour"); // NOI18N
        CmbEndHour.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmbEndHourItemStateChanged(evt);
            }
        });

        LblSecondColon.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        LblSecondColon.setForeground(new java.awt.Color(255, 255, 255));
        LblSecondColon.setText(":");
        LblSecondColon.setName("LblSecondColon"); // NOI18N

        CmbEndMinute.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        CmbEndMinute.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "05", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55" }));
        CmbEndMinute.setName("CmbEndMinute"); // NOI18N

        CmbStartAM.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 11)); // NOI18N
        CmbStartAM.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "AM", "PM" }));
        CmbStartAM.setName("CmbStartAM"); // NOI18N
        CmbStartAM.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmbStartAMItemStateChanged(evt);
            }
        });

        CmbEndAM.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 11)); // NOI18N
        CmbEndAM.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "AM", "PM" }));
        CmbEndAM.setName("CmbEndAM"); // NOI18N

        SecondSubTotalPanel.setBackground(new java.awt.Color(11, 45, 55));
        SecondSubTotalPanel.setName("SecondSubTotalPanel"); // NOI18N

        LblSecondSubjectTotalMsg.setBackground(new java.awt.Color(255, 255, 255));
        LblSecondSubjectTotalMsg.setFont(new java.awt.Font("Verdana", 1, 13)); // NOI18N
        LblSecondSubjectTotalMsg.setForeground(new java.awt.Color(255, 255, 255));
        LblSecondSubjectTotalMsg.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LblSecondSubjectTotalMsg.setText("Chemistry");
        LblSecondSubjectTotalMsg.setName("LblSecondSubjectTotalMsg"); // NOI18N
        LblSecondSubjectTotalMsg.setPreferredSize(new java.awt.Dimension(94, 24));

        LblSecondMultiply.setBackground(new java.awt.Color(255, 255, 255));
        LblSecondMultiply.setFont(new java.awt.Font("Verdana", 1, 13)); // NOI18N
        LblSecondMultiply.setForeground(new java.awt.Color(255, 255, 255));
        LblSecondMultiply.setText("*");
        LblSecondMultiply.setName("LblSecondMultiply"); // NOI18N

        CmbSecondSubjectTotal.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        CmbSecondSubjectTotal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
        CmbSecondSubjectTotal.setToolTipText("Marks Per Question");
        CmbSecondSubjectTotal.setName("CmbSecondSubjectTotal"); // NOI18N
        CmbSecondSubjectTotal.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmbSecondSubjectTotalItemStateChanged(evt);
            }
        });

        LblSecondEqual.setBackground(new java.awt.Color(255, 255, 255));
        LblSecondEqual.setFont(new java.awt.Font("Verdana", 1, 13)); // NOI18N
        LblSecondEqual.setForeground(new java.awt.Color(255, 255, 255));
        LblSecondEqual.setText("=");
        LblSecondEqual.setName("LblSecondEqual"); // NOI18N

        TxtSecondSubTotal.setEditable(false);
        TxtSecondSubTotal.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        TxtSecondSubTotal.setName("TxtSecondSubTotal"); // NOI18N

        LblSubSecondTotal.setFont(new java.awt.Font("Verdana", 1, 13)); // NOI18N
        LblSubSecondTotal.setForeground(new java.awt.Color(255, 255, 255));
        LblSubSecondTotal.setText("999");
        LblSubSecondTotal.setName("LblSubSecondTotal"); // NOI18N
        LblSubSecondTotal.setPreferredSize(new java.awt.Dimension(27, 27));

        javax.swing.GroupLayout SecondSubTotalPanelLayout = new javax.swing.GroupLayout(SecondSubTotalPanel);
        SecondSubTotalPanel.setLayout(SecondSubTotalPanelLayout);
        SecondSubTotalPanelLayout.setHorizontalGroup(
            SecondSubTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SecondSubTotalPanelLayout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(LblSecondSubjectTotalMsg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblSubSecondTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblSecondMultiply)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(CmbSecondSubjectTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblSecondEqual)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TxtSecondSubTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        SecondSubTotalPanelLayout.setVerticalGroup(
            SecondSubTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SecondSubTotalPanelLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addGroup(SecondSubTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblSecondMultiply, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CmbSecondSubjectTotal)
                    .addComponent(TxtSecondSubTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblSecondEqual, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblSecondSubjectTotalMsg, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblSubSecondTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4))
        );

        ThirdSubTotalPanel.setBackground(new java.awt.Color(11, 45, 55));
        ThirdSubTotalPanel.setName("ThirdSubTotalPanel"); // NOI18N

        LblThirdSubjectTotalMsg.setBackground(new java.awt.Color(255, 255, 255));
        LblThirdSubjectTotalMsg.setFont(new java.awt.Font("Verdana", 1, 13)); // NOI18N
        LblThirdSubjectTotalMsg.setForeground(new java.awt.Color(255, 255, 255));
        LblThirdSubjectTotalMsg.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LblThirdSubjectTotalMsg.setText("Mathematics");
        LblThirdSubjectTotalMsg.setName("LblThirdSubjectTotalMsg"); // NOI18N
        LblThirdSubjectTotalMsg.setPreferredSize(new java.awt.Dimension(94, 24));

        LblThirdMultiply.setBackground(new java.awt.Color(255, 255, 255));
        LblThirdMultiply.setFont(new java.awt.Font("Verdana", 1, 13)); // NOI18N
        LblThirdMultiply.setForeground(new java.awt.Color(255, 255, 255));
        LblThirdMultiply.setText("*");
        LblThirdMultiply.setName("LblThirdMultiply"); // NOI18N

        CmbThirdSubjectTotal.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        CmbThirdSubjectTotal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
        CmbThirdSubjectTotal.setToolTipText("Marks Per Question");
        CmbThirdSubjectTotal.setName("CmbThirdSubjectTotal"); // NOI18N
        CmbThirdSubjectTotal.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmbThirdSubjectTotalItemStateChanged(evt);
            }
        });

        LblThirdEqual.setBackground(new java.awt.Color(255, 255, 255));
        LblThirdEqual.setFont(new java.awt.Font("Verdana", 1, 13)); // NOI18N
        LblThirdEqual.setForeground(new java.awt.Color(255, 255, 255));
        LblThirdEqual.setText("=");
        LblThirdEqual.setName("LblThirdEqual"); // NOI18N

        TxtThirdSubTotal.setEditable(false);
        TxtThirdSubTotal.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        TxtThirdSubTotal.setName("TxtThirdSubTotal"); // NOI18N

        LblSubThirdTotal.setFont(new java.awt.Font("Verdana", 1, 13)); // NOI18N
        LblSubThirdTotal.setForeground(new java.awt.Color(255, 255, 255));
        LblSubThirdTotal.setText("999");
        LblSubThirdTotal.setName("LblSubThirdTotal"); // NOI18N
        LblSubThirdTotal.setPreferredSize(new java.awt.Dimension(27, 27));

        javax.swing.GroupLayout ThirdSubTotalPanelLayout = new javax.swing.GroupLayout(ThirdSubTotalPanel);
        ThirdSubTotalPanel.setLayout(ThirdSubTotalPanelLayout);
        ThirdSubTotalPanelLayout.setHorizontalGroup(
            ThirdSubTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ThirdSubTotalPanelLayout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(LblThirdSubjectTotalMsg, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblSubThirdTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblThirdMultiply)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(CmbThirdSubjectTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblThirdEqual)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TxtThirdSubTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        ThirdSubTotalPanelLayout.setVerticalGroup(
            ThirdSubTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ThirdSubTotalPanelLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addGroup(ThirdSubTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblThirdMultiply, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CmbThirdSubjectTotal)
                    .addComponent(TxtThirdSubTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblThirdEqual)
                    .addComponent(LblThirdSubjectTotalMsg, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblSubThirdTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4))
        );

        javax.swing.GroupLayout RightBodyPanelLayout = new javax.swing.GroupLayout(RightBodyPanel);
        RightBodyPanel.setLayout(RightBodyPanelLayout);
        RightBodyPanelLayout.setHorizontalGroup(
            RightBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(RightBodyPanelLayout.createSequentialGroup()
                .addGroup(RightBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(RightBodyPanelLayout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(CmbStartHour, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(7, 7, 7)
                        .addComponent(LblFirstColon)
                        .addGap(7, 7, 7)
                        .addComponent(CmbStartMinute, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(LblTo))
                    .addGroup(RightBodyPanelLayout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addComponent(CmbStartAM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(RightBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(RightBodyPanelLayout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addComponent(CmbEndHour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(7, 7, 7)
                        .addComponent(LblSecondColon)
                        .addGap(7, 7, 7)
                        .addComponent(CmbEndMinute, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, RightBodyPanelLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(CmbEndAM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29)))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(RightBodyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(RightBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(FirstSubTotalPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(RightBodyPanelLayout.createSequentialGroup()
                        .addGroup(RightBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ChkTotalMarks, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ChkDate)
                            .addComponent(ChkTime, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(SecondSubTotalPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ThirdSubTotalPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, RightBodyPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(DatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(107, 107, 107))
        );
        RightBodyPanelLayout.setVerticalGroup(
            RightBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, RightBodyPanelLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(ChkTotalMarks, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(FirstSubTotalPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(SecondSubTotalPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ThirdSubTotalPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(ChkDate, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(DatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(ChkTime, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(RightBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CmbStartHour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblFirstColon)
                    .addComponent(CmbStartMinute, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblTo)
                    .addComponent(CmbEndHour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblSecondColon)
                    .addComponent(CmbEndMinute, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(RightBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CmbStartAM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CmbEndAM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(FooterPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(LeftBodyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(3, 3, 3)
                        .addComponent(MiddleBodyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(3, 3, 3)
                        .addComponent(RightBodyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(HeaderPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(HeaderPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(LeftBodyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 448, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(MiddleBodyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(RightBodyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(10, 10, 10)
                .addComponent(FooterPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void DatePickerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DatePickerActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_DatePickerActionPerformed

    private void ChkTimeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkTimeItemStateChanged
        // TODO add your handling code here:
//        if(chktime.isSelected() == true)
//        {
//            cmbtime1.setEnabled(true);
//            cmbtime2.setEnabled(true);
//            cmbtime3.setEnabled(true);
//            cmbtime4.setEnabled(true);
//        }
//        else
//        {
//            cmbtime1.setEnabled(false);
//            cmbtime2.setEnabled(false);
//            cmbtime3.setEnabled(false);
//            cmbtime4.setEnabled(false);
//        }

//aniket//        ChkTime.setSelected(true);

    }//GEN-LAST:event_ChkTimeItemStateChanged

    private void ChkTotalMarksItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkTotalMarksItemStateChanged
        // TODO add your handling code here:
        if (printingPaperType.equalsIgnoreCase("YearWise") || printingPaperType.equalsIgnoreCase("GroupWise"))
            ChkTotalMarks.setSelected(true);

    }//GEN-LAST:event_ChkTotalMarksItemStateChanged

    private void BtnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBackActionPerformed
        // TODO add your handling code here:
        pdfPageSetup.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_BtnBackActionPerformed
    
    private void CmbFirstSubjectTotalItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmbFirstSubjectTotalItemStateChanged
        // TODO add your handling code here:
        if (evt.getStateChange() == java.awt.event.ItemEvent.SELECTED) 
            setTotalValues();     
    }//GEN-LAST:event_CmbFirstSubjectTotalItemStateChanged

    private void BtnNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnNextActionPerformed
        // TODO add your handling code here:
        updateSettings();
        if(pdfFirstFormatBean != null) {
            new PdfFinalPanel(pdfPageSetupBean, pdfFirstFormatBean,selectedQuestionList,subjectList,chaptersList,printingPaperType,totalMarks,questionsList,this,quesPageObject,pdfPageSetup).setVisible(true);
            this.setVisible(false);
        }
    }//GEN-LAST:event_BtnNextActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
    // TODO add your handling code here:
        Object[] options = {"YES", "CANCEL"};
        int i = JOptionPane.showOptionDialog(rootPane, "Are You Sure go to Home Page?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (i == 0) {
            if(SingleChapterQuestionsSelection.class.isInstance(quesPageObject)) {
                SingleChapterQuestionsSelection frm = (SingleChapterQuestionsSelection)quesPageObject;
                frm.dispose();
            } else if(MultipleChapterQuestionsSelection.class.isInstance(quesPageObject)) {
                MultipleChapterQuestionsSelection frm = (MultipleChapterQuestionsSelection)quesPageObject;
                frm.dispose();
            } else if(MultipleYearQuestionsSelection.class.isInstance(quesPageObject)) {
                MultipleYearQuestionsSelection frm = (MultipleYearQuestionsSelection)quesPageObject;
                frm.dispose();
            }
            pdfPageSetup.dispose();
            new HomePage().setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_formWindowClosing

    private void ChkDepartmentItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkDepartmentItemStateChanged
        // TODO add your handling code here:
//aniket//        ChkDepartment.setSelected(true);
    }//GEN-LAST:event_ChkDepartmentItemStateChanged

    private void ChkPaperNameItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkPaperNameItemStateChanged
        // TODO add your handling code here:
        //        if(chkpapername.isSelected()==true)
        //    {
            //        cmbpapername.setEnabled(true);
            //    }
        //    else
        //    {
            //        cmbpapername.setEnabled(false);
            //    }

//aniket//        ChkPaperName.setSelected(true);
    }//GEN-LAST:event_ChkPaperNameItemStateChanged

    private void ChkDivisionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkDivisionItemStateChanged
        // TODO add your handling code here:
        //        if(chkdivision.isSelected()==true)
        //        {
            //            cmbdivision.setEnabled(true);
            //        }
        //        else
        //        {
            //            cmbdivision.setEnabled(false);
            //        }

//aniket//        ChkDivision.setSelected(true);
    }//GEN-LAST:event_ChkDivisionItemStateChanged

    private void ChkTotalQuestionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkTotalQuestionItemStateChanged
        // TODO add your handling code here:
        if (printingPaperType.equalsIgnoreCase("YearWise") || printingPaperType.equalsIgnoreCase("GroupWise"))
            ChkTotalQuestion.setSelected(true);
    }//GEN-LAST:event_ChkTotalQuestionItemStateChanged

    private void CmbStartHourItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmbStartHourItemStateChanged
        // TODO add your handling code here:
        if(evt.getStateChange() == java.awt.event.ItemEvent.SELECTED) {
            CmbStartAM.removeAllItems();
            CmbEndAM.removeAllItems();
            if(CmbStartHour.getSelectedIndex() == 12) {
                CmbStartAM.addItem("PM");
                CmbEndAM.addItem("PM");
            } else if(CmbStartHour.getSelectedIndex() == 0) {
                CmbStartAM.addItem("AM");
                
                CmbEndAM.addItem("AM");
                CmbEndAM.addItem("PM");
            } else if(CmbStartHour.getSelectedIndex() != -1) {
                CmbStartAM.addItem("AM");
                CmbStartAM.addItem("PM");
                
                CmbEndAM.addItem("AM");
                CmbEndAM.addItem("PM");
            }
        }
    }//GEN-LAST:event_CmbStartHourItemStateChanged

    private void CmbEndHourItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmbEndHourItemStateChanged
        // TODO add your handling code here:
        CmbEndMinute.setSelectedIndex(0);
    }//GEN-LAST:event_CmbEndHourItemStateChanged

    private void CmbStartAMItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmbStartAMItemStateChanged
        // TODO add your handling code here:
        
    }//GEN-LAST:event_CmbStartAMItemStateChanged

    private void CmbSecondSubjectTotalItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmbSecondSubjectTotalItemStateChanged
        // TODO add your handling code here:
        if (evt.getStateChange() == java.awt.event.ItemEvent.SELECTED) 
            setTotalValues();
    }//GEN-LAST:event_CmbSecondSubjectTotalItemStateChanged

    private void CmbThirdSubjectTotalItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmbThirdSubjectTotalItemStateChanged
        // TODO add your handling code here:
        if (evt.getStateChange() == java.awt.event.ItemEvent.SELECTED) 
            setTotalValues();
    }//GEN-LAST:event_CmbThirdSubjectTotalItemStateChanged

    private void ChkSubjectItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkSubjectItemStateChanged
        // TODO add your handling code here:
        if (printingPaperType.equalsIgnoreCase("YearWise") || printingPaperType.equalsIgnoreCase("GroupWise"))
            ChkSubject.setSelected(true);
        
    }//GEN-LAST:event_ChkSubjectItemStateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(QuestFirstPageSettingA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(QuestFirstPageSettingA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(QuestFirstPageSettingA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(QuestFirstPageSettingA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>
        /* Create and display the form */
        
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnBack;
    private javax.swing.JButton BtnNext;
    private javax.swing.JCheckBox ChkDate;
    private javax.swing.JCheckBox ChkDepartment;
    private javax.swing.JCheckBox ChkDivision;
    private javax.swing.JCheckBox ChkPaperName;
    private javax.swing.JCheckBox ChkSubject;
    private javax.swing.JCheckBox ChkTime;
    private javax.swing.JCheckBox ChkTotalMarks;
    private javax.swing.JCheckBox ChkTotalQuestion;
    private javax.swing.JComboBox CmbDepartment;
    private javax.swing.JComboBox CmbDivision;
    private javax.swing.JComboBox<String> CmbEndAM;
    private javax.swing.JComboBox CmbEndHour;
    private javax.swing.JComboBox CmbEndMinute;
    private javax.swing.JComboBox CmbFirstSubjectTotal;
    private javax.swing.JComboBox CmbPaperName;
    private javax.swing.JComboBox CmbSecondSubjectTotal;
    private javax.swing.JComboBox<String> CmbStartAM;
    private javax.swing.JComboBox CmbStartHour;
    private javax.swing.JComboBox CmbStartMinute;
    private javax.swing.JComboBox CmbSubject;
    private javax.swing.JComboBox CmbThirdSubjectTotal;
    private org.jdesktop.swingx.JXDatePicker DatePicker;
    private javax.swing.JPanel FirstSubTotalPanel;
    private javax.swing.JPanel FooterPanel;
    private javax.swing.JPanel HeaderPanel;
    private javax.swing.JLabel LblFirstColon;
    private javax.swing.JLabel LblFirstEqual;
    private javax.swing.JLabel LblFirstMultiply;
    private javax.swing.JLabel LblFirstSubjectTotalMsg;
    private javax.swing.JLabel LblHeader;
    private javax.swing.JLabel LblPaperImage;
    private javax.swing.JLabel LblSecondColon;
    private javax.swing.JLabel LblSecondEqual;
    private javax.swing.JLabel LblSecondMultiply;
    private javax.swing.JLabel LblSecondSubjectTotalMsg;
    private javax.swing.JLabel LblSubFirstTotal;
    private javax.swing.JLabel LblSubSecondTotal;
    private javax.swing.JLabel LblSubThirdTotal;
    private javax.swing.JLabel LblThirdEqual;
    private javax.swing.JLabel LblThirdMultiply;
    private javax.swing.JLabel LblThirdSubjectTotalMsg;
    private javax.swing.JLabel LblTo;
    private javax.swing.JPanel LeftBodyPanel;
    private javax.swing.JPanel MiddleBodyPanel;
    private javax.swing.JPanel RightBodyPanel;
    private javax.swing.JPanel SecondSubTotalPanel;
    private javax.swing.JPanel ThirdSubTotalPanel;
    private javax.swing.JTextField TxtFirstSubTotal;
    private javax.swing.JTextField TxtSecondSubTotal;
    private javax.swing.JTextField TxtThirdSubTotal;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables

    //Aniket
    private void setDefaultSettings() {
        if(printingPaperType.equalsIgnoreCase("ChapterWise") || printingPaperType.equalsIgnoreCase("UnitWise") || printingPaperType.equalsIgnoreCase("SubjectWise")) {
            LblPaperImage.setIcon(new ImageIcon(getClass().getResource("/ui/images/FormatFirstSingle.png")));
            subFirstQuesCount = selectedQuestionList.size();
//            System.out.println("selectedQuestionList.size()"+selectedQuestionList.size());
//            System.out.println("selectedQuestionList.get(0).getQuestionId()"+selectedQuestionList.get(0).getQuestionId());
            LblSubFirstTotal.setText(""+subFirstQuesCount);
            
            LblSecondSubjectTotalMsg.setVisible(false);
            LblSubSecondTotal.setVisible(false);
            LblSecondMultiply.setVisible(false);
            CmbSecondSubjectTotal.setVisible(false);
            LblSecondEqual.setVisible(false);
            TxtSecondSubTotal.setVisible(false);
            
            LblThirdSubjectTotalMsg.setVisible(false);
            LblSubThirdTotal.setVisible(false);
            LblThirdMultiply .setVisible(false);
            CmbThirdSubjectTotal.setVisible(false);
            LblThirdEqual.setVisible(false);
            TxtThirdSubTotal.setVisible(false);
            
            ChkSubject.setText("Show Subject Name");
            String subjectOne = subjectList.get(0).getSubjectName().trim();
            LblFirstSubjectTotalMsg.setText(subjectOne);
            CmbSubject.removeAllItems();
            CmbSubject.addItem(subjectOne);
            CmbSubject.addItem(subjectOne+"-I");
            CmbSubject.addItem(subjectOne+"-II");
            CmbSubject.addItem(groupName+" "+subjectOne);
            
            CmbPaperName.removeAllItems();
            CmbPaperName.addItem(groupName+" "+subjectOne);
            CmbPaperName.addItem(groupName+" MAIN "+subjectOne);
            CmbPaperName.addItem(groupName+" Advance "+subjectOne);
            
            CmbDepartment.removeAllItems();
            CmbDepartment.addItem(groupName+" CELL");
            CmbDepartment.addItem(subjectOne+" Department");
            
            
        } else {
            LblPaperImage.setIcon(new ImageIcon(getClass().getResource("/ui/images/FormatFirstGroup.png")));
            subFirstQuesCount = 0;
            subSecondQuesCount = 0;
            subThirdQuesCount = 0;
            
//            LblChapterName.setVisible(false);
//            ChkChapterName.setVisible(false);
            for (QuestionBean questionsBean : selectedQuestionList) {
                if(questionsBean.getSubjectId() == subjectList.get(0).getSubjectId())
                    subFirstQuesCount ++;
                else if(questionsBean.getSubjectId() == subjectList.get(1).getSubjectId())
                    subSecondQuesCount ++;
                else if(questionsBean.getSubjectId() == subjectList.get(2).getSubjectId())
                    subThirdQuesCount ++;
            }
            
            LblFirstSubjectTotalMsg.setText(subjectList.get(0).getSubjectName().trim());
            LblSecondSubjectTotalMsg.setText(subjectList.get(1).getSubjectName().trim());
            LblThirdSubjectTotalMsg.setText(subjectList.get(2).getSubjectName().trim());
            
            LblSubFirstTotal.setText(""+subFirstQuesCount);
            LblSubSecondTotal.setText(""+subSecondQuesCount);
            LblSubThirdTotal.setText(""+subThirdQuesCount);
            
            ChkSubject.setText("Show Subjects Name");
            CmbSubject.setVisible(false);

            CmbPaperName.removeAllItems();
            CmbPaperName.addItem(groupName);
            CmbPaperName.addItem(groupName+" MAIN");
            CmbPaperName.addItem(groupName+" Advance");

            CmbDepartment.removeAllItems();
            CmbDepartment.addItem(groupName+" CELL");
        }
        setTotalValues();
        CmbStartHour.setSelectedIndex(10);
        CmbStartMinute.setSelectedIndex(0);
        CmbStartAM.setSelectedIndex(0);
        CmbEndHour.setSelectedIndex(12);
        CmbEndMinute.setSelectedIndex(0);
        CmbEndAM.setSelectedIndex(1);
        DatePicker.setFormats("dd-MMM-yyyy");
        Calendar calendar = new GregorianCalendar();
        DatePicker.setDate(calendar.getTime());
    }
    
    private void setTotalValues() {
        int valueOne = 0;
        valueOne = (CmbFirstSubjectTotal.getSelectedIndex() + 1) * subFirstQuesCount;
        TxtFirstSubTotal.setText("" + valueOne);
        
        int valueTwo = 0;
        int valueThree = 0;
        if(printingPaperType.equalsIgnoreCase("GroupWise") || printingPaperType.equalsIgnoreCase("YearWise")) {
            valueTwo = (CmbSecondSubjectTotal.getSelectedIndex() + 1) * subSecondQuesCount;
            TxtSecondSubTotal.setText("" + valueTwo);
            valueThree = (CmbThirdSubjectTotal.getSelectedIndex() + 1) * subThirdQuesCount;
            TxtThirdSubTotal.setText("" + valueThree);
        }
        totalMarks = valueOne + valueTwo + valueThree;
    }

    //Aniket
    private void updateSettings() {
        pdfFirstFormatBean = new PdfFirstFormatBean();
        pdfFirstFormatBean.setShowSubjectName(ChkSubject.isSelected());
        pdfFirstFormatBean.setShowTotalQuestions(ChkTotalQuestion.isSelected());
        pdfFirstFormatBean.setShowDivision(ChkDivision.isSelected());
        pdfFirstFormatBean.setShowPaperName(ChkPaperName.isSelected());
        pdfFirstFormatBean.setShowDepartmentName(ChkDepartment.isSelected());
        pdfFirstFormatBean.setShowTotal(ChkTotalMarks.isSelected());
        pdfFirstFormatBean.setShowDate(ChkDate.isSelected());
        pdfFirstFormatBean.setShowTime(ChkTime.isSelected());
        
        pdfFirstFormatBean.setSubjectName(CmbSubject.getSelectedItem().toString());
        pdfFirstFormatBean.setDivisionName(CmbDivision.getSelectedItem().toString());
        pdfFirstFormatBean.setPaperName(CmbPaperName.getSelectedItem().toString());
        /*if(TxtTestId.getText().isEmpty())
            pdfFirstFormatBean.setTestId("");
        else
            pdfFirstFormatBean.setTestId(TxtTestId.getText());*/
        pdfFirstFormatBean.setDepartmentName(CmbDepartment.getSelectedItem().toString());
        pdfFirstFormatBean.setMarksPerQueSubjectOne(CmbFirstSubjectTotal.getSelectedIndex()+1);
        
        if(TxtSecondSubTotal.getText().isEmpty())
            pdfFirstFormatBean.setMarksPerQueSubjectTwo(0);
        else
            pdfFirstFormatBean.setMarksPerQueSubjectTwo(CmbSecondSubjectTotal.getSelectedIndex()+1);
        
        if(TxtThirdSubTotal.getText().isEmpty())
            pdfFirstFormatBean.setMarksPerQueSubjectThree(0);
        else
            pdfFirstFormatBean.setMarksPerQueSubjectThree(CmbThirdSubjectTotal.getSelectedIndex()+1);
        
//        if(ChkDate.isSelected())
            pdfFirstFormatBean.setSelectedDate(new Utilities().dateToString(DatePicker.getDate(), "dd-MMM-yyyy"));
//        else
//            pdfFirstFormatBean.setSelectedDate("             ");
//        pdfFirstFormatBean.setStartHour(CmbStartHour.getSelectedIndex());
//        pdfFirstFormatBean.setStartMinute(CmbStartMinute.getSelectedIndex());
//        pdfFirstFormatBean.setStartAM(CmbStartAM.getSelectedItem().toString());
//        pdfFirstFormatBean.setEndHour(CmbEndHour.getSelectedIndex());
//        pdfFirstFormatBean.setEndMinute(CmbEndMinute.getSelectedIndex());
//        pdfFirstFormatBean.setEndAM(CmbEndAM.getSelectedItem().toString());
        String time = CmbStartHour.getSelectedItem()+":"+CmbStartMinute.getSelectedItem()+" "+CmbStartAM.getSelectedItem()+" To "+CmbEndHour.getSelectedItem()+":"+CmbEndMinute.getSelectedItem()+" "+CmbEndAM.getSelectedItem();
        pdfFirstFormatBean.setSelectedTime(time);
    }
    
}
